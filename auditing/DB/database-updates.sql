set role r_corporativo_sispat_da;

--drop table simas.auditing_log;

/*Create Table*/
CREATE TABLE simas.auditing_log
(
   id bigserial PRIMARY KEY NOT NULL,
   simas_module varchar(50) NOT NULL,
   endpoint_type int NOT NULL,
   facade_class varchar(255) NOT NULL,
   facade_method varchar(255) NOT NULL,
   operation varchar(255) NOT NULL,
   operation_date timestamp NOT NULL,
   parameters text,
   return_object text,
   request_uri varchar(255),
   id_user int,
   id_simas_user varchar(50) NOT NULL,
   is_success bool,
   operation_amount_time bigint,
   transaction_id varchar(37) NOT NULL,
   error_type varchar(255),
   error_component varchar(255)
)
;

ALTER TABLE simas.auditing_log ALTER COLUMN error_component TYPE varchar(1000);

ALTER TABLE "simas"."auditing_log" ALTER COLUMN operation DROP NOT NULL;



----------


select suporte.fc_altera_para_owner_padrao('simas');
select suporte.fc_privilegios_padroes('simas');


select auditoria.fc_tabela_atualizar('simas');
select auditoria.fc_tabela_estrutura_atualizar('simas');


