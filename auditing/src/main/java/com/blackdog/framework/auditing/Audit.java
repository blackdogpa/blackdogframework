package com.blackdog.framework.auditing;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.interceptor.InterceptorBinding;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com)
 *
 */
@Retention(RUNTIME)
@Target({ METHOD, TYPE })
@InterceptorBinding
public @interface Audit {
  
  int limitReturnLog() default -1;
}
