package com.blackdog.framework.auditing.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import com.blackdog.framework.auditing.dto.AuditingSearch;
import com.blackdog.framework.auditing.model.AuditingLog;
import com.blackdog.framework.configurations.ApplicationConfig;
import com.blackdog.framework.exceptions.BusinessException;
import com.blackdog.framework.persistence.dao.BaseAbstractDAO;
import com.blackdog.framework.persistence.dao.TemporalParameter;
import com.blackdog.framework.persistence.pagination.Page;
import com.blackdog.framework.persistence.pagination.PageImpl;
import com.blackdog.framework.persistence.pagination.Pageable;
import com.blackdog.framework.persistence.pagination.PageableQueryParam;
import com.blackdog.framework.persistence.pagination.Sort;

/**
 * TODO Problema com esse @PersistenceContext + @RequestScoped
 * @author thiago.soares (thiago.soares.jr@gmail.com)
 *
 */
//@RequestScoped
//@ApplicationScoped
public class AuditLogDAO extends BaseAbstractDAO /*extends AbstractDAO*/ {
  

  
  //@Inject
  //@AuditPersistence
  @PersistenceContext(unitName = "auditing-ds")
  private EntityManager em;
  
  @Inject
  protected ApplicationConfig config;

  
  public void create(AuditingLog log) {
    
    em.persist(log);

    /*URL pre = this.getClass().getResource("/META-INF/persistence.xml");
    
    System.out.println(pre);
    
    
    EntityManagerFactory emf;
    Map<String, String> properties = new HashMap<String, String>();
    properties.put("hibernate.connection.driver_class", "org.postgresql.Driver");
    properties.put("hibernate.connection.url", "jdbc:postgresql://localhost:5432/blackdog");
    properties.put("hibernate.connection.username", "postgres");
    properties.put("hibernate.connection.password", "123456");
    properties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
    properties.put("hibernate.show-sql", "true");
    properties.put("provider", "org.hibernate.ejb.HibernatePersistence");
    
    emf = Persistence.createEntityManagerFactory("simas-ro-ca-ds", properties);
    
    
    em = emf.createEntityManager();
    
    List rs = em.createQuery("SELECT a FROM AuditingLog").getResultList();
    
    System.out.println(rs);*/
    
    /////////////////////////////////
    
    //Map properties = new HashMap<>();
    //properties.put("", "");
    //Persistence.createEntityManagerFactory("auditing-ds", properties);
    
    
    /*EntityManager entityManager = entityManagerFactory.createEntityManager();
    entityManager.getTransaction().begin();
    entityManager.persist( new Event( "Our very first event!", new Date() ) );
    entityManager.persist( new Event( "A follow up event", new Date() ) );
    entityManager.getTransaction().commit();
    entityManager.close();*/
    
    ////////////////////////////////////
    
   /* InitialContext ctx;
    try {
      ctx = new InitialContext();
      DataSource ds = (javax.sql.DataSource) ctx.lookup("java:jboss/datasources/BackdogDS");
      
      Map props = new HashMap();  
      props.put("javax.persistence.jtaDataSource", ds);  
      props.put("hibernate.default_schema", "auditing");
      EntityManagerFactory  emf = Persistence.createEntityManagerFactory("auditing-ds", props);
      EntityManager em = emf.createEntityManager();
      
      List rs = em.createQuery("SELECT a FROM AuditingLog").getResultList();
      
      System.out.println(rs);
      
    } catch (NamingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }*/
  }

  public Long countByParameters(AuditingSearch search) throws BusinessException {
    Long countRows = (Long) searchByParametersUtil(search, true).getSingleResult();
    return countRows;
  }
  
  public Page<AuditingLog> searchByParameters(AuditingSearch search) throws BusinessException {
    
    System.out.println(config.getEnvironmentId());
    
    if(search.getPageable() == null) {
      search.setPageable(new PageableQueryParam());
    }
   
    try {
      
      Long countRows              = (Long) searchByParametersUtil(search, true).getSingleResult();
      List<AuditingLog> resultSet =        searchByParametersUtil(search, false).getResultList();
      
      return new PageImpl<>(resultSet, search.getPageable(), countRows );
    } catch (javax.persistence.NoResultException e) {
      return new PageImpl<AuditingLog>(new ArrayList<AuditingLog>(), search.getPageable(), 0L );
    }
    
  }
  
  private Query searchByParametersUtil(AuditingSearch logSearch, Boolean count) throws BusinessException {
    
    StringBuffer from = new StringBuffer();
    
    if(count) {
      from.append("SELECT COUNT(r) FROM AuditingLog r ");
    } else {
      from.append("SELECT r FROM AuditingLog r ");
    }
    
    StringBuffer where = new StringBuffer();
    Map<String, Object> parametros = new HashMap<String, Object>();
    
    if(logSearch.getId() != null) {
      
      where.append("WHERE r.id = :id ");
      parametros.put("id", logSearch.getId());
      
    } else {
    
      where.append(" WHERE 0=0 ");
      
      if(logSearch.getDateStart() != null && logSearch.getDateStop() != null) {
        
        where.append(" AND DATE(r.operationDate) BETWEEN :start AND :end ");
        parametros.put("start", new TemporalParameter(logSearch.getDateStart(), TemporalType.DATE) );
        parametros.put("end", new TemporalParameter(logSearch.getDateStop(), TemporalType.DATE));
        
        /* TODO É necessário identificar se se trata de um Date ou TimeStamp
        where.append(" AND r.operationDate BETWEEN :start AND :end ");
        parametros.put("start", new TemporalParameter(logSearch.getDateStart(), TemporalType.TIMESTAMP) );
        parametros.put("end", new TemporalParameter(logSearch.getDateStop(), TemporalType.TIMESTAMP));*/
        
      } else {
        
        if(logSearch.getDateStart() != null ) {
          where.append(" AND DATE(r.operationDate) >= :start ");
          parametros.put("start", new TemporalParameter(logSearch.getDateStart(), TemporalType.DATE) );
        } 
        
        if(logSearch.getDateStop() != null) {
          where.append(" AND DATE(r.operationDate) <= :end ");
          parametros.put("end", new TemporalParameter(logSearch.getDateStop(), TemporalType.DATE));
        }
        
      }
      
      if(logSearch.getTimeStart() != null && logSearch.getTimeStop() != null) {
        
        //where.append(" AND DATE(r.operationDate) BETWEEN :start AND :end ");
        //parametros.put("start", new TemporalParameter(logSearch.getTimeStart(), TemporalType.DATE) );
        //parametros.put("end", new TemporalParameter(logSearch.getTimeStop(), TemporalType.DATE));
        
        /* TODO É necessário identificar se se trata de um Date ou TimeStamp*/
        where.append(" AND r.operationDate BETWEEN :start AND :end ");
        parametros.put("start", new TemporalParameter(logSearch.getTimeStart(), TemporalType.TIMESTAMP) );
        parametros.put("end", new TemporalParameter(logSearch.getTimeStop(), TemporalType.TIMESTAMP));
        
      } else {
        
        if(logSearch.getTimeStart() != null ) {
          where.append(" AND r.operationDate >= :start ");
          parametros.put("start", new TemporalParameter(logSearch.getTimeStart(), TemporalType.TIMESTAMP) );
        } 
        
        if(logSearch.getTimeStop() != null) {
          where.append(" AND r.operationDate <= :end ");
          parametros.put("end", new TemporalParameter(logSearch.getTimeStop(), TemporalType.TIMESTAMP));
        }
        
      }
      
      if(logSearch.getEndpointType() != null) {
        where.append(" AND r.endpointType = :endpointType ");
        parametros.put("endpointType",logSearch.getEndpointType());
      }
      
      
      if(logSearch.getUser() != null) {
        where.append(" AND r.idUser = :idUser ");
        parametros.put("idUser",logSearch.getUser());
      }

      if(logSearch.getTransactionId() != null) {
        where.append(" AND r.transactionID = :transactionID ");
        parametros.put("transactionID", logSearch.getTransactionId());
      }
      
      if(logSearch.getSuccess() != null) {
        where.append(" AND r.success = :success ");
        parametros.put("success", logSearch.getSuccess());
      }
      
      if(logSearch.getExceptionType() != null) {
        where.append(" AND r.exceptionType = :exceptionType ");
        parametros.put("exceptionType", logSearch.getExceptionType());
      }
      
      
      //TODO continuar
      
    }
    
    aplicarOrder(where, logSearch.getPageable(), count);
    
    Query query = em.createQuery(from + where.toString());
    
    aplicaParametros(query, parametros, logSearch.getPageable(), count);
    
    return query;
  }
  
  /*protected void aplicarOrder(StringBuffer query, Pageable pagination, Boolean isCount) {

    if(!isCount) {
      if(pagination != null && pagination.getSort() != null &&  !pagination.getSort().isEmpty()) {
        query.append("ORDER BY ");
        for(Sort sort :pagination.getSort()) {
          query.append(sort.getProperty() + " "+ sort.getDirection() + ", ");
        }
        query.delete(query.length()-2, query.length());
      } else {
        //TODO Deve pegar o campo da classe que ordena por default. 
        //query.append("ORDER BY l.numeroRemessa");
      }
    }
  }
  
  protected void aplicaParametros(Query query, Map<String, Object> parametros, Pageable pagination, Boolean isCount) throws BusinessException {
    
    aplicaParametros(query, parametros);
    
    if(!isCount) {
      Integer pageSize = pagination.getPageSize();
      
      if(pageSize == null || pageSize < 1) {
        pageSize  = config.getDefaultPageSize().intValue();
      } else if(pageSize > config.getMaxPageSize()) {
        pageSize = config.getMaxPageSize().intValue();
      }
      
      if(pagination.getPageSize() != null) {
        query.setMaxResults(pagination.getPageSize());
      }
      
      
      if(pagination.getOffset() != null) {
        query.setFirstResult(pagination.getOffset());
      }
    }
    
  }
  
  protected void aplicaParametros(Query query, Map<String, Object> parametros) {
    for (Entry<String, Object> parametro : parametros.entrySet()) {
      
      if(parametro.getValue() != null) {
        
        if(parametro.getValue() instanceof TemporalParameter) {
          TemporalParameter tparam = (TemporalParameter) parametro.getValue();
          query.setParameter(parametro.getKey(), tparam.getDate(), tparam.getTemporalType());
        } else {
          query.setParameter(parametro.getKey(), parametro.getValue());
        }
        
      } else {
        query.setParameter(parametro.getKey(), parametro.getValue());
      }
    }
  }*/
  
  public AuditingLog findById(Long id) {
    try {
      return em.find(AuditingLog.class, id);
      //return null;
    } catch (NoResultException e) {
      return null;
    } catch (NonUniqueResultException e) {
      return null;
    }
    
  }
  
}
