package com.blackdog.framework.auditing.dto;

import java.util.List;

import com.blackdog.framework.core.model.EndpointType;


public class AuditingLogDto {

  private String logId;
  
  private String simasModule;
  
  private EndpointType endpointType;
  
  private String operation;

  private String facadeClasse;

  private String facadeMetodo;

  private String requestUri;
  
  private Long user;
  
  private String idSimasUser;
  
  private Boolean success;
  
  private List<Object> parameters;
  
  private Object returnObject;

  private Long operationTime;
  
  private String exceptionType;
  
  private String errorComponent;
  
  public AuditingLogDto() {
    super();
    this.success = true;
  }

  public String getLogId() {
    return logId;
  }

  public void setLogId(String logId) {
    this.logId = logId;
  }

  public EndpointType getEndpointType() {
    return endpointType;
  }

  public void setEndpointType(EndpointType endpointType) {
    this.endpointType = endpointType;
  }

  public String getOperation() {
    return operation;
  }

  public void setOperation(String operation) {
    this.operation = operation;
  }

  public String getFacadeClasse() {
    return facadeClasse;
  }

  public void setFacadeClasse(String facadeClasse) {
    this.facadeClasse = facadeClasse;
  }

  public String getFacadeMetodo() {
    return facadeMetodo;
  }

  public void setFacadeMetodo(String facadeMetodo) {
    this.facadeMetodo = facadeMetodo;
  }

  public Long getUser() {
    return user;
  }

  public void setUser(Long user) {
    this.user = user;
  }

  public Boolean getSuccess() {
    return success;
  }

  public void setSuccess(Boolean success) {
    this.success = success;
  }

  public List<Object> getParameters() {
    return parameters;
  }

  public void setParameters(List<Object> parameters) {
    this.parameters = parameters;
  }

  public Object getReturnObject() {
    return returnObject;
  }

  public void setReturnObject(Object returnObject) {
    this.returnObject = returnObject;
  }

  public Long getOperationTime() {
    return operationTime;
  }

  public void setOperationTime(Long operationTime) {
    this.operationTime = operationTime;
  }

  public String getExceptionType() {
    return exceptionType;
  }

  public void setExceptionType(String exceptionType) {
    this.exceptionType = exceptionType;
  }

  public String getRequestUri() {
    return requestUri;
  }

  public void setRequestUri(String serviceUri) {
    this.requestUri = serviceUri;
  }

  public String getErrorComponent() {
    return errorComponent;
  }

  public void setErrorComponent(String errorComponent) {
    this.errorComponent = errorComponent;
  }

  public String getSimasModule() {
    return simasModule;
  }

  public void setSimasModule(String simasModule) {
    this.simasModule = simasModule;
  }

  public String getIdSimasUser() {
    return idSimasUser;
  }

  public void setIdSimasUser(String idSimasUser) {
    this.idSimasUser = idSimasUser;
  }

  @Override
  public String toString() {
    return "AuditingLogWSDto [logId=" + logId + ", simasModule=" + simasModule + ", endpointType=" + endpointType + ", operation=" + operation
            + ", facadeClasse=" + facadeClasse + ", facadeMetodo=" + facadeMetodo + ", requestUri=" + requestUri + ", user=" + user + ", idSimasUser="
            + idSimasUser + ", success=" + success + ", parameters=" + parameters + ", returnObject=" + returnObject + ", operationTime="
            + operationTime + ", exceptionType=" + exceptionType + ", errorComponent=" + errorComponent + "]";
  }
  
}