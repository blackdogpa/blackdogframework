package com.blackdog.framework.auditing.dto;

import java.util.List;

import com.blackdog.framework.core.model.EndpointType;


public class AuditingLogWSDto {

  private String logId;
  
  //TODO private String simasModule;
  //TODO private String idSimasUser;
  
  private String requestIp;
  
  private EndpointType endpointType;
  
  private String operation;

  private String facadeClasse;

  private String facadeMetodo;

  private String requestUri;
  
  private String user;
  
  private Long userId;
  
  private Boolean success;
  
  private List<Object> parameters;
  
  private Object returnObject;

  private Long operationTime;
  
  private String exceptionCode;
  
  private String exceptionType;
  
  private String errorComponent;
  
  private String errorReturn;
  
  private int limitReturnLog;
  
  public AuditingLogWSDto() {
    super();
    this.success = true;
  }

  public String getLogId() {
    return logId;
  }

  public void setLogId(String logId) {
    this.logId = logId;
  }

  public EndpointType getEndpointType() {
    return endpointType;
  }

  public void setEndpointType(EndpointType endpointType) {
    this.endpointType = endpointType;
  }

  public String getOperation() {
    return operation;
  }

  public void setOperation(String operation) {
    this.operation = operation;
  }

  public String getFacadeClasse() {
    return facadeClasse;
  }

  public void setFacadeClasse(String facadeClasse) {
    this.facadeClasse = facadeClasse;
  }

  public String getFacadeMetodo() {
    return facadeMetodo;
  }

  public void setFacadeMetodo(String facadeMetodo) {
    this.facadeMetodo = facadeMetodo;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long user) {
    this.userId = user;
  }

  public Boolean getSuccess() {
    return success;
  }

  public void setSuccess(Boolean success) {
    this.success = success;
  }

  public List<Object> getParameters() {
    return parameters;
  }

  public void setParameters(List<Object> parameters) {
    this.parameters = parameters;
  }

  public Object getReturnObject() {
    return returnObject;
  }

  public void setReturnObject(Object returnObject) {
    this.returnObject = returnObject;
  }

  public Long getOperationTime() {
    return operationTime;
  }

  public void setOperationTime(Long operationTime) {
    this.operationTime = operationTime;
  }

  public String getExceptionType() {
    return exceptionType;
  }

  public void setExceptionType(String exceptionType) {
    this.exceptionType = exceptionType;
  }

  public String getRequestUri() {
    return requestUri;
  }

  public void setRequestUri(String serviceUri) {
    this.requestUri = serviceUri;
  }

  public String getErrorComponent() {
    return errorComponent;
  }

  public void setErrorComponent(String errorComponent) {
    this.errorComponent = errorComponent;
  }

  public String getRequestIp() {
    return requestIp;
  }

  public void setRequestIp(String requestIp) {
    this.requestIp = requestIp;
  }
  

  public String getExceptionCode() {
    return exceptionCode;
  }

  public void setExceptionCode(String exceptionCode) {
    this.exceptionCode = exceptionCode;
  }
  
  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public String getErrorReturn() {
    return errorReturn;
  }

  public void setErrorReturn(String errorReturn) {
    this.errorReturn = errorReturn;
  }

  public int getLimitReturnLog() {
    return limitReturnLog;
  }

  public void setLimitReturnLog(int limitReturnLog) {
    this.limitReturnLog = limitReturnLog;
  }

  @Override
  public String toString() {
    return "AuditingLogWSDto [logId=" + logId + ", endpointType=" + endpointType + ", operation=" + operation + ", facadeClasse=" + facadeClasse
            + ", facadeMetodo=" + facadeMetodo + ", requestUri=" + requestUri + ", user=" + userId + ", success=" + success + ", parameters="
            + parameters + ", returnObject=" + returnObject + ", operationTime=" + operationTime + ", exceptionType=" + exceptionType
            + ", errorComponent=" + errorComponent + "]";
  }
  
}