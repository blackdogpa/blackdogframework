package com.blackdog.framework.auditing.dto;

import java.util.Date;
import java.util.Map;

import com.blackdog.framework.core.model.EndpointType;
import com.blackdog.framework.persistence.pagination.Pageable;


@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2017-02-02T18:36:11.447Z")
public class AuditingSearch   {
  
  
  private Long id;
  
  private String operation;
  private String facadeClasse;
  private String facadeMetodo;
  private EndpointType endpointType;
  
  private Long user;
  
  private Boolean success;
  private String exceptionType;

  private String transactionId;
  
  private Map<String, String> where;
  
  /*
   * Periodos
   */
  private Date dateStart; 
  private Date dateStop;
  
  private Date timeStart; 
  private Date timeStop;
  
  private Pageable pageable;
  

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getOperation() {
    return operation;
  }

  public void setOperation(String operation) {
    this.operation = operation;
  }

  public String getFacadeClasse() {
    return facadeClasse;
  }

  public void setFacadeClasse(String facadeClasse) {
    this.facadeClasse = facadeClasse;
  }

  public String getFacadeMetodo() {
    return facadeMetodo;
  }

  public void setFacadeMetodo(String facadeMetodo) {
    this.facadeMetodo = facadeMetodo;
  }

  public EndpointType getEndpointType() {
    return endpointType;
  }

  public void setEndpointType(EndpointType endpointType) {
    this.endpointType = endpointType;
  }

  public Long getUser() {
    return user;
  }

  public void setUser(Long user) {
    this.user = user;
  }

  public Boolean getSuccess() {
    return success;
  }

  public void setSuccess(Boolean success) {
    this.success = success;
  }

  public String getExceptionType() {
    return exceptionType;
  }

  public void setExceptionType(String exceptionType) {
    this.exceptionType = exceptionType;
  }

  public Map<String, String> getWhere() {
    return where;
  }

  public void setWhere(Map<String, String> where) {
    this.where = where;
  }

  public Date getDateStart() {
    return dateStart;
  }

  public void setDateStart(Date dateStart) {
    this.dateStart = dateStart;
  }

  public Date getDateStop() {
    return dateStop;
  }

  public void setDateStop(Date dateStop) {
    this.dateStop = dateStop;
  }

  public Pageable getPageable() {
    return pageable;
  }

  public void setPageable(Pageable pageable) {
    this.pageable = pageable;
  }

  public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  public Date getTimeStart() {
    return timeStart;
  }

  public void setTimeStart(Date timeStart) {
    this.timeStart = timeStart;
  }

  public Date getTimeStop() {
    return timeStop;
  }

  public void setTimeStop(Date timeStop) {
    this.timeStop = timeStop;
  }
  
}