package com.blackdog.framework.auditing.interceptor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.jws.WebService;
import javax.ws.rs.Path;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import com.blackdog.framework.auditing.Audit;
import com.blackdog.framework.auditing.dto.AuditingLogWSDto;
import com.blackdog.framework.auditing.service.AuditingService;
import com.blackdog.framework.configurations.ApplicationConfig;
import com.blackdog.framework.core.dto.RequestInfo;
import com.blackdog.framework.core.exceptions.BlackExceptions;
import com.blackdog.framework.core.model.EndpointType;
import com.blackdog.framework.core.security.CAOperation;
import com.blackdog.framework.exceptions.SecurityException;
import com.blackdog.framework.exceptions.SystemException;
import com.blackdog.framework.exceptions.util.ExceptionCastUtil;
import com.blackdog.framework.security.sso.Identity;

/**
 * 
 * @author thiago
 * @deprecated use com.blackdog.framework.auditing.interceptor.RestInterceptor instead of
 * 
 */
@Deprecated
@Audit
@Interceptor
public class AuditInterceptor {

  @Inject
  private Identity identity;
  
  @Inject
  private AuditingService logService;
  
  @Inject
  private RequestInfo requestInfo; 
  
  @Inject
  private ApplicationConfig applicationConfig;
  
  @AroundInvoke
  public Object interceptar(InvocationContext invocationContext) throws Throwable {
    long start = System.currentTimeMillis();
    
    AuditingLogWSDto log = new AuditingLogWSDto(); 
    Object result = null;
    
    try {
      
      EndpointType endpointType = null;
      
      //EndpontType
      if(invocationContext.getMethod().isAnnotationPresent(Path.class)) {
        endpointType = EndpointType.REST;
      } else 
      if(invocationContext.getMethod().isAnnotationPresent(WebService.class)) {
        endpointType = EndpointType.SOAP;
      } else {
        throw new SecurityException("Tipo de Endpoint não identificado.");
      }
      
      log.setEndpointType(endpointType);
      log.setFacadeClasse(invocationContext.getMethod().getDeclaringClass().getName());    
      log.setFacadeMetodo(invocationContext.getMethod().getName());
      
      log.setRequestIp(requestInfo.getIp());
      log.setRequestUri(requestInfo.getRequestUri());
      log.setLogId(requestInfo.getTransactionID());
      
      //Tipo de operacao @NaturalOperation
      /*NaturalOperation operationAnnot = invocationContext.getMethod().getAnnotation(NaturalOperation.class);
      if(operationAnnot != null) {
        log.setOperation(operationAnnot.value());
      }*/
      
      //TODO Tambem tem a @CAOperation
      CAOperation caOperationAnnot = invocationContext.getMethod().getAnnotation(CAOperation.class);
      if(caOperationAnnot != null) {
        log.setOperation(caOperationAnnot.value());
      }
      
      if(StringUtils.isBlank(log.getOperation())) {
        throw new SecurityException("Operação não definida.");
      }
      
      //Parametros da operacao
      Object[] params = invocationContext.getParameters();
      log.setParameters(Arrays.asList(params));
      
      if(identity.isLoggedIn()) {
        log.setUserId(identity.getAuthenticatedUser().getId());
      } else {
        //throw new SecurityException("Usuário não autenticado.");
      }
      
      result = invocationContext.proceed();
      
      log.setReturnObject(result);
    
    } catch (Throwable e) {
      
      log.setSuccess(false);
      log.setReturnObject(e);
      
      log.setErrorComponent(ExceptionCastUtil.getExceptionThrowClass(e));
      
      if(e instanceof BlackExceptions) {
        
        BlackExceptions be = (BlackExceptions) e;
        be.setId(log.getLogId());
        be.setApplicationId(applicationConfig.getCaApplicationID().toString());
        be.setApplicationName(applicationConfig.getApplicationShortName() + " / " + applicationConfig.getEnvironmentId() );
        log.setExceptionType(be.getType());
        log.setExceptionCode(be.getCode());
        
      } else {
        log.setExceptionType("SYSTEM");
        //Se não for qualquer dos tipos da aplicação, relancar dendro de uma system 
        
        if(e.getCause() instanceof SystemException) {
          
          BlackExceptions be = (BlackExceptions) e.getCause();
          be.setId(log.getLogId());
          be.setApplicationId(applicationConfig.getCaApplicationID().toString());
          be.setApplicationName(applicationConfig.getApplicationShortName() + " / " + applicationConfig.getEnvironmentId() );
          log.setExceptionType(be.getType());
          log.setExceptionCode(be.getCode());
          
          throw new SystemException(log.getLogId(), e, be.getMessage());
        } else 
        if(e.getCause() instanceof SecurityException) {
          
          BlackExceptions be = (BlackExceptions) e.getCause();
          be.setId(log.getLogId());
          be.setApplicationId(applicationConfig.getCaApplicationID().toString());
          be.setApplicationName(applicationConfig.getApplicationShortName() + " / " + applicationConfig.getEnvironmentId() );
          log.setExceptionType(be.getType());
          log.setExceptionCode(be.getCode());
          
          throw new SecurityException(log.getLogId(), e, be.getMessage());
        } else
        if(e.getCause() instanceof javax.ws.rs.NotFoundException) {
            
            throw e.getCause();  
        }
        
        throw new SystemException(log.getLogId(), e, "Desculpe, tivemos problemas com a solicitação.");
      }
      
      throw e;
    } finally {
      long end = System.currentTimeMillis();
      log.setOperationTime(end - start);
      logService.registerAsyn(log);
    }
    
    return result;
  }
  
  
 
  
  
}

