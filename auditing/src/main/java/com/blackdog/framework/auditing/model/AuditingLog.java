package com.blackdog.framework.auditing.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.blackdog.framework.core.model.EndpointType;
import com.thoughtworks.xstream.annotations.XStreamAlias;


@Entity
@Table(name =  "auditing_log")
@XStreamAlias("AuditingLog")
public class AuditingLog {

  @Id
  @Column(name = "id", insertable = false, updatable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  
  //@Column(name = "simas_module", nullable = false)
  //private String simasModule;
  
  @Column(name = "transaction_id", nullable = false)
  private String transactionID;
  
  @Enumerated
  @Column(name = "endpoint_type", nullable = false)
  private EndpointType endpointType;
  
  @Column(name = "operation", nullable = false)
  private String operation;
  
  @Column(name = "facade_class", nullable = false)
  private String facadeClass;
  
  @Column(name = "facade_method", nullable = false)
  private String facadeMethod;

  @Column(name = "request_ip", nullable = true)
  private String requestIp;
  
  @Column(name = "request_uri")
  private String requestUri;
  
  @Column(name = "user_id", nullable = true)
  private Long idUser;
  
  @Column(name = "user_login", nullable = true)
  private String user;
  
  //@Column(name = "id_simas_user", nullable = false)
  //private String idSimasUser;
  
  @Column(name = "is_success", nullable = false)
  private Boolean success;
  
  //@Lob
  @Column(name = "parameters", columnDefinition = "TEXT")
  private String parameters;
  
  //@Lob
  @Column(name = "return_object", columnDefinition = "TEXT")
  private String returnObject;
  
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "operation_date", nullable = false)
  private Date operationDate;

  @Column(name = "operation_amount_time", nullable = false)
  private Long operationTime;
  
  @Column(name = "error_code", nullable = true)
  private String exceptionCode;
  
  @Column(name = "error_type", nullable = true)
  private String exceptionType;
  
  @Column(name = "error_component", nullable = true)
  private String errorComponent;
  
  @Column(name = "error_trace", nullable = true)
  private String errorTrace;
  
  public AuditingLog() {
    super();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFacadeClass() {
    return facadeClass;
  }

  public void setFacadeClass(String facadeClass) {
    this.facadeClass = facadeClass;
  }

  public String getFacadeMethod() {
    return facadeMethod;
  }

  public void setFacadeMethod(String facadeMethod) {
    this.facadeMethod = facadeMethod;
  }

  public String getRequestUri() {
    return requestUri;
  }

  public void setRequestUri(String requestUri) {
    this.requestUri = requestUri;
  }

  public EndpointType getEndpointType() {
    return endpointType;
  }

  public void setEndpointType(EndpointType endpointType) {
    this.endpointType = endpointType;
  }

  public Long getIdUser() {
    return idUser;
  }

  public void setIdUser(Long idUser) {
    this.idUser = idUser;
  }

  public String getParameters() {
    return parameters;
  }

  public void setParameters(String parameters) {
    this.parameters = parameters;
  }

  public String getReturnObject() {
    return returnObject;
  }

  public void setReturnObject(String returnObject) {
    this.returnObject = returnObject;
  }

  public Date getOperationDate() {
    return operationDate;
  }

  public void setOperationDate(Date operationDate) {
    this.operationDate = operationDate;
  }

  public Boolean getSuccess() {
    return success;
  }

  public void setSuccess(Boolean success) {
    this.success = success;
  }

  public Long getOperationTime() {
    return operationTime;
  }

  public void setOperationTime(Long operationTime) {
    this.operationTime = operationTime;
  }

  public String getTransactionID() {
    return transactionID;
  }

  public void setTransactionID(String transactionID) {
    this.transactionID = transactionID;
  }

  public String getOperation() {
    return operation;
  }

  public void setOperation(String operation) {
    this.operation = operation;
  }

  public String getExceptionType() {
    return exceptionType;
  }

  public void setExceptionType(String exceptionType) {
    this.exceptionType = exceptionType;
  }
  
  public String getErrorComponent() {
    return errorComponent;
  }

  public void setErrorComponent(String errorComponent) {
    this.errorComponent = errorComponent;
  }
  
  public String getRequestIp() {
    return requestIp;
  }

  public void setRequestIp(String requestIp) {
    this.requestIp = requestIp;
  }
  

  public String getExceptionCode() {
    return exceptionCode;
  }

  public void setExceptionCode(String exceptionCode) {
    this.exceptionCode = exceptionCode;
  }
  
  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public String getErrorTrace() {
    return errorTrace;
  }

  public void setErrorTrace(String errorTrace) {
    this.errorTrace = errorTrace;
  }

  @Override
  public String toString() {
    return "AuditingLog [id=" + id + ", transactionID=" + transactionID + ", endpointType=" + endpointType
            + ", operation=" + operation + ", facadeClass=" + facadeClass + ", facadeMethod=" + facadeMethod + ", requestUri=" + requestUri
            + ", idUser=" + idUser + ", success=" + success + ", parameters=" + parameters + ", returnObject="
            + returnObject + ", operationDate=" + operationDate + ", operationTime=" + operationTime + ", exceptionType=" + exceptionType
            + ", errorComponent=" + errorComponent + "]";
  }

  
  
}