package com.blackdog.framework.auditing.service;
        
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.blackdog.framework.auditing.dao.AuditLogDAO;
import com.blackdog.framework.auditing.dto.AuditingLogWSDto;
import com.blackdog.framework.auditing.dto.AuditingSearch;
import com.blackdog.framework.auditing.model.AuditingLog;
import com.blackdog.framework.core.audit.AuditEvent;
import com.blackdog.framework.core.exceptions.BlackExceptions;
import com.blackdog.framework.exceptions.BusinessException;
import com.blackdog.framework.persistence.pagination.Page;
import com.blackdog.framework.utils.serialization.jackson.JacksonJsonUtils;
import com.blackdog.framework.utils.uuid.UUIDUtils;

@RequestScoped
public class AuditingEventService {
  
  
  /*
  public void registerLogEvent(@Observes @AuditEvent AuditingLogWSDtoString logDto) throws BusinessException {
	  
	  System.out.println(logDto);
	  
  }
*/
  
}


