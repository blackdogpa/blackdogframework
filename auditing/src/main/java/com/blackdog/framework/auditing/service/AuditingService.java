package com.blackdog.framework.auditing.service;
        
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.blackdog.framework.auditing.dao.AuditLogDAO;
import com.blackdog.framework.auditing.dto.AuditingLogWSDto;
import com.blackdog.framework.auditing.dto.AuditingSearch;
import com.blackdog.framework.auditing.model.AuditingLog;
import com.blackdog.framework.core.audit.AuditEvent;
import com.blackdog.framework.core.exceptions.BlackExceptions;
import com.blackdog.framework.exceptions.BusinessException;
import com.blackdog.framework.persistence.pagination.Page;
import com.blackdog.framework.utils.serialization.jackson.JacksonJsonUtils;
import com.blackdog.framework.utils.uuid.UUIDUtils;

@Stateless
public class AuditingService {
  
  @Inject
  private AuditLogDAO dao; 
  
  
  //@Asynchronous
  public void registerAsyn(AuditingLogWSDto logDto) throws BusinessException {
    
    try {
    
      AuditingLog log = new AuditingLog();
      
      
      if(logDto.getLogId() == null) {
        //log.setTransactionID(UUID.randomUUID().toString());
        log.setTransactionID(UUIDUtils.generateId());
      } else {
        log.setTransactionID(logDto.getLogId());
      }
      
      log.setIdUser(logDto.getUserId());
      log.setUser(logDto.getUser());
      //log.setIdSimasUser(logDto.getIdSimasUser());
      //log.setSimasModule(logDto.getSimasModule());
      log.setEndpointType(logDto.getEndpointType());
      log.setOperation(logDto.getOperation());
      log.setFacadeClass(logDto.getFacadeClasse());
      log.setFacadeMethod(logDto.getFacadeMetodo());
      //log.setServiceUri(logDto.get);
      log.setOperationDate(new Date());
      log.setOperationTime(logDto.getOperationTime());
      
      log.setSuccess(logDto.getSuccess());
      
      log.setRequestIp(logDto.getRequestIp());
      log.setRequestUri(logDto.getRequestUri());
      
      log.setExceptionCode(logDto.getExceptionCode());
      log.setExceptionType(logDto.getExceptionType());
      
      log.setErrorComponent(logDto.getErrorComponent());
      
      if(logDto.getParameters() != null && !logDto.getParameters().isEmpty()) {
        try {
          
          List<Object> params = new ArrayList<>();
          for(Object obj: logDto.getParameters()) {
            if(obj != null) {
              //TODO GAMBI
              if(!obj.getClass().getName().contains("com.sun.proxy")
                ) {
                params.add(obj);
              }
            }
          }
          //log.setParameters(JsonUtils.toJson(logDto.getParameters()));
          log.setParameters(JacksonJsonUtils.toJson(params));
          
          if(log.getParameters().length() > 1000) {
            log.setParameters(log.getParameters().substring(0, 995) + "...");
          }
          
        } catch (Exception e) {
          throw new BusinessException("Não foi possível serializar os parâmetros da consulta: " + e.getMessage());
        }
      }
      
      if(logDto.getReturnObject() != null) {
        
        if(logDto.getSuccess()) {
          
          if(logDto.getReturnObject() instanceof Response) {
            
            Response response = (Response) logDto.getReturnObject();
            
            log.setReturnObject(JacksonJsonUtils.toJson(response.getEntity(), logDto.getLimitReturnLog()));
            
          } else {
            
            log.setReturnObject(JacksonJsonUtils.toJson(logDto.getReturnObject(), logDto.getLimitReturnLog()));
            
          }

          
        } else {
          
          if(logDto.getReturnObject() instanceof BlackExceptions) {

            //TODO Não ficou bom. Serializou yum objeto gigante que não é o a BlackExceptions
            //log.setReturnObject(JacksonJsonUtils.toJson((BlackExceptions)logDto.getReturnObject()));

            log.setReturnObject(logDto.getErrorReturn());
            log.setErrorTrace(getTrace((Throwable) logDto.getReturnObject()));
            
          } else if(logDto.getReturnObject() instanceof Throwable) {
            
            log.setReturnObject(logDto.getErrorReturn());
            log.setErrorTrace(getTrace((Throwable) logDto.getReturnObject()));
            
          } else if(logDto.getReturnObject() instanceof String) {
            
            log.setReturnObject((String)logDto.getReturnObject());
            log.setErrorTrace(logDto.getErrorReturn());
            
          } else {
            
            log.setReturnObject(logDto.getReturnObject().toString());
            log.setErrorTrace(logDto.getErrorReturn());
            
          }
          
        }
        
      }
      
      dao.create(log);
    } catch (javax.ejb.EJBException e) {
      e.printStackTrace();
    } catch (javax.persistence.PersistenceException e) {
      e.printStackTrace();
    }
  }
  
  
  public AuditingLog selectById(Long id) throws BusinessException {
    
    AuditingLog result = dao.findById(id);
    
    if(result == null) {
      throw new NotFoundException("The resource "+id+" is not found.");
    }
    
    return result;
  }

  public Page<AuditingLog> search(AuditingSearch search) throws BusinessException {
    
    Page<AuditingLog> result = dao.searchByParameters(search);    
    
    return result;
  }
  
  public Long count(AuditingSearch search) throws BusinessException {
    return dao.countByParameters(search);
  }
  
  
  private String getTrace(Throwable e) {
    
    StringBuilder trace = new StringBuilder();

    Throwable[] ts = ExceptionUtils.getThrowables(e);
    for (Throwable throwable : ts) {
      
      /*if(throwable instanceof HttpClientErrorException) {
        
        HttpClientErrorException hce = (HttpClientErrorException) throwable;
        trace.append("\n\n\n");
        trace.append("## HttpClientErrorException.StatusCode :: " + hce.getStatusText());
        trace.append("#\n#\n");
        trace.append("## HttpClientErrorException.Body :: "       + hce.getResponseBodyAsString());
        trace.append("\n\n\n");
        
      } else if(throwable instanceof HttpServerErrorException) {
        HttpServerErrorException hse = (HttpServerErrorException) throwable;
        
        trace.append("\n\n\n");
        trace.append("## HttpServerErrorException.StatusCode :: " + hse.getStatusText());
        trace.append("#\n#\n");
        trace.append("## HttpServerErrorException.Body :: "       + hse.getResponseBodyAsString());
        trace.append("\n\n\n");
        
      } else if(throwable instanceof BusinessException) {
        
        BusinessException bw = (BusinessException) throwable;
        
        trace.append("\n\n\n");
        trace.append("## BusinessWSException :: " + bw.toString());
        trace.append("#\n#\n");
        trace.append("## BusinessWSException.trace :: " + ExceptionUtils.getStackTrace(bw));
        trace.append("\n\n\n");*/
      
      if(throwable instanceof BlackExceptions) {
        
        BlackExceptions bw = (BlackExceptions) throwable;
        
        trace.append("\n\n\n");
        trace.append("## "+bw.getClass()+" :: " + bw.toString());
        trace.append("#\n#\n");
        trace.append("## "+bw.getClass()+".trace :: " + ExceptionUtils.getStackTrace(throwable));
        trace.append("\n\n\n");
        
      } else {
        trace.append(ExceptionUtils.getStackTrace(throwable));
      }
      
      trace.append("\n\n\n\n\n\n\n\n");
    }
    
    return trace.toString();

  }
  
}


