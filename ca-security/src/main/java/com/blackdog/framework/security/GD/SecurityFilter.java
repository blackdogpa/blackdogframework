package com.blackdog.framework.security.GD;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.blackdog.framework.utils.security.HttpUtil;

public class SecurityFilter implements Filter {

  public static final String SESSION_COOKIE = "KSESSIONID";

  //private static final String LOGIN_PAGE_URI = "/governodigital/check.seam";
  private static final String NEGADO_PAGE_URI = "/index.html";

  //private Set restrictedResources;
  private Set<String> openResources;

  //@Inject 
  //SSOClientController clientSeamSispat;

  public void init(FilterConfig filterConfig) throws ServletException {
    this.openResources = new HashSet<String>();
    this.openResources.add("/index.html");
  }

  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
    String contextPath = ((HttpServletRequest) req).getContextPath();
    String requestUri = ((HttpServletRequest) req).getRequestURI();

    // this.logger.debug("contextPath = " + contextPath);
    // this.logger.debug("requestUri = " + requestUri);

    //if (!this.contains(requestUri, contextPath)) { ???
    if(!requestUri.contains(contextPath)) {
      // this.logger.debug("authorization failed");
      ((HttpServletRequest) req).getRequestDispatcher(NEGADO_PAGE_URI).forward(req, res);
    } else {
      
      if(!this.authorize((HttpServletRequest) req)) {
        ((HttpServletRequest) req).getRequestDispatcher(NEGADO_PAGE_URI).forward(req, res);
        //((HttpServletRequest) req).getRequestDispatcher(SSOConfig.SERVER_CHECK_SEAM + "?" + SSOConfig.CLIENT_REDIRECT_PARAMETER + "=" + contextPath).forward(req, res);  
      } else {
        // this.logger.debug("authorization succeeded");
        chain.doFilter(req, res);
      }
      
    }
  }

  public void destroy() {

  }

  private boolean contains(String value, String contextPath) {
    Iterator<String> ite = this.openResources.iterator();
    while (ite.hasNext()) {
      String openResource = (String) ite.next();
      if ((contextPath + openResource).equalsIgnoreCase(value)) {
        return true;
      }
    }
    return false;
  }

  private boolean authorize(HttpServletRequest request) {
    
    if(getSessionCookie(request) == null) {
      return false;
    } else {
      //TODO return this.clientSeamSispat.isLoggedIn();
    	return false;
    }
   
  }

  private void check(HttpServletRequest request, HttpServletResponse response) {
    String sessionCookie = getSessionCookie(request);
    /*
     * if (sessionCookie == null) { // Autenticacao if (request.getSession())
     * 
     * if (identity.isLoggedIn()) { identity.logout(); } SSOConfig.print(
     * "    [Redirecionar para pagina de checagem do servidor para obter um novo Cookie de Sessao]"
     * , 1); serverCheckRedirect(facesContext, request); } else {
     * SSOConfig.print("[+] [1. Cookie de Sessao existe...]", 1); if
     * (identity.isLoggedIn()) { // Atualizacao do acesso
     * SSOConfig.print("[+] [2. Identidade autenticada...]", 2);
     * SSOConfig.print("    [Redireciona para pagina principal do Cliente]", 2);
     * clientMainRedirect(); } else { // Autorizacao
     * SSOConfig.print("[-] [2. Identidade nao autenticada...]", 2); try {
     * usuarioDto = autorizar(getClientApplicationId(), sessionCookie);
     * SSOConfig.print("[+] [3. Autorizacao valida]", 3);
     * identity.getCredentials().setUsername(usuarioDto.getLogin() + "_" + (new
     * Date()).toString()); identity.login();
     * SSOConfig.print("    [Redireciona para pagina principal do Cliente]", 3);
     * clientMainRedirect(); } catch (Exception e) {
     * SSOConfig.print("[-] [3. Acesso negado]", 3); if (e instanceof
     * NegocioException &&
     * e.getMessage().equals(SSOError.SESSAO_INEXISTENTE.obterMensagemErro())) {
     * // PassportInvalidException SSOConfig.deleteSessionCookie(request,
     * response); // Apaga Cookie de Sessao invalido SSOConfig.print(
     * "    [Redirecionar para pagina de checagem do servidor para obter um novo Cookie de Sessao]"
     * , 3); serverCheckRedirect(facesContext, request); } else {
     * e.printStackTrace();
     * SSOConfig.print("    [Redirecionar para pagina de erro do Cliente]", 3);
     * //clientErrorRedirect(); serverCheckRedirect(facesContext, request); } }
     * } }
     */
  }

  // ///////////
  // Cookies //
  // ///////////

  /**
   * Retorna o Cookie de Sessao.
   */
  public static String getSessionCookie(HttpServletRequest request) {
    return HttpUtil.getCookie(request, SESSION_COOKIE);
  }

  /**
   * Grava o Cookie de Sessao.
   */
  public static void setSessionCookie(String value, HttpServletRequest request, HttpServletResponse response) {
    // HttpUtil.setCookie(response, SESSION_COOKIE, value,
    // request.getServerName(), COOKIE_PATH, -1);
  }

  /**
   * Apaga Cookie de Sessao.
   */
  public static void deleteSessionCookie(HttpServletRequest request, HttpServletResponse response) {
    // HttpUtil.setCookie(response, SESSION_COOKIE, "", request.getServerName(),
    // COOKIE_PATH, 0);
  }

  /**
   * Apaga Cookie de Sessao.
   */
  public static void deleteJSESSIONID(HttpServletRequest request, HttpServletResponse response) {
    // HttpUtil.setCookie(response, "JSESSIONID", "", request.getServerName(),
    // COOKIE_PATH, 0);
  }
}