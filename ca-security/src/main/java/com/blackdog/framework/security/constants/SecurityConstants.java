package com.blackdog.framework.security.constants;

import com.blackdog.framework.core.model.ErrorMessageConstant;

public class SecurityConstants {
  
  
  public static final String AUTHENTICATED_USER = "authenticatedUser";
  public static final String SESSION_COOKIE = "sessionCookie";

  public static enum SecurityErrors implements ErrorMessageConstant {
    
    SECURITY_000("SECURITY-000", "Security Fail."),
    SECURITY_001("SECURITY-001", "A operação não foi definida."), 
    SECURITY_002("SECURITY-002", "O usuário não está autenticado."),
    SECURITY_003("SECURITY-003", "Usuário sem permissão para a operação."),
    SECURITY_004("SECURITY-004", "O usuário não está autenticado."),
    SECURITY_005("SECURITY-005", "Cliente Governo Digital sem identificação."),
    SECURITY_006("SECURITY-006", "Falha ao recuperar os dados do usuário autenticado."),
    SECURITY_007("SECURITY-007", "Falha ao Autenticar o usuário no Controle Acesso: {0}."),
    SECURITY_008("SECURITY-008", "Falha ao Autenticar o usuário no Controle Acesso: {0}."),
    SECURITY_009("SECURITY-009", "Identificação do usuário inválida."),
    
    SECURITY_010("SECURITY-010", "Falha ao localizar os dados do usuário: {0}."),
    SECURITY_011("SECURITY-011", "O Token de Autendicação não foi informado."),
    SECURITY_012("SECURITY-012", "Não foi possível consultar a base de tokens. Considere criar um @Producer para o \"UserTokenDao\"."),
    SECURITY_013("SECURITY-013", "O Token não é válido, não foi registrado ou está corrompido."),
    SECURITY_014("SECURITY-014", "Token Inválido: {0}."),
    SECURITY_015("SECURITY-015", "Token Inválido: {0}."),
    SECURITY_016("SECURITY-016", "O Token de Autendicação não foi informado."),
    SECURITY_017("SECURITY-017", "O Authorization header é inválido."),
    SECURITY_018("SECURITY-018", "Token de acesso inválido."),
    SECURITY_019("SECURITY-019", "O Token de Acesso está mal formado."),
    SECURITY_020("SECURITY-020", "Token de acesso inválido."),
    SECURITY_021("SECURITY-021", "O Token de acesso é não localizado."),
    SECURITY_022("SECURITY-022", "Token Inválido para o Ambinete de {0}."),
    SECURITY_023("SECURITY-023", "."),
    SECURITY_024("SECURITY-024", "."),
    SECURITY_025("SECURITY-025", "."),
    SECURITY_026("SECURITY-026", "."),
    SECURITY_027("SECURITY-027", "."),
    SECURITY_028("SECURITY-028", "."),
    SECURITY_029("SECURITY-029", "."),
    
    SECURITY_030("SECURITY-030", "O Identificador do usuário não pode ser nulo."),
    SECURITY_031("SECURITY-031", "O tipo de Token não pode ser nulo."),
    SECURITY_032("SECURITY-032", "."),
    ;
    
    private String code;
    private String message;
    
    private SecurityErrors(String code, String message) {
      this.code = code;
      this.message = message;
    }

    public String getCode() { return code; }

    public String getMessage() { return message; }
    
  }
  
}
