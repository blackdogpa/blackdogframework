package com.blackdog.framework.security.controleacesso.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.blackdog.framework.security.dto.AuthenticatedUser;
import com.blackdog.framework.security.dto.OrgaoDto;


public class SecurityDAO {

  @PersistenceContext(unitName = "security-ca-ds")
  protected EntityManager emCA;
  
  
  private static final String query01 = "SELECT u.id, u.login, u.descricao, "
          + "(SELECT ARRAY_TO_STRING(ARRAY_AGG(up.id_perfil), ',') FROM controleacesso.ws_usuario_ws_perfil up WHERE up.id_usuario = u.id) as perfis, "
          + "u.id_orgao, o.id_orgao_adabas, o.sigla, pj.nome_fantasia, pj.cnpj  "
          + "FROM controleacesso.ws_usuario u "
          + "JOIN nucleopa.orgao o ON o.id_orgao = u.id_orgao "
          + "JOIN nucleopa.pessoa_juridica pj ON pj.id = u.id_orgao "; 

  
  public AuthenticatedUser loadUser(Long idUser) throws SecurityException {
    
    try {
      Object[] user = (Object[]) 
              emCA.createNativeQuery(query01 + "WHERE u.id = :id")
                  .setParameter("id", idUser)
                  .getSingleResult();
      return cast(user);
    } catch (NoResultException e) {
      throw new SecurityException("Token de acesso inválido: Token não resgistrado.");
    } catch (NonUniqueResultException e) {
      throw new SecurityException("Token de acesso inválido: Token não resgistrado.");
    }
  }
  
  private AuthenticatedUser cast(Object[] result) throws SecurityException {
    try {
           
      AuthenticatedUser u = new AuthenticatedUser();
      u.setId(Long.parseLong(String.valueOf(result[0])));
      u.setLogin(String.valueOf(result[1]));
      u.setEmail(null);      
      
      String nome = String.valueOf(result[2]);
      if(nome.indexOf(" ") < 0) {
        
        u.setFirstName(nome);
        
      } else {
        
        u.setFirstName(nome.substring(0, nome.indexOf(" ")));
        u.setLastName(nome.substring(nome.indexOf(" ")).trim());
        
      }
      
      
      
      OrgaoDto o = new OrgaoDto();
      o.setId(Long.parseLong(String.valueOf(result[4])));
      o.setCodigo(String.valueOf(result[5]));
      o.setSigla(String.valueOf(result[6]));
      o.setNome(String.valueOf(String.valueOf(result[7])));
      o.setCnpj(String.valueOf(result[8]));
      u.setOrgao(o);
      
      // Popular perfis no usuario
      /*String perfisSTR = (String) result[7];
      
      if(perfisSTR != null && !perfisSTR.equals("")) {
        List<Integer> perfis = new ArrayList<Integer>();
        if(!perfisSTR.contains(",")) {
          perfis.add(new Integer(perfisSTR));
        } else {
          String[] split = perfisSTR.split(",");
          for (String id : split) {
            perfis.add(new Integer(id));
          }
        }
        
        u.setPerfis(perfis);

        if(perfis.contains(37)) { //TODO Orgao  >> add no configApp
          u.setLogLevel(LogLevel.WARN);
        }
        
        if(perfis.contains(38)) { //TODO ADMIN  >> add no configApp
          u.setLogLevel(LogLevel.DEBUG);
        }
      }*/
      
      
      
      return u;
      
    } catch (Exception e) {
      throw new SecurityException("Falha ao efetuar o login");
    }
  }
  
  public AuthenticatedUser login(String login, String senha) throws SecurityException {
    try {
      Object[] result = (Object[]) 
              emCA.createNativeQuery(query01 + "WHERE login = :login AND senha = md5(:senha)")
                  .setParameter("login", login)
                  .setParameter("senha", senha)
                  .getSingleResult();
      
      return cast(result);
    } catch (NoResultException e) {
      throw new SecurityException("Usuário ou senha Inválidos.");
    } catch (NonUniqueResultException e) {
      throw new SecurityException("O usuário " + login + " é inválido.");
    }
  }
  
  public Long getUserId(String login, String senha) throws SecurityException {
    try {
      
      StringBuilder select = new StringBuilder("SELECT id FROM controleacesso.usuario WHERE login = :login");
      
      if(senha != null) {
        select.append(" AND senha = md5(:senha) ");
      }
      
      Query query = emCA.createNativeQuery(select.toString());
      
      query.setParameter("login", login);
                  
      if(senha != null) {
        query.setParameter("senha", senha);
      }
      
      BigInteger biId = (BigInteger) query.getSingleResult();
      
      if(biId != null) {
        return biId.longValue();
      }
      
      return null;
      
    } catch (NoResultException e) {
      return null;
    } catch (NonUniqueResultException e) {
      return null;
    }
  }
  
  public Long getWsUserId(String login, String senha) throws SecurityException {
    try {
      
      StringBuilder select = new StringBuilder("SELECT id FROM controleacesso.ws_usuario WHERE login = :login");
      
      if(senha != null) {
        select.append(" AND senha = md5(:senha) ");
      }
      
      Query query = emCA.createNativeQuery(select.toString());
      
      query.setParameter("login", login);
                  
      if(senha != null) {
        query.setParameter("senha", senha);
      }
      
      BigInteger biId = (BigInteger) query.getSingleResult();
      
      if(biId != null) {
        return biId.longValue();
      }
      
      return null;
      
    } catch (NoResultException e) {
      return null;
    } catch (NonUniqueResultException e) {
      return null;
    }
  }
  
  public Boolean hasRole(AuthenticatedUser loggedUser, String operation) throws SecurityException {
   return  hasRole(loggedUser, operation, null, null);
  }
  
  public Boolean hasRole(AuthenticatedUser loggedUser, String operation, String[] hasAllRoles, String[] hasAnyRoles) throws SecurityException {
    
    try {
      
      StringBuilder q = new StringBuilder();
      q.append("SELECT COUNT(1) > 0 ");
      q.append("FROM controleacesso.ws_usuario_ws_perfil up ");
      q.append("JOIN controleacesso.ws_perfil_ws_operacao po ON po.id_perfil = up.id_perfil ");
      q.append("JOIN controleacesso.ws_operacao o ON o.id = po.id_operacao ");
      q.append("JOIN controleacesso.ws_perfil p ON p.id = up.id_perfil ");
      q.append("WHERE p.ativo = 1 AND o.ativo = 1 AND up.id_usuario = :idUsuario ");
      
      
      if(operation != null) {
        
        if(operation.startsWith("P#")) {
          q.append("AND p.codigo = :codigo ");
          operation = operation.substring(2);
        } else {
          q.append("AND o.codigo = :codigo ");
        }
        
      } else if(hasAllRoles != null){
        
        //TODO ??? 
        
      } else if(hasAnyRoles != null){
       
      //TODO IN() ???
        
      }
      
      Boolean result = (Boolean) 
              emCA.createNativeQuery(q.toString())
                  .setParameter("idUsuario", loggedUser.getId())
                  .setParameter("codigo", operation)
                  .getSingleResult();
      
      
      return result;
      
    } catch (NoResultException e) {
      return null;
    } catch (NonUniqueResultException e) {
      throw new SecurityException("Usuário Inválido.");
    }
  }
  

}
