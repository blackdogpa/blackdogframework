
package com.blackdog.framework.security.controleacesso.wsclient;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "ControleAcessoFacadeWS", targetNamespace = "http://sistemas.pa.gov.br/controleacesso/ns/ControleAcessoFacadeWS")
@SOAPBinding(style = SOAPBinding.Style.RPC)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface ControleAcessoFacadeWS {


    /**
     * 
     * @param idUsuarioManut
     * @param usuarioComplementoDto
     * @throws ControleAcessoWSException
     */
    @WebMethod
    public void alterarDadosComplementaresUsuario(
        @WebParam(name = "usuarioComplementoDto", partName = "usuarioComplementoDto")
        UsuarioComplementoDtoWS usuarioComplementoDto,
        @WebParam(name = "idUsuarioManut", partName = "idUsuarioManut")
        long idUsuarioManut)
        throws ControleAcessoWSException
    ;

    /**
     * 
     * @param novaSenha
     * @param login
     * @param senhaAtual
     * @return
     *     returns boolean
     * @throws ControleAcessoWSException
     */
    @WebMethod
    @WebResult(partName = "return")
    public boolean alterarSenha(
        @WebParam(name = "login", partName = "login")
        String login,
        @WebParam(name = "senhaAtual", partName = "senhaAtual")
        String senhaAtual,
        @WebParam(name = "novaSenha", partName = "novaSenha")
        String novaSenha)
        throws ControleAcessoWSException
    ;

    /**
     * 
     * @param passaporte
     * @param aplicacao
     * @param login
     * @return
     *     returns boolean
     * @throws ControleAcessoWSException
     */
    @WebMethod
    @WebResult(partName = "return")
    public boolean atualizarAcesso(
        @WebParam(name = "aplicacao", partName = "aplicacao")
        long aplicacao,
        @WebParam(name = "login", partName = "login")
        String login,
        @WebParam(name = "passaporte", partName = "passaporte")
        String passaporte)
        throws ControleAcessoWSException
    ;

    /**
     * 
     * @param senha
     * @param login
     * @return
     *     returns br.pa.gov.sead.simas.controleacesso.wsclientUsuarioDtoWS
     * @throws ControleAcessoWSException
     */
    @WebMethod
    @WebResult(partName = "return")
    public UsuarioDtoWS autenticar(
        @WebParam(name = "login", partName = "login")
        String login,
        @WebParam(name = "senha", partName = "senha")
        String senha)
        throws ControleAcessoWSException
    ;

    /**
     * 
     * @param passaporte
     * @param aplicacao
     * @return
     *     returns br.pa.gov.sead.simas.controleacesso.wsclientUsuarioDtoWS
     * @throws ControleAcessoWSException
     */
    @WebMethod
    @WebResult(partName = "return")
    public UsuarioDtoWS autorizar(
        @WebParam(name = "aplicacao", partName = "aplicacao")
        long aplicacao,
        @WebParam(name = "passaporte", partName = "passaporte")
        String passaporte)
        throws ControleAcessoWSException
    ;

    /**
     * 
     * @param passaporte
     * @param aplicacao
     * @param atualizarAcesso
     * @return
     *     returns br.pa.gov.sead.simas.controleacesso.wsclientUsuarioDtoWS
     * @throws ControleAcessoWSException
     */
    @WebMethod
    @WebResult(partName = "return")
    public UsuarioDtoWS autorizarAcesso(
        @WebParam(name = "aplicacao", partName = "aplicacao")
        long aplicacao,
        @WebParam(name = "passaporte", partName = "passaporte")
        String passaporte,
        @WebParam(name = "atualizarAcesso", partName = "atualizarAcesso")
        boolean atualizarAcesso)
        throws ControleAcessoWSException
    ;

    /**
     * 
     * @param usuarioComplementoDto
     * @return
     *     returns br.pa.gov.sead.simas.controleacesso.wsclientUsuarioComplementoDtoWS
     * @throws ControleAcessoWSException
     */
    @WebMethod
    @WebResult(partName = "return")
    public UsuarioComplementoDtoWS buscarDadosComplementaresUsuario(
        @WebParam(name = "usuarioComplementoDto", partName = "usuarioComplementoDto")
        UsuarioComplementoDtoWS usuarioComplementoDto)
        throws ControleAcessoWSException
    ;

    /**
     * 
     * @param aplicacao
     * @return
     *     returns br.pa.gov.sead.simas.controleacesso.wsclientParametroDtoWSArray
     */
    @WebMethod
    @WebResult(partName = "return")
    public ParametroDtoWSArray buscarParametroDtoWSList(
        @WebParam(name = "aplicacao", partName = "aplicacao")
        long aplicacao);

    /**
     * 
     * @param passaporte
     * @return
     *     returns boolean
     * @throws ControleAcessoWSException
     */
    @WebMethod
    @WebResult(partName = "return")
    public boolean encerrarSessao(
        @WebParam(name = "passaporte", partName = "passaporte")
        String passaporte)
        throws ControleAcessoWSException
    ;

    /**
     * 
     * @param tipoDocumento
     * @param numeroDocumento
     * @param email
     * @return
     *     returns boolean
     * @throws ControleAcessoWSException
     */
    @WebMethod
    @WebResult(partName = "return")
    public boolean enviarSenha(
        @WebParam(name = "tipoDocumento", partName = "tipoDocumento")
        int tipoDocumento,
        @WebParam(name = "numeroDocumento", partName = "numeroDocumento")
        String numeroDocumento,
        @WebParam(name = "email", partName = "email")
        String email)
        throws ControleAcessoWSException
    ;

    /**
     * 
     * @param operacao
     * @param usuario
     * @return
     *     returns boolean
     * @throws ControleAcessoWSException
     */
    @WebMethod
    @WebResult(partName = "return")
    public boolean hasOperacao(
        @WebParam(name = "usuario", partName = "usuario")
        long usuario,
        @WebParam(name = "operacao", partName = "operacao")
        String operacao)
        throws ControleAcessoWSException
    ;

    /**
     * 
     * @param passaporte
     * @return
     *     returns boolean
     * @throws ControleAcessoWSException
     */
    @WebMethod
    @WebResult(partName = "return")
    public boolean validarSessao(
        @WebParam(name = "passaporte", partName = "passaporte")
        String passaporte)
        throws ControleAcessoWSException
    ;

    /**
     * 
     * @param aplicacao
     * @param nome
     * @return
     *     returns java.lang.String
     */
    @WebMethod
    @WebResult(partName = "return")
    public String valorConfiguracao(
        @WebParam(name = "nome", partName = "nome")
        String nome,
        @WebParam(name = "aplicacao", partName = "aplicacao")
        long aplicacao);

}
