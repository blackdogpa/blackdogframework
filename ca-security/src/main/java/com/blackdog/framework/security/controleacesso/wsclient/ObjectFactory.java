
package com.blackdog.framework.security.controleacesso.wsclient;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.gov.pa.sistemas.controleacesso.ns.controleacessofacadews package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://sistemas.pa.gov.br/controleacesso/ns/ControleAcessoFacadeWS", "Exception");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.gov.pa.sistemas.controleacesso.ns.controleacessofacadews
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ControleAcessoWSExceptionFaultInfo }
     * 
     */
    public ControleAcessoWSExceptionFaultInfo createException() {
        return new ControleAcessoWSExceptionFaultInfo();
    }

    /**
     * Create an instance of {@link ParametroDtoWS }
     * 
     */
    public ParametroDtoWS createParametroDtoWS() {
        return new ParametroDtoWS();
    }

    /**
     * Create an instance of {@link UsuarioDtoWS }
     * 
     */
    public UsuarioDtoWS createUsuarioDtoWS() {
        return new UsuarioDtoWS();
    }

    /**
     * Create an instance of {@link PerfilDtoWS }
     * 
     */
    public PerfilDtoWS createPerfilDtoWS() {
        return new PerfilDtoWS();
    }

    /**
     * Create an instance of {@link UsuarioComplementoDtoWS }
     * 
     */
    public UsuarioComplementoDtoWS createUsuarioComplementoDtoWS() {
        return new UsuarioComplementoDtoWS();
    }

    /**
     * Create an instance of {@link AplicacaoDtoWS }
     * 
     */
    public AplicacaoDtoWS createAplicacaoDtoWS() {
        return new AplicacaoDtoWS();
    }

    /**
     * Create an instance of {@link ParametroDtoWSArray }
     * 
     */
    public ParametroDtoWSArray createParametroDtoWSArray() {
        return new ParametroDtoWSArray();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ControleAcessoWSExceptionFaultInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sistemas.pa.gov.br/controleacesso/ns/ControleAcessoFacadeWS", name = "Exception")
    public JAXBElement<ControleAcessoWSExceptionFaultInfo> createException(ControleAcessoWSExceptionFaultInfo value) {
        return new JAXBElement<ControleAcessoWSExceptionFaultInfo>(_Exception_QNAME, ControleAcessoWSExceptionFaultInfo.class, null, value);
    }

}
