
package com.blackdog.framework.security.controleacesso.wsclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>
 * Java class for perfilDtoWS complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="perfilDtoWS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descricao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "perfilDtoWS", propOrder = { "codigo", "descricao", "id" })
public class PerfilDtoWS {

  protected String codigo;
  protected String descricao;
  protected Long id;

  
  
  public PerfilDtoWS() {
    super();
  }

  public PerfilDtoWS(String codigo) {
    super();
    this.codigo = codigo;
  }

  /**
   * Gets the value of the codigo property.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getCodigo() {
    return codigo;
  }

  /**
   * Sets the value of the codigo property.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setCodigo(String value) {
    this.codigo = value;
  }

  /**
   * Gets the value of the descricao property.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getDescricao() {
    return descricao;
  }

  /**
   * Sets the value of the descricao property.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setDescricao(String value) {
    this.descricao = value;
  }

  /**
   * Gets the value of the id property.
   * 
   * @return possible object is {@link Long }
   * 
   */
  public Long getId() {
    return id;
  }

  /**
   * Sets the value of the id property.
   * 
   * @param value allowed object is {@link Long }
   * 
   */
  public void setId(Long value) {
    this.id = value;
  }

}
