
package com.blackdog.framework.security.controleacesso.wsclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for usuarioComplementoDtoWS complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="usuarioComplementoDtoWS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dataManut" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idPessoaJuridica" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idUsuario" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="imagem" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "usuarioComplementoDtoWS", propOrder = {
    "dataManut",
    "idPessoaJuridica",
    "idUsuario",
    "imagem"
})
public class UsuarioComplementoDtoWS {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataManut;
    protected Long idPessoaJuridica;
    protected Long idUsuario;
    protected byte[] imagem;
    

    /**
     * Gets the value of the dataManut property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataManut() {
        return dataManut;
    }

    /**
     * Sets the value of the dataManut property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataManut(XMLGregorianCalendar value) {
        this.dataManut = value;
    }

    /**
     * Gets the value of the idPessoaJuridica property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdPessoaJuridica() {
        return idPessoaJuridica;
    }

    /**
     * Sets the value of the idPessoaJuridica property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdPessoaJuridica(Long value) {
        this.idPessoaJuridica = value;
    }

    /**
     * Gets the value of the idUsuario property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdUsuario() {
        return idUsuario;
    }

    /**
     * Sets the value of the idUsuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdUsuario(Long value) {
        this.idUsuario = value;
    }

    /**
     * Gets the value of the imagem property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getImagem() {
        return imagem;
    }

    /**
     * Sets the value of the imagem property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setImagem(byte[] value) {
        this.imagem = value;
    }

}
