package com.blackdog.framework.security.dao;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.blackdog.framework.configurations.util.PropertiesUtils;
import com.blackdog.framework.exceptions.BusinessException;
import com.blackdog.framework.exceptions.SecurityException;
import com.blackdog.framework.security.service.TokenType;

public class UserTokenDao {
  
  
  
  
  protected EntityManager em;
  
  private String schema;
  private String table;
  
  public UserTokenDao(EntityManager em, String schema, String table) {
    super();
    this.em = em;
    this.schema = schema;
    this.table = table;
  }
  
  public UserTokenDao(EntityManager em, String schema) {
    super();
    this.em = em;
    this.schema = schema;
    this.table = "user_tokens";
  }
  
  /**
   * Verify if the token exists
   * 
   * @param token
   * @return
   * @throws BusinessException 
   */
  public Boolean exists(Long idUser) throws BusinessException {
    
    Boolean result = (Boolean) em.createNativeQuery("SELECT COUNT(1) > 0 FROM "+getDbSchema()+".user_tokens tk WHERE tk.id_user = :id_user").setParameter("id_user", idUser).getSingleResult();
    
    return result;
  }
  
  /**
   * Verify if the token exists
   * 
   * @param token
   * @return
   * @throws BusinessException 
   */
  public String findToken(Long idUser) throws BusinessException {
    
    try {
      String result = (String) em.createNativeQuery("SELECT tk.token FROM "+getDbSchema()+".user_tokens tk WHERE tk.id_user = :id_user AND tk.is_enable = true;").setParameter("id_user", idUser).getSingleResult();
      
      return result;
    } catch (NoResultException | NonUniqueResultException e) {
      return null;
    }
  }
  
  /**
   * Verify if the token is valid. 
   *   if exists
   *  +if is enable
   *  
   *  In token +if NotBefore is less than now (time of life) 
   *  In token +if Type not equal PERMANENT
   *  In token   +if NotBefore is less than now (time of life)
   *  In token   +if ExpiresAt is greater than now (time of life)
   * 
   * @param token
   * @return 
   * @throws BusinessException 
   */
  public Boolean isValid(String token) throws BusinessException {
    
    if(token.startsWith("Bearer ")) {
      token = token.substring(7);
    }
    
    Boolean result = (Boolean) em.createNativeQuery("SELECT COUNT(1) > 0 FROM "+getDbSchema()+".user_tokens tk WHERE tk.token = :tk AND tk.is_enable = true;").setParameter("tk", token).getSingleResult();
    
    return result;
  }
  
  /**
   * Persiste a token
   * 
   *  - token
   *  - userId
   *  - type (SESSION, TIMED, PERMANENT)
   *  - isEnable
   *  - NotBefore (the criation date)
   *  - ExpiresAt
   *  - [usuario Criacao]
   * 
   * @param userId
   * @param token
   * @throws BusinessException 
   */
  public void createPersistent(Long userId, Boolean isEnable, String token) throws BusinessException {
    create(userId, TokenType.PERMANENT, isEnable, null, token);
  }
  
  public void create(Long userId, TokenType type, Boolean isEnable, Date expiresAt, String token) throws BusinessException {
    
    test();
  
    //Gravar o token, idUsuario, isEnable, e data Criacao, [usuario Criacao]
    
    String insert = null;
    
    if(type.equals(TokenType.PERMANENT) || type.equals(TokenType.CROSSENV) ) {
      insert = "INSERT INTO "+getDbSchema()+".user_tokens (id_user, type, is_enable, not_before, create_date, expires_at, token) VALUES (:id_user, :type, :is_emabel, :not_before, :create_date, null, :token);";
    } else {
      insert = "INSERT INTO "+getDbSchema()+".user_tokens (id_user, type, is_enable, not_before, create_date, expires_at, token) VALUES (:id_user, :type, :is_emabel, :not_before, :create_date, :expires_at, :token);";
    }
    
    
    Query query = em.createNativeQuery(insert)
        .setParameter("id_user", userId)
        .setParameter("type", type.toString())
        .setParameter("is_emabel", isEnable)
        .setParameter("not_before", new Date())
        .setParameter("create_date", new Date())
        .setParameter("token", token);
    
    if(type.equals(TokenType.PERMANENT) || type.equals(TokenType.CROSSENV)) {
      //query.setParameter("expires_at", null);
    } else {
      
      if(expiresAt == null) {
        throw new BusinessException("The ExpiresAt date can't be null.");
      }
      
      query.setParameter("expires_at", expiresAt);
    }
    
    query.executeUpdate();

    
  }
  
  
  /**
   * Remove a Token
   * 
   * @param token
   * @throws BusinessException 
   */
  public void delete(String token) throws BusinessException {
    em.createNativeQuery("DELETE FROM "+getDbSchema()+".user_tokens WHERE token = :tk").setParameter("tk", token).executeUpdate();
  }
  
  /**
   * Remove the Tokens of user
   * 
   * @param token
   * @throws BusinessException 
   */
  public void delete(Long idUser) throws BusinessException {
    em.createNativeQuery("DELETE FROM "+getDbSchema()+".user_tokens WHERE id_user = :id_user").setParameter("id_user", idUser).executeUpdate();
  }

  /**
   * Enablem a Token
   * 
   * @param token
   * @throws BusinessException 
   */
  public void enable(String token) throws BusinessException {
    em.createNativeQuery("UPDATE "+getDbSchema()+".user_tokens set is_enable = true, update_date = :update_date WHERE token = :tk").setParameter("tk", token).setParameter("update_date", new Date()).executeUpdate();
  }
  
  /**
   * Disable a Token
   * 
   * @param token
   * @throws BusinessException 
   */
  public void disable(String token) throws BusinessException {
    em.createNativeQuery("UPDATE "+getDbSchema()+".user_tokens set is_enable = false, update_date = :update_date WHERE token = :tk").setParameter("tk", token).setParameter("update_date", new Date()).executeUpdate(); 
  }
  
  public void disable(Long idUser) throws BusinessException {
    em.createNativeQuery("UPDATE "+getDbSchema()+".user_tokens set is_enable = false, update_date = :update_date WHERE id_user = :id_user").setParameter("id_user", idUser).setParameter("update_date", new Date()).executeUpdate(); 
  }
  
  private void test() throws  BusinessException {
    try {
      em.createNativeQuery("SELECT COUNT(1) FROM " +schema+"."+table).getSingleResult();
    } catch (Exception e) {
      throw new BusinessException("Verifique se a tabela \"" + schema+"."+table + "\" foi criada.");
    }
  }
  
  private String dbSchema = null;
  private String getDbSchema() throws BusinessException {
    if(dbSchema == null) {
      dbSchema = PropertiesUtils.getParameter(PropertiesUtils.APPLICATION_DB_SCHEMA);
      if(StringUtils.isBlank(dbSchema)) {
        throw new SecurityException("A identificação do DB Schema não foi definida. Favor defina a chave \""+PropertiesUtils.APPLICATION_DB_SCHEMA+"\" no seu arquivo \"blackdog.properties\". ");
      }
    }
    return dbSchema;
  }
  
}
