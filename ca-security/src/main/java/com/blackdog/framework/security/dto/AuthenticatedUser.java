package com.blackdog.framework.security.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * @author thiago
 *
 */
public class AuthenticatedUser implements Serializable {

  private static final long serialVersionUID = 1990360728314136168L;

  /* ControleAcesso */
  private Long id;
  private String login;
  private String firstName;
  private String lastName;
  
  private String email;
  
  private OrgaoDto orgao;
  
  @JsonProperty(value = "details")
  private UserDetails details;
  
  //@JSON(include=false)
  /*
   * Utilizado quando o GovernoDigital estiver envolvido e houver sessão do usuário
   */
  @JsonIgnore
  private String sessionCookie;
  
  
  @JsonProperty("authorities")
  public List<String> getAuthorities() {
    
    if(details == null) {
      return new ArrayList<>();
    } else {
      
      
      ArrayList<String> authorities = new ArrayList<>();
      
      authorities.addAll(details.getRoles());
      authorities.addAll(details.getGrants());
      
      return authorities;
    }
  }
  
  
  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public OrgaoDto getOrgao() {
    return orgao;
  }

  public void setOrgao(OrgaoDto orgao) {
    this.orgao = orgao;
  }

  public UserDetails getDetails() {
    return details;
  }

  public void setDetails(UserDetails details) {
    this.details = details;
  }

  public String getSessionCookie() {
    return sessionCookie;
  }

  public void setSessionCookie(String sessionCookie) {
    this.sessionCookie = sessionCookie;
  }

  @Override
  public String toString() {
    return "AuthenticatedUser [id=" + id + ", login=" + login + ", name=" + firstName + "" + lastName + ", email=" + email + ", orgao=" + orgao + ", details=" + details + "]";
  }
  
  
  
  
}
