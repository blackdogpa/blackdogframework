package com.blackdog.framework.security.dto;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.blackdog.framework.security.service.TokenType;

public class CreateTokenDto {

  
  private AuthenticatedUser authenticatedUser;  
  
  //private String ambiente;
  //private String sistema;
  
  private String environmentID;
  
  private Date expiresAt;
  
  private TokenType tokenType;

  private Map<String, Object> extraClaims = new HashMap<>();
  
  private Map<String, Object> headers = new HashMap<>();

  public CreateTokenDto() {
    super();
  }
  
  
  public void addExtraClaims(String key, Object value) {
    extraClaims.put(key, value);
  }

  public Map<String, Object> getExtraClaims() {
    return extraClaims;
  }

  public void setExtraClaims(Map<String, Object> extraClaims) {
    this.extraClaims = extraClaims;
  }

  public Map<String, Object> getHeaders() {
    return headers;
  }

  public void setHeaders(Map<String, Object> headers) {
    this.headers = headers;
  }

  public Date getExpiresAt() {
    return expiresAt;
  }

  public void setExpiresAt(Date expiresAt) {
    this.expiresAt = expiresAt;
  }
  
  public TokenType getTokenType() {
    return tokenType;
  }

  public void setTokenType(TokenType tokenType) {
    this.tokenType = tokenType;
  }

  public AuthenticatedUser getAuthenticatedUser() {
    return authenticatedUser;
  }


  public void setAuthenticatedUser(AuthenticatedUser authenticatedUser) {
    this.authenticatedUser = authenticatedUser;
  }

  public String getEnvironmentID() {
    return environmentID;
  }

  public void setEnvironmentID(String environmentID) {
    this.environmentID = environmentID;
  }


  @Override
  public String toString() {
    return "CreateTokenDto [authenticatedUser=" + authenticatedUser + ", expiresAt=" + expiresAt + ", tokenType=" + tokenType + ", extraClaims="
            + extraClaims + ", headers=" + headers + "]";
  }

}
