package com.blackdog.framework.security.dto;

import java.io.Serializable;

public class LoginDto implements Serializable {

  private static final long serialVersionUID = 2293676341649729167L;

  private String login;
  private String senha;

  public LoginDto() {
    super();
  }
  
  public LoginDto(String login, String senha) {
    super();
    this.login = login;
    this.senha = senha;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getSenha() {
    return senha;
  }

  public void setSenha(String senha) {
    this.senha = senha;
  }


}
