package com.blackdog.framework.security.dto;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com)
 *
 */
public class OrgaoDto {

  private Long id;
  private String cnpj;
  private String codigo;
  private String nome;
  private String sigla;

  public OrgaoDto() {
    super();
  }


  public OrgaoDto(Long id, String codigo, String nome, String sigla) {
    super();
    this.id = id;
    this.codigo = codigo;
    this.nome = nome;
    this.sigla = sigla;
  }

  public String getCodigo() {
    return codigo;
  }

  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getSigla() {
    return sigla;
  }

  public void setSigla(String sigla) {
    this.sigla = sigla;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  
  public String getCnpj() {
    return cnpj;
  }


  public void setCnpj(String cnpj) {
    this.cnpj = cnpj;
  }


  @Override
  public String toString() {
    return "OrgaoDto [id=" + id + ", codigo=" + codigo + ", nome=" + nome + ", sigla=" + sigla + "]";
  }
  
  

}
