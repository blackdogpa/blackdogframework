package com.blackdog.framework.security.dto;

import java.util.Arrays;
import java.util.Set;

/**
 * @author thiago.soares (thiago.soares.jr@gmail.com)
 *
 */
public class UserDetails {

  private Set<String> roles;
  private Set<String> grants;

  private boolean activated = false;
  private String langKey;
  
  private byte[] image;

  public UserDetails() {
    super();
  }

  public Set<String> getRoles() {
    return roles;
  }

  public void setRoles(Set<String> roles) {
    this.roles = roles;
  }

  public Set<String> getGrants() {
    return grants;
  }

  public void setGrants(Set<String> grants) {
    this.grants = grants;
  }

  public boolean isActivated() {
    return activated;
  }

  public void setActivated(boolean activated) {
    this.activated = activated;
  }

  public String getLangKey() {
    return langKey;
  }

  public void setLangKey(String langKey) {
    this.langKey = langKey;
  }

  public byte[] getImage() {
    return image;
  }

  public void setImage(byte[] image) {
    this.image = image;
  }

  @Override
  public String toString() {
    return "UserDetails [roles=" + roles + ", grants=" + grants + ", activated=" + activated + ", langKey=" + langKey + "]";
  }
  
}
