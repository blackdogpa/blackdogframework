package com.blackdog.framework.security.interceptor;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Priorities;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Request;

import com.blackdog.framework.configurations.ApplicationConfig;
import com.blackdog.framework.configurations.Configurations;
import com.blackdog.framework.core.dto.RequestInfo;
import com.blackdog.framework.core.exceptions.BlackExceptions;
import com.blackdog.framework.core.security.CAOperation;
import com.blackdog.framework.core.security.Secure;
import com.blackdog.framework.exceptions.NotAuthenticatedException;
import com.blackdog.framework.exceptions.NotAuthorizedException;
import com.blackdog.framework.exceptions.SecurityException;
import com.blackdog.framework.exceptions.util.BusinessExceptionUtil;
import com.blackdog.framework.exceptions.util.ExceptionCastUtil;
import com.blackdog.framework.security.constants.SecurityConstants.SecurityErrors;
import com.blackdog.framework.security.sso.Identity;

/**
 * LoggedInInterceptor Intercepta as requisições para métodos anotoados
 * com @LoggedIn e checa se o usuário forneceu as credenciais.
 *
 * @author thiago.soares (thiago.soares.jr@gmail.com)
 * @deprecated use com.blackdog.framework.auditing.interceptor.RestInterceptor instead of
 */

@Secure
@Interceptor
@Priority(Priorities.AUTHENTICATION)
public class SecureInterceptor {

  @Inject
  private ApplicationConfig applicationConfig;
  
  @Inject
  private Identity identity;
  
  //@Inject
  //private AuditingService logService;
  
  //@Inject
  //private RequestInfo requestInfo; 
  
  @AroundInvoke
  public Object aroundInvoke(InvocationContext ic) throws Exception {
    //long start = System.currentTimeMillis();
    try {

      CAOperation caOperation = ic.getMethod().getAnnotation(CAOperation.class);
      
      Boolean simpleRoleType = false;
      Boolean hasRoleType    = false;
      Boolean hasAnyRoleType = false;
      
      if (caOperation == null) {
        BusinessExceptionUtil.throwSecurityException(SecurityErrors.SECURITY_001);
      } else {
        /*if (    ( caOperation.value() == null || caOperation.value().equals("") )
             && ( caOperation.hasRole() == null    || caOperation.hasRole().length == 0    || (caOperation.hasRole().length == 1 && caOperation.hasRole()[0].equals("")) )   
             && ( caOperation.hasAnyRole() == null || caOperation.hasAnyRole().length == 0 || (caOperation.hasAnyRole().length == 1 && caOperation.hasAnyRole()[0].equals("")))
           ) {
          BusinessExceptionUtil.throwSecurityException(SecurityErrors.SECURITY_001);
        }*/
        
        if ( caOperation.value() != null && !caOperation.value().equals("") ) {
          simpleRoleType = true;
        } else 
        if ( caOperation.hasRole() != null && !caOperation.hasRole()[0].equals("") ) {
          hasRoleType = true;
          //BusinessExceptionUtil.throwSecurityException(SecurityErrors.SECURITY_001);
        } else
        if ( caOperation.hasAnyRole() != null && !caOperation.hasAnyRole()[0].equals("") ) {
          hasAnyRoleType = true;
          //BusinessExceptionUtil.throwSecurityException(SecurityErrors.SECURITY_001);
        }
        
        if(!simpleRoleType && !hasRoleType && !hasAnyRoleType ) {
          BusinessExceptionUtil.throwSecurityException(SecurityErrors.SECURITY_001);
        }
        
      }
              
      
      
      if(applicationConfig.isCacheAuthorizarion()) {
        
        //Se a cache será utilizada. Deve-se ser as credenciais agora. Mas depende de quais Creds são solicitadas.  (NaturalOperation or CAOperation)
        identity.login(caOperation != null);
      
      } else {
      
        //Se não será utilizada cache, as credenciais não serão lidas no início, mas o backend fará a validação. 
        identity.login();
        
      }
      
      
      if(!identity.isLoggedIn()) {
        throw new NotAuthenticatedException(SecurityErrors.SECURITY_002);
      }
      
      Boolean checkAutorization = caOperation != null;
      
      if(checkAutorization) {
        
        //Realiza a Autorizacao da ACAO
        if(applicationConfig.isCacheAuthorizarion()) {
          
          //TODO Valida a autorizacao com a cache do Identity
          
          if(caOperation != null) {

            Boolean isValid = false;
            if(simpleRoleType) {
              
              isValid = identity.hasRoleInCache(caOperation.value());
              
            } else if( hasRoleType) {
              
              //TODO isValid = identity.hasRoleInCache(caOperation.hasRole());
              
            } else if( hasAnyRoleType) {
              
              //TODO isValid = identity.hasAnyRoleInCache(caOperation.hasAnyRole());
              
            }
            
            if(!isValid) {
              throw new NotAuthorizedException(SecurityErrors.SECURITY_003);
            }
          
          }
          
        } else {
          //identity.validarAutorizacaoNatural(ntOperation.value());
          
          if(caOperation != null) {
            Boolean isValid = false;
            if(simpleRoleType) {
              
              isValid = identity.hasRole(caOperation.value());
              
            } else if( hasRoleType) {
              
              isValid = identity.hasRole(caOperation.hasRole());
              
            } else if( hasAnyRoleType) {
              
              isValid = identity.hasAnyRole(caOperation.hasAnyRole());
              
            }
            
            if(!isValid) {
              throw new NotAuthorizedException(SecurityErrors.SECURITY_003);
            }
            
            /*if(!identity.hasRole(caOperation.value())) {
              throw new NotAuthorizedException(SecurityErrors.SECURITY_003);
            }*/
          }
          
        }
      }
      
      //TODO Registrar log dessa operação
      
      return ic.proceed();
    } catch (Throwable e) {
      
      e.printStackTrace();
      
      /*AuditingLogWSDto log = new AuditingLogWSDto(); 

      log.setEndpointType(EndpointType.REST);
      log.setFacadeClasse(this.getClass().getName());    
      log.setFacadeMetodo("securityInterceptorMethod");
      
      log.setRequestIp(requestInfo.getIp());
      log.setRequestUri(requestInfo.getRequestUri());
      log.setLogId(requestInfo.getTransactionID());
      
      log.setSuccess(false);
      log.setReturnObject(e);
      
      log.setErrorComponent(ExceptionCastUtil.getExceptionThrowClass(e));*/
      
      
      
      if(e instanceof BlackExceptions) {
        
        BlackExceptions be = (BlackExceptions) e;
        
        be.setId(identity.getTransactionID());
        be.setApplicationId(applicationConfig.getCaApplicationID().toString());
        be.setApplicationName(applicationConfig.getApplicationShortName() + " / " + applicationConfig.getEnvironmentId() );
        
        //log.setExceptionType(be.getType());
        //log.setExceptionCode(be.getCode());
        
      } /*else {
        
        log.setExceptionType(BlackExceptions.SECURITY);
        log.setExceptionCode(SecurityErrors.SECURITY_000.getCode());
        
      }
      
      long end = System.currentTimeMillis();
      log.setOperationTime(end - start);
      logService.registerAsyn(log);*/
      
      throw e;
      
    }
  }
  
}
