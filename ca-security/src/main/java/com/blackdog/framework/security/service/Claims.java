package com.blackdog.framework.security.service;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.blackdog.framework.security.dto.AuthenticatedUser;

public class Claims implements Serializable {
  
  private static final long serialVersionUID = -7724330296942550062L;
  
  private Long appId;
  private Long userId;
  //private ContextRequestInfo loggetUser;
  
  private String issuer;
  private String jwtId;
  private String subject;
  private List<String> audience; 

  private Date expirationTime;
  private Date issuedAt;
  private Date notBefore;
  
  private TokenType tokenType;
  
  private AuthenticatedUser authenticatedUser;
  
  private Map<String, Object> extraClaims = new HashMap<>();
  

  public Claims() {
    super();
  }

  public Long getAppId() {
    return appId;
  }

  public void setAppId(Long appId) {
    this.appId = appId;
  }

  public String getIssuer() {
    return issuer;
  }

  public void setIssuer(String issuer) {
    this.issuer = issuer;
  }

  public List<String> getAudience() {
    return audience;
  }

  public void setAudience(List<String> audience) {
    this.audience = audience;
  }

  public String getJwtId() {
    return jwtId;
  }

  public void setJwtId(String jwtId) {
    this.jwtId = jwtId;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public Date getExpirationTime() {
    return expirationTime;
  }

  public void setExpirationTime(Date expirationTime) {
    this.expirationTime = expirationTime;
  }

  public Date getIssuedAt() {
    return issuedAt;
  }

  public void setIssuedAt(Date issuedAt) {
    this.issuedAt = issuedAt;
  }

  public Date getNotBefore() {
    return notBefore;
  }

  public void setNotBefore(Date notBefore) {
    this.notBefore = notBefore;
  }
  
  public AuthenticatedUser getAuthenticatedUser() {
    return authenticatedUser;
  }

  public void setAuthenticatedUser(AuthenticatedUser authenticatedUser) {
    this.authenticatedUser = authenticatedUser;
  }
  
  public Map<String, Object> getExtraClaims() {
    return extraClaims;
  }

  public void setExtraClaims(Map<String, Object> extraClaims) {
    this.extraClaims = extraClaims;
  }
  
  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  
  public TokenType getTokenType() {
    return tokenType;
  }

  public void setTokenType(TokenType tokenType) {
    this.tokenType = tokenType;
  }

  @Override
  public String toString() {
    return "Claims [appId=" + appId + ", " + userId + ", issuer=" + issuer + ", jwtId=" + jwtId + ", subject=" + subject + ", audience=" + audience
            + ", expirationTime=" + expirationTime + ", issuedAt=" + issuedAt + ", notBefore=" + notBefore + ", " + extraClaims + "]";
  }

  
}
