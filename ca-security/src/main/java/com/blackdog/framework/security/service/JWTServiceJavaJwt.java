package com.blackdog.framework.security.service;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator.Builder;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.blackdog.framework.configurations.util.PropertiesUtils;
import com.blackdog.framework.exceptions.BusinessException;
import com.blackdog.framework.exceptions.SecurityException;
import com.blackdog.framework.security.dto.CreateTokenDto;

public class JWTServiceJavaJwt {

  
  public static final String PRODEPA_SUBJECT_ID       = "http://prodepa.pa.gov.br";
  
  public static final String CA_APPLICATION_ID_CLAIM  = "ca.application.id";
  public static final String CA_USER_ID_CLAIM         = "ca.user.id";
  public static final String ENVIRONMENT_ID_CLAIM     = "environment.id";
  
  
  
  private String jwtHmacKey;
  private String jwtIssuer;
  private String applicationCaID;
  private String environmentID;
  
  
  public String createToken(CreateTokenDto dto) throws BusinessException {
    
    try {
      
      /* **  Requided ** */
      if(dto.getAuthenticatedUser() == null) {
        throw new SecurityException("O Identificador do usuário não pode ser nulo.");
      }
      
      
      /* **  Requided ** */
      
      if(jwtHmacKey == null) {
        jwtHmacKey = PropertiesUtils.getParameter(PropertiesUtils.JWT_HMAC_KEY);
        if(StringUtils.isBlank(jwtHmacKey)) {
          throw new SecurityException("A chave de assinaturas do projeto não foi definida. Favor defina a chave \"jwt.jwtHmacKey\" no seu arquivo \"blackdog.properties\". ");
        }
      }
      
      if(jwtIssuer == null) {
        jwtIssuer = PropertiesUtils.getParameter(PropertiesUtils.JWT_ISSUER);
        if(StringUtils.isBlank(jwtIssuer)) {
          throw new SecurityException("A identificação de assinador do projeto não foi definida. Favor defina a chave \"jwt.issuer\" no seu arquivo \"blackdog.properties\". ");
        }
      }
      
      if(applicationCaID == null) {
        applicationCaID = PropertiesUtils.getParameter(PropertiesUtils.CA_APPLICATION_ID);
        if(StringUtils.isBlank(applicationCaID)) {
          throw new SecurityException("A identificação da aplicação no controle de acesso não foi definida. Favor defina a chave \"ca.application.id\" no seu arquivo \"blackdog.properties\". ");
        }
      }
      
      if(environmentID == null) {
        environmentID = PropertiesUtils.getParameter(PropertiesUtils.ENVIRONMENT_ID);
        if(StringUtils.isBlank(environmentID)) {
          throw new SecurityException("A identificação do tipo de ambiente não foi definida. Favor defina a chave \"environment.id\" no seu arquivo \"blackdog.properties\". ");
        }
      }
      
      
      
      Date now = new Date();
      
      Builder tkBuilder = JWT.create();
      
      //IDentificador Unico do Token
      tkBuilder.withJWTId(UUID.randomUUID().toString());
      
      //Emissor do Token //TODO Ver de onde vira essa informação
      tkBuilder.withIssuer(jwtIssuer);
      
      //Data Criacao do Token
      tkBuilder.withIssuedAt(now);
      
      //O Token não será valido antes dessa data
      tkBuilder.withNotBefore(now);
      
      
      
      /* ******************** Dados do Usuario e Aplicação ********************** */
      
      //Definição da ENTIDADE a quem se destina o Token
      tkBuilder.withSubject(PRODEPA_SUBJECT_ID);
      
      //Definição do Destinatario
      tkBuilder.withAudience( String.valueOf(dto.getAuthenticatedUser().getId()) );
      
      
      //Definição do ID do Usuario
      tkBuilder.withClaim(CA_USER_ID_CLAIM, dto.getAuthenticatedUser().getId().toString());
      
      //Definição do ID do Sistema
      tkBuilder.withClaim(CA_APPLICATION_ID_CLAIM, applicationCaID);
      
      //Definição do ID do Ambiente
      tkBuilder.withClaim(ENVIRONMENT_ID_CLAIM, environmentID);
      
      
      //Definição de limite de uso do token
      if(dto.getExpiresAt() != null) {
        tkBuilder.withExpiresAt(dto.getExpiresAt());
      }

      //Dados Extra para o Token
      if(dto.getExtraClaims() != null && !dto.getExtraClaims().isEmpty()) {
        //tkBuilder.withHeader(dto.getExtra());
        
        for(String key : dto.getExtraClaims().keySet()) {
          
          Object value = dto.getExtraClaims().get(key);
          
          if(value instanceof String) {
            tkBuilder.withClaim(key, (String) value);
          } else
                    
          if(value instanceof Boolean) {
            tkBuilder.withClaim(key, (Boolean) value);
          } else
          
          if(value instanceof Integer) {
            tkBuilder.withClaim(key, (Integer) value);
          } else
          
          if(value instanceof Date) {
            tkBuilder.withClaim(key, (Date) value);
          } else
          
          if(value instanceof Double) {
            tkBuilder.withClaim(key, (Double) value);
          } else
          
          if(value instanceof String[]) {
            tkBuilder.withArrayClaim(key, (String[]) value);
          } else
          
          if(value instanceof Integer[]) {
            tkBuilder.withArrayClaim(key, (Integer[]) value);
          }
          else {
            throw new BusinessException("O tipo \""+value.getClass().getSimpleName()+"\" não pode ser adicionado como claim do token");
          }
          
        }
      }
      
      //Headers para o Token
      if(dto.getHeaders() != null && !dto.getHeaders().isEmpty()) {
        tkBuilder.withHeader(dto.getHeaders());
      }
    
    
      /* ******************** Assinatura do Token ********************** */
      
      //Assinar o Token
      String token = tkBuilder.sign(Algorithm.HMAC256(jwtHmacKey));
      
      return token;
    } catch (IllegalArgumentException | JWTCreationException | UnsupportedEncodingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    return null;
  }
  
}
