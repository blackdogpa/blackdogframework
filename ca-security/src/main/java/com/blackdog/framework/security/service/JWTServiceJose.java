package com.blackdog.framework.security.service;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.NumericDate;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.lang.JoseException;

import com.blackdog.framework.configurations.ApplicationConfig;
import com.blackdog.framework.configurations.dao.ConfigurationDAO;
import com.blackdog.framework.configurations.util.PropertiesUtils;
import com.blackdog.framework.exceptions.BusinessException;
import com.blackdog.framework.exceptions.NotAuthenticatedException;
import com.blackdog.framework.exceptions.SecurityException;
import com.blackdog.framework.security.constants.SecurityConstants;
import com.blackdog.framework.security.dto.AuthenticatedUser;
import com.blackdog.framework.security.dto.CreateTokenDto;
import com.blackdog.framework.utils.serialization.jackson.JacksonJsonUtils;


public class JWTServiceJose {
  
  public static final String CA_APPLICATION_ID_CLAIM  = "ca.application.id";
  public static final String CA_USER_ID_CLAIM         = "ca.user.id";
  //public static final String ENVIRONMENT_ID_CLAIM     = "environment.id";
  public static final String TOKEN_TYPE_CLAIM     = "token.type";
  
  private String jwtHmacKey;
  private String jwtIssuer;
  private String applicationCaID;
  private String environmentID;
  
  
  private String jwtKeyType;
  private String jwtRsaKeyConfig;
  //private String jwtSubjectId;
  private String jwkKeyid;

  private RsaJsonWebKey rsaJsonWebKey = null;
  
  @Inject 
  private ApplicationConfig applicationConfig;
  
  public RsaJsonWebKey getRsakey() throws SecurityException, BusinessException {
    try {
      
      if(rsaJsonWebKey == null) {
        
        if(jwkKeyid == null) {
          //jwkKeyid = PropertiesUtils.getParameter(PropertiesUtils.JWT_KEY_ID);
          throw new SecurityException("Problemas com a configuração do JWT: jwt.key.id is null");
        }
        
        if(jwtRsaKeyConfig == null) {
          //jwtRsaKeyConfig = PropertiesUtils.getParameter(PropertiesUtils.JWT_RSA_KEY);
          throw new SecurityException("Problemas com a configuração do JWT: jwt.rsa.key is null");
        }

        if (jwtRsaKeyConfig == null) {
          throw new SecurityException("A chave RSA não foi localida no daews.properties. Configure a chave abaixo ou outra de sua preferência e reinicie a app");
        } else {
          rsaJsonWebKey = (RsaJsonWebKey) RsaJsonWebKey.Factory.newPublicJwk(jwtRsaKeyConfig);
        }
        
        rsaJsonWebKey.setKeyId(jwkKeyid );
      }
      
      return rsaJsonWebKey;
      
    } catch (JoseException e) {
      throw new SecurityException("Problemas com a configuração do JWT.", e);
    }
  }
  
  public String createToken(CreateTokenDto dto) throws BusinessException {
    
    try {
      
      //environmentID = PropertiesUtils.ENVIRONMENT_ID_PRODUCAO_VALUE;
      
      configTokenValidations();

      createTokenValidations(dto);
      
      getRsakey();
      
      
      if(dto.getEnvironmentID() != null) {
        environmentID = dto.getEnvironmentID();
      }
      
      //Date now = new Date();
      NumericDate now = NumericDate.now();
      
      JwtClaims claims = new JwtClaims();
      
      //IDentificador Unico do Token
      claims.setJwtId(UUID.randomUUID().toString());
      
      //Emissor do Token //TODO Ver de onde vira essa informação
      claims.setIssuer(jwtIssuer);
      
      //Data Criacao do Token
      claims.setIssuedAt(now);
      
      //O Token não será valido antes dessa data
      claims.setNotBefore(now);
            
      
      /* ******************** Dados do Usuario e Aplicação ********************** */
      
      //Definição da ENTIDADE a quem se destina o Token
      //claims.setSubject(environmentID + "::" + dto.getEntidadeDestino() + "::" + applicationCaID);
      claims.setSubject(environmentID + "::" + applicationCaID);
      
      //Definição do AuthenticatedUser
      claims.setClaim(SecurityConstants.AUTHENTICATED_USER, JacksonJsonUtils.toJson(dto.getAuthenticatedUser()));
      
      //Definição do Destinatario
      //claims.setAudience( String.valueOf(dto.getIdUsuario()) );//TODO Preciso estudar melhor o conteúdo desse campo
      
      //Definição do ID do Usuario
      claims.setClaim(CA_USER_ID_CLAIM, dto.getAuthenticatedUser().getId());
      
      //Definição do ID do Sistema
      claims.setClaim(CA_APPLICATION_ID_CLAIM, applicationCaID);
      
      //Definição do ID do Ambiente
      claims.setClaim(PropertiesUtils.ENVIRONMENT_ID, environmentID);
      
      // Definição do ID do Ambiente
      claims.setClaim(TOKEN_TYPE_CLAIM, dto.getTokenType());
      
      
      //Definição de limite de uso do token
      if(dto.getExpiresAt() != null) {
        claims.setExpirationTime(NumericDate.fromMilliseconds(dto.getExpiresAt().getTime()));
      }

      //Dados Extra para o Token
      if(dto.getExtraClaims() != null && !dto.getExtraClaims().isEmpty()) {
        //tkBuilder.withHeader(dto.getExtra());
        
        for(String key : dto.getExtraClaims().keySet()) {
          
          Object value = dto.getExtraClaims().get(key);
          
          if(value instanceof String) {
            claims.setStringClaim(key, (String) value);
          } else 
                    
          if(value instanceof Date) {
            claims.setNumericDateClaim(key, NumericDate.fromMilliseconds(dto.getExpiresAt().getTime()));
          } else
          
          
          if(value instanceof AuthenticatedUser) {
            claims.setClaim(key, JacksonJsonUtils.toJson(value));
          } else  
            
          if(value instanceof String[]) {
            claims.setStringListClaim(key, (String[]) value);
          } else
          
          if(value instanceof List<?>) {
            claims.setStringListClaim(key, (List<String>) value);
          }
          else {
            throw new BusinessException("O tipo \""+value.getClass().getSimpleName()+"\" não pode ser adicionado como claim do token");
          }
          
        }
      }
      
      //Headers para o Token
      if(dto.getHeaders() != null && !dto.getHeaders().isEmpty()) {
        //TODO No Jose os Headers não fazem parte dos Clains
        //tkBuilder.withHeader(dto.getHeaders());cla
        
      }
    
    
      /* ******************** Assinatura do Token ********************** */
      
      JsonWebSignature jws = new JsonWebSignature();
      jws.setPayload(claims.toJson());
      jws.setKey(rsaJsonWebKey.getPrivateKey());
      jws.setKeyIdHeaderValue(rsaJsonWebKey.getKeyId());
      jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);
      String compactSerialization = jws.getCompactSerialization();
      
      
      //TODO Persiste Token
      
      return compactSerialization;
    } catch (JoseException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SecurityException(e, "Falha ao criar o Token de Acesso");
    }
  }
  
  public String createEncryptedToken(CreateTokenDto dto) throws BusinessException {
    //TODO 
    return null;
  }
  
  public String encryptToken(CreateTokenDto dto) throws BusinessException {
    //TODO 
    return null;
  }
  
  public Claims verifyToken(String jwt) throws BusinessException {
    
    //TODO  fazer esse metodo logo. Considerar obrigatoriamente o Ambiente, Subject, Audience (usuario da req), Expiracao , CA_USER_ID_CLAIM,  CA_APPLICATION_ID_CLAIM, ENVIRONMENT_ID_CLAIM
    
    //TODO ExpirationTime > Ver como testar isso com e sem o ExpirationAt
    
    JwtClaims jwtClaims = null;
    

    //createTokenValidations(dto);
    
    jwt = checkTokenHeader(jwt);
    
    configTokenValidations();
    
    getRsakey();

    if (jwt != null && !jwt.isEmpty()) {
      
      JwtConsumer jwtConsumer = new JwtConsumerBuilder()
      // .setRequireExpirationTime()   // the JWT must have an expiration time
      .setAllowedClockSkewInSeconds(1) // allow some leeway in validating time
                                       // based claims to account for clock skew
      .setExpectedIssuer(jwtIssuer)    // whom the JWT needs to have been issued by
      //.setExpectedAudience(String.valueOf(dto.getIdUsuario())) // to whom the JWT is intended for
      //.setExpectedSubject(environmentID + "::" + dto.getEntidadeDestino() + "::" + applicationCaID)
      .setExpectedSubject(environmentID + "::" + applicationCaID)
      .setVerificationKey(rsaJsonWebKey.getKey()) // verify the signature with the public key
      .build(); // create the JwtConsumer instance

      try {
        jwtClaims = jwtConsumer.processToClaims(jwt);

        Claims claims = new Claims();

        claims.setAppId(Long.parseLong(applicationCaID));
        
        //
        //claims.setLoggetUser(JacksonUtil.fromJson((String) jwtClaims.getClaimValue("user"), ContextRequestInfo.class));

        claims.setAudience(jwtClaims.getAudience());
        claims.setIssuer(jwtClaims.getIssuer());
        claims.setJwtId(jwtClaims.getJwtId());
        claims.setSubject(jwtClaims.getSubject());

        if (jwtClaims.getExpirationTime() != null) {
          claims.setExpirationTime(new Date(jwtClaims.getExpirationTime().getValueInMillis()));
        }
        if (jwtClaims.getIssuedAt() != null) {
          claims.setIssuedAt(new Date(jwtClaims.getIssuedAt().getValueInMillis()));
        }
        if (jwtClaims.getNotBefore() != null) {
          claims.setNotBefore(new Date(jwtClaims.getNotBefore().getValueInMillis()));
        }
        
        
        //Get authenticatedUser
        String authenticatedUserJson = (String) jwtClaims.getClaimValue(SecurityConstants.AUTHENTICATED_USER);
        
        if(authenticatedUserJson != null) {
          AuthenticatedUser authenticatedUser = JacksonJsonUtils.fromJson(authenticatedUserJson, AuthenticatedUser.class);
          
          //Get Session Cookie Value
          authenticatedUser.setSessionCookie((String) jwtClaims.getClaimValue(SecurityConstants.SESSION_COOKIE));
          
          claims.setAuthenticatedUser(authenticatedUser);
        }
        
        
        if(jwtClaims.getClaimsMap() != null) {
          
          if(jwtClaims.getClaimsMap().containsKey(CA_APPLICATION_ID_CLAIM)) {
            claims.setAppId(Long.parseLong((String) jwtClaims.getClaimValue(CA_APPLICATION_ID_CLAIM)));
          }
          
          if(jwtClaims.getClaimsMap().containsKey(CA_USER_ID_CLAIM)) {
            claims.setUserId((Long) jwtClaims.getClaimValue(CA_USER_ID_CLAIM));
          }
          
        }
        
        claims.setTokenType(TokenType.valueOf((String) jwtClaims.getClaimValue(TOKEN_TYPE_CLAIM)));
        
        claims.setExtraClaims(jwtClaims.getClaimsMap());
        
        
        
        //TODO VAlidacoes de acordo com o TIPO de tokem
        if(claims.getTokenType().equals(TokenType.SESSION) || claims.getTokenType().equals(TokenType.TIMED)) {
          // ????
        }
        
        

        return claims;
      } catch (InvalidJwtException e) {
        
        String msg = e.getMessage();
        
        //TODO Melhorar isso com o uso de Regex
        if(msg.contains("Additional details: [Subject (sub) claim value (" )) {
          
          if(environmentID.equals(PropertiesUtils.ENVIRONMENT_ID_DESENVOLVER_VALUE)) {
            
            if(!msg.contains("\"environment.id\":\""+PropertiesUtils.ENVIRONMENT_ID_DESENVOLVER_VALUE+"\"")) {
              throw new SecurityException(SecurityConstants.SecurityErrors.SECURITY_022, PropertiesUtils.ENVIRONMENT_ID_DESENVOLVER_DESC);
            }
            
          } else if(environmentID.equals(PropertiesUtils.ENVIRONMENT_ID_HOMOLOGAR_VALUE)) {
            
            if(!msg.contains("\"environment.id\":\""+PropertiesUtils.ENVIRONMENT_ID_HOMOLOGAR_VALUE+"\"")) {
              throw new SecurityException(SecurityConstants.SecurityErrors.SECURITY_022, PropertiesUtils.ENVIRONMENT_ID_HOMOLOGAR_DESC);
            }
            
          } else if(environmentID.equals(PropertiesUtils.ENVIRONMENT_ID_TREINAR_VALUE)) {
            
            if(!msg.contains("\"environment.id\":\""+PropertiesUtils.ENVIRONMENT_ID_TREINAR_VALUE+"\"")) {
              throw new SecurityException(SecurityConstants.SecurityErrors.SECURITY_022, PropertiesUtils.ENVIRONMENT_ID_TREINAR_DESC);
            }
            
          } else if(environmentID.equals(PropertiesUtils.ENVIRONMENT_ID_PRODUCAO_VALUE)) {
            
            if(!msg.contains("\"environment.id\":\""+PropertiesUtils.ENVIRONMENT_ID_PRODUCAO_VALUE+"\"")) {
              throw new SecurityException(SecurityConstants.SecurityErrors.SECURITY_022, PropertiesUtils.ENVIRONMENT_ID_PRODUCAO_DESC);
            }
            
          }
          
        }
        
        
        throw new SecurityException(SecurityConstants.SecurityErrors.SECURITY_018);
      } catch (MalformedClaimException e) {
        throw new SecurityException(SecurityConstants.SecurityErrors.SECURITY_019);
      } catch (Exception e) {
        throw new SecurityException(SecurityConstants.SecurityErrors.SECURITY_020);
      }
    } else {
      throw new SecurityException(SecurityConstants.SecurityErrors.SECURITY_021);
    }
  }
  
  
  private void createTokenValidations(CreateTokenDto dto) throws BusinessException {
    
    /* **  Requided ** */
    if(dto.getAuthenticatedUser() == null) {
      throw new SecurityException(SecurityConstants.SecurityErrors.SECURITY_030);
    }
    
    if(dto.getTokenType() == null) {
      throw new SecurityException(SecurityConstants.SecurityErrors.SECURITY_031);
    }
    
  }
  
  
  private String checkTokenHeader(String token) throws BusinessException {

    
    if(token == null) {
      throw new NotAuthenticatedException(SecurityConstants.SecurityErrors.SECURITY_016);
    } else {
      
      if (token.startsWith("Bearer ")) {
        try {
          token = token.substring(7); // The part after "Bearer "
        } catch (Exception e) {
          throw new NotAuthenticatedException(SecurityConstants.SecurityErrors.SECURITY_017);
        }
      }
      
    }

    return token;
  }
  
  //TODO Providenciar cache para esses valores. Dessa forma esta consultando o FileSystem e o DataBase a cada requisição para buscar dados que não se alteram. 
  private void configTokenValidations() throws BusinessException {
    
    /* **  Requided Configurations** */
    
    if(jwtKeyType == null) {
      jwtKeyType = PropertiesUtils.getParameter(PropertiesUtils.JWT_KEY_TYPE);
      if(StringUtils.isBlank(jwtKeyType)) {
        throw new SecurityException("O tipo de chave não foi definido. Favor defina a chave \"jwt.key.type\" no seu arquivo \"blackdog.properties\" com os valore RSA ou HMAC. ");
      }
    }
    
    if(applicationCaID == null) {
      applicationCaID = PropertiesUtils.getParameter(PropertiesUtils.CA_APPLICATION_ID);
      if(StringUtils.isBlank(applicationCaID)) {
        throw new SecurityException("A identificação da aplicação no controle de acesso não foi definida. Favor defina a chave \"ca.application.id\" no seu arquivo \"blackdog.properties\". ");
      }
    }
    
    if(environmentID == null) {
      //TODO Não deve vir do Property, mas sim do banco.
      //environmentID = PropertiesUtils.getParameter(PropertiesUtils.ENVIRONMENT_ID);
      
      //environmentID = getConfigurationDAO().find(PropertiesUtils.ENVIRONMENT_ID);
      environmentID = applicationConfig.getString(PropertiesUtils.ENVIRONMENT_ID);
      
      if(StringUtils.isBlank(environmentID)) {
        throw new SecurityException("A identificação do tipo de ambiente não foi definida. Favor defina a chave \"environment.id\" no seu arquivo \"blackdog.properties\". ");
      }
      environmentID = environmentID.toUpperCase();
    }

    
    if(jwtIssuer == null) {
      jwtIssuer = PropertiesUtils.getParameter(PropertiesUtils.JWT_ISSUER);
      if(StringUtils.isBlank(jwtIssuer)) {
        throw new SecurityException("A identificação de assinador do projeto não foi definida. Favor defina a chave \"jwt.issuer\" no seu arquivo \"blackdog.properties\". ");
      }
    }
    
    
    /*if(jwtSubjectId == null) {
      jwtSubjectId = PropertiesUtils.getParameter(PropertiesUtils.JWT_SUBJECT);
      if(StringUtils.isBlank(jwtSubjectId)) {
        throw new SecurityException("A identificação do tipo de ambiente não foi definida. Favor defina a chave \"jwt.subject\" no seu arquivo \"blackdog.properties\". ");
      }
    }*/
    
    if(jwtKeyType.equals("HMAC")) {
      
      if(jwtHmacKey == null) {
        jwtHmacKey = PropertiesUtils.getParameter(PropertiesUtils.JWT_HMAC_KEY);
        if(StringUtils.isBlank(jwtHmacKey)) {
          throw new SecurityException("A chave de assinaturas do projeto não foi definida. Favor defina a chave \"jwt.jwtHmacKey\" no seu arquivo \"blackdog.properties\". ");
        }
      }
      
    } else 
    if(jwtKeyType.equals("RSA")) {
      
      if(jwtRsaKeyConfig == null) {
        jwtRsaKeyConfig = PropertiesUtils.getParameter(PropertiesUtils.JWT_RSA_KEY);
        if(StringUtils.isBlank(jwtRsaKeyConfig)) {
          
          try {
            RsaJsonWebKey chave = RsaJwkGenerator.generateJwk(2048);
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "A chave de assinaturas RSA \"jwt.rsa.key\". Configure a chave abaixo ou outra de sua preferência em \"jwt.rsa.key\" e reinicie a app ");
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "jwt.rsa.key=" + chave.toJson(JsonWebKey.OutputControlLevel.INCLUDE_PRIVATE));
          } catch (JoseException e) {
            throw new SecurityException("A chave de assinaturas RSA do projeto não pode ser criada ");
          }
          
          throw new SecurityException("A chave de assinaturas RSA do projeto não foi definida. Favor defina a chave \"jwt.rsa.key\" no seu arquivo \"blackdog.properties\". (Ver Log) ");
        }
      }
      
    }
    
    
    if(jwkKeyid == null) {
      jwkKeyid = PropertiesUtils.getParameter(PropertiesUtils.JWT_KEY_ID);
      if(StringUtils.isBlank(jwkKeyid)) {
        throw new SecurityException("A chave de acesso ao token do projeto não foi definida. Favor defina a chave \"jwt.key.id\" no seu arquivo \"blackdog.properties\". ");
      }
    }
    
  }
  
  
  /*private ConfigurationDAO getConfigurationDAO() {
    
    try {
      return configurationDaoSource.get();
    } catch (Exception e) {
      e.printStackTrace();
      throw new com.blackdog.framework.exceptions.SystemException("Não foi possível consultar a base de configurações. Considere criar um @Producer para o \"ConfigurationDAO\". ");
    }
    
  }*/
  
}
