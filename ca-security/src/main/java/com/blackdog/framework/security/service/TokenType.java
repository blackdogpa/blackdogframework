package com.blackdog.framework.security.service;

public enum TokenType {

  SESSION, TIMED, PERMANENT, CROSSENV
  
}
