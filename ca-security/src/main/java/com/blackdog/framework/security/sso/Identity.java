package com.blackdog.framework.security.sso;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.BindingProvider;

import org.apache.commons.lang3.StringUtils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.blackdog.framework.configurations.ApplicationConfig;
import com.blackdog.framework.core.dto.RequestInfo;
import com.blackdog.framework.exceptions.BusinessException;
import com.blackdog.framework.exceptions.NotAuthenticatedException;
import com.blackdog.framework.exceptions.SecurityException;
import com.blackdog.framework.security.constants.SecurityConstants;
import com.blackdog.framework.security.constants.SecurityConstants.SecurityErrors;
import com.blackdog.framework.security.controleacesso.dao.SecurityDAO;
import com.blackdog.framework.security.controleacesso.wsclient.ControleAcessoFacadeBeanWSService;
import com.blackdog.framework.security.controleacesso.wsclient.ControleAcessoFacadeWS;
import com.blackdog.framework.security.controleacesso.wsclient.ControleAcessoWSException;
import com.blackdog.framework.security.controleacesso.wsclient.PerfilDtoWS;
import com.blackdog.framework.security.controleacesso.wsclient.UsuarioComplementoDtoWS;
import com.blackdog.framework.security.controleacesso.wsclient.UsuarioDtoWS;
import com.blackdog.framework.security.dao.UserTokenDao;
import com.blackdog.framework.security.dto.AuthenticatedUser;
import com.blackdog.framework.security.dto.CreateTokenDto;
import com.blackdog.framework.security.dto.OrgaoDto;
import com.blackdog.framework.security.dto.UserDetails;
import com.blackdog.framework.security.service.Claims;
import com.blackdog.framework.security.service.JWTServiceJose;
import com.blackdog.framework.security.service.TokenType;
import com.blackdog.framework.utils.uuid.UUIDUtils;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com)
 *
 */
@Named("identity")
@RequestScoped
public class Identity implements Serializable {

  private static final long serialVersionUID = 1L;
  
  @Inject
  private RequestInfo requestInfo;

  @Inject //@Configurations
  private ApplicationConfig applicationConfig;

  @Inject
  private JWTServiceJose jwtService;
  
  @Inject
  private SecurityDAO securityDAO;
  
  @Inject 
  private Instance<UserTokenDao> userTokenDaoSource;
  
  /*
   * Identificação da Transação
   * 
   * */
  private String transactionID;
  
  /*
   * Informações do Usuario da Requisição
   * 
   * */
  private AuthenticatedUser authenticatedUser;
  private UserDetails userDetails;
  private JWT jwt;
  private Claims jwtClaims;
  
  
  public void login() throws BusinessException {
    login(false);
  }
  
  public void login(Boolean loadCACredentials) throws BusinessException {
    
    //Identificador da Transação
    this.transactionID = requestInfo.getTransactionID();
    if(this.transactionID == null) {
      this.transactionID = UUIDUtils.generateId();//UUID.randomUUID().toString();
    }
    
    if(requestInfo.getToken() == null) {
      
      if(requestInfo.getGovernoDigitalCookie() == null) {
        throw new NotAuthenticatedException(SecurityErrors.SECURITY_004);
      }
      
      //login o CA com o COOKIE e produção do Token
      controleAcessoLogin();
      
    } else {

      //Validar JWT
      verifyJWTToken(requestInfo.getToken());

      if(jwtClaims.getTokenType() != null && jwtClaims.getTokenType().equals(TokenType.SESSION)) {
        
        if(requestInfo.getGovernoDigitalCookie() == null) {
          throw new NotAuthenticatedException(SecurityErrors.SECURITY_005);
        }
         
        if(requestInfo.getGovernoDigitalCookie().equals(authenticatedUser.getSessionCookie())) {
          //Atualizar sessao do usuario
          controleAcessoAtualizarAcesso();
        } else {
          //O cookie mudou. É outro usuário
          controleAcessoLogin();
        }
      }
    }
    
    if(authenticatedUser == null) {
      throw new SecurityException(SecurityErrors.SECURITY_006); 
    }
    
    //TODO SEMPRE PRECISA ATUALIZAR SESSAO
    
      
    /*if (!isLoggedIn() && applicationConfig != null && authorizationToken != null) {

      try {
        usuarioDtoWs = getSSOFacadeRemote().autorizar(applicationConfig.getCaApplicationID(), authorizationToken);
        
        if(usuarioDtoWs == null) {
          throw new NotAuthenticatedException("Usuário não autenticado.");
        }
      } catch (com.blackdog.framework.security.controleacesso.wsclient.ControleAcessoWSException e) {
        throw new SecurityException(e, "Falha na Autenticação do usuário no CA: " + e.getMessage());
      } catch (Exception e) {
        throw new SecurityException(e, "Falha na Autenticação do usuário no CA: " + e.getMessage());
      }
      
      OrgaoDto orgaoUsuario = new OrgaoDto(usuarioDtoWs.getIdOrgao(), "", usuarioDtoWs.getSiglaOrgao(), usuarioDtoWs.getSiglaOrgao()); // orgaoDao.select(usuarioAutenticadoDto.getIdOrgao());
      
      authenticatedUser = new AuthenticatedUser();
      authenticatedUser.setId(usuarioDtoWs.getId());
      authenticatedUser.setLogin(usuarioDtoWs.getLogin());
      authenticatedUser.setName(usuarioDtoWs.getNome());
      authenticatedUser.setOrgao(orgaoUsuario);
      
      userDetails = new UserDetails();
      
      userDetails.setActivated(true); //TODO
      
      String GDToken = GDSSOUtils.getSessionCookie(httpRequest);
      
      caService.findNaturalId(authenticatedUser, GDToken);
      
      //authenticatedUser.setNaturalUserId(naturalUserID);
      
      if(loadCACredentials) {

        loadCARoles(usuarioDtoWs);
        
      }
    }*/
    
    //if (!isLoggedIn() && applicationConfig != null && requestInfo.getToken() != null) {
      
      
      //Long idUsuario = jwt.getClaim("");
      //securityDAO.loadUser(idUsuario );
      
      
    //}
    
    
  }
  
  private void controleAcessoLogin() throws BusinessException {

    if (!isLoggedIn() && applicationConfig != null && requestInfo.getGovernoDigitalCookie() != null) {

      UsuarioDtoWS usuarioDtoWs;
      try {
        usuarioDtoWs = getSSOFacadeRemote().autorizar(applicationConfig.getCaApplicationID(), requestInfo.getGovernoDigitalCookie());

        if (usuarioDtoWs == null) {
          throw new NotAuthenticatedException("Usuário não autenticado.");
        }
      } catch (com.blackdog.framework.security.controleacesso.wsclient.ControleAcessoWSException e) {
        throw new SecurityException(e, SecurityErrors.SECURITY_007, e.getMessage());
      } catch (Exception e) {
        throw new SecurityException(e, SecurityErrors.SECURITY_007, e.getMessage());
      }

      OrgaoDto orgaoUsuario = new OrgaoDto(usuarioDtoWs.getIdOrgao(), "", usuarioDtoWs.getSiglaOrgao(), usuarioDtoWs.getSiglaOrgao()); // orgaoDao.select(usuarioAutenticadoDto.getIdOrgao());

      authenticatedUser = new AuthenticatedUser();
      authenticatedUser.setId(usuarioDtoWs.getId());
      authenticatedUser.setLogin(usuarioDtoWs.getLogin());
      
      String nome = usuarioDtoWs.getNome();
      if(nome.indexOf(" ") < 0) {
        
        authenticatedUser.setFirstName(nome);
        
      } else {
        
        authenticatedUser.setFirstName(nome.substring(0, nome.indexOf(" ")));
        authenticatedUser.setLastName(nome.substring(nome.indexOf(" ")).trim());
        
      }
      
      authenticatedUser.setOrgao(orgaoUsuario);

      userDetails = new UserDetails();

      userDetails.setActivated(true); // TODO

      loadCARoles(usuarioDtoWs);

      // TODO Criar o token ? E se ja tiver o token ?
      
      if(requestInfo.getToken() == null) {

        CreateTokenDto createTokenDto = new CreateTokenDto();
        
        createTokenDto.setAuthenticatedUser(authenticatedUser);
        createTokenDto.setTokenType(TokenType.SESSION);
        //createTokenDto.setIdUsuario(authenticatedUser.getId());
        //createTokenDto.setEntidadeDestino(String.valueOf(authenticatedUser.getOrgao().getId())); //TODO Acredito que não seja a melhor opção apenas o ID. Pode ser ID+SIGLA
        
        
        Calendar expiresAt = Calendar.getInstance();
        expiresAt.add(Calendar.DAY_OF_MONTH, 1); 
        //expiresAt.add(Calendar.MINUTE, 5);
        createTokenDto.setExpiresAt(expiresAt.getTime());
        
        //createTokenDto.addExtraClaims("authenticatedUser", JsonUtils.toJson(authenticatedUser));
        createTokenDto.addExtraClaims(SecurityConstants.AUTHENTICATED_USER, authenticatedUser);
        createTokenDto.addExtraClaims(SecurityConstants.SESSION_COOKIE, requestInfo.getGovernoDigitalCookie());
        
        
        String jwt = jwtService.createToken(createTokenDto);
        
        requestInfo.setToken(jwt);
      }
      

    }

  }
  
  private void controleAcessoAtualizarAcesso() throws BusinessException {

    if (!isLoggedIn() && applicationConfig != null && requestInfo.getGovernoDigitalCookie() != null) {

      try {
        Boolean ok = getSSOFacadeRemote().atualizarAcesso(applicationConfig.getCaApplicationID(), authenticatedUser.getLogin(),  requestInfo.getGovernoDigitalCookie());

        if (!ok) {
          throw new NotAuthenticatedException(SecurityErrors.SECURITY_009);
        }
      } catch (com.blackdog.framework.security.controleacesso.wsclient.ControleAcessoWSException e) {
        throw new SecurityException(e, SecurityErrors.SECURITY_008, e.getMessage());
      } catch (Exception e) {
        throw new SecurityException(e, SecurityErrors.SECURITY_008, e.getMessage());
      }

    }

  }
  
  private void loadCARoles(UsuarioDtoWS usuarioDtoWs) {
    Set<String> perfis = new HashSet<>();
    for(PerfilDtoWS p :usuarioDtoWs.getPerfis()) {
      perfis.add(p.getCodigo());
    }
    userDetails.setRoles(perfis);
    
    //Oeracoes de Acesso do CA
    Set<String> operations = new HashSet<>();
    for(String op : usuarioDtoWs.getOperacoes()) {
      operations.add(op);
    }
    userDetails.setGrants(operations);
    
    //TODO GAMBI para integracao com o JHipster
    userDetails.getRoles().add("ROLE_USER");
    userDetails.getRoles().add("ROLE_ADMIN");
  }
  
  public boolean isLoggedIn() {
    return authenticatedUser != null && authenticatedUser.getId() != null;
  }

  /*public void hasCaRole(String role) {
	  if(!getUserDetails().getGrants().contains(role)) {
	    throw new SecurityException("O Usuário não possui essa autorização no ControleAcesso.");
	  }
  }*/
  
  public boolean accessUpdate() {
    try {
      if (!isLoggedIn()) {
        login();
      }

      if (applicationConfig != null && isLoggedIn()) {
        //return getSSOFacadeRemote().atualizarAcesso(applicationConfig.getCaApplicationID(), getAuthenticatedUser().getLogin(), requestInfo.getToken());
        return getSSOFacadeRemote().atualizarAcesso(applicationConfig.getCaApplicationID(), getAuthenticatedUser().getLogin(), requestInfo.getGovernoDigitalCookie());
      }
      return false;
    } catch (Exception e) {
      e.printStackTrace();
      return false;
    }
  }
  
  public UsuarioComplementoDtoWS userDatails() throws Exception {
    if (!isLoggedIn()) {
      return null;
    }
    
    /*if(complementoUsuarioWs == null) {
      complementoUsuarioWs = new UsuarioComplementoDtoWS();
      complementoUsuarioWs.setIdUsuario(getAuthenticatedUser().getId());
      
      complementoUsuarioWs = getSSOFacadeRemote().buscarDadosComplementaresUsuario(complementoUsuarioWs);
    }

    return complementoUsuarioWs;*/
    return null;
  }
  
  public UserDetails getUserDetails()  {

    try {

      if(userDetails != null && authenticatedUser.getDetails() == null) {
        
        UsuarioComplementoDtoWS usuarioDtoWsComplemento = new UsuarioComplementoDtoWS();
        usuarioDtoWsComplemento.setIdUsuario(authenticatedUser.getId());
        usuarioDtoWsComplemento = getSSOFacadeRemote().buscarDadosComplementaresUsuario(usuarioDtoWsComplemento);
        
        //loadCARoles(usuarioDtoWs);
        
        if(usuarioDtoWsComplemento != null) {
          userDetails.setImage(usuarioDtoWsComplemento.getImagem());
        }
        
        userDetails.setLangKey("pt-BR");
        
      }
    } catch (ControleAcessoWSException e) {
      e.printStackTrace();
      throw new SecurityException(SecurityErrors.SECURITY_010, e.getMessage());
    }
    
    return userDetails;
  }

  /**
   * Sair do sistema.
   */
  public void logout(HttpServletRequest request, HttpServletResponse response) {
    SSOConfig.print("[CLIENT] [efetuarLogout]");
    
    if (isLoggedIn()) {
      String sessionCookie = SSOConfig.getSessionCookie(request);
      if (sessionCookie != null) {
        try {
          getSSOFacadeRemote().encerrarSessao(sessionCookie);
        } catch (Exception e) {
          e.printStackTrace();
        }
        SSOConfig.deleteSessionCookie(request, response);
      }
      this.authenticatedUser = null;
      request.getSession().invalidate();
    }
  }

  public boolean hasRoleInCache(String role) {
    if (userDetails.getRoles().contains(role) || userDetails.getGrants().contains(role)) {
      return true;
    }
    return false;
  }
  
  
  public boolean hasRole(String[] role) {
    //TODO Isso devera utilizar um metodo espscifico de securityDAO.hasRole() para executar somente uma querie
    for (String r : role) {
      if(!hasRole(r)) {
        return false;
      }
    }
    
    return true;
  }
  
  public boolean hasAnyRole(String[] role) {
    
    //TODO Isso devera utilizar um metodo espscifico de securityDAO.hasRole() para executar somente uma querie
    for (String r : role) {
      if(hasRole(r)) {
        return true;
      }
    }
    
    return false;
  }

  public boolean hasRole(String role) {
    
    if(authenticatedUser != null) {
      return securityDAO.hasRole(authenticatedUser, role);
    } else {
      return false;  
    }
    
  }
  

  public void verifyJWTToken(String token) {
    try {
      
      if(StringUtils.isBlank(token)) {
        
        //TODO NESTE CASO SERA NULL PARA ?????
        
        throw new SecurityException(SecurityErrors.SECURITY_011);
      }
      
      
      jwtClaims = jwtService.verifyToken(token);

      UserTokenDao userTokenDao;
      try {
        userTokenDao = userTokenDaoSource.get();
      } catch (Exception e) {
        e.printStackTrace();
        throw new com.blackdog.framework.exceptions.SystemException(SecurityErrors.SECURITY_012);
      }
      
      if(!userTokenDao.isValid(token)) {
        throw new SecurityException(SecurityErrors.SECURITY_013);
      }
      
      authenticatedUser = jwtClaims.getAuthenticatedUser();
      
      //Assumindo que senha HMAC256. Depois isso será melhorado
      //JWTVerifier verifier = JWT.require(Algorithm.HMAC256(applicationConfig.getJwtHmacKey())).build();
        
      //jwt = (JWT) verifier.verify(token);
      
      //authenticatedUser = (AuthenticatedUser) jwt.getClaim("authenticatedUser");
      
    } catch (JWTVerificationException e){
      throw new SecurityException(e, SecurityErrors.SECURITY_014, e.getMessage());
    } catch (Exception e) {
      throw new SecurityException(e, SecurityErrors.SECURITY_015, e.getMessage());
    }
  }
  
  
  /*private String getAuthorizationToken() {

    String authToken = httpRequest.getHeader("Authorization");
    
    if (authToken != null) {

      if (!authToken.startsWith("Basic ")) {
        throw new NotAuthenticatedException("O Authorization header não foi localizado ou é inválido. (CD001)");
      }

      try {
        authToken = authToken.substring(6); // The part after "Basic "
      } catch (Exception e) {
        throw new NotAuthenticatedException("O Authorization header não foi localizado ou é inválido. (CD002)");
      }

    } else {
      // Verificar se o cliente enviou o KSESSIONID do CA via cookei
      authToken = GDSSOUtils.getSessionCookie(httpRequest);
    }

    return authToken;
  }*/

  
  /**
   * 
   * TODO Trocar para REST
   */
  private ControleAcessoFacadeWS getSSOFacadeRemote() {
    try {
      ControleAcessoFacadeBeanWSService servico = new ControleAcessoFacadeBeanWSService();
      ControleAcessoFacadeWS port = servico.getControleAcessoFacadeBeanWSPort();
      // O Usuario deve vir do formulario ou de um Identity
      ((BindingProvider) port).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, applicationConfig.getCaWsUser());
      ((BindingProvider) port).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, applicationConfig.getCaWsPassword());
      ((BindingProvider) port).getRequestContext().put("set-jaxb-validation-event-handler", "false");
      return port;
    } catch (Throwable e) {
      e.printStackTrace();
    }
    return null;
  }
  

  public void setUserDetails(UserDetails userDetails) {
    this.userDetails = userDetails;
  }
  
  public AuthenticatedUser getAuthenticatedUser() {
    return authenticatedUser;
  }

  public void setAuthenticatedUser(AuthenticatedUser authenticatedUser) {
    this.authenticatedUser = authenticatedUser;
  }

  public String getTransactionID() {
    return transactionID;
  }

  public void setTransactionID(String transactionID) {
    this.transactionID = transactionID;
  }

  public JWT getJwt() {
    return jwt;
  }

  public void setJwt(JWT jwt) {
    this.jwt = jwt;
  }


}