package com.blackdog.framework.security.sso;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.blackdog.framework.utils.security.HttpUtil;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com)
 *
 */
public class SSOConfig {

	public static final boolean DEBUG = false;

	public static final String FACADE_REMOTE = "jnp://controleacesso-srv:1099/controleacesso/ControleAcessoSSOFacadeBean/remote";

	public static final String CHECK_XHTML = "/check.xhtml";

	// Client

	public static final String CLIENT_REDIRECT_PARAMETER = "redirect";
	public static final String CLIENT_APPLICATION_ID_PARAMETER = "sso.APPLICATION_ID";
	public static final String CLIENT_MODULE_ID_PARAMETER = "sso.MODULE_ID";
	public static final String CLIENT_MAIN_XHTML_PARAMETER = "sso.MAIN_XHTML";
	
	public static final String CLIENT_LOGIN_WS_PARAMETER = "sso.LOGIN_WS";
	public static final String CLIENT_PASSWORD_WS_PARAMETER = "sso.PASSWORD_WS";

	// Server

	public static final String SERVER_CONTEXT = "/governodigital";
	public static final String SERVER_CHECK_SEAM = SERVER_CONTEXT + CHECK_XHTML;

	// Cookie

	public static final String COOKIE_PATH = "/";
	public static final String SESSION_COOKIE = "KSESSIONID";
	public static final String JSESSIONID = "JSESSIONID";

	// ///////////
	// Cookies //
	// ///////////

	/**
	 * Retorna o Cookie de Sessao.
	 */
	public static String getSessionCookie(HttpServletRequest request) {
		print("[SSOConfig] [getSessionCookie]");
		return HttpUtil.getCookie(request, SESSION_COOKIE);
	}

	/**
	 * Apaga Cookie de Sessao.
	 */
	public static void deleteSessionCookie(HttpServletRequest request, HttpServletResponse response) {
		print("[SSOConfig] [deleteSessionCookie]");
		print("[serverName][" + request.getServerName() + "]", 1);
		String serverName = request.getServerName();
		if (serverName.startsWith("dev.")) {
			serverName = serverName.substring(4);
			print("[serverName][changed][" + serverName + "]", 1);
		}
		HttpUtil.setCookie(response, SESSION_COOKIE, "", serverName, COOKIE_PATH, 0);
		HttpUtil.setCookie(response, JSESSIONID, "", serverName, COOKIE_PATH, 0); //TEST
	}

	// /////////////
	// Depuracao //
	// /////////////

	public static void print(String text) {
		if (SSOConfig.DEBUG) {
			print(text, 0);
		}
	}

	public static void print(String text, int ident) {
		if (SSOConfig.DEBUG) {
			StringBuffer space = new StringBuffer("");
			while (ident > 0) {
				space.append("    ");
				ident--;
			}
		}
	}
}