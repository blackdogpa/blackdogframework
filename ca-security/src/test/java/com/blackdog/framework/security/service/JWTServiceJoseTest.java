package com.blackdog.framework.security.service;

import static org.junit.Assert.*;

import org.junit.Test;

import com.blackdog.framework.exceptions.BusinessException;
import com.blackdog.framework.security.dto.AuthenticatedUser;
import com.blackdog.framework.security.dto.CreateTokenDto;

public class JWTServiceJoseTest {

  @Test
  public void testGetRsakey() {
    try {
      JWTServiceJose tks = new JWTServiceJose();
      
      CreateTokenDto dto = new CreateTokenDto();
      
      //dto.setIdUsuario(1000L);
      //dto.setEntidadeDestino("ORGAO_TESTE");
      
      AuthenticatedUser au = new AuthenticatedUser();
      au.setId(1L);
      dto.setAuthenticatedUser(au );
      
      
      String tk = tks.createToken(dto);
      System.out.println(tk);
      
    } catch (Exception e) {
      e.printStackTrace();
    }
    
  }

  @Test
  public void testCreateToken() {
    fail("Not yet implemented");
  }
  
  @Test
  public void testValidateTokenRSA() throws BusinessException {
    
      String jwt = "eyJraWQiOiJQUk9ERVBBREVWMjAxNiIsImFsZyI6IlJTMjU2In0.eyJqdGkiOiJmY2NhOWM0OC04ZDkyLTQ4MDEtODczNC1lOTRmN2IxNmRlYTUiLCJpc3MiOiJodHRwOi8vcHJvZGVwYS5wYS5nb3YuYnIvdGVzdGUiLCJpYXQiOjE0ODg5ODE2NTgsIm5iZiI6MTQ4ODk4MTY1OCwic3ViIjoiREVTRU5WT0xWRVI6OjExMSIsImNhLnVzZXIuaWQiOjEwMDAsImNhLmFwcGxpY2F0aW9uLmlkIjoiMTExIiwiZW52aXJvbm1lbnQuaWQiOiJERVNFTlZPTFZFUiJ9.fTt1fuz6Gv40lL5UX3UQFDrR6vcbvC-hoeDtVN4ocYzpxug_uJJ-NRiAGyiZ5mlv2CYMqED2Iuu4UKh6tBaRK5gy7N8uvnoVUd8hEf2pX-bh3LLcGSxcQ86Ml3vdiofMR1sXn4dn1013Sl_DDqEtYCOLAdS1O2M6yp45w4x7-Ev9fbK0YuZRpk-n1pL7YtEDv6NTd8bnJDwumu33afljAcFTi_NLPp6g4DMjdYMoLxsYG1Z7zyOIlMhhXFQzA6yV4SEeVMOwKEXSKn2q6DkYZpuf_7TiDZDA6LLtrW3CAWTGZqHmGDYT-5lHFoeX1pkAyi9b09pZY0oA5oISJi-WmA";
      
     
      JWTServiceJose tks = new JWTServiceJose();
      
      
      Claims claims = tks.verifyToken(jwt);
      
      System.out.println(claims);
      System.out.println(claims.getAppId());
      System.out.println(claims.getUserId());
      System.out.println(claims.getExtraClaims());
    
  }

}
