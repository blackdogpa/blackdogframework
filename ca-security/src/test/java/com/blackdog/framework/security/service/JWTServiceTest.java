package com.blackdog.framework.security.service;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

import com.blackdog.framework.exceptions.BusinessException;
import com.blackdog.framework.security.dto.CreateTokenDto;

public class JWTServiceTest {

  @Test
  public void testCreateToken() throws BusinessException {
    

    CreateTokenDto dto = new CreateTokenDto();
   //dto.setIdUsuario(1L);
    Calendar exp =  Calendar.getInstance();
    exp.add(Calendar.MINUTE, 1);
    dto.setExpiresAt(exp.getTime());
    
    
    String tk = new JWTServiceJavaJwt().createToken(dto);
    
    System.out.println(tk);
    
  }
  
  
  @Test
  public void testCreateTokens() throws BusinessException {
   
    String t = "Bearer 1DDDDD ZZZZ";
    //String t = "";
    
    
    if(t.indexOf(" ") < 0) {
      System.out.println(t);
    } else {
      
      System.out.println(t.indexOf(" "));

      System.out.println(t.substring(0, t.indexOf(" ")));
      
      System.out.println(t.substring(t.indexOf(" ")).trim());
    }
        
  }
  

}
