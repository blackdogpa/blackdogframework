package com.blackdog.framework.components.service;

import java.util.List;

public abstract class AbstractSearchService extends AbstractService {
  
  public abstract <T> T load(Long id, Class<T> clazz);
  
  public abstract <T> T load(Long id, Class<T> clazz, String[] fields);
  
  public abstract <T> Long countPaginatedResultSearch(T bean);
  
  public abstract <T> List<T> findAll(T bean) throws Exception;
  
  public abstract <T> List<T> findAllPaginated(T bean, Integer first, Integer maxResult);
  
}
