package com.blackdog.framework.components.service;
        

import java.math.BigDecimal;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.blackdog.framework.configurations.ApplicationConfig;
import com.blackdog.framework.configurations.Configurations;
import com.blackdog.framework.exceptions.BusinessException;
import com.blackdog.framework.exceptions.util.BusinessExceptionUtil;
import com.blackdog.framework.messages.MessageContext;
import com.blackdog.framework.security.sso.Identity;

public abstract class AbstractService {
  
  @Inject
  protected Logger log;
  
  @Inject
  protected Identity identity;
  
  @Inject 
  protected MessageContext messages;
  
  protected BusinessExceptionUtil exceptionUtil;
  
  @Inject
  //@Configurations
  protected ApplicationConfig appConfig;

  protected BigDecimal cast(String value) {
    if(StringUtils.isNotBlank(value)) {
      if(StringUtils.isNumeric(value)) {
        return new BigDecimal(value);
      }
    }
    return null;
  }
  
  
  protected BigDecimal castNotNull(String value) throws BusinessException {
    BigDecimal big = cast(value);
    if(big == null) {
      throw new BusinessException("Invalid Null Value");
    } else {
      return big;
    }
  }
  
  
  protected boolean isBlank(String value) {
    return StringUtils.isBlank(value);
  }
}
