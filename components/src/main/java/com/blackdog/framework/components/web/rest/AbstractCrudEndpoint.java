package com.blackdog.framework.components.web.rest;

import com.blackdog.framework.components.service.AbstractCrudService;

public abstract class AbstractCrudEndpoint<S extends AbstractCrudService> extends AbstractSearchEndpoint<S> {

  private static final long serialVersionUID = -6987531652378920860L;

  /**********************
   * Crud methods
   * @throws BusinessException  Exceções de negócio 
   **********************/

  /*@POST
  @Path("/insert")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Response insert(T bean) throws ValidationException {
    try {
      beforeInsert(bean);
      getBusiness().insert(bean);
      Response response = RestMessage.success(getInsertSuccessMessage(bean));
      afterInsert(bean);
      return response;
    } catch (ValidationException e) {
      throw e;
    } catch (Exception e) {
      throw new BusinessException(e);
    }
  }

  @POST
  @Path("/update")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Response update(T bean) throws ValidationException {
    try {
      beforeUpdate(bean);
      getBusiness().update(bean);
      afterUpdate(bean);
      return RestMessage.success(getUpdateSuccessMessage(bean));
    } catch (ValidationException e) {
      throw e;
    } catch (Exception e) {
      throw new BusinessException(e);
    }
  }

  @POST
  @Path("/delete/{id:[0-9][0-9]*}")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Response delete(@PathParam("id") Long id) throws ValidationException {
    try {
      beforeDelete();
      getBusiness().delete(id, getBeanClass());
      afterDelete();
      return RestMessage.success(getDeleteSuccessMessage(null));
    } catch (ValidationException e) {
      throw e;
    } catch (Exception e) {
      throw new BusinessException(e);
    }
  }

  @POST
  @Path("/enable")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Response enable(T bean) throws ValidationException {
    try {
      beforeEnable(bean);
      Desabilitavel d = (Desabilitavel) bean;
      d.setAtivo(true);
      bean = getBusiness().update(bean);
      afterEnable(bean);
      return RestMessage.success(getEnableSuccessMessage(bean));
    } catch (ValidationException e) {
      throw e;
    } catch (Exception e) {
      throw new BusinessException(e);
    }
  }

  @POST
  @Path("/disable")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Response disable(T bean) throws ValidationException {
    try {
      beforeDisable(bean);
      Desabilitavel d = (Desabilitavel) bean;
      d.setAtivo(false);
      bean = getBusiness().update(bean);
      afterDisable(bean);
      return RestMessage.success(getDisableSuccessMessage(bean));
    } catch (ValidationException e) {
      throw e;
    } catch (Exception e) {
      throw new BusinessException(e);
    }
  }*/



  /*************************
   * User Messages Methods
   *************************/

  public <T> String getInsertSuccessMessage(T t) {
    return "Cadastro efetuado com sucesso.";
  }

  public <T> String getUpdateSuccessMessage(T t) {
    return "Alteração efetuada com sucesso.";
  }

  public <T> String getDeleteSuccessMessage(T t) {
    return "Exclusão efetuada com sucesso.";
  }

  public <T> String getEnableSuccessMessage(T t) {
    return "Ativação efetuada com sucesso.";
  }

  public <T> String getDisableSuccessMessage(T t) {
    return "Desativação efetuada com sucesso.";
  }

  /*************************
   * Callback Methods
   *************************/

  public <T> void beforeInsert(T bean) throws Exception {
  }

  public <T> void afterInsert(T bean) throws Exception {
  }

  public <T> void beforeUpdate(T bean) throws Exception {
  }

  public <T> void afterUpdate(T bean) throws Exception {
  }

  public <T> void beforeDelete() throws Exception {
  }

  public <T> void afterDelete() throws Exception {
  }

  public <T> void beforeEnable(T bean) throws Exception {
  }

  public <T> void afterEnable(T bean) throws Exception {
  }

  public <T> void beforeDisable(T bean) throws Exception {
  }

  public <T> void afterDisable(T bean) throws Exception {
  }


}
