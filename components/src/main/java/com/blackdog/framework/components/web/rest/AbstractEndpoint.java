package com.blackdog.framework.components.web.rest;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import com.blackdog.framework.core.dto.RequestInfo;
import com.blackdog.framework.exceptions.NotAuthenticatedException;
import com.blackdog.framework.security.sso.Identity;
import com.blackdog.framework.utils.security.GDSSOUtils;

public abstract class AbstractEndpoint implements Serializable {

  private static final long serialVersionUID = 4382407990224836834L;

  protected static final String APPLICATION_JSON =  MediaType.APPLICATION_JSON + ";charset=UTF-8";
  protected static final String APPLICATION_XML =  MediaType.APPLICATION_XML + ";charset=UTF-8";
  
  protected static final int LIMIT_CUT_LOG = 500;
  
  @Inject
  protected Logger logger;
  
  @Context
  private UriInfo uriInfo;
  
  @Inject
  private Identity identity;
  
  @Inject
  protected RequestInfo requestInfo;
  
  //@LoggedUser
  //@Inject
  //private AuthenticatedUser authenticatedUser;
  
  //private @Context Request request;
  //private @Context HttpHeaders headers;
  private @Context HttpServletRequest httpRequest;
  //private @Context ServletContext servletContext;
  
  
  /**
   * 
   * No Jboss 7 não esta sendo possivel injetar o HttpServletRequest fora do componte de @Path. 
   * Isso é uma solução paliativa.
   * 
   */
  @PostConstruct
  public void posConstruct() {
    
    //Esse comportamento deveria estar em um Intercepor ?
    
    //requestInfo.setTransactionID(UUIDUtils.generateId());
    
    //requestInfo.setToken(getAuthorizationToken());
    //requestInfo.setGovernoDigitalCookie(getGovernoDigitalCookie());
    
    //TODO isso deve ir para o SSOFilter. Por enquanto ficara aki
    requestInfo.setRequestUri(uriInfo.getRequestUri().toString());
    requestInfo.setBasePath(uriInfo.getBaseUri().toString());
    requestInfo.setPath(uriInfo.getPath());
  }
  
  
  public UriInfo getUriInfo() {
    return uriInfo;
  }

  public void setUriInfo(UriInfo uriInfo) {
    this.uriInfo = uriInfo;
  }
  
  private String getAuthorizationToken() {

    String authToken = httpRequest.getHeader("Authorization");
    
    if (authToken != null) {

      if (!authToken.startsWith("Bearer ")) {
        throw new NotAuthenticatedException("O Authorization header não foi localizado ou é inválido. (CD001)");
      }

      try {
        return authToken.substring(7); // The part after "Bearer "
      } catch (Exception e) {
        throw new NotAuthenticatedException("O Authorization header não foi localizado ou é inválido. (CD002)");
      }

    }

    return null;
  }
  
  private String getGovernoDigitalCookie() {
    
    String cookie = GDSSOUtils.getSessionCookie(httpRequest);
    
    if(cookie != null && !cookie.trim().equals("")) {
      return cookie;
    } else {
      return null;
    }
    
  }

  
}
