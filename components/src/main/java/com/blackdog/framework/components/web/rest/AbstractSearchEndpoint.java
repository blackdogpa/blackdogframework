package com.blackdog.framework.components.web.rest;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.blackdog.framework.components.service.AbstractSearchService;
import com.blackdog.framework.exceptions.BusinessException;

public abstract class AbstractSearchEndpoint<S extends AbstractSearchService> extends AbstractSimpleEndpoint<S>{

  private static final long serialVersionUID = -9136194902164208571L;

  /**********************
   * Crud methods
   * @throws BusinessException  Exceções de negócio 
   **********************/
  /*@GET
  @Path("/load/{id:[0-9][0-9]*}")
  @Produces(MediaType.APPLICATION_JSON)
  public <T> T load(@PathParam("id") Long id) throws BusinessException {
    
    try {
      T bean = null;
      if (getFieldsToLoad() != null) {
        bean = getBusiness().load(id, getBeanClass(), getFieldsToLoad());
      } else {
        bean = getBusiness().load(id, getBeanClass());
      }
      afterBeanLoad(bean);
      return bean;
    } catch (Exception e) {
      throw new BusinessException(e);
    }
  }
  

  @POST
  @Path("/search")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Map<String, Object> search(T bean, @QueryParam("first") Integer first, @QueryParam("maxResults") Integer maxResults) throws BusinessException {
    try {
      beforeSearch(bean);
      Map<String, Object> result = new HashMap<String, Object>();
      result.put("count", getBusiness().countPaginatedResultSearch(bean));
      result.put("list", getBusiness().findAllPaginated(bean, first, maxResults));
      afterSearch(bean);
      return result;
    } catch (Exception e) {
      throw new BusinessException(e);
    }
  }*/
  
  //public abstract Map<String, Object> init();
  
  @GET
  @Path("/init")
  @Produces(MediaType.APPLICATION_JSON)
  public Map<String, Object> init() {
    Map<String, Object> result = new HashMap<String, Object>();
  
    /*if(clientController.hasProfile(PerfisCA.ADMINISTRADOR)){
      result.put("orgaosCombo", sessionCache.getOrgaos());
    } else {
      result.put("orgaoSiafem", sessionCache.getOrgaoSiafemUsuarioAtual());
    }*/
    return result;
  }
  
  public abstract <T> T load(@PathParam("id") Long id) throws BusinessException;
  
  public abstract <T> Map<String, Object> search(T bean, @QueryParam("first") Integer first, @QueryParam("maxResults") Integer maxResults) throws BusinessException;


  /*************************
   * Callback Methods
   *************************/

  public <T> void afterBeanLoad(T bean) throws Exception { }
  
  public <T> void beforeBeanLoad(T bean) throws Exception { }


  public <T> void afterSearch(T bean) throws Exception { }

  public <T> void beforeSearch(T bean) throws Exception { }

}
