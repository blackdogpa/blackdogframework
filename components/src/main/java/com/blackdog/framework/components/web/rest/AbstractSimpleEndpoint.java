package com.blackdog.framework.components.web.rest;

import java.lang.reflect.ParameterizedType;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import com.blackdog.framework.components.service.AbstractService;
import com.blackdog.framework.utils.reflection.Beans;
import com.blackdog.framework.utils.reflection.Reflections;

public abstract class AbstractSimpleEndpoint<S extends AbstractService> extends AbstractEndpoint {

  private static final long serialVersionUID = 4382407990224836834L;

  @Inject
  protected Logger logger;

  

  private Object id;

  private Class<S> businessClass;

  @SuppressWarnings("unchecked")
  public AbstractSimpleEndpoint() {
    try {
      businessClass = (Class<S>) ((ParameterizedType) getClass().getSuperclass().getGenericSuperclass()).getActualTypeArguments()[0];
    } catch (Exception e) {
      businessClass = (Class<S>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
  }

  /*************************
   * Getters/Setters
   *************************/
  /*protected Class<T> getBeanClass() {
    if (this.beanClass == null) {
      this.beanClass = Reflections.getGenericTypeArgument(this.getClass(), 0);
    }

    return this.beanClass;
  }*/

  protected Class<S> getBusinessClass() {
    if (this.businessClass == null) {
      this.businessClass = Reflections.getGenericTypeArgument(this.getClass(), 1);
    }

    return this.businessClass;
  }

  public S getBusiness() {
    return Beans.getReference(getBusinessClass());
  }

  public String[] getFieldsToLoad() {
    return null;
  }

  public Object getId() {
    return id;
  }

  public void setId(Object id) {
    this.id = id;
  }

  
  

}
