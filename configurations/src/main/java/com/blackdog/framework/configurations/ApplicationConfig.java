package com.blackdog.framework.configurations;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.blackdog.framework.configurations.dao.ConfigurationDAO;
import com.blackdog.framework.configurations.util.PropertiesUtils;
import com.blackdog.framework.exceptions.BusinessException;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com)
 *
 */
//@ApplicationScoped
public class ApplicationConfig implements Serializable {

	private static final long serialVersionUID = 5912688940876237544L;
	
	private Boolean cached = false;
	
	private Map<String, String> map; 
	
	//@Inject
	private ConfigurationDAO configurationDao;
	
	public ApplicationConfig(Boolean cached, ConfigurationDAO configurationDao) throws BusinessException {
    super();
    
    this.cached = cached;
    this.configurationDao = configurationDao;
    
    if(this.cached) {
      loadApplicationConfig();
    } else {
      loadApplicationConfigResource();
    }
    
  }
	
	public ApplicationConfig(Boolean cached, Map<String, String> map) {
    super();
    this.cached = cached;
    
    if(map != null) {
      this.map = new HashMap<>();
      for(String key : map.keySet()) {
        this.map.put(key.toUpperCase(), map.get(key));
      }
    }

  }

  public ApplicationConfig(Map<String, String> map) throws BusinessException {
	  super();
  	if(map != null) {
  	  this.map = new HashMap<>();
  	  for(String key : map.keySet()) {
  	    this.map.put(key.toUpperCase(), map.get(key));
  	  }
	  }
	}
	

  /* ******************
   *   
   *   GET
   * 
   * ******************
   */
  
  private String get(String key) throws BusinessException {
    if(key != null) {
      
      key = key.toUpperCase().trim();
      
      if(this.cached) {
        
        if(map.containsKey(key)) {
          return (String) map.get(key).trim();
        }
        
      } else {
        
        
        if(map.containsKey(key)) {
          return (String) map.get(key).trim();
        } else {

          String value = loadConfig(key);
          
          if(value != null) {
            value = value.trim();//.toUpperCase();
            //Cache dentro do request
            map.put(key, value);
          }
          
          return value;
        }
      }
    }
    return null;
  }
  
  public String getString(String key) throws BusinessException {
    /*if(key != null) {
      if(map.containsKey(key.toUpperCase())) {
        return map.get(key.toUpperCase()).trim();
      }
    }
    return null;*/
    return get(key);
  }
  
  public Long getLong(String key) throws BusinessException {
    String value = get(key);
    if(value != null && StringUtils.isNumeric(value)) {
      return Long.parseLong(value);
    }
    return null;
  }
  
  public Integer getInteger(String key) throws BusinessException {
    String value = get(key);
    if(value != null && StringUtils.isNumeric(value)) {
      return Integer.parseInt(value);
    }
    return null;
  }
  
  public Boolean getBoolean(String key) throws BusinessException {
    
    String value = get(key);
    if(value != null) {
      if(value.equals("1") || map.get(key.toUpperCase()).trim().toUpperCase().equals("TRUE")) {
        return true;
      } else {
        return false;
      }
    }
    return null;
    
    /*if(key != null) {
      if(map.containsKey(key.toUpperCase())) {
        if(map.get(key.toUpperCase()).trim().equals("1") || map.get(key.toUpperCase()).trim().toUpperCase().equals("TRUE")) {
          return true;
        } else {
          return false;
        }
      }
    }
    return null;*/
  } 
	
  
  
  /**
   * 
   * # >> getPropertie
   * $ >> getEnv
   * 
   * @param key
   * @return
   * @throws BusinessException
   */
  public String getPath(String key) throws BusinessException {
    String value = get(key);
    if(value != null) {
      if(value.contains("#{") & value.contains("}")) {
        
        int i1 = value.indexOf("#{") + 2;
        int i2 = value.indexOf("}");
        
        String envName = value.substring(i1, i2);
        
        String envValue = System.getenv(envName);
        
        String path = envValue + value.substring( i2+1 );
         
        return path;
      }
      
      if(value.contains("${") & value.contains("}")) {
          
        int i1 = value.indexOf("${") + 2;
        int i2 = value.indexOf("}");
        
        String envName = value.substring(i1, i2);
        
        String envValue = System.getProperty(envName);
        
        String path = envValue + value.substring( i2+1 );
        
        return path;
      }
      
    }
    return null;
  }
  
	
	/*
	 * Application
	 */
	
	public String getApplicationName() throws BusinessException {
    return get(PropertiesUtils.APPLICATION_NAME);
  }
  
	public String getApplicationShortName() throws BusinessException {
    return get(PropertiesUtils.APPLICATION_SHORT_NAME);
  }
  
	
	/* ******************
	 *   
	 *   Controle Acesso
	 * 
	 * ******************
	 */
	
	public Long getCaApplicationID() throws BusinessException {
		return getLong(PropertiesUtils.CA_APPLICATION_ID);
	}
	
	public String getCaWsUser() throws BusinessException {
    return get(PropertiesUtils.CA_WS_USER);
  }
	
	public String getCaWsPassword() throws BusinessException {
    return get(PropertiesUtils.CA_WS_PASSWORD);
  }
	
	public Boolean isCacheAuthorizarion() throws BusinessException {
    return getBoolean(PropertiesUtils.AUTHENTICATION_CACHE);
  }

	public String getGdMainPage() throws BusinessException {
    return get(PropertiesUtils.GD_MAIN_PAGE);
	}


  /* ******************
   *   
   *   JWT
   * 
   * ******************
   */
  
  
	public String getJwtKeyType() throws BusinessException {
    return get(PropertiesUtils.JWT_KEY_TYPE);
  }
  
  public String getJwtKeyId() throws BusinessException {
    return get(PropertiesUtils.JWT_KEY_ID);
  }

  public String getJwtRsaKey() throws BusinessException {
    return get(PropertiesUtils.JWT_RSA_KEY);
  }

  public String getJwtHmacType() throws BusinessException {
    return get(PropertiesUtils.JWT_HMAC_TYPE);
  }

  public String getJwtHmacKey() throws BusinessException {
    return get(PropertiesUtils.JWT_HMAC_KEY);
  }
  
  public String getJwtIssuer() throws BusinessException {
    return get(PropertiesUtils.JWT_ISSUER);
  }

  
  /* ******************
   *   
   *   BlackDog Configuration
   * 
   * ******************
   */
  

  public Long getDefaultPageSize() throws BusinessException {
    return getLong(PropertiesUtils.BD_DEFAULT_PAGE_SIZE);
  }
  
  public Long getMaxPageSize() throws BusinessException {
    return getLong(PropertiesUtils.BD_MAX_PAGE_SIZE);
  }

  
  
  /* ******************
   *   
   *  Environment
   * 
   * ******************
   */
  
  public String getEnvironmentId() throws BusinessException {
    return get(PropertiesUtils.ENVIRONMENT_ID);
  }
  
  public Boolean isDesenvolver() throws BusinessException {
    
    String env = get(PropertiesUtils.ENVIRONMENT_ID);
    if(env != null) {
      return env.equals(PropertiesUtils.ENVIRONMENT_ID_DESENVOLVER_VALUE);
    }
      
    return false;
  }
  
  public Boolean isHomologar() throws BusinessException {
    
    String env = get(PropertiesUtils.ENVIRONMENT_ID);
    if(env != null) {
      return env.equals(PropertiesUtils.ENVIRONMENT_ID_HOMOLOGAR_VALUE);
    }
      
    return false;
  }

  public Boolean isTreinar() throws BusinessException {
  
    String env = get(PropertiesUtils.ENVIRONMENT_ID);
    if(env != null) {
      return env.equals(PropertiesUtils.ENVIRONMENT_ID_TREINAR_VALUE);
    }
      
    return false;
  }

  public Boolean isProducao() throws BusinessException {
    
    String env = get(PropertiesUtils.ENVIRONMENT_ID);
    if(env != null) {
      return env.equals(PropertiesUtils.ENVIRONMENT_ID_PRODUCAO_VALUE);
    }
      
    return false;
  }
  
  
  /* ******************
   *   
   *   Application Version
   * 
   * ******************
   */
  public String getBuildVersion() throws BusinessException {
    return get(PropertiesUtils.BUILD_VERSION);
  }
  
  public String getBuildNumber() throws BusinessException {
    return get(PropertiesUtils.BUILD_NUMBER);
  }
  
  
  /*
   *
   * Load All
   * 
   * 
   */
  
  private void loadApplicationConfigResource() throws BusinessException {
    
    Map<String, String> configs = new HashMap<String, String>();
    
    //Load from Property resource config. TODO in the future, the resource config will configurable
    configs.putAll(PropertiesUtils.loadAsMap(PropertiesUtils.CONFIG_RESOURCE));

    this.map = new HashMap<>();
    for(String key : configs.keySet()) {
      this.map.put(key.toUpperCase(), configs.get(key));
    }
    
  }
  
  private void loadApplicationConfig() throws BusinessException {
    
    Map<String, String> configs = new HashMap<String, String>();
    
    //Load from Property resource config. TODO in the future, the resource config will configurable
    configs.putAll(PropertiesUtils.loadAsMap(PropertiesUtils.CONFIG_RESOURCE));
    
    if(configurationDao != null) {

      configs.putAll(configurationDao.load());
      
      this.map = new HashMap<>();
      for(String key : configs.keySet()) {
        this.map.put(key.toUpperCase(), configs.get(key));
      }
      
    }
    
  }
  
  
  private String loadConfig(String key) throws BusinessException {
  
    if(configurationDao != null) {
      return configurationDao.find(key.toUpperCase().trim());
    }
    
    return null;
  }
  

}
