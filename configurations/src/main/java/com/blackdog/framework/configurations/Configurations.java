package com.blackdog.framework.configurations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com)
 *
 */
@Qualifier
@Retention(RUNTIME)
@Target({ METHOD, TYPE, FIELD })
public @interface Configurations {
  
}
