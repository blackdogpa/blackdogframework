package com.blackdog.framework.configurations.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import com.blackdog.framework.exceptions.BusinessException;



/**
 * @author thiago.soares (thiago.soares.jr@gmail.com)
 * 
 * 
 * TODO Não concluido. Essa classe deve buscar as configurações do banco de dados. Mas como definir o EntityManager ? Como definir o schema e o nome da tabela ? Vai ter um @Entity ?
 *
 */
public class ConfigurationDAO  {

  private EntityManager em;
  
  private String schema;
  private String table;
  
  public ConfigurationDAO(EntityManager em, String schema, String table) {
    super();
    this.em = em;
    this.schema = schema;
    this.table = table;
  }
  
  public ConfigurationDAO(EntityManager em, String schema) {
    super();
    this.em = em;
    this.schema = schema;
    this.table = "app_config";
  }
  
  private void test() throws  BusinessException {
    try {
      em.createNativeQuery("SELECT COUNT(1) FROM " +schema+"."+table).getSingleResult();
    } catch (Exception e) {
      throw new BusinessException("Verifique se a tabela \"" + schema+"."+table + "\" foi criada.");
    }
  }
  

  @SuppressWarnings("unchecked")
  public Map<String, String> load() throws  BusinessException {
    
    List<Object[]> result = em.createNativeQuery("SELECT nome, valor FROM " +schema+"."+table+ " ORDER BY nome ").getResultList();
    
    Map<String, String> rows = new HashMap<String, String>();
    for (Object[] row : result) {
      rows.put((String)row[0], (String)row[1]);
    }
    
    return rows;
  }
  
  public Boolean contains(String key) {
    return (Boolean) em.createNativeQuery("select  COUNT(1)>0 from " +schema+"."+table+ " where UPPER(nome) = :key").setParameter("key", prepareKey(key)).getSingleResult();
  }
  
  public String find(String key) throws BusinessException {
    try {
      test();
      return (String) em.createNativeQuery("select valor from " +schema+"."+table+ " where UPPER(nome) = :key").setParameter("key", prepareKey(key)).getSingleResult();
    } catch (NoResultException e) {
      return null;
    }
  }
  
  public String add(String key, String value) throws BusinessException {
    key = prepareKey(key);
    value = prepareValue(value);
    if(!contains(key)) {
      em.createNativeQuery("INSERT INTO " +schema+"."+table+ " (nome,valor) VALUES (:nome, :valor);").setParameter("nome", key).setParameter("valor",value).executeUpdate();
    } else {
      throw new BusinessException("Já existe uma configuração com o nome de " + key);
    }
    
    return value;
  }

  public String update(String key, String value) throws BusinessException {
    key = prepareKey(key);
    value = prepareValue(value);
    if(contains(key)) {
      em.createNativeQuery("update " +schema+"."+table+ " set valor = :valor where nome = :nome ;").setParameter("nome", key).setParameter("valor",value).executeUpdate();
    } else {
      throw new BusinessException("Não existe uma configuração com o nome de " + key);
    }
    return value;
  }

  public void delete(String key) throws BusinessException {
    key = prepareKey(key);
    if(contains(key)) {
      em.createNativeQuery("DELETE FROM " +schema+"."+table+ " WHERE nome = :nome ;").setParameter("nome", key).executeUpdate();
    } else {
      throw new BusinessException("Não existe uma configuração com o nome de " + key);
    }
  }

  private String prepareKey(String key) {
    return key.toUpperCase();
  }
  
  private String prepareValue(String value) {
    
    //TODO Validar formatos
    //
    
    return value;
  }


}
