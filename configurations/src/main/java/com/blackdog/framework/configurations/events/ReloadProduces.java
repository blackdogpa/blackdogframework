package com.blackdog.framework.configurations.events;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

/**
 * TODO Não feito ainda
 * Defini o nome do arquivo property que contem os valores
 * 
 * TODO Seria legal definir o Arquivo e o Prefixo
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com)
 *
 */
@Qualifier
@Retention(RUNTIME)
@Target({ TYPE, FIELD, METHOD, PARAMETER })
public @interface ReloadProduces {
  
}
