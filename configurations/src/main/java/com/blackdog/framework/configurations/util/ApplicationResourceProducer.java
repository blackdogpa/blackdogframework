package com.blackdog.framework.configurations.util;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;

import com.blackdog.framework.configurations.ApplicationConfig;
import com.blackdog.framework.configurations.dao.ConfigurationDAO;
import com.blackdog.framework.configurations.events.ReloadProduces;
import com.blackdog.framework.exceptions.BusinessException;

/**
 * 
 * TODO Como saber que as configurações mudaram ou como forcar uma recarga das configurações ?
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com)
 */
//@ApplicationScoped
@RequestScoped
public class ApplicationResourceProducer {

  //@Inject
  //@Configurations
  //private ApplicationConfig applicationConfig;
  
  @Inject 
  private Instance<ConfigurationDAO> configurationDaoSource;
    
  @Inject @ReloadProduces
  private Event<ApplicationConfig> applicationConfigEvent;
  
  private ConfigurationDAO configurationDao;
  
  private ApplicationConfig propertyConfig;
  
  
  //@Configurations
  //@ApplicationScoped
  //@Produces
  public ApplicationConfig initApplicationConfig(InjectionPoint injectionPoint) throws BusinessException {
    
    if(propertyConfig == null) {
      loadApplicationConfig();
    }
    
    return propertyConfig;
  }
  
  public void loadApplicationConfig() throws BusinessException {
    
    Map<String, String> configs = new HashMap<String, String>();
    
    //Load from Property resource config. TODO in the future, the resource config will configurable
    configs.putAll(PropertiesUtils.loadAsMap(PropertiesUtils.CONFIG_RESOURCE));
    
    if(configurationDao == null) {

      try {
        configurationDao = configurationDaoSource.get();
        
        configs.putAll(configurationDao.load());
        
      } catch (Exception e) { }
      
    }
    
    propertyConfig = new ApplicationConfig(configs);
    
  }
  
  
  public void reloadApplicationConfigProducer(@Observes(notifyObserver = Reception.IF_EXISTS) @ReloadProduces ApplicationConfig document) throws BusinessException {
    System.out.println("Reload Event is Fired");
    loadApplicationConfig();
  }

}
