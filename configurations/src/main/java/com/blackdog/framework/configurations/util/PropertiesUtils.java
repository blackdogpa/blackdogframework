package com.blackdog.framework.configurations.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.blackdog.framework.exceptions.BusinessException;
import com.blackdog.framework.exceptions.SystemException;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 * 
 * 
 */
public class PropertiesUtils {

  /**
   * Default file name of configuration file.
   */
  public static final String CONFIG_RESOURCE      = "blackdog.properties";
  
  
  
  /*
   * Application
   */
  public static final String APPLICATION_NAME       = "application.name";
  public static final String APPLICATION_SHORT_NAME = "application.short.name";
  
  public static final String APPLICATION_SUPER_USER_NAME = "application.super.user.name";
  public static final String APPLICATION_LAST_NOTICE_CHECK = "APPLICATION.LAST.NOTICE.CHECK";
  public static final String APPLICATION_DB_SCHEMA     = "application.db.schema";
  
  
  public static final String BUILD_NUMBER = "build.number";
  public static final String BUILD_VERSION = "build.version";
  
  
  /*
   * Entironment
   */
  public static final String ENVIRONMENT_ID                    = "environment.id";
  public static final String ENVIRONMENT_ID_DESENVOLVER_VALUE  = "DESENVOLVER";
  public static final String ENVIRONMENT_ID_DESENVOLVER_DESC   = "Desenvolvimento";
  public static final String ENVIRONMENT_ID_HOMOLOGAR_VALUE    = "HOMOLOGAR";
  public static final String ENVIRONMENT_ID_HOMOLOGAR_DESC     = "Homologação";
  public static final String ENVIRONMENT_ID_TREINAR_VALUE      = "TREINAR";
  public static final String ENVIRONMENT_ID_TREINAR_DESC       = "Treinamento";
  public static final String ENVIRONMENT_ID_PRODUCAO_VALUE     = "PRODUCAO";
  public static final String ENVIRONMENT_ID_PRODUCAO_DESC      = "Produção";
  
  
  public static Boolean isValidEnvironmenteId(String id) {
    if(id != null && !id.equals("")) {
      
      if(id.equals(ENVIRONMENT_ID_DESENVOLVER_VALUE)) {
        return true;
      } else if(id.equals(ENVIRONMENT_ID_HOMOLOGAR_VALUE)) {
        return true;
      } else if(id.equals(ENVIRONMENT_ID_TREINAR_VALUE)) {
        return true;
      } else if(id.equals(ENVIRONMENT_ID_PRODUCAO_VALUE)) {
        return true;
      }
    }
    
    return false;
  }
  
  /*
   * Mail
   */
  public static final String MAIL_CONFIG      = "application.mail.config";
  
  
  /*
   * ControleAcesso Keys.
   */
  public static final String CA_APPLICATION_ID      = "ca.application.id";
  //public static final String CA_APPLICATION_NAME    = "ca.application.name";
  public static final String CA_WS_USER      = "ca.ws.user";
  public static final String CA_WS_PASSWORD      = "ca.ws.password";
  
  public static final String GD_MAIN_PAGE      = "gd.main.page";
  
  public static final String AUTHENTICATION_CACHE = "bd.authentication.cache.enable";
  
  
  /*    ********** BlackDog Config  ************ */
  public static final String BD_DEFAULT_PAGE_SIZE      = "bd.default.page.size";
  public static final String BD_MAX_PAGE_SIZE          = "bd.max.page.size";
  
  
  /*    ********** JWT  ************ */
  public static final String JWT_KEY_ID    = "jwt.key.id";
  public static final String JWT_KEY_TYPE  = "jwt.key.type";

  public static final String JWT_ISSUER    = "jwt.issuer";
  //public static final String JWT_SUBJECT    = "jwt.subject";
  
  
  public static final String JWT_HMAC_TYPE = "jwt.hmac.type";
  public static final String JWT_HMAC_KEY  = "jwt.hmac.key";
  
  public static final String JWT_RSA_KEY   = "jwt.rsa.key";
  
  
  /*
   * Código Adabas que representa o módulo do SIMAS
   * 
   * @see simas.application.id
   * 
   * 76I300:REQUISICAO 
   * 76I310:INCLUSAO 
   * 76I320:ALTERACAO 
   * 76I330:EXCLUSAO
   * 76I340:CONSULTAS 
   * 76I341:REQUISICAO POR NUMERO 
   * 76I342:REQUISICAO PENDENTES POR C.CUSTOS 
   * 76I343:REQUISICAO AUTORIZADAS POR C.CUSTOS 
   * 76I344:REQUISICAO ATENDIDAS POR C.CUSTOS 
   * 76I345:REQUISICAO ATENDIDAS NO PERIODO
   * 76I346:RELACAO DE MATERIAIS 
   * 76I350:ALTERACAO DE SENHA 
   * 76I360:AUTORIZACAO
   * 
   */
  
  /*public static Boolean isCacheNaturalAuthorization() throws BusinessException {
    
    String param = getParameter(CONFIG_RESOURCE, AUTHENTICATION_CACHE);
    
    return (StringUtils.isBlank(param) || param == "0" || param.toLowerCase() == "false") ? false : true;
  }*/
  
  /**
   * Busca parametros do arquivo de configuracao default
   * 
   * @param key key name
   * @return    value for key
   * @throws BusinessException
   */
  public static String getParameter(String key) throws BusinessException {
   return getParameter(CONFIG_RESOURCE, key); 
  }
  
  /**
   * Busca parametros do arquivo de configuracao
   * 
   * @param resource resource name
   * @param key      key name
   * @return         value for key
   * @throws BusinessException
   */
  public static String getParameter(String resource, String key) throws BusinessException {
    Properties prop = loadPropertyFileFromResourceFolder(resource);
    return prop.getProperty(key);
  }
  
  private static Properties loadPropertyFileFromResourceFolder(String propFileName) throws BusinessException  {
    try {

      Properties prop = new Properties();
      
      InputStream inputStream = PropertiesUtils.class.getClassLoader().getResourceAsStream(propFileName);
      
      if (inputStream != null) {
        prop.load(inputStream);
      } else {
        throw new SystemException("O Arquivo de configuração \""+propFileName+"\" não foi localizado.");
      }
      return prop;
      
    } catch (IOException e) {
      throw new SystemException("O Arquivo de configuração \""+propFileName+"\" não foi localizado.");
    }
  }
  
  
  public static Map<String, String> loadAsMap(String resource) throws BusinessException {
    
    Properties prop = loadPropertyFileFromResourceFolder(resource);
    
    Map<String, String> map = new HashMap<>();
    
    for(Object key : prop.keySet()) {
      
      map.put((String) key, (String) prop.get(key));
    }
    
    return map;
  }
  
  
  
  
}
