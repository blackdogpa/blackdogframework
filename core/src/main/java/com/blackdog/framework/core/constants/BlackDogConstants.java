package com.blackdog.framework.core.constants;

public class BlackDogConstants {

	/**
	 *
	 */
	public static final String REST_ROOT = "/api";

	/**
	 * 
	 */
	public static final String BASE_NAMESPACE = "http://blackdog.com";


  /**
   * 
   */
  public static final String DTO_NAMESPACE = BASE_NAMESPACE + "/dto";
  
  /**
   * 
   */
  public static final String EXCEPTION_NAMESPACE = BASE_NAMESPACE + "/exceptions";
  
  /**
   * 
   */
  public static final String SOAP_NAMESPACE = BASE_NAMESPACE + "/ws/soap";
  
  /**
   * 
   */
  public static final String REST_NAMESPACE = BASE_NAMESPACE + "/ws/rest";

	
	/**
	 *
	 */
	public static final Integer DEFAULT_MAX_REQUEST_COUNT = 100;
	
	
  /**
   * 
   */
  public static final String NATUTRAL_PREFIX_MESSAGE = "NAT-";


}
