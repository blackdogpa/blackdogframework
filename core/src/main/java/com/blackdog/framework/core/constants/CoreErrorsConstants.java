package com.blackdog.framework.core.constants;

import com.blackdog.framework.core.model.ErrorMessageConstant;

public class CoreErrorsConstants {
  
  public static enum PersistenceErrors implements ErrorMessageConstant {
    
    PERSISTENCE_001("PERSISTENCE-001", "Desculpe. Tivemos problemas com os dados."), //Conexao com o banco de dados falhou.
    PERSISTENCE_002("PERSISTENCE-002", "."),
    PERSISTENCE_003("PERSISTENCE-003", "."),
    PERSISTENCE_004("PERSISTENCE-004", "."),
    ;
    
    private String code;
    private String message;
    
    private PersistenceErrors(String code, String message) {
      this.code = code;
      this.message = message;
    }

    public String getCode() { return code; }

    public String getMessage() { return message; }
    
  }
  
  public static enum SerializationErrors implements ErrorMessageConstant {
    
    SERIALIZATION_001("SERIALIZATION_001", "Não foi possivel serializar o objeto para XML."), 
    SERIALIZATION_002("SERIALIZATION_002", "."),
    SERIALIZATION_003("SERIALIZATION_003", "."),
    SERIALIZATION_004("SERIALIZATION_004", "."),
    ;
    
    private String code;
    private String message;
    
    private SerializationErrors(String code, String message) {
      this.code = code;
      this.message = message;
    }

    public String getCode() { return code; }

    public String getMessage() { return message; }
    
  }
  
}
