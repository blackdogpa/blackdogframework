package com.blackdog.framework.core.dto;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.blackdog.framework.core.constants.BlackDogConstants;

@RequestScoped
//@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(name = "SessionInfo", namespace = BlackDogConstants.DTO_NAMESPACE)
public class RequestInfo implements Serializable {

	private static final long serialVersionUID = -6174141344670681307L;

	/* Request Info */
	private String ip;
	
	private String naturalUserId;
	private Long userId;
	
	private String token;
	
	private String governoDigitalCookie;

	/* Transaction Identifier */
	private String transactionID;
	
	private String requestUri;
	private String basePath;
	private String path;

	public RequestInfo() {
		super();
	}
	
	public boolean matchToken(String authToken) {
		return  getToken() != null && getToken().equals(authToken);
	}
	

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getTransactionID() {
		return transactionID;
	}

	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}

  public String getNaturalUserId() {
    return naturalUserId;
  }

  public void setNaturalUserId(String naturalId) {
    this.naturalUserId = naturalId;
  }

  public String getGovernoDigitalCookie() {
    return governoDigitalCookie;
  }

  public void setGovernoDigitalCookie(String governoDigitalCookie) {
    this.governoDigitalCookie = governoDigitalCookie;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public String getBasePath() {
    return basePath;
  }

  public void setBasePath(String basePath) {
    this.basePath = basePath;
  }

  public String getRequestUri() {
    return requestUri;
  }

  public void setRequestUri(String requestUri) {
    this.requestUri = requestUri;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long usarId) {
    this.userId = usarId;
  }
  
  
}