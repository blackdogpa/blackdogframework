package com.blackdog.framework.core.exceptions;

public interface BlackExceptions {

  
  public static final String BUSINESS   = "BUSINESS";
  public static final String SECURITY   = "SECURITY";
  public static final String SYSTEM     = "SYSTEM";
  public static final String MAIL       = "MAIL";
  public static final String RATE_LIMIT = "RATE_LIMIT";
  public static final String SERIALIZATION = "SERIALIZATION";
  
  
  public ErrorMessage getErrorMessage();
  
  public String getId();
  public void setId(String id);

  public void setCode(String code);
  public String getCode();
  
  public String getMessage();
  public String getType();
  
  public String getApplicationId();
  public void setApplicationId(String applicationId);

  public String getApplicationName();
  public void setApplicationName(String applicationName);


}