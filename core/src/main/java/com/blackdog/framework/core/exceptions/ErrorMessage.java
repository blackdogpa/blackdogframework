package com.blackdog.framework.core.exceptions;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
//import com.thoughtworks.xstream.annotations.XStreamAlias;


/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com)
 *
 */
//@XStreamAlias("ErrorMessage")
//@XmlRootElement(name = "ErrorMessage")
/**
 * @author thiago
 *
 */
@JsonInclude(Include.NON_EMPTY)
@JsonPropertyOrder(value = {"type", "applicationId", "applicationName", "statusCode", "status", "message", "timestamp", "transactionId", "validations", "support", "trace"})
public class ErrorMessage implements Serializable {

  private static final long serialVersionUID = 1L;
  
  private String transactionId;
  private String applicationId;
  private String applicationName;
  
  private int statusCode;
  private String status;
  private String type;
  private String message;

  private List<Validation> validations = new ArrayList<Validation>();
  private List<HelpMessage> support = new ArrayList<HelpMessage>();

  private Date timestamp;
  private String trace;
  
  public ErrorMessage() {
    super();
    this.timestamp = new Date();
  }

  public Validation getValidation(String code) {
    
    if(this.validations != null) {
      
      for(Validation validation: this.validations) {
      
        if(validation.getCode().equals(code)) {
          return validation;
        }
        
      }
      
    }
    
    return null;
  }
  
  
  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public List<Validation> getValidations() {
    return validations;
  }

  public void setValidations(List<Validation> validations) {
    this.validations = validations;
  }

  public String getTrace() {
    return trace;
  }

  public void setTrace(String trace) {
    this.trace = trace;
  }
  
  public int getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(int statusCode) {
    this.statusCode = statusCode;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  
  public String getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(String applicationId) {
    this.applicationId = applicationId;
  }

  public String getApplicationName() {
    return applicationName;
  }

  public void setApplicationName(String applicationName) {
    this.applicationName = applicationName;
  }
  

  public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }
  
  public Date getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Date timestamp) {
    this.timestamp = timestamp;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  
  public List<HelpMessage> getSupport() {
    return support;
  }

  public void setSupport(List<HelpMessage> support) {
    this.support = support;
  }

  @Override
  public String toString() {
    return "BusinessExceptionDTO [mensagem=" + message + ", trace=" + trace + ", validations=" + validations + "]";
  }

}