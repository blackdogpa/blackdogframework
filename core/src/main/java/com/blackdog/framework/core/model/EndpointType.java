package com.blackdog.framework.core.model;

public enum EndpointType {
  SOAP, REST, SECURITY;
}
