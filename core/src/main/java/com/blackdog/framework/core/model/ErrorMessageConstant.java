package com.blackdog.framework.core.model;

public interface ErrorMessageConstant {
  
  public String getCode();
  
  public String getMessage();

}
