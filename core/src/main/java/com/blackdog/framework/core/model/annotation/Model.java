package com.blackdog.framework.core.model.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com)
 *
 */
@Retention(RUNTIME)
@Target({ METHOD, TYPE })
public @interface Model {
  
  String name();
  
}
