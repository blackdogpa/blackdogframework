#Como Criar um par de chaves RSA com OpenSSL

##Da ferramenta

a ferramenta é o [OpenSSL](https://www.openssl.org/). Ele possui um [Cookbook](https://www.feistyduck.com/library/openssl-cookbook/online/ch-openssl.html)

##Criando Chave  


- Criar arquivo com a CHAVE

```
  echo "prodepa00" > id_aes.txt
  
```
  
- Encriptar

``` 
  openssl aes-128-cbc -a -salt -in id_aes.txt -out id_aes.key
  
```
  
- Converter para Base64

```    
  openssl base64 -in id_aes.key -out id_aes.key.bin
  
```




###REFs
https://www.openssl.org/docs/man1.0.2/apps/enc.html