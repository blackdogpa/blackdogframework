#Como Criar um par de chaves RSA com OpenSSL

##Glossario


- x509 

  É tipo de certificado

- Binary (DER) certificate
  
  Contains an X.509 certificate in its raw form, using DER ASN.1 encoding.

- ASCII (PEM) certificate(s)
  
  Contains a base64-encoded DER certificate, with -----BEGIN CERTIFICATE----- used as the header and -----END CERTIFICATE----- as the footer. Usually seen with only one certificate per file, although some programs allow more than one certificate depending on the context. For example, older Apache web server versions require the server certificate to be alone in one file, with all intermediate certificates together in another.

- Binary (DER) key
  
  Contains a private key in its raw form, using DER ASN.1 encoding. OpenSSL creates keys in its own traditional (SSLeay) format. There’s also an alternative format called PKCS#8 (defined in RFC 5208), but it’s not widely used. OpenSSL can convert to and from PKCS#8 format using the pkcs8 command.

- ASCII (PEM) key
  
  Contains a base64-encoded DER key, sometimes with additional metadata (e.g., the algorithm used for password protection).

- PKCS#7 certificate(s)

  A complex format designed for the transport of signed or encrypted data, defined in RFC 2315. It’s usually seen with .p7b and .p7c extensions and can include the entire certificate chain as needed. This format is supported by Java’s keytool utility.

- PKCS#12 (PFX) key and certificate(s)

  A complex format that can store and protect a server key along with an entire certificate chain. It’s commonly seen with .p12 and .pfx extensions. This format is commonly used in Microsoft products, but is also used for client certificates. These days, the PFX name is used as a synonym for PKCS#12, even though PFX referred to a different format a long time ago (an early version of PKCS#12). It’s unlikely that you’ll encounter the old version anywhere.

##Da ferramenta

a ferramenta é o [OpenSSL](https://www.openssl.org/). Ele possui um [Cookbook](https://www.feistyduck.com/library/openssl-cookbook/online/ch-openssl.html)

###Criando Chave Privada 

Criar chave privada com tamanho de 2048 bits e encriptada com o algoritimo AES256.


```
openssl genrsa -aes256 -out id_rsa.key 2048
```
**Atenção** Será requirida uma ```pass phrase```. Ela é muito importante e deve ser forte, pois será a necessária para acesso a essa chave.

Comandos Complementares

```
cat id_rsa.key
```

```
openssl rsa -text -in id_rsa.key 
```

###Exportando uma chave pública


```
openssl rsa -in id_rsa.key -pubout -out id_rsa_pub.key

```

Agora temos o par de chaves

Comandos Complementares

```
cat id_rsa.pub.key
```

```
openssl rsa -text -in id_rsa.key 
```


###Criando Certificate Signing Requests

Gerar um Certificate Signing Request (CSR).This is a formal request asking a CA to sign a certificate, and it contains the public key of the entity requesting the certificate and some information about the entity. This data will all be part of the certificate. A CSR is always signed with the private key corresponding to the public key it carries. 

```
openssl req -new -key id_rsa.key -out id_rsa.csr
```

Verificar o resultado

```
openssl req -text -in id_rsa.csr -noout
```

###Criar um Certificado auto assinado (Signing Your Own Certificates)
If you’re installing a TLS server for your own use, you probably don’t want to go to a CA for a publicly trusted certificate. It’s much easier just to use a self-signed certificate. If you’re a Firefox user, on your first visit to the web site you can create a certificate exception, after which the site will be as secure as if it were protected with a publicly trusted certificate.


```
openssl x509 -req -days 3650 -in fd.csr -signkey id_rsa.key -out id_rsa-prodepa.crt

```


###Examinando Certificados


```
openssl x509 -text -in id_rsa-prodepa.crt -noout
```



##Conversão de formatos de chaves

Pode ser necessário converter as chaves para outros tipos

**Ver Glassário**

###PEM and DER Conversion
Certificate conversion between PEM and DER formats is performed with the x509 tool. To convert a certificate from PEM to DER format:

```
openssl x509 -inform PEM -in fd.pem -outform DER -out fd.der
```

openssl x509 -inform PEM -in id_rsa.key -outform DER -out id_rsa.der

To convert a certificate from DER to PEM format:

$ openssl x509 -inform DER -in fd.der -outform PEM -out fd.pem
The syntax is identical if you need to convert private keys between DER and PEM formats, but different commands are used: rsa for RSA keys, and dsa for DSA keys.

