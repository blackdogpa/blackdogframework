#Como Criar um par de chaves RSA com OpenSSL

##Da ferramenta

a ferramenta é o [OpenSSL](https://www.openssl.org/). Ele possui um [Cookbook](https://www.feistyduck.com/library/openssl-cookbook/online/ch-openssl.html)

##Criando Chave Privada 

Criar chave privada com tamanho de 2048 bits e encriptada com o algoritimo AES256.


```
openssl genrsa -aes256 -out id_rsa.pem 2048
```

Exportar para o formato DER, aceito pela aplicacao

```
openssl pkcs8 -topk8 -inform PEM -outform DER -in id_rsa.pem -out id_rsa.der -nocrypt
```


**Atenção** Será requirida uma ```pass phrase```. Ela é muito importante e deve ser forte, pois será a necessária para acesso a essa chave.

Comandos Complementares

```
cat id_rsa.pem
```

```
openssl rsa -text -in id_rsa.pem 
```

##Exportando uma chave pública


```
openssl rsa -in id_rsa.pem -pubout -outform DER -out id_rsa_pub.der

```

Agora temos o par de chaves

Comandos Complementares

```
cat id_rsa.pub.der
```

```
openssl rsa -text -in id_rsa.der 
```



##Converter para Base64


```
openssl base64 -in id_rsa.der -out id_rsa.der.bin
```

```
openssl base64 -in id_rsa_pub.der -out id_rsa_pub.der.bin
```

Os conteudos dos arquivos **id_rsa.der.bin** e **id_rsa_pub.der.bin** serão adicionados ao banco de dados





