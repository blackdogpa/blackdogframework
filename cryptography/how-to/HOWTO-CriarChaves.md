#Criar chaves para o Cryptor com OpenSSL

##Da ferramenta

a ferramenta é o [OpenSSL](https://www.openssl.org/). Ele possui um [Cookbook](https://www.feistyduck.com/library/openssl-cookbook/online/ch-openssl.html)

##Criar Chave AES 


- Criar arquivo com a CHAVE, onde a chave deve conter 16 caracterss

```
  echo "prodepa000000000" > id_aes.txt
  
```
  
- Converter para Base64

```    
  openssl base64 -in id_aes.txt -out id_aes.bin
  
```


##Criar Chaves RSA

### Criando Chave Privada 

Criar chave privada com tamanho de 2048 bits e encriptada com o algoritimo AES256.


```
openssl genrsa -aes256 -out id_rsa.pem 2048
```

Exportar para o formato DER, aceito pela aplicacao

```
openssl pkcs8 -topk8 -inform PEM -outform DER -in id_rsa.pem -out id_rsa.der -nocrypt
```


**Atenção** Será requirida uma ```pass phrase```. Ela é muito importante e deve ser forte, pois será a necessária para acesso a essa chave.


### Exportando uma chave pública

Exportar para o formato DER, aceito pela aplicacao

```
openssl rsa -in id_rsa.pem -pubout -outform DER -out id_rsa_pub.der

```

### Converter para Base64


```
openssl base64 -in id_rsa.der -out id_rsa.der.bin
```

```
openssl base64 -in id_rsa_pub.der -out id_rsa_pub.der.bin
```

Os conteudos dos arquivos **id_rsa.der.bin** e **id_rsa_pub.der.bin** serão adicionados ao banco de dados



##Inserir chaves na base de dados


### Chave AES
```
INSERT INTO cryptor.app_config(nome, valor) VALUES ('APPLICATION.AES.KEY.SECRET', 'AES_KEY_PASSWD');
INSERT INTO "cryptor"."chaves" (id,nome,descricao,formato,algoritimo,conteudo,conteudo_formato,ativo,create_date,id_create_user,update_date,id_update_user) 
   VALUES (1,'APPLICATION.AES.KEY_001','APPLICATION.AES.KEY_001','RAW','AES','KEY_CONTENT','BASE64',true,now(),0,now(),0);
```

###Chave RSA PRIVATE

```
INSERT INTO cryptor.app_config(nome, valor) VALUES ('APPLICATION.RSA.PRIVATE.KEY.SECRET', 'RSA_KEY_PASSWD');
INSERT INTO "cryptor"."chaves" (id,nome,descricao,formato,algoritimo,conteudo,conteudo_formato,ativo,create_date,id_create_user,update_date,id_update_user)
 VALUES (2,'APPLICATION.RSA.PRIVATE.KEY_001','APPLICATION.RSA.PRIVATE.KEY_001','PKCS#8','RSA','KEY_CONTENT','BASE64',true,now(),0,now(),0);
```

###Chave RSA PUBLIC

```
INSERT INTO cryptor.app_config(nome, valor) VALUES ('APPLICATION.RSA.PUBLIC.KEY.SECRET', 'RSA_KEY_PASSWD');
INSERT INTO "cryptor"."chaves" (id,nome,descricao,formato,algoritimo,conteudo,conteudo_formato,ativo,create_date,id_create_user,update_date,id_update_user) 
 VALUES (3,'APPLICATION.RSA.PUBLIC.KEY_001','APPLICATION.RSA.PUBLIC.KEY_001','X.509','RSA','KEY_CONTENT','BASE64',true,now(),0,now(),0);
```
 
 
 ### Atenção: Os trechos **AES_KEY_PASSWD**, **RSA_KEY_PASSWD** e **KEY_CONTENT** devem ser trocadas pelos respscitivos conteúdos.

