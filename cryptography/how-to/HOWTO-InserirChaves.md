##Inserir chaves


### AES
```
INSERT INTO cryptor.app_config(nome, valor) VALUES ('APPLICATION.AES.KEY.SECRET', 'KEY_PASSWD');
INSERT INTO "cryptor"."chaves" (id,nome,descricao,formato,algoritimo,conteudo,conteudo_formato,ativo,create_date,id_create_user,update_date,id_update_user) 
   VALUES (1,'APPLICATION.AES.KEY_001','APPLICATION.AES.KEY_001','RAW','AES','KEY_CONTENT','BASE64',true,now(),0,now(),0);
```

###RSA PRIVATE

```
INSERT INTO cryptor.app_config(nome, valor) VALUES ('APPLICATION.RSA.PRIVATE.KEY.SECRET', 'KEY_PASSWD');
INSERT INTO "cryptor"."chaves" (id,nome,descricao,formato,algoritimo,conteudo,conteudo_formato,ativo,create_date,id_create_user,update_date,id_update_user)
 VALUES (2,'APPLICATION.RSA.PRIVATE.KEY_001','APPLICATION.RSA.PRIVATE.KEY_001','PKCS#8','RSA','KEY_CONTENT','BASE64',true,now(),0,now(),0);
```

###RSA PUBLIC

```
INSERT INTO cryptor.app_config(nome, valor) VALUES ('APPLICATION.RSA.PUBLIC.KEY.SECRET', 'KEY_PASSWD');
INSERT INTO "cryptor"."chaves" (id,nome,descricao,formato,algoritimo,conteudo,conteudo_formato,ativo,create_date,id_create_user,update_date,id_update_user) 
 VALUES (3,'APPLICATION.RSA.PUBLIC.KEY_001','APPLICATION.RSA.PUBLIC.KEY_001','X.509','RSA','KEY_CONTENT','BASE64',true,now(),0,now(),0);
```
 
 
 ### Atenção: As chaves **KEY_PASSWD** e **KEY_CONTENT** devem ser trocadas pelos respscitivos conteúdos