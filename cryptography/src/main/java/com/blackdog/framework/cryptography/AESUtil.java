package com.blackdog.framework.cryptography;

import static org.apache.commons.codec.binary.Hex.*;
import static org.apache.commons.io.FileUtils.*;
import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.ArrayUtils;

public class AESUtil {

  public static final int EASKEYSIZE_128 = 128;
  public static final int EASKEYSIZE_256 = 256;

  /*
   * Generate
   */
  
  public static SecretKey generateKey(int keySize) throws NoSuchAlgorithmException {
    KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
    keyGenerator.init(keySize); // 128 default; 192 and 256 also possible
    return keyGenerator.generateKey();
  }

  public static SecretKey generateKey(String secretkey, Boolean useHash, int keySize) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        
    byte[] key = secretkey.getBytes("UTF-8");
    
    if(key.length < keySize) {
      //TODO PRoblema
    }
    
    if(useHash) {
      MessageDigest sha = MessageDigest.getInstance("SHA-1");
      key = sha.digest(key);
    }
    
    if(keySize == 128) {
      key = Arrays.copyOf(key, 16);  //Para chave de 128
    } else if(keySize == 192) {
      key = Arrays.copyOf(key, 24);  //Para chave de 192
    } else if(keySize == 256) {
      key = Arrays.copyOf(key, 32);  //Para chave de 256
      
    }
    
    SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
    
    return secretKeySpec;
    
    /*KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
    keyGenerator.init(keySize); // 128 default; 192 and 256 also possible
    return keyGenerator.generateKey();*/
    
  }
  
  /*
   * SAVE
   * 
   */
  
  public static void saveKey(SecretKey key, String path) throws IOException {
    saveKey(key, new File(path));
  }
  
  public static void saveKey(SecretKey key, File file) throws IOException {
    char[] hex = encodeHex(key.getEncoded());
    writeStringToFile(file, String.valueOf(hex));
  }

  /*
   * Load
   */
  
  public static SecretKey loadKey(String path) throws IOException {
    return loadKey(new File(path));
  }
  
  public static SecretKey loadKey(File file) throws IOException {
    String data = new String(readFileToByteArray(file));
    byte[] encoded;
    try {
      encoded = decodeHex(data.toCharArray());
    } catch (DecoderException e) {
      e.printStackTrace();
      return null;
    }
    return new SecretKeySpec(encoded, "AES");
  }
  
  public static SecretKey loadKeyBase64(String key) throws IOException {
    byte[] decoded = Base64.decodeBase64(key);
    
    decoded = cleanKey(decoded);
    
    return new SecretKeySpec(decoded, "AES");
  }
  
  
  
  /**
   * 
   * @param content Conteudo a ser criptografado
   * @param aeskey Chave AES (BASE64)
   * @param debug Imprimir ou não o debug
   * @return conteudo criptografado
   * @throws Exception
   */
  public static String encrypt(String content, byte[] aeskey, Boolean debug) throws Exception {
    
    aeskey = cleanKey(aeskey);
    
    SecretKey aesKey = new SecretKeySpec(aeskey, "AES");
    
    byte[] chave = aesKey.getEncoded();
    
    // -- B) Cifrando o texto com a chave simétrica gerada
    Cipher aescf = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivspec = new IvParameterSpec(new byte[16]);
    aescf.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(chave, "AES"), ivspec);
    byte[] encryptedContent = aescf.doFinal(content.getBytes("UTF-8"));
    
    return Base64.encodeBase64String(encryptedContent);
  }
  
  public static String encryptBase64(String content, byte[] aeskey, Boolean debug) throws Exception {
    
    aeskey = cleanKey(aeskey);
    
    SecretKey aesKey = new SecretKeySpec(aeskey, "AES");
    
    byte[] chave = aesKey.getEncoded();
    
    // -- B) Cifrando o texto com a chave simétrica gerada
    Cipher aescf = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivspec = new IvParameterSpec(new byte[16]);
    aescf.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(chave, "AES"), ivspec);
    byte[] encryptedContent = aescf.doFinal( Base64.decodeBase64(content) );
    
    return Base64.encodeBase64String(encryptedContent);
  }
  
  
  /**
   * TODO Definir uma nome melhor ....
   * 
   * @param content conteudo a ser criptografado
   * @param aeskeyPath caminho para a chave AES
   * @param debug Imprimir ou não o debug
   * @return conteudo criptografado
   * @throws Exception
   */
  public static String encryptKeyPath(String content, String aeskeyPath, Boolean debug) throws Exception {
    
    SecretKey aesKey = AESUtil.loadKey(aeskeyPath);
    
    byte[] chave = aesKey.getEncoded();
    
    // -- B) Cifrando o texto com a chave simétrica gerada
    Cipher aescf = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivspec = new IvParameterSpec(new byte[16]);
    aescf.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(chave, "AES"), ivspec);
    byte[] encryptedContent = aescf.doFinal(content.getBytes("UTF-8"));
    
    return Base64.encodeBase64String(encryptedContent);
  }
    
  public static String decrypt(String content,  byte[] aeskey, Boolean debug) throws Exception {
    
    aeskey = cleanKey(aeskey);
    
    SecretKey aesKey = new SecretKeySpec(aeskey, "AES");
    
    byte[] encriptedContent = Base64.decodeBase64(content);
    
    Cipher aescf = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivspec = new IvParameterSpec(new byte[16]);
    aescf.init(Cipher.DECRYPT_MODE, new SecretKeySpec(aesKey.getEncoded(), "AES"), ivspec);
    
    byte[] decryptedContent = aescf.doFinal(encriptedContent);
    
    return new String(decryptedContent);
  }
  
  public static String decryptPath(String content, String aeskeyPath, Boolean debug) throws Exception {
    
    SecretKey aesKey = AESUtil.loadKey(aeskeyPath);
    
    byte[] encriptedContent = Base64.decodeBase64(content);
    
    Cipher aescf = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivspec = new IvParameterSpec(new byte[16]);
    aescf.init(Cipher.DECRYPT_MODE, new SecretKeySpec(aesKey.getEncoded(), "AES"), ivspec);
    
    byte[] decryptedContent = aescf.doFinal(encriptedContent);
    
    return new String(decryptedContent);
  }

  
  private static byte[] cleanKey(byte[] key) {
    if(key[key.length-1] == 10 ) {
      key = ArrayUtils.remove(key, key.length-1);
    }
    return key;
  }

}
