package com.blackdog.framework.cryptography;

import static org.apache.commons.codec.binary.Hex.*;
import static org.apache.commons.io.FileUtils.*;
import java.io.*;
import java.security.NoSuchAlgorithmException;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

public class AESUtil2 {

  public static final int EASKEYSIZE_128 = 128;
  public static final int EASKEYSIZE_256 = 256;

  public static SecretKey generateKey(int keySize) throws NoSuchAlgorithmException {
    KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
    keyGenerator.init(keySize); // 128 default; 192 and 256 also possible
    return keyGenerator.generateKey();
  }

  public static void saveKey(SecretKey key, String path) throws IOException {
    
    saveKey(key, new File(path));
    
  }
  
  public static void saveKey(SecretKey key, File file) throws IOException {
    String hex = Base64.encodeBase64String(key.getEncoded());
    writeStringToFile(file, hex);
  }

  public static SecretKey loadKey(String path) throws IOException {
    return loadKey(new File(path));
  }
  
  public static SecretKey loadKey(File file) throws IOException {
    String data = new String(readFileToByteArray(file));
    byte[] encoded;
    encoded = Base64.decodeBase64(data);
    return new SecretKeySpec(encoded, "AES");
  }
  
  public static String encrypt(String content, String aeskeyPath, Boolean debug) throws Exception {
    
    System.out.println("T>>> " +content.length());
    System.out.println("B>>> " +content.getBytes("UTF-8").length);
    
    SecretKey aesKey = AESUtil2.loadKey(aeskeyPath);
    
    byte[] chave = aesKey.getEncoded();
    
    // -- B) Cifrando o texto com a chave simétrica gerada
    Cipher aescf = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivspec = new IvParameterSpec(new byte[16]);
    aescf.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(chave, "AES"), ivspec);
    byte[] encryptedContent = aescf.doFinal(content.getBytes("UTF-8"));
    
    System.out.println("B>>> " +encryptedContent.length);
    
    String ret = Base64.encodeBase64String(encryptedContent);
    
    System.out.println("T>>> " +ret.length());
    return ret;
  }
  
  
  public static String decrypt(String content, String aeskeyPath, Boolean debug) throws Exception {
    
    SecretKey aesKey = AESUtil2.loadKey(aeskeyPath);
    
    byte[] encriptedContent = Base64.decodeBase64(content);
    
    Cipher aescf = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivspec = new IvParameterSpec(new byte[16]);
    aescf.init(Cipher.DECRYPT_MODE, new SecretKeySpec(aesKey.getEncoded(), "AES"), ivspec);
    
    byte[] decryptedContent = aescf.doFinal(encriptedContent);
    
    return new String(decryptedContent);
  }
  

}
