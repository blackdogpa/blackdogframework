
package com.blackdog.framework.cryptography;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAKeyGenParameterSpec;
import java.security.spec.X509EncodedKeySpec;
//import java.util.Base64;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.ArrayUtils;


/**
 * 
 * **
 * ** OPENSSL
 * **
 * # generate a 2048-bit RSA private key
 * $ openssl genrsa -out private_key.pem 2048
 *
 * # convert private Key to PKCS#8 format (so Java can read it)
 * $ openssl pkcs8 -topk8 -inform PEM -outform DER -in private_key.pem -out private_key.der -nocrypt
 *
 * # output public key portion in DER format (so Java can read it)
 * $ openssl rsa -in private_key.pem -pubout -outform DER -out public_key.der
 * 
 * 
 * 
 * **
 * ** ssh-keygen
 * **
 * ssh-keygen -t rsa -b 2048 -f cryptor_producao_id_rsa -C "CRYPTOR / PRODUCAO ( PRODEPA - EMPRESA DE TECNOLOGIA DA INFORMACAO E COMUNICACAO DO ESTADO DO PARA ) "
 * 
 * 
 * 
 * 
 * @see https://www.openssl.org/docs/
 * @see http://codeartisan.blogspot.com.br/2009/05/public-key-cryptography-in-java.html
 * @see https://wiki.openssl.org/index.php/Manual:Openssl(1)
 * @see http://niels.nu/blog/2016/java-rsa.html
 * 
 * @see http://adrianorosa.com/blog/seguranca/ssh-keys.html
 * @see http://adrianorosa.com/blog/seguranca/como-criar-ssh-key-pair-unix.html
 * 
 * @author thiago.soares.jr@gmail.com
 *
 */
public class RSAUtil {

  public static final int RSAKEYSIZE = 1024;
  public static final String DEFAULT_SEPARATOR = "!";
  
  /*
   * 
   * Create Key Pair
   * 
   */
  
  /**
   * Generate Key Pair in PEM format
   * 
   * RSA 
   *   1024
   *   
   * AES
   *   128
   *   256   
   * 
   * @return KeyPair keyPair
   * @throws NoSuchAlgorithmException
   * @throws InvalidAlgorithmParameterException
   */
  public static KeyPair generatePemKeyPair(int keySize) throws NoSuchAlgorithmException, InvalidAlgorithmParameterException {
    KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");

    kpg.initialize(new RSAKeyGenParameterSpec(keySize, RSAKeyGenParameterSpec.F4));
    return kpg.genKeyPair();
  }
  
  /**
   * Generate Key Pair in PEM format
   * 
   * @param fPub
   * @param fPvk
   * @throws IOException
   * @throws NoSuchAlgorithmException
   * @throws InvalidAlgorithmParameterException
   * @throws CertificateException
   * @throws KeyStoreException
   */
  public static void generatePemKeyPair(File fPub, File fPvk) throws IOException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, CertificateException, KeyStoreException {
    
    KeyPair kpr = generatePemKeyPair(RSAKEYSIZE);
    PrivateKey priv = kpr.getPrivate();
    PublicKey pub = kpr.getPublic();
    
    // -- Gravando a chave pública em formato serializado
    ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fPub));
    oos.writeObject(pub);
    oos.close();
    // -- Gravando a chave privada em formato serializado
    // -- Não é a melhor forma (deveria ser guardada em um keystore, e protegida
    // por senha),
    // -- mas isto é só um exemplo
    oos = new ObjectOutputStream(new FileOutputStream(fPvk));
    oos.writeObject(priv);
    oos.close();
  }
  
  /*
   * 
   * Encrypt Data
   * 
   */

  /**
   * Encrypt Data
   * 
   * @param privateKey
   * @param message
   * @return
   * @throws Exception
   */
  public static byte[] encrypt(PrivateKey privateKey, String message) throws Exception {
    Cipher cipher = Cipher.getInstance("RSA");
    cipher.init(Cipher.ENCRYPT_MODE, privateKey);

    return cipher.doFinal(message.getBytes());
  }

  
  /**
   * Decrypt Data
   * 
   * @param publicKey
   * @param encrypted
   * @return
   * @throws Exception
   */
  public static byte[] decrypt(PublicKey publicKey, byte[] encrypted) throws Exception {
    Cipher cipher = Cipher.getInstance("RSA");
    cipher.init(Cipher.DECRYPT_MODE, publicKey);

    return cipher.doFinal(encrypted);
  }

  
  /*
   * 
   * Load Keys :: PEM
   * 
   */
  
  
  /**
   * Load a PublicKey in PEM format
   * @param fPub
   * @return
   * @throws IOException
   * @throws ClassNotFoundException
   * @throws InvalidKeySpecException 
   * @throws NoSuchAlgorithmException 
   */
  public static PublicKey loadPublicKeyPem(File fPub) throws Exception {
    if(fPub == null) {
      throw new Exception("Chave Public não informada");
    }
    return loadPublicKeyPem(fPub.getAbsolutePath());
  }
  
  /**
   * Load a PublicKey in PEM format
   * 
   * @param fPub
   * @return
   * @throws IOException
   * @throws ClassNotFoundException
   * @throws NoSuchAlgorithmException 
   * @throws InvalidKeySpecException 
   */
  public static PublicKey loadPublicKeyPem(String fPub) throws Exception {
    /*ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fPub));
    PublicKey ret = (PublicKey) ois.readObject();
    ois.close();
    return ret;*/

    byte[] keyBytes = Files.readAllBytes(Paths.get(fPub));
    
    keyBytes = cleanKey(keyBytes);
    
    return loadPublicKeyPem(keyBytes);
  }
  
  /**
   * Load a PublicKey in PEM format
   * 
   * @param fPub
   * @return
   * @throws IOException
   * @throws ClassNotFoundException
   */
  public static PublicKey loadPublicKeyPem(byte[] fPub) throws Exception {
    
    fPub = cleanKey(fPub);
    
    X509EncodedKeySpec spec = new X509EncodedKeySpec(fPub);
    KeyFactory kf = KeyFactory.getInstance("RSA");
    
    PublicKey publicKey = kf.generatePublic(spec);
    return publicKey;
  }
 
  /**
   * 
   * Load a PrivateKey in PEM format
   * 
   * @param fPvk
   * @return
   * @throws IOException
   * @throws ClassNotFoundException
   */
  public static PrivateKey loadPrivateKeyPem(String fPvk) throws IOException, ClassNotFoundException {
    return loadPrivateKeyPem(new File(fPvk));
  }
  
  /**
   * Load a PrivateKey in PEM format
   * 
   * @param fPvk
   * @return
   * @throws IOException
   * @throws ClassNotFoundException
   */
  public static PrivateKey loadPrivateKeyPem(File fPvk) throws IOException, ClassNotFoundException {
    ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fPvk));
    PrivateKey ret = (PrivateKey) ois.readObject();
    ois.close();
    return ret;
  }
  
  
  
  
  /*
   * 
   * Load Keys :: DER
   * 
   */
  
  
  /**
   * Load a PublicKey in DER format
   * @param fPub
   * @return
   * @throws IOException
   * @throws InvalidKeySpecException 
   * @throws NoSuchAlgorithmException 
   */
  public static PublicKey loadPublicKeyDer(File fPub) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
    return loadPublicKeyDer(fPub.getAbsolutePath());
  }
  
  /**
   * Load a PublicKey in DER format
   * 
   * @param fPub
   * @return
   * @throws IOException
   * @throws NoSuchAlgorithmException 
   * @throws InvalidKeySpecException 
   */
  public static PublicKey loadPublicKeyDer(String fPub) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
    
    byte[] keyBytes = Files.readAllBytes(Paths.get(fPub));
    
    keyBytes = cleanKey(keyBytes);
    
    X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
    KeyFactory kf = KeyFactory.getInstance("RSA");
    PublicKey publicKey = kf.generatePublic(spec);
    
    return publicKey;
  }
  
  public static PublicKey loadPublicKeyDer(byte[] keyBytes) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {

    keyBytes = cleanKey(keyBytes);
    
    X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
    KeyFactory kf = KeyFactory.getInstance("RSA");
    PublicKey publicKey = kf.generatePublic(spec);
    
    return publicKey;
  }
  
  public static PublicKey loadPublicKeyDerBase64(String fPub) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {

    byte[] keyBytes = Base64.decodeBase64(fPub);
    
    keyBytes = cleanKey(keyBytes);
    
    X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
    KeyFactory kf = KeyFactory.getInstance("RSA");
    PublicKey publicKey = kf.generatePublic(spec);
    
    return publicKey;
  }
 
  /**
   * 
   * Load a PrivateKey in Der format
   * 
   * @param fPvk
   * @return
   * @throws IOException
   * @throws InvalidKeySpecException 
   * @throws NoSuchAlgorithmException 
   */
  public static PrivateKey loadPrivateKeyDer(File fPvk) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
    return loadPrivateKeyDer(fPvk.getAbsolutePath());
  }
  
  /**
   * Load a PrivateKey in Der format
   * 
   * @param fPvk
   * @return
   * @throws IOException
   * @throws NoSuchAlgorithmException 
   * @throws InvalidKeySpecException 
   */
  public static PrivateKey loadPrivateKeyDer(String fPvk) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
   
    byte[] keyBytes = Files.readAllBytes(Paths.get(fPvk));
    
    keyBytes = cleanKey(keyBytes);

    PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
    
    KeyFactory kf = KeyFactory.getInstance("RSA");
    
    PrivateKey privateKey = kf.generatePrivate(spec);
    
    return privateKey;
  }
  
  public static PrivateKey loadPrivateKeyDer(byte[] keyBytes) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
    
    keyBytes = cleanKey(keyBytes);
    
    PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
    
    KeyFactory kf = KeyFactory.getInstance("RSA");
    
    PrivateKey privateKey = kf.generatePrivate(spec);
    
    return privateKey;
  }
  
  
  /*
   * 
   * Sign Data
   * 
   */
  public static String sign(String plainText, PrivateKey privateKey) throws Exception {
    Signature privateSignature = Signature.getInstance("SHA256withRSA");
    privateSignature.initSign(privateKey);
    privateSignature.update(plainText.getBytes("UTF-8"));

    byte[] signature = privateSignature.sign();

    return Base64.encodeBase64String(signature);
  }
  
  public static boolean verify(String plainText, String signature, PublicKey publicKey) throws Exception {
    Signature publicSignature = Signature.getInstance("SHA256withRSA");
    publicSignature.initVerify(publicKey);
    publicSignature.update(plainText.getBytes("UTF-8"));

    byte[] signatureBytes = Base64.decodeBase64(signature);

    return publicSignature.verify(signatureBytes);
}

  
  private static byte[] cleanKey(byte[] key) {
    if(key[key.length-1] == 10 ) {
      key = ArrayUtils.remove(key, key.length-1);
    }
    return key;
  }
}
