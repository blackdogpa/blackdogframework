package com.blackdog.framework.cryptography;

import java.io.*;
import java.nio.file.Path;
import java.util.*;
import javax.crypto.*;
import javax.crypto.spec.*;

import org.apache.commons.codec.binary.Base64;

import java.security.*;
import java.security.spec.*;
import java.security.cert.*;


public class RsaAesEncryptingUtil {
  
  public  static final String DEFAULT_SEPARATOR = "!"; 
  private static final int    EAS_KEY_SIZE = 128; //128, 192, 256
  
  /*
   * 
   * ENCRYPT
   *     
   * */
  
  public static String encryptWithPrivate(String content, String privateRsaKeyPath, String aeskeyPath, Boolean debug) throws Exception {
    
    // -- Cifrando a mensagem "Hello, world!"
    byte[] textoClaro = content.getBytes("UTF-8");
    
    PrivateKey pvk = RSAUtil.loadPrivateKeyDer(privateRsaKeyPath);
    SecretKey aesKey = AESUtil.loadKey(aeskeyPath);
    byte[] cifrado = encryptData(pvk, aesKey, textoClaro);
    
    String cifrado01 = Base64.encodeBase64String(cifrado);
    
    if(debug) {
      
      System.out.println(">>>> ENCRYPT ");
      System.out.println("HEXA \n\n");
      
      printHex(cifrado);
      
      System.out.println("BASE64 \n\n");
      
      System.out.println(cifrado01);
      
      System.out.println("LENGTH");
      System.out.println(content.length());
      System.out.println(cifrado01.length());
      System.out.println(cifrado.length);
      
    }

    return cifrado01;
  }
  
  public static String encryptWithPrivate(String content, String privateRsaKeyPath, Boolean debug) throws Exception {
    
    // -- Cifrando a mensagem "Hello, world!"
    byte[] textoClaro = content.getBytes("UTF-8");
    
    PrivateKey pvk = RSAUtil.loadPrivateKeyDer(privateRsaKeyPath);

    byte[][] cifrado = encryptData(pvk, textoClaro);
    
    String textoCifrado = Base64.encodeBase64String(cifrado[0]);
    String chaveCifrado = Base64.encodeBase64String(cifrado[1]);
    
    if(debug) {
      
      System.out.println(">>>> ENCRYPT \n");
      System.out.println("HEXA>>>>>>>>>>>>>");
      System.out.println("Contente : "  + cifrado[0].length);
      printHex(cifrado[0]);
      System.out.println("Key :" + cifrado[1].length);
      printHex(cifrado[1]);
      
      System.out.println("BASE64 \n\n");
      
      System.out.println(textoCifrado);
      
      System.out.println("LENGTH");
      System.out.println(content.length());
      System.out.println(textoCifrado.length());
      System.out.println(cifrado.length);
      
    }

    return textoCifrado + DEFAULT_SEPARATOR + chaveCifrado;
  }
  
  public static String encryptWithPublic(String content, String publicRsaKeyPath, String easKeyPath, Boolean debug) throws Exception {
    
    // -- Cifrando a mensagem "Hello, world!"
    byte[] textoClaro = content.getBytes("UTF-8");
    
    PublicKey pub = RSAUtil.loadPublicKeyDer(publicRsaKeyPath);
    SecretKey aesKey = AESUtil.loadKey(easKeyPath);
    byte[] cifrado = encryptData(pub, aesKey, textoClaro);
    
    String cifrado01 = Base64.encodeBase64String(cifrado);
    
    
    if(debug) {
      
      System.out.println(">>>> ENCRYPT ");
      System.out.println("HEXA \n\n");
      
      printHex(cifrado);
      
      System.out.println("BASE64 \n\n");
      
      System.out.println(cifrado01);
      
      System.out.println("LENGTH");
      System.out.println(content.length());
      System.out.println(cifrado01.length());
      
    }

    return cifrado01;
  }
  
  public static String encryptWithPublic(String content, String publicRsaKeyPath, Boolean debug) throws Exception {
    
    // -- Cifrando a mensagem "Hello, world!"
    byte[] textoClaro = content.getBytes("UTF-8");
    
    PublicKey pub = RSAUtil.loadPublicKeyDer(publicRsaKeyPath);
    byte[][] cifrado = encryptData(pub, textoClaro);
    
    String textoCifrado = Base64.encodeBase64String(cifrado[0]);
    String chaveCifrado = Base64.encodeBase64String(cifrado[1]);
    
    
    if(debug) {
      System.out.println(">>>> ENCRYPT \n");
      System.out.println("HEXA>>>>>>>>>>>>>");
      System.out.println("Contente : "  + cifrado[0].length);
      printHex(cifrado[0]);
      System.out.println("Key :" + cifrado[1].length);
      printHex(cifrado[1]);
      
      System.out.println("BASE64 \n\n");
      
      System.out.println(textoCifrado);
      
      System.out.println("LENGTH");
      System.out.println(content.length());
      System.out.println(textoCifrado.length());
      System.out.println(cifrado.length);
      
    }

    return textoCifrado + DEFAULT_SEPARATOR + chaveCifrado;
  }
  
  
  
  /*
   * 
   * DECRYPT
   *     
   * */
  
  /**
   * 
   * Chave EAS estatica
   * 
   * @param content
   * @param privateKeyPath
   * @param easKeyPath
   * @param debug
   * @return
   * @throws Exception
   */
  public static String decryptWithPrivate(String content, String privateKeyPath, String easKeyPath,  Boolean debug) throws Exception {
    
    if(debug) {
      System.out.println("DECR");
      printHex(Base64.decodeBase64(content));
      
      System.out.println(Base64.decodeBase64(content));
    }
    
    // -- Decifrando a mensagem
    PrivateKey pvk = RSAUtil.loadPrivateKeyDer(privateKeyPath);
    SecretKey aesKey = AESUtil.loadKey(easKeyPath);
    byte[] decifrado = dencrypt(pvk, aesKey, Base64.decodeBase64(content));
    
    String depriptado = new String(decifrado, "UTF-8");
    
    return depriptado;
  }
  
  /**
   * 
   * Chave EAS gerada dinamicamente 
   * 
   * @param content
   * @param privateKeyPath
   * @param easKeyPath
   * @param debug
   * @return
   * @throws Exception
   */
  public static String decryptWithPrivate(String content, String privateKeyPath, Boolean debug) throws Exception {
    
    if(content == null || content.equals("")) {
      //TODO Excecacao
    }
    
    if(!content.contains(DEFAULT_SEPARATOR)) {
    //TODO Excecacao
    }
    
    String[] chuncks = content.split(DEFAULT_SEPARATOR);
    
    if(debug) {
      System.out.println("DECRIPTAR ");
      printHex(Base64.decodeBase64(content));
      printHex(Base64.decodeBase64(chuncks[0]));
      printHex(Base64.decodeBase64(chuncks[1]));
      
      System.out.println(Base64.decodeBase64(content));
    }
    
    // -- Decifrando a mensagem
    PrivateKey pvk = RSAUtil.loadPrivateKeyDer(privateKeyPath);
    byte[] decifrado = dencrypt(pvk, Base64.decodeBase64(chuncks[0]), Base64.decodeBase64(chuncks[1]));
    
    String depriptado = new String(decifrado, "UTF-8");
    
    System.out.println(depriptado);
    
    return depriptado;
  }
  
  public static String decryptWithPublic(String content, String publicKeyPath, String easKeyPath, Boolean debug) throws Exception {
    
    if(debug) {
      
      System.out.println("DECR");
      //printHex(Base64.decodeBase64(content));
      
      System.out.println(Base64.decodeBase64(content));
    }
    
    // -- Decifrando a mensagem
    PublicKey pub = RSAUtil.loadPublicKeyDer(publicKeyPath);
    SecretKey aesKey = AESUtil.loadKey(easKeyPath);
    byte[] decifrado = dencrypt(pub, aesKey, Base64.decodeBase64(content));
    
    String depriptado = new String(decifrado, "UTF-8");
    
    return depriptado;
  }

  public static String decryptWithPublic(String content, String publicKeyPath, Boolean debug) throws Exception {
    
    if(content == null || content.equals("")) {
      //TODO Excecacao
    }
    
    if(!content.contains(DEFAULT_SEPARATOR)) {
    //TODO Excecacao
    }
    
    String[] chuncks = content.split(DEFAULT_SEPARATOR);
    
    if(debug) {
      
      System.out.println("DECR");
      printHex(Base64.decodeBase64(content));
      printHex(Base64.decodeBase64(chuncks[0]));
      printHex(Base64.decodeBase64(chuncks[1]));
      
      System.out.println(Base64.decodeBase64(content));
    }
    
    // -- Decifrando a mensagem
    PublicKey pub = RSAUtil.loadPublicKeyDer(publicKeyPath);
    byte[] decifrado = dencrypt(pub, Base64.decodeBase64(chuncks[0]), Base64.decodeBase64(chuncks[1]));
    
    String depriptado = new String(decifrado, "UTF-8");
    
    return depriptado;
  }


  
  private static byte[] encryptData(PublicKey pub, SecretKey sk, byte[] textoClaro) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
    
    byte[] textoCifrado = null;
    //byte[] chaveCifrada = null;
    
    // -- A) Gerando uma chave simétrica de 128 bits
    //KeyGenerator kg = KeyGenerator.getInstance("AES");
    //kg.init(EAS_KEY_SIZE);
    //SecretKey sk = kg.generateKey();
    
    byte[] chave = sk.getEncoded();
    
    // -- B) Cifrando o texto com a chave simétrica gerada
    Cipher aescf = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivspec = new IvParameterSpec(new byte[16]);
    aescf.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(chave, "AES"), ivspec);
    textoCifrado = aescf.doFinal(textoClaro);

    // -- C) Cifrando a chave com a chave pública
    //Cipher rsacf = Cipher.getInstance("RSA");
    //rsacf.init(Cipher.ENCRYPT_MODE, pub);
    //chaveCifrada = rsacf.doFinal(chave);
    //return new byte[][] { textoCifrado, chaveCifrada };
    
    return textoCifrado;
  }
  
  private static byte[][] encryptData(PublicKey pub, byte[] textoClaro) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
    
    byte[] textoCifrado = null;
    byte[] chaveCifrada = null;
    
    // -- A) Gerando uma chave simétrica de 128 bits
    KeyGenerator kg = KeyGenerator.getInstance("AES");
    kg.init(EAS_KEY_SIZE);
    SecretKey sk = kg.generateKey();
    
    byte[] chave = sk.getEncoded();
    
    // -- B) Cifrando o texto com a chave simétrica gerada
    Cipher aescf = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivspec = new IvParameterSpec(new byte[16]);
    aescf.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(chave, "AES"), ivspec);
    textoCifrado = aescf.doFinal(textoClaro);

    // -- C) Cifrando a chave com a chave pública
    Cipher rsacf = Cipher.getInstance("RSA");
    rsacf.init(Cipher.ENCRYPT_MODE, pub);
    chaveCifrada = rsacf.doFinal(chave);
    return new byte[][] { textoCifrado, chaveCifrada };
    
    //return textoCifrado;
  }
  
  private static byte[] encryptData(PrivateKey pub, SecretKey sk, byte[] textoClaro) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
    
    byte[] textoCifrado = null;
    //byte[] chaveCifrada = null;

    // -- A) Gerando uma chave simétrica de 128 bits
    //KeyGenerator kg = KeyGenerator.getInstance("AES"); 
    //kg.init(EAS_KEY_SIZE);
    //SecretKey sk = kg.generateKey();
    byte[] chave = sk.getEncoded();
    
    // -- B) Cifrando o texto com a chave simétrica gerada
    Cipher aescf = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivspec = new IvParameterSpec(new byte[16]);
    aescf.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(chave, "AES"), ivspec);
    textoCifrado = aescf.doFinal(textoClaro);
    
    // -- C) Cifrando a chave com a chave pública
    //Cipher rsacf = Cipher.getInstance("RSA");
    //rsacf.init(Cipher.ENCRYPT_MODE, pub);
    //chaveCifrada = rsacf.doFinal(chave);
    //return new byte[][] { textoCifrado, chaveCifrada };
    
    return textoCifrado;
  }
  
  private static byte[][] encryptData(PrivateKey pub, byte[] textoClaro) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
    
    byte[] textoCifrado = null;
    byte[] chaveCifrada = null;

    // -- A) Gerando uma chave simétrica de 128 bits
    KeyGenerator kg = KeyGenerator.getInstance("AES"); 
    kg.init(EAS_KEY_SIZE);
    SecretKey sk = kg.generateKey();
    byte[] chave = sk.getEncoded();
    
    // -- B) Cifrando o texto com a chave simétrica gerada
    Cipher aescf = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivspec = new IvParameterSpec(new byte[16]);
    aescf.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(chave, "AES"), ivspec);
    textoCifrado = aescf.doFinal(textoClaro);
    
    // -- C) Cifrando a chave com a chave pública
    Cipher rsacf = Cipher.getInstance("RSA");
    rsacf.init(Cipher.ENCRYPT_MODE, pub);
    chaveCifrada = rsacf.doFinal(chave);
    return new byte[][] { textoCifrado, chaveCifrada };
  }
  
  private static byte[] dencrypt(PrivateKey pvk, SecretKey sk, byte[] textoCifrado) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
    byte[] textoDecifrado = null;
   
    // -- A) Decifrando a chave simétrica com a chave privada
    //Cipher rsacf = Cipher.getInstance("RSA");
    //rsacf.init(Cipher.DECRYPT_MODE, pvk);
    //byte[] chaveDecifrada = rsacf.doFinal(chaveCifrada);
   
    // -- B) Decifrando o texto com a chave simétrica decifrada
    Cipher aescf = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivspec = new IvParameterSpec(new byte[16]);
    aescf.init(Cipher.DECRYPT_MODE, new SecretKeySpec(sk.getEncoded(), "AES"), ivspec);
    textoDecifrado = aescf.doFinal(textoCifrado);
    return textoDecifrado;
  }
  
  private static byte[] dencrypt(PrivateKey pvk, byte[] textoCifrado, byte[] chaveCifrada) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
    byte[] textoDecifrado = null;
   
    // -- A) Decifrando a chave simétrica com a chave privada
    Cipher rsacf = Cipher.getInstance("RSA");
    rsacf.init(Cipher.DECRYPT_MODE, pvk);
    byte[] chaveDecifrada = rsacf.doFinal(chaveCifrada);
   
    // -- B) Decifrando o texto com a chave simétrica decifrada
    Cipher aescf = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivspec = new IvParameterSpec(new byte[16]);
    aescf.init(Cipher.DECRYPT_MODE, new SecretKeySpec(chaveDecifrada, "AES"), ivspec);
    textoDecifrado = aescf.doFinal(textoCifrado);
    return textoDecifrado;
  }
  
  private static byte[] dencrypt(PublicKey pvk, SecretKey sk, byte[] textoCifrado) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
    byte[] textoDecifrado = null;
    // -- A) Decifrando a chave simétrica com a chave privada
    //Cipher rsacf = Cipher.getInstance("RSA");
    //rsacf.init(Cipher.DECRYPT_MODE, pvk);
    //byte[] chaveDecifrada = rsacf.doFinal(chaveCifrada);
    
    // -- B) Decifrando o texto com a chave simétrica decifrada
    Cipher aescf = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivspec = new IvParameterSpec(new byte[16]);
    aescf.init(Cipher.DECRYPT_MODE, new SecretKeySpec(sk.getEncoded(), "AES"), ivspec);
    textoDecifrado = aescf.doFinal(textoCifrado);
    return textoDecifrado;
  }
  
  private static byte[] dencrypt(PublicKey pvk, byte[] textoCifrado, byte[] chaveCifrada) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
    byte[] textoDecifrado = null;
   
    // -- A) Decifrando a chave simétrica com a chave privada
    Cipher rsacf = Cipher.getInstance("RSA");
    rsacf.init(Cipher.DECRYPT_MODE, pvk);
    byte[] chaveDecifrada = rsacf.doFinal(chaveCifrada);
   
    // -- B) Decifrando o texto com a chave simétrica decifrada
    Cipher aescf = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivspec = new IvParameterSpec(new byte[16]);
    aescf.init(Cipher.DECRYPT_MODE, new SecretKeySpec(chaveDecifrada, "AES"), ivspec);
    textoDecifrado = aescf.doFinal(textoCifrado);
    return textoDecifrado;
  }
  
  /*public static PublicKey loadPublicKey(File fPub) throws IOException, ClassNotFoundException {
    ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fPub));
    PublicKey ret = (PublicKey) ois.readObject();
    ois.close();
    return ret;
  }
 
  private PrivateKey loadPrivateKey(File fPvk) throws IOException, ClassNotFoundException {
    ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fPvk));
    PrivateKey ret = (PrivateKey) ois.readObject();
    ois.close();
    return ret;
  }*/

  private static void printHex(byte[] b) {
    /*if (b == null) {
      System.out.println("(null)");
    } else {
      for (int i = 0; i < b.length; ++i) {
        if (i % 16 == 0) {
          System.out.print(Integer.toHexString((i & 0xFFFF) | 0x10000).substring(1, 5) + " - ");
        }
        System.out.print(Integer.toHexString((b[i] & 0xFF) | 0x100).substring(1, 3) + " ");
        if (i % 16 == 15 || i == b.length - 1) {
          int j;
          for (j = 16 - i % 16; j > 1; --j)
            System.out.print("   ");
          System.out.print(" - ");
          int start = (i / 16) * 16;
          int end = (b.length < i + 1) ? b.length : (i + 1);
          for (j = start; j < end; ++j)
            if (b[j] >= 32 && b[j] <= 126)
              System.out.print((char) b[j]);
            else
              System.out.print(".");
          System.out.println();
        }
      }
      System.out.println();
    }*/
  }


}


