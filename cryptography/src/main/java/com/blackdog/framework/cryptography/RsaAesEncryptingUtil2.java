package com.blackdog.framework.cryptography;

import java.io.*;
import java.nio.file.Path;
import java.util.*;
import javax.crypto.*;
import javax.crypto.spec.*;

import org.apache.commons.codec.binary.Base64;


import java.security.*;
import java.security.spec.*;
import java.security.cert.*;

@Deprecated
public class RsaAesEncryptingUtil2 {
  
  public String encrypt(String content, String publicKeyPath, String privateKeyPath) throws Exception {
    
    // -- Cifrando a mensagem "Hello, world!"
    byte[] textoClaro = content.getBytes("UTF-8");
    CarregadorChavePublica ccp = new CarregadorChavePublica();
    PublicKey pub = ccp.carregaChavePublica(new File(publicKeyPath));
    Cifrador cf = new Cifrador();
    byte[][] cifrado = cf.cifra(pub, textoClaro);
    printHex(cifrado[0]);
    printHex(cifrado[1]);

    System.out.println();
    
    String cifrado01 = new String(cifrado[0]);
    String cifrado02 = new String(cifrado[1]);
    
    System.out.println(cifrado01);
    System.out.println(cifrado02);
    
    System.out.println();
    System.out.println(content.length());
    System.out.println(cifrado01.length());
    System.out.println(cifrado02.length());
    System.out.println(cifrado01.length());
    System.out.println(cifrado02.length());
    
    return cifrado01 + "###" + cifrado02;
  }
  
  
  public String decrypt(String content, String privateKeyPath) throws Exception {
    
    
    String[] contents = content.split("###");
    
    // -- Decifrando a mensagem
    CarregadorChavePrivada ccpv = new CarregadorChavePrivada();
    PrivateKey pvk = ccpv.carregaChavePrivada(new File(privateKeyPath));
    Decifrador dcf = new Decifrador();
    byte[] decifrado = dcf.decifra(pvk, contents[0].getBytes(), contents[1].getBytes());
    
    String depriptado = new String(decifrado, "UTF-8");
    
    System.out.println(depriptado);
    
    return depriptado;
  }
  
  
 public void generateRsaKeyPair(String publicKeyPath, String privateKeyPath) throws NoSuchAlgorithmException, InvalidAlgorithmParameterException, CertificateException, KeyStoreException, IOException {
    
   GeradorParChaves gpc = new GeradorParChaves();
   gpc.geraParChaves(new File(publicKeyPath), new File(privateKeyPath));
   
  }
  
  public static void main(String[] args) throws Exception {
    
    String mensagem = "Hellooooooo World !!!!";
    //String mensagem = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgWCgkLGBYPDQwYGCEaGxogICYrKiAmHx8kHSksJCYxJysfOT05Jik3ODo3Iys/RD8sQzQuNzcBCgoKBQULDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAFAAPAMBIgACEQEDEQH/xAAcAAADAAMAAwAAAAAAAAAAAAAABgcEBQgBAgP/xAA7EAABAwMCBAMFBQUJAAAAAAABAgMEAAURBiEHEjFxQWGBExQiUZEVMkKx8AgjJFKhFkNTgpKywdHx/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/ALjRRSxxB1WzpDTj1wWAuQv93Fa/mWenoOp7UGdqPVFl0zHD16npjBX3EdVK7JGSfpSBI476fblltm3SHmB/e4SCeySr8zU/RojUGrIJ1FdJhfmTRztoJySPDsPkBXyl8KL5Dt4lute0T+IJPxJ8+Xx9KC76V13p7VR9nap38TjJjLHIv6Hr/lJpmrji4W+Xp+THfbfU0+khaFD4VJI6HyNdGcJdbK1fYlJmqH2nDIRIxtzA/dXjz3z5jtQPdFFFAVBP2i3nnNQWWK6rERLaljupWFf0CavdQXiPcJM/iAhqXDSuOyswYgJ25lJByoZ8SrtgUFds7TTURKGkgIQEpR2A2/pWasBSSCMg+FK8DV1ihrMGZKVFcb+H2rrakIVjb4V4waYGLjCktF2NMbebSOYrSoEAdwaCL8b2Wst+7xeVxJ/eqCenrSxwZuL8DiDbUMucrcoqYeT/ADAgkD/UEn0qkcQdQaWutvmw/tJLjwSUhLY5yT5Y2z61DLPcpFmu0W4w8e3iLDrYUNsj57j86DtIUVh2mS5MtcOU8j2bjzaHFo+RIBIrMoCphrTTcQa2bvkptC2Xgyy4D1BJKQr8h9Kp9abVVtauVmksrT8WMBY6geOKDQSdFWW9MvLvEcTJDuEl7KhgJO2BnCVfPHXtWsn6Xj6Z4fagiQCA24w4pQSNyrBycnPhgY8qztKyTabfJTcXwmO2ErQ8onoR4k/retbr/W9njaZnRG3lSX5bKm2lIGBlQ2yfDrmgn+jtIWnUOi3npQEeW2pxKJXMTgHBGQBv+um9IlytzX9o126DIMlCnEsod8VEkD86zdP6iVbbJcrcRn3kAtk9AfH60ycC7W1cteJdlNe1ENtUlJO+FZASe+/jQdLNIDTSG09EgJHpXvRRQFeCARg9K81qtQaitOnIZl3iamM3vygn4lY8Ep6k9qCTcVC77+bI1hu3FTSVLySofiI5fly/Pr6Gva/WSDCtwTatJsXGOEKWqQp5QIx4EFQO25pbveu7NftVyLmlhcJQCExZCzzD4P8AEQM7HPnTzYtNw9Y2b7WunMqY+HE+x5lJb8QFJI6g7HmTt33oIWuMtpp+4pY93jr5kx055gTkAgE9cA1Rf2f75bIF+uEW4LDMqelCY7pOASCcp7nIPp2pO1Iy9Zbj9m3yz+ydYzyoCvgIP4k4GCNvCtIJEAKz7iSNtuc/9frNB2lRXJtk4i6ksL6jbJ6vdlY/hnSXU+nNuPQiqNbOPjIhoF1sqjKH3lNKHIfMA7jtk96D5az448yXImko+M7e/OD/AGI/5V9KjdyuU26y1y7lLXKkL6uLUSf/ADtWLRQZtmtki8XOPb4iCp19QRsM4BO5PkOtdWqeRp6xQky1Bao6W44I2zsE7fLpnFQvgddIVq1PLXNVyF1hSW1+YUCR9BVYn3H7SQokAtlQUkbEAYoI9xcvqL5qFLrTodhpThjl8PA/WkKnLXVnVGlv+6ozGaIKV9/lSbQFFFFB/9k=";
    
    String resourcePath = "/home/thiago/Workspaces/Prodepa/Crypto/Criptor-1.0-master/codigo_fonte/java/src/test/resources/keys";
    
    String publicKeyPath = resourcePath.toString() + "/chave.publica";
    String privateKeyPath = resourcePath.toString() + "/chave.privada";
    
    // -- Gera o par de chaves, em dois arquivos (chave.publica e chave.privada)
    GeradorParChaves gpc = new GeradorParChaves();
    gpc.geraParChaves(new File(publicKeyPath), new File(privateKeyPath));
    
    
    // -- Cifrando a mensagem "Hello, world!"
    byte[] textoClaro = mensagem.getBytes("UTF-8");
    CarregadorChavePublica ccp = new CarregadorChavePublica();
    PublicKey pub = ccp.carregaChavePublica(new File(publicKeyPath));
    Cifrador cf = new Cifrador();
    byte[][] cifrado = cf.cifra(pub, textoClaro);
    printHex(cifrado[0]);
    printHex(cifrado[1]);

    System.out.println();
    
    String cifrado01 = Base64.encodeBase64String(cifrado[0]);
    String cifrado02 = Base64.encodeBase64String(cifrado[1]);
    
    System.out.println(cifrado01);
    System.out.println(cifrado02);
    
    System.out.println();
    System.out.println(mensagem.length());
    System.out.println(cifrado01.length());
    System.out.println(cifrado02.length());
    
    
    // -- Decifrando a mensagem
    CarregadorChavePrivada ccpv = new CarregadorChavePrivada();
    PrivateKey pvk = ccpv.carregaChavePrivada(new File(privateKeyPath));
    Decifrador dcf = new Decifrador();
    byte[] decifrado = dcf.decifra(pvk, cifrado[0], cifrado[1]);
    System.out.println(new String(decifrado, "UTF-8"));
  }
 

  
  
  static void printHex(byte[] b) {
    if (b == null) {
      System.out.println("(null)");
    } else {
      for (int i = 0; i < b.length; ++i) {
        if (i % 16 == 0) {
          System.out.print(Integer.toHexString((i & 0xFFFF) | 0x10000).substring(1, 5) + " - ");
        }
        System.out.print(Integer.toHexString((b[i] & 0xFF) | 0x100).substring(1, 3) + " ");
        if (i % 16 == 15 || i == b.length - 1) {
          int j;
          for (j = 16 - i % 16; j > 1; --j)
            System.out.print("   ");
          System.out.print(" - ");
          int start = (i / 16) * 16;
          int end = (b.length < i + 1) ? b.length : (i + 1);
          for (j = start; j < end; ++j)
            if (b[j] >= 32 && b[j] <= 126)
              System.out.print((char) b[j]);
            else
              System.out.print(".");
          System.out.println();
        }
      }
      System.out.println();
    }
  }

  
  
  
  
 
}

class Cifrador {

  public byte[][] cifra(PublicKey pub, byte[] textoClaro) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
    byte[] textoCifrado = null;
    byte[] chaveCifrada = null;
    // -- A) Gerando uma chave simétrica de 128 bits
    KeyGenerator kg = KeyGenerator.getInstance("AES");
    kg.init(128);
    SecretKey sk = kg.generateKey();
    byte[] chave = sk.getEncoded();
    // -- B) Cifrando o texto com a chave simétrica gerada
    Cipher aescf = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivspec = new IvParameterSpec(new byte[16]);
    aescf.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(chave, "AES"), ivspec);
    textoCifrado = aescf.doFinal(textoClaro);
    // -- C) Cifrando a chave com a chave pública
    Cipher rsacf = Cipher.getInstance("RSA");
    rsacf.init(Cipher.ENCRYPT_MODE, pub);
    chaveCifrada = rsacf.doFinal(chave);
    return new byte[][] { textoCifrado, chaveCifrada };
  }
}

class Decifrador {
  public byte[] decifra(PrivateKey pvk, byte[] textoCifrado, byte[] chaveCifrada) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
    byte[] textoDecifrado = null;
    // -- A) Decifrando a chave simétrica com a chave privada
    Cipher rsacf = Cipher.getInstance("RSA");
    rsacf.init(Cipher.DECRYPT_MODE, pvk);
    byte[] chaveDecifrada = rsacf.doFinal(chaveCifrada);
    // -- B) Decifrando o texto com a chave simétrica decifrada
    Cipher aescf = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivspec = new IvParameterSpec(new byte[16]);
    aescf.init(Cipher.DECRYPT_MODE, new SecretKeySpec(chaveDecifrada, "AES"), ivspec);
    textoDecifrado = aescf.doFinal(textoCifrado);
    return textoDecifrado;
  }
}

class CarregadorChavePublica {
  public PublicKey carregaChavePublica(File fPub) throws IOException, ClassNotFoundException {
    ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fPub));
    PublicKey ret = (PublicKey) ois.readObject();
    ois.close();
    return ret;
  }
}

class CarregadorChavePrivada {
  public PrivateKey carregaChavePrivada(File fPvk) throws IOException, ClassNotFoundException {
    ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fPvk));
    PrivateKey ret = (PrivateKey) ois.readObject();
    ois.close();
    return ret;
  }
}

class GeradorParChaves {
  private static final int RSAKEYSIZE = 1024;

  public void geraParChaves(File fPub, File fPvk) throws IOException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, CertificateException, KeyStoreException {
    KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
    kpg.initialize(new RSAKeyGenParameterSpec(RSAKEYSIZE, RSAKeyGenParameterSpec.F4));
    KeyPair kpr = kpg.generateKeyPair();
    PrivateKey priv = kpr.getPrivate();
    PublicKey pub = kpr.getPublic();
    // -- Gravando a chave pública em formato serializado
    ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fPub));
    oos.writeObject(pub);
    oos.close();
    // -- Gravando a chave privada em formato serializado
    // -- Não é a melhor forma (deveria ser guardada em um keystore, e protegida
    // por senha),
    // -- mas isto é só um exemplo
    oos = new ObjectOutputStream(new FileOutputStream(fPvk));
    oos.writeObject(priv);
    oos.close();
  }
}


