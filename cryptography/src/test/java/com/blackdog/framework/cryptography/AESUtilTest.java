package com.blackdog.framework.cryptography;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.security.NoSuchAlgorithmException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.plaf.synth.SynthSeparatorUI;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

public class AESUtilTest {

  
  static final String aeskey = "/opt/as/sistemas/cryptor/keys/openssl3/aes.bin";
  static final String aeskeyB64 = "/opt/as/sistemas/cryptor/keys/openssl3/aes.bin";
  
  @Test
  public void testGenerateKey() throws NoSuchAlgorithmException {
    
    SecretKey key = AESUtil.generateKey(AESUtil.EASKEYSIZE_128);
    
    System.out.println(new String(key.getEncoded()));
    
    assertNotNull(key);
    
  }

  @Test
  public void testSaveKeySecretKeyString() throws NoSuchAlgorithmException, IOException {
    
    SecretKey key = AESUtil.generateKey(AESUtil.EASKEYSIZE_256);
    
    assertNotNull(key);
    
    
    AESUtil.saveKey(key, "/home/thiago/Downloads/TMP/aes256.key");
  }

  @Test
  public void testSaveKeySecretKeyFile() {
    fail("Not yet implemented");
  }
  
  @Test
  public void testGenerateKeySecretKey128() throws NoSuchAlgorithmException, IOException {
    
    SecretKey key = AESUtil.generateKey("1234567890123456111", false, AESUtil.EASKEYSIZE_128);
    
    System.out.println(new String(key.getEncoded()));
    
    
    AESUtil.saveKey(key, "/home/thiago/Downloads/TMP/c-aes128.key");
    
    assertNotNull(key);
    
  }
  
  @Test
  public void testGenerateKeySecretKey256() throws NoSuchAlgorithmException, IOException {
    
    SecretKey key = AESUtil.generateKey("12345678901234561234567890123456", false, AESUtil.EASKEYSIZE_256);
    
    System.out.println(new String(key.getEncoded()));
    
    AESUtil.saveKey(key, "/home/thiago/Downloads/TMP/c-aes256.key");
    
    assertNotNull(key);
    
  }

  
  @Test
  public void testGenerateKeySecretKey128Hash() throws NoSuchAlgorithmException, IOException {
    
    SecretKey key = AESUtil.generateKey("1234567890123456111", true, AESUtil.EASKEYSIZE_128);
    
    System.out.println(new String(key.getEncoded()));
    
    
    AESUtil.saveKey(key, "/home/thiago/Downloads/TMP/c-aes128.key");
    
    assertNotNull(key);
    
  }
  
  @Test
  public void testGenerateKeySecretKey256Hash() throws NoSuchAlgorithmException, IOException {
    
    SecretKey key = AESUtil.generateKey("12345678901234561234567890123456", true, AESUtil.EASKEYSIZE_256);
    
    System.out.println(new String(key.getEncoded()));
    
    AESUtil.saveKey(key, "/home/thiago/Downloads/TMP/c-aes256.key");
    
    assertNotNull(key);
    
  }
  
  
  
  

  @Test
  public void testLoadKeyString() throws IOException {
    
    SecretKey key =  AESUtil.loadKey("/opt/as/sistemas/cryptor/keys/aes.key");
    //SecretKey key =  AESUtil.loadKey(this.getClass().getResource("/").getPath() + "/keys/aes.key");
    
    
    System.out.println(new String(key.getEncoded()));
    System.out.println(String.valueOf(key.getEncoded()));
    System.out.println(Base64.encodeBase64String(key.getEncoded()));
    
    System.out.println(Hex.encodeHex(key.getEncoded()));
    assertNotNull(key);
  }

  @Test
  public void testLoadKeyFile() {
    fail("Not yet implemented");
  }
  
  @Test
  public void testEncrypt() throws Exception {
    
    String original = "TESTE";
    
    String encrypted = AESUtil.encryptKeyPath(original, aeskey, true);
    
    System.out.println(encrypted);
    
    saveFile(encrypted, "/home/thiago/Downloads/TMP/enc.txt");
    
    String decrypted = AESUtil.decryptPath(encrypted, aeskey, true);
    
    System.out.println(decrypted);
    
    assertEquals(original, decrypted);
  }
  
  @Test
  public void testEncryptw() throws Exception {
    
    String original = "TESTE";
    

    for (int i = 0; i < 1000; i++) {
      original = original + "TESTE" + i;
    }
    
    
    String encrypted = AESUtil2.encrypt(original, aeskeyB64, true);
    
    System.out.println(encrypted);
    
    saveFile(encrypted, "/home/thiago/Downloads/TMP/enc.txt");
    
    String decrypted = AESUtil2.decrypt(encrypted, aeskeyB64, true);
    
    System.out.println(decrypted);
    
    assertEquals(original, decrypted);
  }
  
  
  @Test
  public void testDecrypt() throws Exception {
    
    
    String encrypted = "BAMDOHZlTeLXk5ey0ZqbXYbQJ7OqK9UHxEkjMqj90gwpJyN+Xv4gKxuAtZDLslXdJJKBy4KoMidfa9rXEN4w5y6mK54JO03AY1PiNqhRy6QN3WwFUYb7jqRZ2nO0kQNcUGB62AD5DW8pVjBvFhG081IxkBR017veN4W0Gn+cDfl2opswP5eN68I4RCwF9sVPX2Y3Q3zSTEbtHO8dpW7mwFTJoO6LX71F/v/5aSo0ErPmVfxhfICA0/poC3CdyHMmn/yuXQIBaVdvWnYTXIwTfDZTih/CL9XqPuqvkEf6dM3gc7rr3wEy02tRn8UW7oNFOuNtfvKc/rfDk2yFw6/wep4zJaPh24v8p9fXyPrULa/4f5IPTVWU4L8MxbB1gNbGQh9z44OdJs+3/Uq7v76BRmeUhuAsGPUkpF4wt5MIM0fPbRoEdhEG8lpLNuKDcC7ZrllGveN21YTrh9skmlzJpa1+6yVyl2ga/5QGx6mrepsTynvNhSfOoJ3te6a1Lms2h/4E8LSsUXcGx2Kj0apbbuwezGPdh6GdHPzRmdFeLnDczGNtDgmBYgl3KHw0ch//repF5RHFMXCpY5Y1IgGRsihE4PhCwDdd4bR91iZhzWEIj8KZLy3WTyN8I4z2qz0hVC7TYKQVaq1iuB8k7bmtls+iMPts8PM1ELRPh9Y2VQnC+djJKB4aonlawlXNfu7by2bLLLvZC8OrlNycsp0aIdZ/E7JxHncT8TGDL5xfbGp8dJloQK1Z88SXXqoCfPcL0KrCRfzonht+c38Qmbvs2YKWBWO/oXF66cq/siKpQ+j0bHVnD44di8QTxpzF9PBa+YPv1J77ClaV7Vvs1DaGty7fPsCQIYigRWvFc6dy0g5MhE3wdHq1UeDXHOii9mD6fOsTgolbKFOdgQlq3vrd4Ax+dQlS6KVnqTexHvtHNlZBYqTaDeSvYSZ7VeRFr8+vb6xqzXBLc3RZeF9Fp68x3G2yvctdvdE1b13LapltjohI2A50BsGafgvL1sarNpjspoEc/bFUbE5vXfdBitURx5HIUCNUecTJJNLs1kURthZiLPkKgOnb9wSWaEZ445dn+KnJvq6WfMRMp4+aaZYqAtROuAkhv4QlPyK6ox6ZyGDNOKbXE6V5GHWTuU59oWEO14iYHTs/UgZbG/0VKOa0YFrrYaoARvZTmHkooWgoMifEcYej25GlQj3bSjoLffUX4dgimlNEbAS+UxG/rFLnwgTEYqTG1ktQeAHdmqaag+JipTuoFWUDsx6ng3EzNIZilkf8h7Xi4TqgWApemE7K98yixKkpDROx9z0oa2fNXe7mC4VdXaVMUpHWB2kJJaKwBIv9RfpLhqleZf8Ilqz8YOHdVV/8nOa4DB+vCGjxQCz4lWIlvv+Zunt3xxoGpBgC/O/Zt0VZlCoc8zUbyNE9saNQnAVkE+JCfMxTBr+dPMod/cbUdt3aH6NFyGAXhvjpFNRGfry0+rH31groH4n+OWxdtC9c+wgCoj6pQSc9I7YGzmAJAGzCNVDaJ5ZwNg+OjCQ61/v7rKb5ZbZPKJvJsV0SJJMPMaw/Tnk9ZJZZbeRBuybvdOFUrrvwyOBJ9/u8VwDCFTQ3OwS6ckmvVooIgY0cWDqjDQl37BAiW6kzH+rni/p5Fx5s2cCBEHBrAnbsQ7a23c/verviaFqP6/rcswmqcQz2zXO+e8TwqegqYk4PoNQIL5dlX1hmhekumY8GeSZjkM877JCHsBniY7pu2AVN6EaAYuRuKg+22kH0hvKE67xhm9mDjwRjuGe9o/ex0dJH6qTTIihM7yv4CFo2AptaQiNI752yOqJfPEfre09+xeYNXWpn+z8wx769PwSOi4IGteGD1/9OfiTRLSjYb/w1KYy2O34SlNik9vbmkNSqyvd8TCZm4WEGmVcsyF5h9zDFW8jFlNHm9reww6Vp1pAMlApXpywt8V2EhQRbissNNPbR2VRVH+qRLtj+33JT";
    
    String decrypted = AESUtil.decryptPath(encrypted, aeskey, true);
    
    System.out.println(decrypted);
    
  }
  
  
  @Test
  public void testDecrypt2() throws Exception {
    
    
    String encrypted = "U2FsdGVkX18O3nSTtsB2+0nX2lMQbPQnpWgq49gbzfo=";
    
    byte[] encriptedContent = Base64.decodeBase64(encrypted);
    
    Cipher aescf = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivspec = new IvParameterSpec(new byte[16]);
    //aescf.init(Cipher.DECRYPT_MODE, new SecretKeySpec(Base64.decodeBase64("5035bbbde3bdd959b0c07edf755dc1ee"), "AES"), ivspec);
    aescf.init(Cipher.DECRYPT_MODE, new SecretKeySpec(Base64.decodeBase64("5035bbbde3bdd959b0c07edf755dc1ee"), "AES"), ivspec);
    
    byte[] decryptedContent = aescf.doFinal(encriptedContent);
    
    System.out.println(new String(decryptedContent));
    
  }
  
  
  @Test
  public void testDecrypt3() throws Exception {
    
    String original = "TESTE";

    String encrypted = AESUtil.encryptKeyPath(original, aeskey, true);
    
    System.out.println(encrypted);
    
    saveFile(encrypted, "/home/thiago/Downloads/TMP/enc.txt");
  
    
    System.out.println(Base64.decodeBase64("U2FsdGVkX1/dgJQ3WnybscVnUNR1KsWmJ3VTX8wZJNE="));
    
  }
  
  
  @Test
  public void testDecrypt4() throws Exception {
    
    
    KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
    keyGenerator.init(128);
    
    SecretKey k = keyGenerator.generateKey();
    
    System.out.println(k.getAlgorithm());
    System.out.println(k.getFormat());
    
    //FileUtils.writeStringToFile(new File("/home/thiago/Downloads/TMP/aesB64.key"), Base64.encodeBase64String(k.getEncoded()));
    
  }
  
  

  @Test
  public void testLoadBase64() throws Exception {
    
    
    //String bk = FileUtils.readFileToString(new File("/opt/as/sistemas/cryptor/keys/openssl/aes/aes_key.bin"));
    String bk = FileUtils.readFileToString(new File("/opt/as/sistemas/cryptor/keys/openssl/aes/cryptor.key.bin"));
    
    System.out.println(bk);
    
    System.out.println(Hex.encodeHex( Base64.decodeBase64( bk.getBytes()) ));
    
    //SecretKey k = AESUtil.loadKeyBase64(bk);
    
    String encryptedContent = AESUtil.encryptKeyPath("SOMEDATA", bk, true);
    
    System.out.println(encryptedContent);
  }
  
  @Test
  public void testLoadBase642() throws Exception {
    
    
    String encryptedContent = AESUtil.encryptKeyPath("SOMEDATA", "/opt/as/sistemas/cryptor/keys/openssl/aes/id_aes_cryptor.key", true);
    
    System.out.println(encryptedContent);
  }
  
  @Test
  public void testAllw() throws Exception {
    
    System.out.println(new String(  Base64.encodeBase64("prodepa000000000".getBytes() )));
    
  }
  
  @Test
  public void testAll() throws Exception {
    
    
    /////////  PEGAR CHAVE ////////////
    
    byte[] chaveB64 = FileUtils.readFileToByteArray(new File("/opt/as/sistemas/cryptor/keys/openssl2/aes/id_aes.key.bin"));
    
    System.out.println(new String(chaveB64));
    
    
    SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
    KeySpec keySpec = new PBEKeySpec("123456".toCharArray(), new byte[6], 65536, 128);
    SecretKey tmp = factory.generateSecret(keySpec);
    SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");


    
    
    Cipher aescf = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivspec = new IvParameterSpec(new byte[6]);
    aescf.init(Cipher.DECRYPT_MODE, new SecretKeySpec("123456".getBytes(), "AES"), ivspec);
    byte[] CHAVE = aescf.doFinal(chaveB64);
    
    System.out.println(new String(CHAVE));
    
    
    byte[] chaveAes = Base64.decodeBase64(chaveB64);
    
    System.out.println(new String(chaveAes) + "  " + chaveAes.length);
    
    ///////// ENCRIPTAR DADOS ///////////
    Cipher aescf1 = Cipher.getInstance("AES/CBC/PKCS5Padding");
   
    IvParameterSpec ivspec1 = new IvParameterSpec(new byte[16]);
    //IvParameterSpec ivspec = new IvParameterSpec("PRODEPA00000000".getBytes());
    
    aescf1.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(chaveAes, "AES"), ivspec1);
    byte[] encryptedContent = aescf1.doFinal("TESTE".getBytes("UTF-8"));
    
    System.out.println("CONTEUDO ENCRIPTADO " + new String(encryptedContent));
    
    /////////  DECRIPTAR ///////////
    
    Cipher aescf2 = Cipher.getInstance("AES/CBC/PKCS5Padding");
    
    IvParameterSpec ivspec2 = new IvParameterSpec(new byte[16]);
    
    aescf2.init(Cipher.DECRYPT_MODE, new SecretKeySpec(chaveAes, "AES"), ivspec2);
    byte[] encryptedContent3 = aescf2.doFinal(encryptedContent);
    
    System.out.println("CONTEUDO DECRIPTADO " + new String(encryptedContent3));
    
  }
  
  
  @Test
  public void testAll1() throws Exception {
    
    
    
    ///////// ENCRIPTAR DADOS ///////////
    Cipher aescf1 = Cipher.getInstance("AES/CBC/PKCS5Padding");
   
    IvParameterSpec ivspec1 = new IvParameterSpec(new byte[16]);
    //IvParameterSpec ivspec = new IvParameterSpec("PRODEPA00000000".getBytes());
    
    aescf1.init(Cipher.ENCRYPT_MODE, new SecretKeySpec("prodepa".getBytes(), "AES"), ivspec1);
    byte[] encryptedContent = aescf1.doFinal("TESTE".getBytes("UTF-8"));
    
    System.out.println("CONTEUDO ENCRIPTADO " + new String(encryptedContent));
    
    /////////  DECRIPTAR ///////////
    
    Cipher aescf2 = Cipher.getInstance("AES/CBC/PKCS5Padding");
    
    IvParameterSpec ivspec2 = new IvParameterSpec(new byte[16]);
    
    aescf2.init(Cipher.DECRYPT_MODE, new SecretKeySpec("prodepa".getBytes(), "AES"), ivspec2);
    byte[] encryptedContent3 = aescf2.doFinal(encryptedContent);
    
    System.out.println("CONTEUDO DECRIPTADO " + new String(encryptedContent3));
    
  }
  
  
  
  private void descriptChave() throws Exception {
   
    
    InputStream cipherInputStream = null;
    try {
      
        
        String aes = FileUtils.readFileToString(new File("/opt/as/sistemas/cryptor/keys/openssl/aes2/id_aes.json"));
        
      
        final StringBuilder output = new StringBuilder();
        final byte[] secretKey = javax.xml.bind.DatatypeConverter.parseHexBinary("E4A38479A2349177EAE6038A018483318350E7F5430BDC8F82F1974715CB54E5");
        final byte[] initVector = javax.xml.bind.DatatypeConverter.parseHexBinary("629E2E1500B6BA687A385D410D5B08E3");
        
        final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(secretKey, "AES"), new IvParameterSpec(initVector, 0, cipher.getBlockSize()));
        cipherInputStream = new CipherInputStream(new FileInputStream("text_ENCRYPTED"), cipher);

        final String charsetName = "UTF-8";

        final byte[] buffer = new byte[8192];
        int read = cipherInputStream.read(buffer);

        while (read > -1) {
            output.append(new String(buffer, 0, read, charsetName));
            read = cipherInputStream.read(buffer);
        }

        System.out.println(output);
    } finally {
        if (cipherInputStream != null) {
            cipherInputStream.close();
        }
    }
    
  }
  
  
  
  protected static void saveFile(String content, String destFilePath) {
    try {
      
      byte[] data = Base64.decodeBase64(content);
      try (OutputStream stream = new FileOutputStream(destFilePath)) {
          stream.write(data);
      }
      
      //Files.write(Paths.get(new File(destFilePath).getPath()), content.getBytes());
      
    } catch (Exception e) {
      // TODO: handle exception
    }
  }
  
  
  
}
