package com.blackdog.framework.cryptography;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

import javax.crypto.Cipher;

import org.junit.Test;

public class RSAUtilTest {

  static final String privatePEM = "/opt/as/sistemas/cryptor/keys/rsa/id_rsa_cryptor";
  static final String privateDER = "/opt/as/sistemas/cryptor/keys/rsa/id_rsa_cryptor.der";
  static final String publicPEM  = "/opt/as/sistemas/cryptor/keys/rsa/id_rsa_cryptor.pub";
  static final String publicDER  = "/opt/as/sistemas/cryptor/keys/rsa/id_rsa_cryptor.pub.der";
  
  
  @Test
  public void testGeneratePemKeyPairInt() {
    fail("Not yet implemented");
  }

  @Test
  public void testGeneratePemKeyPairFileFile() {
    fail("Not yet implemented");
  }

  @Test
  public void testEncrypt() {
    fail("Not yet implemented");
  }

  @Test
  public void testDecrypt() {
    fail("Not yet implemented");
  }

  @Test
  public void testLoadPrivateKeyPemString() throws ClassNotFoundException, IOException, NoSuchAlgorithmException, InvalidKeySpecException {
    
    //PrivateKey key = RSAUtil.loadPrivateKeyPem("/opt/as/sistemas/cryptor/keys/chave.privada");
    //PrivateKey key = RSAUtil.loadPrivateKeyDer("/opt/as/sistemas/cryptor/keys/chave.privada");
    
    PrivateKey key = RSAUtil.loadPrivateKeyPem(privatePEM);
    
    
    assertNotNull(key);
    
    System.out.println(key.getAlgorithm());
    System.out.println(key.getFormat());
  }
  
  @Test
  public void testLoadPublicKeyPemString() throws Exception {
    
    
    RSAUtil.loadPublicKeyPem("");
    
  }
  
  @Test
  public void testLoadPublicKeyPemFile() {
    fail("Not yet implemented");
  }
  

  @Test
  public void testLoadPrivateKeyPemFile() {
    fail("Not yet implemented");
  }
  
  
  /*
   * DER
   */

  @Test
  public void testLoadPublicKeyDerFile() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
    
    File f = new File(publicDER);
    
    PublicKey key = RSAUtil.loadPublicKeyDer(f);
    
    assertNotNull(key);
    
    assertEquals("RSA", key.getAlgorithm());
    assertEquals("X.509", key.getFormat());
  }

  @Test
  public void testLoadPublicKeyDerString() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
    PublicKey key = RSAUtil.loadPublicKeyDer(publicDER);
    
    assertNotNull(key);
    
    assertEquals("RSA", key.getAlgorithm());
    assertEquals("X.509", key.getFormat());
  }

  @Test
  public void testLoadPrivateKeyDerFile() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
    
    File f = new File(privateDER);
    
    PrivateKey key = RSAUtil.loadPrivateKeyDer(f);
    
    assertNotNull(key);
    
    assertEquals("RSA", key.getAlgorithm());
    assertEquals("PKCS#8", key.getFormat());
  }

  @Test
  public void testLoadPrivateKeyDerString() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
    PrivateKey key = RSAUtil.loadPrivateKeyDer(privateDER);
    
    assertNotNull(key);
    
    assertEquals("RSA", key.getAlgorithm());
    assertEquals("PKCS#8", key.getFormat());
  }

  
  
  @Test
  public void testSign() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException, Exception {
    
    
    String signature = RSAUtil.sign("TESTE", RSAUtil.loadPrivateKeyDer(privateDER));
    
    System.out.println(signature.length());
    System.out.println(signature);
    
    String sing = "nD581+mhH//WZiqIfARKAep+DwKKbf6WI6nOBrEuPL88AzcNqbamm2caqhqx9ZEfdAKrBHORfTw5qGnYv9llI4U4wKUOcmH0s0U+wHIqSOK2+X9oUJvU7/7b0COyQw1DiFfAZ++U+GBp2kuskMRYy88vNB1YpjnABLdl8x2Hlq4=";
     
    Boolean isOK = RSAUtil.verify("TESTE", sing, RSAUtil.loadPublicKeyDer(publicDER));
    
    assertTrue(isOK);
    
    
  }
  
  
  @Test
  public void testENC() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException, Exception {
    
    
   /* ObjectInputStream ois = new ObjectInputStream(new FileInputStream("/opt/as/sistemas/cryptor/keys/openssl/rsa/id_rsa.key"));
    PrivateKey privateKey = (PrivateKey) ois.readObject();
    ois.close();*/

    
    
    byte[] keyBytes = Files.readAllBytes(Paths.get("/opt/as/sistemas/cryptor/keys/openssl/rsa/id_rsa.der"));
    

    PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
    
    KeyFactory kf = KeyFactory.getInstance("RSA");
    
    PrivateKey privateKey = kf.generatePrivate(spec);
    
    String message = "TESTE";
    
    Cipher cipher = Cipher.getInstance("RSA");
    cipher.init(Cipher.ENCRYPT_MODE, privateKey);

    byte[] encriptado = cipher.doFinal(message.getBytes());
    
    System.out.println(new String(encriptado));
    
    
  }
}
