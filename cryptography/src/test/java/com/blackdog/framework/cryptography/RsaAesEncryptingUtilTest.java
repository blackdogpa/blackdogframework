package com.blackdog.framework.cryptography;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

public class RsaAesEncryptingUtilTest {

  
  //static String resourcePath = "/home/thiago/Workspaces/Prodepa/Crypto/Criptor-1.0-master/codigo_fonte/java/src/test/resources/keys";
  //static String publicKeyPath  = resourcePath + "/chave.publica";
  //static String privateKeyPath = resourcePath + "/chave.privada";
  
  
  static String resourcePath = "/opt/as/sistemas/cryptor/keys/rsa";
  
  static String publicKeyPath  = resourcePath + "/id_rsa_gitlab_prodepa.pub";
  static String privateKeyPath = resourcePath + "/id_rsa_gitlab_prodepa";
  
  static String easKeyPath     = resourcePath + "/aes.key";
  
  @Test
  public void testEncryptSendServerToClientChaveEstatica() throws Exception {
    
    String plain = "TEXTO";
    //String plain = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgWCgkLGBYPDQwYGCEaGxogICYrKiAmHx8kHSksJCYxJysfOT05Jik3ODo3Iys/RD8sQzQuNzcBCgoKBQULDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAFAAPAMBIgACEQEDEQH/xAAcAAADAAMAAwAAAAAAAAAAAAAABgcEBQgBAgP/xAA7EAABAwMCBAMFBQUJAAAAAAABAgMEAAURBiEHEjFxQWGBExQiUZEVMkKx8AgjJFKhFkNTgpKywdHx/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/ALjRRSxxB1WzpDTj1wWAuQv93Fa/mWenoOp7UGdqPVFl0zHD16npjBX3EdVK7JGSfpSBI476fblltm3SHmB/e4SCeySr8zU/RojUGrIJ1FdJhfmTRztoJySPDsPkBXyl8KL5Dt4lute0T+IJPxJ8+Xx9KC76V13p7VR9nap38TjJjLHIv6Hr/lJpmrji4W+Xp+THfbfU0+khaFD4VJI6HyNdGcJdbK1fYlJmqH2nDIRIxtzA/dXjz3z5jtQPdFFFAVBP2i3nnNQWWK6rERLaljupWFf0CavdQXiPcJM/iAhqXDSuOyswYgJ25lJByoZ8SrtgUFds7TTURKGkgIQEpR2A2/pWasBSSCMg+FK8DV1ihrMGZKVFcb+H2rrakIVjb4V4waYGLjCktF2NMbebSOYrSoEAdwaCL8b2Wst+7xeVxJ/eqCenrSxwZuL8DiDbUMucrcoqYeT/ADAgkD/UEn0qkcQdQaWutvmw/tJLjwSUhLY5yT5Y2z61DLPcpFmu0W4w8e3iLDrYUNsj57j86DtIUVh2mS5MtcOU8j2bjzaHFo+RIBIrMoCphrTTcQa2bvkptC2Xgyy4D1BJKQr8h9Kp9abVVtauVmksrT8WMBY6geOKDQSdFWW9MvLvEcTJDuEl7KhgJO2BnCVfPHXtWsn6Xj6Z4fagiQCA24w4pQSNyrBycnPhgY8qztKyTabfJTcXwmO2ErQ8onoR4k/retbr/W9njaZnRG3lSX5bKm2lIGBlQ2yfDrmgn+jtIWnUOi3npQEeW2pxKJXMTgHBGQBv+um9IlytzX9o126DIMlCnEsod8VEkD86zdP6iVbbJcrcRn3kAtk9AfH60ycC7W1cteJdlNe1ENtUlJO+FZASe+/jQdLNIDTSG09EgJHpXvRRQFeCARg9K81qtQaitOnIZl3iamM3vygn4lY8Ep6k9qCTcVC77+bI1hu3FTSVLySofiI5fly/Pr6Gva/WSDCtwTatJsXGOEKWqQp5QIx4EFQO25pbveu7NftVyLmlhcJQCExZCzzD4P8AEQM7HPnTzYtNw9Y2b7WunMqY+HE+x5lJb8QFJI6g7HmTt33oIWuMtpp+4pY93jr5kx055gTkAgE9cA1Rf2f75bIF+uEW4LDMqelCY7pOASCcp7nIPp2pO1Iy9Zbj9m3yz+ydYzyoCvgIP4k4GCNvCtIJEAKz7iSNtuc/9frNB2lRXJtk4i6ksL6jbJ6vdlY/hnSXU+nNuPQiqNbOPjIhoF1sqjKH3lNKHIfMA7jtk96D5az448yXImko+M7e/OD/AGI/5V9KjdyuU26y1y7lLXKkL6uLUSf/ADtWLRQZtmtki8XOPb4iCp19QRsM4BO5PkOtdWqeRp6xQky1Bao6W44I2zsE7fLpnFQvgddIVq1PLXNVyF1hSW1+YUCR9BVYn3H7SQokAtlQUkbEAYoI9xcvqL5qFLrTodhpThjl8PA/WkKnLXVnVGlv+6ozGaIKV9/lSbQFFFFB/9k=";
    
    RsaAesEncryptingUtil util = new RsaAesEncryptingUtil();
    
    String encriptado = util.encryptWithPrivate(plain, privateKeyPath, easKeyPath, true);
    
    String decriptado = util.decryptWithPublic(encriptado, publicKeyPath, easKeyPath, true);
    
    System.out.println(">>>>> ENCRYPT Result");
    System.out.println(plain);
    System.out.println(encriptado);
    System.out.println(decriptado);
    
    assertEquals(plain, decriptado);
  }
  
  
  @Test
  public void testEncryptSendServerToClientChaveDinamica() throws Exception {
    
    //String plain = "TEXTO";
    String plain = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgWCgkLGBYPDQwYGCEaGxogICYrKiAmHx8kHSksJCYxJysfOT05Jik3ODo3Iys/RD8sQzQuNzcBCgoKBQULDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAFAAPAMBIgACEQEDEQH/xAAcAAADAAMAAwAAAAAAAAAAAAAABgcEBQgBAgP/xAA7EAABAwMCBAMFBQUJAAAAAAABAgMEAAURBiEHEjFxQWGBExQiUZEVMkKx8AgjJFKhFkNTgpKywdHx/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/ALjRRSxxB1WzpDTj1wWAuQv93Fa/mWenoOp7UGdqPVFl0zHD16npjBX3EdVK7JGSfpSBI476fblltm3SHmB/e4SCeySr8zU/RojUGrIJ1FdJhfmTRztoJySPDsPkBXyl8KL5Dt4lute0T+IJPxJ8+Xx9KC76V13p7VR9nap38TjJjLHIv6Hr/lJpmrji4W+Xp+THfbfU0+khaFD4VJI6HyNdGcJdbK1fYlJmqH2nDIRIxtzA/dXjz3z5jtQPdFFFAVBP2i3nnNQWWK6rERLaljupWFf0CavdQXiPcJM/iAhqXDSuOyswYgJ25lJByoZ8SrtgUFds7TTURKGkgIQEpR2A2/pWasBSSCMg+FK8DV1ihrMGZKVFcb+H2rrakIVjb4V4waYGLjCktF2NMbebSOYrSoEAdwaCL8b2Wst+7xeVxJ/eqCenrSxwZuL8DiDbUMucrcoqYeT/ADAgkD/UEn0qkcQdQaWutvmw/tJLjwSUhLY5yT5Y2z61DLPcpFmu0W4w8e3iLDrYUNsj57j86DtIUVh2mS5MtcOU8j2bjzaHFo+RIBIrMoCphrTTcQa2bvkptC2Xgyy4D1BJKQr8h9Kp9abVVtauVmksrT8WMBY6geOKDQSdFWW9MvLvEcTJDuEl7KhgJO2BnCVfPHXtWsn6Xj6Z4fagiQCA24w4pQSNyrBycnPhgY8qztKyTabfJTcXwmO2ErQ8onoR4k/retbr/W9njaZnRG3lSX5bKm2lIGBlQ2yfDrmgn+jtIWnUOi3npQEeW2pxKJXMTgHBGQBv+um9IlytzX9o126DIMlCnEsod8VEkD86zdP6iVbbJcrcRn3kAtk9AfH60ycC7W1cteJdlNe1ENtUlJO+FZASe+/jQdLNIDTSG09EgJHpXvRRQFeCARg9K81qtQaitOnIZl3iamM3vygn4lY8Ep6k9qCTcVC77+bI1hu3FTSVLySofiI5fly/Pr6Gva/WSDCtwTatJsXGOEKWqQp5QIx4EFQO25pbveu7NftVyLmlhcJQCExZCzzD4P8AEQM7HPnTzYtNw9Y2b7WunMqY+HE+x5lJb8QFJI6g7HmTt33oIWuMtpp+4pY93jr5kx055gTkAgE9cA1Rf2f75bIF+uEW4LDMqelCY7pOASCcp7nIPp2pO1Iy9Zbj9m3yz+ydYzyoCvgIP4k4GCNvCtIJEAKz7iSNtuc/9frNB2lRXJtk4i6ksL6jbJ6vdlY/hnSXU+nNuPQiqNbOPjIhoF1sqjKH3lNKHIfMA7jtk96D5az448yXImko+M7e/OD/AGI/5V9KjdyuU26y1y7lLXKkL6uLUSf/ADtWLRQZtmtki8XOPb4iCp19QRsM4BO5PkOtdWqeRp6xQky1Bao6W44I2zsE7fLpnFQvgddIVq1PLXNVyF1hSW1+YUCR9BVYn3H7SQokAtlQUkbEAYoI9xcvqL5qFLrTodhpThjl8PA/WkKnLXVnVGlv+6ozGaIKV9/lSbQFFFFB/9k=";
    
    RsaAesEncryptingUtil util = new RsaAesEncryptingUtil();
    
    String encriptado = util.encryptWithPrivate(plain, privateKeyPath, true);
    
    String decriptado = util.decryptWithPublic(encriptado, publicKeyPath, true);
    
    System.out.println(">>>>> ENCRYPT Result");
    System.out.println(plain);
    System.out.println(encriptado);
    System.out.println("CONTENT: "+encriptado.split("##")[0]);
    System.out.println("CHAVE: "+encriptado.split("##")[1].length());
    System.out.println(decriptado);
    
    assertEquals(plain, decriptado);
  }
  
  
  @Test
  public void testEncryptSendClientToServer() throws Exception {
    
    String plain = "TEXTO";
    //String plain = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgWCgkLGBYPDQwYGCEaGxogICYrKiAmHx8kHSksJCYxJysfOT05Jik3ODo3Iys/RD8sQzQuNzcBCgoKBQULDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAFAAPAMBIgACEQEDEQH/xAAcAAADAAMAAwAAAAAAAAAAAAAABgcEBQgBAgP/xAA7EAABAwMCBAMFBQUJAAAAAAABAgMEAAURBiEHEjFxQWGBExQiUZEVMkKx8AgjJFKhFkNTgpKywdHx/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/ALjRRSxxB1WzpDTj1wWAuQv93Fa/mWenoOp7UGdqPVFl0zHD16npjBX3EdVK7JGSfpSBI476fblltm3SHmB/e4SCeySr8zU/RojUGrIJ1FdJhfmTRztoJySPDsPkBXyl8KL5Dt4lute0T+IJPxJ8+Xx9KC76V13p7VR9nap38TjJjLHIv6Hr/lJpmrji4W+Xp+THfbfU0+khaFD4VJI6HyNdGcJdbK1fYlJmqH2nDIRIxtzA/dXjz3z5jtQPdFFFAVBP2i3nnNQWWK6rERLaljupWFf0CavdQXiPcJM/iAhqXDSuOyswYgJ25lJByoZ8SrtgUFds7TTURKGkgIQEpR2A2/pWasBSSCMg+FK8DV1ihrMGZKVFcb+H2rrakIVjb4V4waYGLjCktF2NMbebSOYrSoEAdwaCL8b2Wst+7xeVxJ/eqCenrSxwZuL8DiDbUMucrcoqYeT/ADAgkD/UEn0qkcQdQaWutvmw/tJLjwSUhLY5yT5Y2z61DLPcpFmu0W4w8e3iLDrYUNsj57j86DtIUVh2mS5MtcOU8j2bjzaHFo+RIBIrMoCphrTTcQa2bvkptC2Xgyy4D1BJKQr8h9Kp9abVVtauVmksrT8WMBY6geOKDQSdFWW9MvLvEcTJDuEl7KhgJO2BnCVfPHXtWsn6Xj6Z4fagiQCA24w4pQSNyrBycnPhgY8qztKyTabfJTcXwmO2ErQ8onoR4k/retbr/W9njaZnRG3lSX5bKm2lIGBlQ2yfDrmgn+jtIWnUOi3npQEeW2pxKJXMTgHBGQBv+um9IlytzX9o126DIMlCnEsod8VEkD86zdP6iVbbJcrcRn3kAtk9AfH60ycC7W1cteJdlNe1ENtUlJO+FZASe+/jQdLNIDTSG09EgJHpXvRRQFeCARg9K81qtQaitOnIZl3iamM3vygn4lY8Ep6k9qCTcVC77+bI1hu3FTSVLySofiI5fly/Pr6Gva/WSDCtwTatJsXGOEKWqQp5QIx4EFQO25pbveu7NftVyLmlhcJQCExZCzzD4P8AEQM7HPnTzYtNw9Y2b7WunMqY+HE+x5lJb8QFJI6g7HmTt33oIWuMtpp+4pY93jr5kx055gTkAgE9cA1Rf2f75bIF+uEW4LDMqelCY7pOASCcp7nIPp2pO1Iy9Zbj9m3yz+ydYzyoCvgIP4k4GCNvCtIJEAKz7iSNtuc/9frNB2lRXJtk4i6ksL6jbJ6vdlY/hnSXU+nNuPQiqNbOPjIhoF1sqjKH3lNKHIfMA7jtk96D5az448yXImko+M7e/OD/AGI/5V9KjdyuU26y1y7lLXKkL6uLUSf/ADtWLRQZtmtki8XOPb4iCp19QRsM4BO5PkOtdWqeRp6xQky1Bao6W44I2zsE7fLpnFQvgddIVq1PLXNVyF1hSW1+YUCR9BVYn3H7SQokAtlQUkbEAYoI9xcvqL5qFLrTodhpThjl8PA/WkKnLXVnVGlv+6ozGaIKV9/lSbQFFFFB/9k=";
    
    RsaAesEncryptingUtil util = new RsaAesEncryptingUtil();
    
    String encriptado = util.encryptWithPublic(plain, publicKeyPath, easKeyPath, true);
    
    String decriptado = util.decryptWithPrivate(encriptado, privateKeyPath, easKeyPath, true);
    
    System.out.println(">>>>> ENCRYPT Result");
    System.out.println(plain);
    System.out.println(encriptado);
    System.out.println(decriptado);
    
    assertEquals(plain, decriptado);
  }
  
  
  @Test
  public void testEncryptSendClientToServerChaveDinamica() throws Exception {
    
    String plain = "TEXTO";
    //String plain = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgWCgkLGBYPDQwYGCEaGxogICYrKiAmHx8kHSksJCYxJysfOT05Jik3ODo3Iys/RD8sQzQuNzcBCgoKBQULDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAFAAPAMBIgACEQEDEQH/xAAcAAADAAMAAwAAAAAAAAAAAAAABgcEBQgBAgP/xAA7EAABAwMCBAMFBQUJAAAAAAABAgMEAAURBiEHEjFxQWGBExQiUZEVMkKx8AgjJFKhFkNTgpKywdHx/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/ALjRRSxxB1WzpDTj1wWAuQv93Fa/mWenoOp7UGdqPVFl0zHD16npjBX3EdVK7JGSfpSBI476fblltm3SHmB/e4SCeySr8zU/RojUGrIJ1FdJhfmTRztoJySPDsPkBXyl8KL5Dt4lute0T+IJPxJ8+Xx9KC76V13p7VR9nap38TjJjLHIv6Hr/lJpmrji4W+Xp+THfbfU0+khaFD4VJI6HyNdGcJdbK1fYlJmqH2nDIRIxtzA/dXjz3z5jtQPdFFFAVBP2i3nnNQWWK6rERLaljupWFf0CavdQXiPcJM/iAhqXDSuOyswYgJ25lJByoZ8SrtgUFds7TTURKGkgIQEpR2A2/pWasBSSCMg+FK8DV1ihrMGZKVFcb+H2rrakIVjb4V4waYGLjCktF2NMbebSOYrSoEAdwaCL8b2Wst+7xeVxJ/eqCenrSxwZuL8DiDbUMucrcoqYeT/ADAgkD/UEn0qkcQdQaWutvmw/tJLjwSUhLY5yT5Y2z61DLPcpFmu0W4w8e3iLDrYUNsj57j86DtIUVh2mS5MtcOU8j2bjzaHFo+RIBIrMoCphrTTcQa2bvkptC2Xgyy4D1BJKQr8h9Kp9abVVtauVmksrT8WMBY6geOKDQSdFWW9MvLvEcTJDuEl7KhgJO2BnCVfPHXtWsn6Xj6Z4fagiQCA24w4pQSNyrBycnPhgY8qztKyTabfJTcXwmO2ErQ8onoR4k/retbr/W9njaZnRG3lSX5bKm2lIGBlQ2yfDrmgn+jtIWnUOi3npQEeW2pxKJXMTgHBGQBv+um9IlytzX9o126DIMlCnEsod8VEkD86zdP6iVbbJcrcRn3kAtk9AfH60ycC7W1cteJdlNe1ENtUlJO+FZASe+/jQdLNIDTSG09EgJHpXvRRQFeCARg9K81qtQaitOnIZl3iamM3vygn4lY8Ep6k9qCTcVC77+bI1hu3FTSVLySofiI5fly/Pr6Gva/WSDCtwTatJsXGOEKWqQp5QIx4EFQO25pbveu7NftVyLmlhcJQCExZCzzD4P8AEQM7HPnTzYtNw9Y2b7WunMqY+HE+x5lJb8QFJI6g7HmTt33oIWuMtpp+4pY93jr5kx055gTkAgE9cA1Rf2f75bIF+uEW4LDMqelCY7pOASCcp7nIPp2pO1Iy9Zbj9m3yz+ydYzyoCvgIP4k4GCNvCtIJEAKz7iSNtuc/9frNB2lRXJtk4i6ksL6jbJ6vdlY/hnSXU+nNuPQiqNbOPjIhoF1sqjKH3lNKHIfMA7jtk96D5az448yXImko+M7e/OD/AGI/5V9KjdyuU26y1y7lLXKkL6uLUSf/ADtWLRQZtmtki8XOPb4iCp19QRsM4BO5PkOtdWqeRp6xQky1Bao6W44I2zsE7fLpnFQvgddIVq1PLXNVyF1hSW1+YUCR9BVYn3H7SQokAtlQUkbEAYoI9xcvqL5qFLrTodhpThjl8PA/WkKnLXVnVGlv+6ozGaIKV9/lSbQFFFFB/9k=";
    
    RsaAesEncryptingUtil util = new RsaAesEncryptingUtil();
    
    String encriptado = util.encryptWithPublic(plain, publicKeyPath, true);
    
    String decriptado = util.decryptWithPrivate(encriptado, privateKeyPath, true);
    
    System.out.println(">>>>> ENCRYPT Result");
    System.out.println(plain);
    System.out.println(encriptado);
    System.out.println(decriptado);
    
    assertEquals(plain, decriptado);
  }
  
  
  @Test(expected = javax.crypto.BadPaddingException.class)
  public void testEncryptSendServerToServerFail() throws Exception {
    
    String plain = "TEXTO";
    //String plain = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgWCgkLGBYPDQwYGCEaGxogICYrKiAmHx8kHSksJCYxJysfOT05Jik3ODo3Iys/RD8sQzQuNzcBCgoKBQULDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAFAAPAMBIgACEQEDEQH/xAAcAAADAAMAAwAAAAAAAAAAAAAABgcEBQgBAgP/xAA7EAABAwMCBAMFBQUJAAAAAAABAgMEAAURBiEHEjFxQWGBExQiUZEVMkKx8AgjJFKhFkNTgpKywdHx/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/ALjRRSxxB1WzpDTj1wWAuQv93Fa/mWenoOp7UGdqPVFl0zHD16npjBX3EdVK7JGSfpSBI476fblltm3SHmB/e4SCeySr8zU/RojUGrIJ1FdJhfmTRztoJySPDsPkBXyl8KL5Dt4lute0T+IJPxJ8+Xx9KC76V13p7VR9nap38TjJjLHIv6Hr/lJpmrji4W+Xp+THfbfU0+khaFD4VJI6HyNdGcJdbK1fYlJmqH2nDIRIxtzA/dXjz3z5jtQPdFFFAVBP2i3nnNQWWK6rERLaljupWFf0CavdQXiPcJM/iAhqXDSuOyswYgJ25lJByoZ8SrtgUFds7TTURKGkgIQEpR2A2/pWasBSSCMg+FK8DV1ihrMGZKVFcb+H2rrakIVjb4V4waYGLjCktF2NMbebSOYrSoEAdwaCL8b2Wst+7xeVxJ/eqCenrSxwZuL8DiDbUMucrcoqYeT/ADAgkD/UEn0qkcQdQaWutvmw/tJLjwSUhLY5yT5Y2z61DLPcpFmu0W4w8e3iLDrYUNsj57j86DtIUVh2mS5MtcOU8j2bjzaHFo+RIBIrMoCphrTTcQa2bvkptC2Xgyy4D1BJKQr8h9Kp9abVVtauVmksrT8WMBY6geOKDQSdFWW9MvLvEcTJDuEl7KhgJO2BnCVfPHXtWsn6Xj6Z4fagiQCA24w4pQSNyrBycnPhgY8qztKyTabfJTcXwmO2ErQ8onoR4k/retbr/W9njaZnRG3lSX5bKm2lIGBlQ2yfDrmgn+jtIWnUOi3npQEeW2pxKJXMTgHBGQBv+um9IlytzX9o126DIMlCnEsod8VEkD86zdP6iVbbJcrcRn3kAtk9AfH60ycC7W1cteJdlNe1ENtUlJO+FZASe+/jQdLNIDTSG09EgJHpXvRRQFeCARg9K81qtQaitOnIZl3iamM3vygn4lY8Ep6k9qCTcVC77+bI1hu3FTSVLySofiI5fly/Pr6Gva/WSDCtwTatJsXGOEKWqQp5QIx4EFQO25pbveu7NftVyLmlhcJQCExZCzzD4P8AEQM7HPnTzYtNw9Y2b7WunMqY+HE+x5lJb8QFJI6g7HmTt33oIWuMtpp+4pY93jr5kx055gTkAgE9cA1Rf2f75bIF+uEW4LDMqelCY7pOASCcp7nIPp2pO1Iy9Zbj9m3yz+ydYzyoCvgIP4k4GCNvCtIJEAKz7iSNtuc/9frNB2lRXJtk4i6ksL6jbJ6vdlY/hnSXU+nNuPQiqNbOPjIhoF1sqjKH3lNKHIfMA7jtk96D5az448yXImko+M7e/OD/AGI/5V9KjdyuU26y1y7lLXKkL6uLUSf/ADtWLRQZtmtki8XOPb4iCp19QRsM4BO5PkOtdWqeRp6xQky1Bao6W44I2zsE7fLpnFQvgddIVq1PLXNVyF1hSW1+YUCR9BVYn3H7SQokAtlQUkbEAYoI9xcvqL5qFLrTodhpThjl8PA/WkKnLXVnVGlv+6ozGaIKV9/lSbQFFFFB/9k=";
    
    RsaAesEncryptingUtil util = new RsaAesEncryptingUtil();
    
    String encriptado = util.encryptWithPrivate(plain, privateKeyPath, easKeyPath, true);
    
    String decriptado = util.decryptWithPrivate(encriptado, privateKeyPath, easKeyPath, true);
    
    System.out.println(">>>>> ENCRYPT Result");
    System.out.println(plain);
    System.out.println(encriptado);
    System.out.println(decriptado);
    
    assertEquals(plain, decriptado);
  }
  

  @Test(expected = javax.crypto.BadPaddingException.class)
  public void testEncryptSendClientToClientFail() throws Exception {
    
    String plain = "TEXTO";
    //String plain = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgWCgkLGBYPDQwYGCEaGxogICYrKiAmHx8kHSksJCYxJysfOT05Jik3ODo3Iys/RD8sQzQuNzcBCgoKBQULDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAFAAPAMBIgACEQEDEQH/xAAcAAADAAMAAwAAAAAAAAAAAAAABgcEBQgBAgP/xAA7EAABAwMCBAMFBQUJAAAAAAABAgMEAAURBiEHEjFxQWGBExQiUZEVMkKx8AgjJFKhFkNTgpKywdHx/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/ALjRRSxxB1WzpDTj1wWAuQv93Fa/mWenoOp7UGdqPVFl0zHD16npjBX3EdVK7JGSfpSBI476fblltm3SHmB/e4SCeySr8zU/RojUGrIJ1FdJhfmTRztoJySPDsPkBXyl8KL5Dt4lute0T+IJPxJ8+Xx9KC76V13p7VR9nap38TjJjLHIv6Hr/lJpmrji4W+Xp+THfbfU0+khaFD4VJI6HyNdGcJdbK1fYlJmqH2nDIRIxtzA/dXjz3z5jtQPdFFFAVBP2i3nnNQWWK6rERLaljupWFf0CavdQXiPcJM/iAhqXDSuOyswYgJ25lJByoZ8SrtgUFds7TTURKGkgIQEpR2A2/pWasBSSCMg+FK8DV1ihrMGZKVFcb+H2rrakIVjb4V4waYGLjCktF2NMbebSOYrSoEAdwaCL8b2Wst+7xeVxJ/eqCenrSxwZuL8DiDbUMucrcoqYeT/ADAgkD/UEn0qkcQdQaWutvmw/tJLjwSUhLY5yT5Y2z61DLPcpFmu0W4w8e3iLDrYUNsj57j86DtIUVh2mS5MtcOU8j2bjzaHFo+RIBIrMoCphrTTcQa2bvkptC2Xgyy4D1BJKQr8h9Kp9abVVtauVmksrT8WMBY6geOKDQSdFWW9MvLvEcTJDuEl7KhgJO2BnCVfPHXtWsn6Xj6Z4fagiQCA24w4pQSNyrBycnPhgY8qztKyTabfJTcXwmO2ErQ8onoR4k/retbr/W9njaZnRG3lSX5bKm2lIGBlQ2yfDrmgn+jtIWnUOi3npQEeW2pxKJXMTgHBGQBv+um9IlytzX9o126DIMlCnEsod8VEkD86zdP6iVbbJcrcRn3kAtk9AfH60ycC7W1cteJdlNe1ENtUlJO+FZASe+/jQdLNIDTSG09EgJHpXvRRQFeCARg9K81qtQaitOnIZl3iamM3vygn4lY8Ep6k9qCTcVC77+bI1hu3FTSVLySofiI5fly/Pr6Gva/WSDCtwTatJsXGOEKWqQp5QIx4EFQO25pbveu7NftVyLmlhcJQCExZCzzD4P8AEQM7HPnTzYtNw9Y2b7WunMqY+HE+x5lJb8QFJI6g7HmTt33oIWuMtpp+4pY93jr5kx055gTkAgE9cA1Rf2f75bIF+uEW4LDMqelCY7pOASCcp7nIPp2pO1Iy9Zbj9m3yz+ydYzyoCvgIP4k4GCNvCtIJEAKz7iSNtuc/9frNB2lRXJtk4i6ksL6jbJ6vdlY/hnSXU+nNuPQiqNbOPjIhoF1sqjKH3lNKHIfMA7jtk96D5az448yXImko+M7e/OD/AGI/5V9KjdyuU26y1y7lLXKkL6uLUSf/ADtWLRQZtmtki8XOPb4iCp19QRsM4BO5PkOtdWqeRp6xQky1Bao6W44I2zsE7fLpnFQvgddIVq1PLXNVyF1hSW1+YUCR9BVYn3H7SQokAtlQUkbEAYoI9xcvqL5qFLrTodhpThjl8PA/WkKnLXVnVGlv+6ozGaIKV9/lSbQFFFFB/9k=";
    
    RsaAesEncryptingUtil util = new RsaAesEncryptingUtil();
    
    String encriptado = util.encryptWithPublic(plain, publicKeyPath, easKeyPath, true);
    
    String decriptado = util.decryptWithPublic(encriptado, publicKeyPath, easKeyPath, true);
    
    System.out.println(">>>>> ENCRYPT Result");
    System.out.println(plain);
    System.out.println(encriptado);
    System.out.println(decriptado);
    
    assertEquals(plain, decriptado);
  }
  
  

  String chars = "abcdefgaijlmnodxyz";
  Random rnd = new Random();
  @Test
  public void testEncryptSendServerToClientNumeros() throws Exception {
    
    /*StringBuffer base64 = new StringBuffer();
    for (int i = 0; i < 100; i++) {
      base64.append(i);
    }*/
    
    StringBuffer base64 = new StringBuffer();
    for (int i = 0; i < 1000; i++) {
      base64.append(chars.charAt(rnd.nextInt(chars.length())));
    }
    
    System.out.println(base64.toString().length());
    
    RsaAesEncryptingUtil util = new RsaAesEncryptingUtil();
    
    String encriptado = util.encryptWithPrivate(base64.toString().substring(0,1000), privateKeyPath, easKeyPath, true);
    
    String decriptado = util.decryptWithPublic(encriptado, publicKeyPath, easKeyPath, true);
    
    System.out.println(">>>>> ENCRYPT Result");
    //System.out.println(base64.toString());
    System.out.println(encriptado);
    //System.out.println(decriptado);
    
    assertEquals(base64.toString(), decriptado);
  }
  
  

  @Test
  public void testPrintHex() {
    fail("Not yet implemented");
  }
  
  
  
  @Test
  public void testRAS() throws Exception {
    

    String encriptado = new RsaAesEncryptingUtil().encryptWithPrivate("TESTE", "/opt/as/sistemas/cryptor/keys/rsa/id_rsa_cryptor.der", true);
    
    
    System.out.println(encriptado);
    
    
    String descriptado = new RsaAesEncryptingUtil().decryptWithPublic(encriptado, "/opt/as/sistemas/cryptor/keys/rsa/id_rsa_cryptor.pub.der", true);
    
    System.out.println(descriptado);
    
    assertEquals("TESTE", descriptado);
    
  }
  
  
  @Test
  public void testRAS2() throws Exception {
    

    String encriptado = new RsaAesEncryptingUtil().encryptWithPublic("TESTE", "/opt/as/sistemas/cryptor/keys/rsa/id_rsa_cryptor.pub.der", true);
    
    
    System.out.println(encriptado);
    
    
    String descriptado = new RsaAesEncryptingUtil().decryptWithPublic(encriptado, "/opt/as/sistemas/cryptor/keys/rsa/id_rsa_cryptor.pub.der", true);
    
    System.out.println(descriptado);
    
    assertEquals("TESTE", descriptado);
    
  }
  
  @Test
  public void testStoreKey() throws Exception {
    
    String textoClaro = "TESTE";

    byte[] textoCifrado = null;
    byte[] chaveCifrada = null;
    
    // -- A) Gerando uma chave simétrica de 128 bits
    SecretKey sk = AESUtil.loadKey(this.getClass().getResource("/").getPath() + "/keys/aes.key");
    byte[] chave = sk.getEncoded();
    
    PublicKey pub = RSAUtil.loadPublicKeyPem("/home/thiago/Workspaces/Prodepa/Crypto/Criptor-1.0-master/codigo_fonte/java/src/test/resources/keys/chave.publica");
    
    // -- B) Cifrando o texto com a chave simétrica gerada
    Cipher aescf = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivspec = new IvParameterSpec(new byte[16]);
    aescf.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(chave, "AES"), ivspec);
    textoCifrado = aescf.doFinal(textoClaro.getBytes());
   
    // -- C) Cifrando a chave com a chave pública
    Cipher rsacf = Cipher.getInstance("RSA");
    rsacf.init(Cipher.ENCRYPT_MODE, pub);
    chaveCifrada = rsacf.doFinal(chave);
    
    System.out.println(textoCifrado);
    System.out.println(chaveCifrada);
    
    System.out.println("<<<<<<<<<<<<<<<<<");
    
    PrivateKey pvk = RSAUtil.loadPrivateKeyPem("/home/thiago/Workspaces/Prodepa/Crypto/Criptor-1.0-master/codigo_fonte/java/src/test/resources/keys/chave.privada");
    
    byte[] textoDecifrado = null;
    // -- A) Decifrando a chave simétrica com a chave privada
    /*Cipher rsacf2 = Cipher.getInstance("RSA");
    rsacf2.init(Cipher.DECRYPT_MODE, pvk);
    byte[] chaveDecifrada = rsacf2.doFinal(chaveCifrada);*/
    
    byte[] chaveDecifrada = sk.getEncoded();
    
    // -- B) Decifrando o texto com a chave simétrica decifrada
    Cipher aescf2 = Cipher.getInstance("AES/CBC/PKCS5Padding");
    IvParameterSpec ivspec2 = new IvParameterSpec(new byte[16]);
    aescf2.init(Cipher.DECRYPT_MODE, new SecretKeySpec(chaveDecifrada, "AES"), ivspec2);
    textoDecifrado = aescf2.doFinal(textoCifrado);
    
    System.out.println(textoDecifrado);
    
    assertEquals(textoClaro, new String(textoDecifrado));
    
  }
  
  
  @Test
  public void testPrivateKeyPlain() throws Exception {
      
    //PublicKey pub = RSAUtil.loadPublicKey("/opt/as/sistemas/cryptor/keys/chave.publica");
    //System.out.println(pub.getFormat());
    //System.out.println(pub.getAlgorithm());
    
    //byte[] keyBytes = Files.readAllBytes(Paths.get("/opt/as/sistemas/cryptor/keys/openssh/private_key.der"));
    
    byte[] keyBytes = Files.readAllBytes(Paths.get("/opt/as/sistemas/cryptor/keys/openssh/private_key.der"));

    PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
    
    KeyFactory kf = KeyFactory.getInstance("RSA");
    
    PrivateKey privateKey = kf.generatePrivate(spec);
    System.out.println(privateKey.getFormat());
    System.out.println(privateKey.getAlgorithm());
    
  }
  
  @Test
  public void testPublicKeyDERPlain() throws Exception {
      
    byte[] keyBytes = Files.readAllBytes(Paths.get("/opt/as/sistemas/cryptor/keys/openssh/public_key.der"));

    
    X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
    KeyFactory kf = KeyFactory.getInstance("RSA");
    PublicKey publicKey = kf.generatePublic(spec);
    
    System.out.println(publicKey.getFormat());
    System.out.println(publicKey.getAlgorithm());
    
  }


  @Test
  public void ttttt() throws Exception {
    
    
    RsaAesEncryptingUtil util = new RsaAesEncryptingUtil();
    
    
    String encriptado = "2!PdF6Z484tUu0C19IEVC5F8UVOmZvMNNtXemu4QDwKJ7yMHPNvE2YBaK8ySIpZzXzhIs2MPohddHAO8eg26nP32aI3nKrtLpzBIAI+wZO7BdRg+ZuhNqA5CA/GyIaHU2ZMxHQFOm/GuZQBzxOyLyNjpHAwyDOj0f5DsS9jFtbiwtFpgFRowNZ6AKECdk8vJ7sc2HdqP5YWqGSn+hANhHyWGmnFJjTN+GtGZhOcDr1HbMzrBfu2vOgX292UG862nAXAOxDAey9a+Zl5cK/ZO1Oa5MIszVKTRxP4J3bn56UX8tmoQoTCA+s+lN05ER0R/I9d8mPS23lvVSYsz2nx5U4xyGTtgebwAUze650RGRphb3BJNIR/EoPSMPo44SL5fiIe1ntsUl9PM3ZQbKgxG3LZDSbg4VbHyVaPtcQXiFl+auJ1HUfe07jSaSpZ+ZXh7BryCGL4JjbsRZ03i7ZnVixcIG6CbFA6hJpLZkmF6cWhTG6LiETqDq9b2G0809OuhvtACp2Mnebp8yZ2I3e+9jhkT7cuHOXUzh1U1Ai3kDH42EXZLtJxACPuUvkDEziesj8+6ml7s7GtomUA+Tkpv3Qrg40cHwBTxO+Mbr4f1LAhJg8DKP705fmZ6qa2sjdJ+sisaYXoIiOsWZ3Rx2IYqAXl5z2imrqb4mRNl+zwJdHS9sCPPzeJWCCjUe8snsRLppgubIvhuzLyLKBF/CQAOh2LIZY/4vQsOwQcD/imQMIlAwAe/JkfM3SUwxYsaihFJANZ4caeiKP7kef8K95RqMpbfLN8QW8ancJZlDb1sKSrNMN5jtBEolQdKmlO0xp4LUXGr+KN9Zymiiaxr2ZeM+KufIU3smrkSMWHBSW413b3i8A5C7PnQbR5Ketter4FOgXjogiMuuywYCPo1G8OZrSEBw+sdk4LGZT13LMdYiGK9lHQ084iYPh9KmHYueTQLcK19NNHdXZglzSTwpEV3nKMeFJavn/Jj6YH4oQL0TU9XmZN2rYkPEzd8Fpp5EQqB3WMGep0IMFdEUGt3JzJIl5q8GN65VUfc41buphbj2oWvjNpn1RDSqoZr2VFcFw+n9m+ii+O1bzFnQfki8s+AgVKIxRvRS/zd82PMAD3DhwmWpYWp7rFmlz9EY4p1HfxpkyGgb86tUH1SvBD0lOK/BJ6aKd2s4okzPhNopQoHsVXxYgjDYG/7yiW2+pWW7BIZRdATCB091i05uNxHPaF9plCRt+ESQP6G5QR5D0SYwwo1VcDYKZjamhIdFn/ScGeJGXsWi/CciZacNMuEJkLGmby4qFA3ytr3Q07yn/9rvDUmUjJUSOcypovekp4WFuTISROZZ80fLfscOIv2J4eWxQXMwbI7pqWM3HIBkarZe2yqBliSd2iaLblDHqaw2UxCeEf5jTVjPRK3Eod9N2XXOiZyHeWr46fEpsKvndUEMNFOUtG816Z/JY5JNBXwHgd0yR68Ux/lTvz0bGRCUWZnhSczMQlT1k/nYcDymzQs4ffN8P54PMiu5etzljj6TNfsLRUaWmTwzwR8srbVYlLmNPNQ2q0P66mCh2j6Dcd6PF+/HViLtpLWVwUTwUf4uMwAnZsGDraUgplxZ3S9IJp2cbWu0+HKUd8CXo0bsFvId4eTd7v0sv+HAppu3vJj5bRkqaJD3XbTkTW7Kadz6Uoohov8LaECeCeRv7VTKVzh2CmKuXIcfdInf4rODg58EhZ4TkPfFCPsNT/gTrSe5Bj2LMTbd2J6HfaeCblf9hFd1Dz66qYdn7dr8w7W38bxdqhT9sxRePAYP1X+o2ydU3s7xjGno2TnRhdew/QuhjtzyeEHb+cQmbr5wm5oc6/IUvpq83mafUuZNXD7xOxRqoh96hl5AcgLJJHiRXnwvsrgxbAEFGFp/3RCUbi+HQQZIPEqNgeWAYgI39393JuMdS3CNJadcTBtpGza5pjfJEQjfH7oUt8H7dphyNe6aZp8GghiEF!tXb2qVYQRuxvhUcQ3WflC3Kd2+BLztCG/oNYIQCOocnCMzu7MotfybGRO5r83ZGz++v6XrO5rv6GGbtPY281Mv51l8J9BZZFrJ5pLbnZ2Y4o83bAhMgLBJLFtq/b2+0n7S5BdX0BP4CdYc3UpgPRkAZOOt0auOCLj9fvJ8nAaKE=";
    
    
    String decriptado = util.decryptWithPublic(encriptado, publicKeyPath, easKeyPath, true);
    
    System.out.println(encriptado);
    System.out.println(decriptado);
    
  }
  
}
