package com.blackdog.framework.cryptography.tests;


import static org.junit.Assert.assertNotNull;

import java.io.*;
import java.util.*;
import javax.crypto.*;
import javax.crypto.spec.*;

import org.apache.commons.codec.binary.Hex;
import org.junit.Test;

import com.blackdog.framework.cryptography.AESUtil;

import java.security.*;
import java.security.spec.*;
import java.security.cert.*;


public class ExemploChaveAesCustom {
  
  
  @Test
  public void testGenerateKey() throws Exception {
    
    
    try {
      
      teste();
    } catch (Exception e) {
      e.printStackTrace();
    }
    
  }
  
  
  public void teste() throws Exception {
    String CONTENT = "BlahBlahBlah";

    String username = "bob@google.org";
    String password = "Password1";
    String SALT2 = "deliciously salty";
    
    //String aesPassword = "1234567812345678";
    String aesPassword = "1234567812345678";

    // Get the Key
    //byte[] key = (SALT2 + username + password).getBytes();
    //System.out.println((SALT2 + username + password).getBytes().length);
    byte[] key = aesPassword.getBytes();
    System.out.println(aesPassword.length());

    // Need to pad key for AES
    // TODO: Best way?

    // Generate the secret key specs.
    SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");

    // Instantiate the cipher
    Cipher cipher = Cipher.getInstance("AES");
    cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);

    byte[] encrypted = cipher.doFinal((CONTENT).getBytes());
    
    System.out.println("encrypted string: " + Hex.encodeHexString(encrypted));

    cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
    byte[] original = cipher.doFinal(encrypted);
    String originalString = new String(original);
    System.out.println("Original string: " + originalString + "\nOriginal string (Hex): " + Hex.encodeHexString(original));
}
  
}