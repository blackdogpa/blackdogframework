package com.blackdog.framework.exceptions;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response.Status;
import javax.xml.ws.WebFault;

import com.blackdog.framework.core.constants.BlackDogConstants;
import com.blackdog.framework.core.exceptions.BlackExceptions;
import com.blackdog.framework.core.exceptions.ErrorMessage;
import com.blackdog.framework.core.exceptions.Validation;
import com.blackdog.framework.utils.string.StringUtils;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
@WebFault(name="BusinessExceptionFault", targetNamespace= BlackDogConstants.EXCEPTION_NAMESPACE)
public class BusinessException extends Exception implements BlackExceptions {

  private static final long serialVersionUID = -5022967813902834759L;

  private String id;
  private String code;
  
  private List<Validation> validations = new ArrayList<Validation>();
  
  private String transactionId;
  private String applicationId;
  private String applicationName;
  
  public BusinessException() {
    super();
  }

  public BusinessException(String message, Object... args) {
    super(StringUtils.messageFormat(message, args));
    this.validations = new ArrayList<Validation>();
    this.validations.add(new Validation(StringUtils.messageFormat(message, args)));
  }
  
  public BusinessException(Throwable cause) {
    super(cause);
    this.validations = new ArrayList<Validation>();
    this.validations.add(new Validation(cause.getMessage()));
  }
  
  public BusinessException(Throwable cause, Validation validation) {
    super(cause);
    this.validations = new ArrayList<Validation>();
    this.validations.add(validation);
    
    this.code = validation.getCode();
  }
  
  public BusinessException(Validation validation) {
    super();
    this.validations = new ArrayList<Validation>();
    this.validations.add(validation);
    
    this.code = validation.getCode();
  }

  @Deprecated//TODO Esse construtor não contem o Code
  public BusinessException(Throwable cause, String message, Object... args) {
    super(StringUtils.messageFormat(message, args), cause);
    this.validations = new ArrayList<Validation>();
    this.validations.add(new Validation(StringUtils.messageFormat(message, args)));
  }
  
  
  public BusinessException(List<Validation> validations) {
    super();
    this.validations = validations;
    
    this.code = getCodeFromList(validations);
  }
  
  public BusinessException(List<Validation> validations, String message,  Object... args) {
    super(StringUtils.messageFormat(message, args));
    this.validations = validations;
    this.code = getCodeFromList(validations);
  }
  
  public BusinessException(List<Validation> validations, Throwable cause) {
    super(cause);
    this.validations = validations;
    this.code = getCodeFromList(validations);
  }
  
  public BusinessException(List<Validation> validations, Throwable cause, String message, Object... args) {
    super(StringUtils.messageFormat(message, args), cause);
    this.validations = validations;
    this.code = getCodeFromList(validations);
  }

  @Override
  public ErrorMessage getErrorMessage() {
    ErrorMessage dto = new ErrorMessage();
    dto.setStatus(Status.BAD_REQUEST.name());
    dto.setStatusCode(Status.BAD_REQUEST.getStatusCode());
    
    if(org.apache.commons.lang3.StringUtils.isNotBlank(this.getMessage())) {
      dto.setMessage(this.getMessage());
    }
    
    if(this.getValidations() != null && !this.getValidations().isEmpty()) {
      dto.setValidations(this.getValidations());
    }
    
    dto.setTransactionId(this.getId());
    dto.setApplicationId(this.getApplicationId());
    dto.setApplicationName(this.getApplicationName());
    
    dto.setType(this.getType());
    
    return dto;
  }
  
  
  public List<Validation> getValidations() {
    return validations;
  }

  public void setValidations(List<Validation> validations) {
    this.validations = validations;
  }
  
  @Override
  public String getType() {
    return BUSINESS;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    
    this.id = id;
  }
  
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
  
  public String getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(String applicationId) {
    this.applicationId = applicationId;
  }

  public String getApplicationName() {
    return applicationName;
  }

  public void setApplicationName(String applicationName) {
    this.applicationName = applicationName;
  }

  
  public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }
  
  @Override
  public String getMessage() {
    
    if(super.getMessage() == null || super.getMessage().equals("")) {
      return null;
    }
    
    StringBuilder builder = new StringBuilder();

    if(applicationId != null && applicationName != null) {
      
      builder.append("[");
      builder.append(applicationId);
      builder.append(".");
      builder.append(applicationName);
      builder.append("]");
      
      builder.append("::");
    }
    if (id != null) {
      //builder.append(id);
      //builder.append(" : ");
      builder.append(super.getMessage());
    } else {
      builder.append(super.getMessage());
    }
    return builder.toString();
  }
  
  private String getCodeFromList(List<Validation> validations) {
    
    StringBuffer codes = new StringBuffer();
    if(validations != null) {
      for (Validation validation : validations) {
        codes.append(validation.getCode());
        codes.append(",");
      }
      
    }
    return codes.substring(0, codes.length()-1);
  }
  
  @Override
  public String toString() {
    return this.getClass().getName() + "["
            + "id="+getId()
            + "applicationId"+getApplicationId()
            + "applicationName="+getApplicationName()
            + "validations=" + validations 
            + "#\n#"
            + ", Class=" + getClass() 
            + ", Cause=" + getCause() 
            + ", Message=" + getMessage() 
            + ", LocalizedMessage=" + getLocalizedMessage()
            + "]";
  }
  
  
  
  
}
