package com.blackdog.framework.exceptions;

import javax.ws.rs.core.Response.Status;
import javax.xml.ws.WebFault;

import com.blackdog.framework.core.constants.BlackDogConstants;
import com.blackdog.framework.core.exceptions.BlackExceptions;
import com.blackdog.framework.core.exceptions.ErrorMessage;
import com.blackdog.framework.core.exceptions.Validation;
import com.blackdog.framework.core.model.ErrorMessageConstant;
import com.blackdog.framework.utils.string.StringUtils;

/**
 * 
 * @author thiago.soares
 *
 */
@WebFault(name="SystemExceptionFault", targetNamespace= BlackDogConstants.EXCEPTION_NAMESPACE)
public class SystemException extends RuntimeException implements BlackExceptions {

  private static final long serialVersionUID = 3190846533842542146L;
  
  private String id;
  private String code;
  
  private String applicationId;
  private String applicationName;

  public SystemException() {
    super();
  }

  public SystemException(String id, String message, Object... args) {
    super(StringUtils.messageFormat(message, args));
  }
  
  public SystemException(String message, Object... args) {
    super(StringUtils.messageFormat(message, args));
  }

  public SystemException(ErrorMessageConstant error, Object... args) {
    super(StringUtils.messageFormat(error.getMessage(), args));
    this.code = error.getCode();
  }
  
  public SystemException(String id, ErrorMessageConstant error, Object... args) {
    super(StringUtils.messageFormat(error.getMessage(), args));
    this.code = error.getCode();
    this.id = id;
  }
  
  public SystemException(Throwable e, ErrorMessageConstant error, Object... args) {
    super(StringUtils.messageFormat(error.getMessage(), args), e);
    this.code = error.getCode();
  }
  
  
  public SystemException(Throwable cause) {
    super(cause);
  }

  public SystemException(Throwable cause, String message, Object... args) {
    super(StringUtils.messageFormat(message, args), cause);
  }
  
  public SystemException(String id, Throwable cause, String message, Object... args) {
    super(StringUtils.messageFormat(message, args), cause);
    this.id = id;
  }
  
  @Override
  public ErrorMessage getErrorMessage() {
    
    ErrorMessage dto = new ErrorMessage();
    dto.setStatus(Status.INTERNAL_SERVER_ERROR.name());
    dto.setStatusCode(Status.INTERNAL_SERVER_ERROR.getStatusCode());
    
    dto.setMessage("Falha de Sistema: " + this.getMessage());
    
    dto.getValidations().add(new Validation(this.getCode(), null, this.getMessage()));
    
    dto.setTransactionId(this.getId());
    dto.setApplicationId(this.getApplicationId());
    dto.setApplicationName(this.getApplicationName());
    
    dto.setType(this.getType());
    
    return dto;
  }
  
  @Override
  public String getType() {
    return SYSTEM;
  }
  
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
  
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
  
  public String getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(String applicationId) {
    this.applicationId = applicationId;
  }

  public String getApplicationName() {
    return applicationName;
  }

  public void setApplicationName(String applicationName) {
    this.applicationName = applicationName;
  }

  @Override
  public String getMessage() {
    StringBuilder builder = new StringBuilder();

    if(applicationId != null && applicationName != null) {
      
      builder.append("[");
      builder.append(applicationId);
      builder.append(".");
      builder.append(applicationName);
      builder.append("]");
      
      builder.append("-");
    }
    if (id != null) {
      //builder.append(id);
      //builder.append(" : ");
      builder.append(super.getMessage());
    } else {
      builder.append(super.getMessage());
    }
    return builder.toString();
  }

  @Override
  public String toString() {
    return this.getClass().getName() + "["
            + "id="+getId()
            + "applicationId"+getApplicationId()
            + "applicationName="+getApplicationName()
            + "#\n#"
            + ", Class=" + getClass() 
            + ", Cause=" + getCause() 
            + ", Message=" + getMessage() 
            + ", LocalizedMessage=" + getLocalizedMessage()
            + "]";
  }

  
}
