package com.blackdog.framework.exceptions.handler;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;

import com.blackdog.framework.core.constants.CoreErrorsConstants.SerializationErrors;
import com.blackdog.framework.core.exceptions.ErrorMessage;
import com.blackdog.framework.core.exceptions.HelpMessage;
import com.blackdog.framework.core.exceptions.Validation;
import com.blackdog.framework.exceptions.BusinessException;
import com.thoughtworks.xstream.XStream;



/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
public abstract class AbstractExceptionHandler /*implements ExceptionMapper<BusinessException>*/ {

  private @Context HttpServletRequest httpRequest;
  
  protected Response buildResponse(Status status, ErrorMessage dto)  {
    
    String accept = httpRequest.getHeader("Accept");

    try {
      
      if(accept == null) {
        accept = MediaType.APPLICATION_JSON;
      }
      
      if(accept.equals(MediaType.WILDCARD)) {
        accept = MediaType.APPLICATION_JSON;
      }
      
      if(accept.equals(MediaType.APPLICATION_XML)) {
        
        return Response.status(status).entity(errorToXml(dto)).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML_TYPE.withCharset("utf-8")).build();
        
      } else {
  
        return Response.status(status).entity(dto).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8")).build();
        
      }
    } catch (BusinessException exception) {
      // TODO: handle exception
      
      dto = new ErrorMessage();
      
      dto.setStatus(Status.BAD_REQUEST.name());
      dto.setStatusCode(Status.BAD_REQUEST.getStatusCode());
      
      if(StringUtils.isNotBlank(exception.getMessage())) {
        dto.setMessage(exception.getMessage());
      }
      
      if(exception.getValidations() != null && !exception.getValidations().isEmpty()) {
        dto.setValidations(exception.getValidations());
      }
      
      dto.setTransactionId(exception.getId());
      dto.setApplicationId(exception.getApplicationId());
      dto.setApplicationName(exception.getApplicationName());
      
      dto.setType(exception.getType());
      
      if(accept.equals(MediaType.APPLICATION_XML)) {
        
        try {
          return Response.status(status).entity(errorToXml(dto)).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML_TYPE.withCharset("utf-8")).build();
        } catch (BusinessException e) {
          return null;
        }
        
      } else {

        return Response.status(status).entity(dto).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8")).build();
        
      }
      
    }
    
  }
  
  protected String errorToXml(ErrorMessage model) throws BusinessException {
    
    try {
      
      XStream xstream = new XStream();
      xstream.processAnnotations(model.getClass());
      
      xstream.ignoreUnknownElements();
      
      xstream.addDefaultImplementation(java.sql.Date.class, Date.class);
      xstream.addDefaultImplementation(java.sql.Timestamp.class, Date.class);

      xstream.alias("ErrorMessage", ErrorMessage.class);
      xstream.alias("Validation", Validation.class);
      xstream.alias("HelpMessage", HelpMessage.class);
      
      
      String responseObject = xstream.toXML(model);
      
      return responseObject;
      
    } catch(Exception e) {
      throw new BusinessException(e, new Validation(SerializationErrors.SERIALIZATION_001.getCode(), null, SerializationErrors.SERIALIZATION_001.getMessage()));//TODO Deveria ser uma SelializationException
    }
  }
  
  
  /*@Override
  public Response toResponse(BusinessException exception) {
    
    exception.printStackTrace();
    
    ErrorMessage dto = new ErrorMessage();
    dto.setStatus(Status.BAD_REQUEST.name());
    dto.setStatusCode(Status.BAD_REQUEST.getStatusCode());
    
    if(StringUtils.isNotBlank(exception.getMessage())) {
      dto.setMessage(exception.getMessage());
    }
    
    if(exception.getValidations() != null && !exception.getValidations().isEmpty()) {
      dto.setValidations(exception.getValidations());
    }
    
    dto.setTransactionId(exception.getId());
    dto.setApplicationId(exception.getApplicationId());
    dto.setApplicationName(exception.getApplicationName());
    
    dto.setType(exception.getType());
    
    
    System.out.println(uriInfo);
    
    System.out.println(request);
    
    
    System.out.println(httpRequest.getHeader("Accept"));
    
    
    return Response.status(Status.BAD_REQUEST).entity(dto).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8")).build(); 
  }
*/
}
