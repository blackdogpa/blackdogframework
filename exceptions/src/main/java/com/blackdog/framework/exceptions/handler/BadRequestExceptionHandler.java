package com.blackdog.framework.exceptions.handler;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.jboss.resteasy.spi.BadRequestException;

import com.blackdog.framework.core.audit.AuditEvent;
import com.blackdog.framework.core.exceptions.ErrorMessage;
import com.blackdog.framework.core.exceptions.Validation;


/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
@Provider
public class BadRequestExceptionHandler extends AbstractExceptionHandler implements ExceptionMapper<BadRequestException> {

  
  /* Não funcionou no JBoss 7.1.1. Funciona no EAP 7
  @Inject
  @AuditEvent
  Event<ErrorMessage> logEvent;*/
  
  
  @Override
  public Response toResponse(BadRequestException exception) {
    
    exception.printStackTrace();
    
    ErrorMessage dto = new ErrorMessage();
    dto.setStatus(Status.BAD_REQUEST.name());
    dto.setStatusCode(Status.BAD_REQUEST.getStatusCode());
    dto.setType("BAD REQUEST");
    
    dto.setMessage("Falha na Requisição: " + exception.getMessage());
    
    dto.getValidations().add(new Validation(exception.getMessage()));
    
    if(exception.getCause() != null) {
      dto.getValidations().add(new Validation(exception.getCause().getMessage()));
    }

    //logEvent.fire(dto);
    
    return buildResponse(Status.BAD_REQUEST, dto);
  }

}
