package com.blackdog.framework.exceptions.handler;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;

import com.blackdog.framework.core.exceptions.ErrorMessage;
import com.blackdog.framework.exceptions.BusinessException;



/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
@Provider
public class BusinessExceptionHandler extends AbstractExceptionHandler implements ExceptionMapper<BusinessException>  {

  private @Context HttpServletRequest httpRequest;
  
  
  @Override
  public Response toResponse(BusinessException exception) {
    
    exception.printStackTrace();
    
    /*ErrorMessage dto = new ErrorMessage();
    dto.setStatus(Status.BAD_REQUEST.name());
    dto.setStatusCode(Status.BAD_REQUEST.getStatusCode());
    
    if(StringUtils.isNotBlank(exception.getMessage())) {
      dto.setMessage(exception.getMessage());
    }
    
    if(exception.getValidations() != null && !exception.getValidations().isEmpty()) {
      dto.setValidations(exception.getValidations());
    }
    
    dto.setTransactionId(exception.getId());
    dto.setApplicationId(exception.getApplicationId());
    dto.setApplicationName(exception.getApplicationName());
    
    dto.setType(exception.getType());*/
    
    //return Response.status(Status.BAD_REQUEST).entity(dto).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8")).build(); 
    return buildResponse(Status.BAD_REQUEST, exception.getErrorMessage());
  }

}
