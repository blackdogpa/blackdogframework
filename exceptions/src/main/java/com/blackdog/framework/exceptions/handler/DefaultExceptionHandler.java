package com.blackdog.framework.exceptions.handler;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.blackdog.framework.core.exceptions.ErrorMessage;
import com.blackdog.framework.core.exceptions.Validation;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
@Provider
public class DefaultExceptionHandler extends AbstractExceptionHandler implements ExceptionMapper<Exception> {

  @Override
  public Response toResponse(Exception exception) {
    
    exception.printStackTrace();
    
    ErrorMessage dto = new ErrorMessage();
    dto.setMessage("Desculpe. Houve uma falha na execução.");
    dto.setType("UNCHECKED");
    
    Status st = Status.INTERNAL_SERVER_ERROR;
    
    dto.setStatus(st.name());
    dto.setStatusCode(st.getStatusCode());
    
    dto.getValidations().add(new Validation(exception.getMessage()));
    
    if(exception.getCause() != null) {
      dto.getValidations().add(new Validation(exception.getCause().getMessage()));
    }
    
    //return Response.status(st).entity(dto).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8")).build(); 
    return buildResponse(st, dto);
  }

}
