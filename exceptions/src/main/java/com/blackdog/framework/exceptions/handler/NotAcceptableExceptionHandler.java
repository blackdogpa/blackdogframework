package com.blackdog.framework.exceptions.handler;

import javax.ws.rs.NotAcceptableException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

import com.blackdog.framework.core.exceptions.ErrorMessage;
import com.blackdog.framework.core.exceptions.Validation;


/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
//@Provider
public class NotAcceptableExceptionHandler extends AbstractExceptionHandler implements ExceptionMapper<NotAcceptableException> {

  @Override
  public Response toResponse(NotAcceptableException exception) {
    
    exception.printStackTrace();
    
    ErrorMessage dto = new ErrorMessage();
    dto.setStatus(Status.NOT_ACCEPTABLE.name());
    dto.setStatusCode(Status.NOT_ACCEPTABLE.getStatusCode());
    
    //dto.setMessage("Falha de Execução: " + exception.getMessage());
    
    dto.setMessage("Falha na Requisição.");
    
    dto.getValidations().add(new Validation(exception.getMessage()));
    
    if(exception.getCause() != null) {
      dto.getValidations().add(new Validation(exception.getCause().getMessage()));
    }
    
    //dto.setTransactionId(requestInfo.getTransactionID());
    
    dto.setType("BAD REQUEST");
    
    //return Response.status(Status.NOT_ACCEPTABLE).entity(dto).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8")).build(); 
    return buildResponse(Status.NOT_ACCEPTABLE, dto);
  }

}
