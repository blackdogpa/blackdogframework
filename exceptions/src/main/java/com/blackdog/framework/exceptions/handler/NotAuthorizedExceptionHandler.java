package com.blackdog.framework.exceptions.handler;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.blackdog.framework.core.exceptions.ErrorMessage;
import com.blackdog.framework.core.exceptions.Validation;
import com.blackdog.framework.exceptions.NotAuthorizedException;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
@Provider
public class NotAuthorizedExceptionHandler extends AbstractExceptionHandler implements ExceptionMapper<NotAuthorizedException> {

  @Override
  public Response toResponse(NotAuthorizedException exception) {

    exception.printStackTrace();
    
    /*ErrorMessage dto = new ErrorMessage();
    dto.setStatus(Status.FORBIDDEN.name());
    dto.setStatusCode(Status.FORBIDDEN.getStatusCode());
    
    dto.setMessage("Permissão Negada: " + exception.getMessage());
    
    dto.getValidations().add(new Validation(exception.getCode(), null, exception.getMessage()));

    dto.setTransactionId(exception.getId());
    dto.setApplicationId(exception.getApplicationId());
    dto.setApplicationName(exception.getApplicationName());
    
    dto.setType(exception.getType());*/
    
    //return Response.status(Status.FORBIDDEN).entity(dto).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8")).build(); 
    return buildResponse(Status.FORBIDDEN, exception.getErrorMessage());
    
  }

}
	