package com.blackdog.framework.exceptions.handler;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;

import com.blackdog.framework.core.audit.AuditEvent;
import com.blackdog.framework.core.exceptions.ErrorMessage;



/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
@Provider
public class NotFoundExceptionHandler extends AbstractExceptionHandler implements ExceptionMapper<javax.ws.rs.NotFoundException> {

	
  /* Não funcionou no JBoss 7.1.1. Funciona no EAP 7
	@Inject
	@AuditEvent
	Event<ErrorMessage> logEvent;*/
	
  @Override
  public Response toResponse(javax.ws.rs.NotFoundException exception) {
    
    exception.printStackTrace();
    
    ErrorMessage dto = new ErrorMessage();
    dto.setStatus(Status.NOT_FOUND.name());
    dto.setStatusCode(Status.NOT_FOUND.getStatusCode());
    
    if(StringUtils.isNotBlank(exception.getMessage())) {
      dto.setMessage(exception.getMessage());
    }
    
    if(exception.getCause() instanceof NumberFormatException) {
      NumberFormatException nfe = (NumberFormatException) exception.getCause();
      dto.setMessage(dto.getMessage() + ". Invalid Value: " +nfe.getMessage());
    }
    
    dto.setType(Status.NOT_FOUND.name());
    
    //logEvent.fire(dto);
    
    return buildResponse(Status.NOT_FOUND, dto);
  }

}
