package com.blackdog.framework.exceptions.handler;

import java.util.ArrayList;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;

import com.blackdog.framework.core.audit.AuditEvent;
import com.blackdog.framework.core.exceptions.ErrorMessage;
import com.blackdog.framework.core.exceptions.HelpMessage;
import com.blackdog.framework.core.exceptions.Validation;



/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
@Provider
public class NotSupportedMediaTypeExceptionHandler extends AbstractExceptionHandler implements ExceptionMapper<javax.ws.rs.NotSupportedException> {

  /* Não funcionou no JBoss 7.1.1. Funciona no EAP 7
	@Inject
	@AuditEvent
	Event<ErrorMessage> logEvent;*/
	
	@Inject
	private HttpServletRequest httpRequest;
	
  @Override
  public Response toResponse(javax.ws.rs.NotSupportedException exception) {
    
    exception.printStackTrace();
    
    ErrorMessage dto = new ErrorMessage();
    dto.setStatus(Status.UNSUPPORTED_MEDIA_TYPE.name());
    dto.setStatusCode(Status.UNSUPPORTED_MEDIA_TYPE.getStatusCode());
    dto.setType(Status.UNSUPPORTED_MEDIA_TYPE.name());
    
    if(StringUtils.isNotBlank(exception.getMessage())) {
      dto.setMessage(exception.getMessage() + ": " + httpRequest.getContentType());
    }
    
    dto.getSupport().add(new HelpMessage("Apenas application/json e application/xml são aceitos como Content-Type"));
    
    //logEvent.fire(dto);
    
    return buildResponse(Status.UNSUPPORTED_MEDIA_TYPE, dto);
  }

}
