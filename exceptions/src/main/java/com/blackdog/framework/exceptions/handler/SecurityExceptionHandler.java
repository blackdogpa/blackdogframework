package com.blackdog.framework.exceptions.handler;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.blackdog.framework.core.exceptions.ErrorMessage;
import com.blackdog.framework.core.exceptions.Validation;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
@Provider
public class SecurityExceptionHandler extends AbstractExceptionHandler implements ExceptionMapper<com.blackdog.framework.exceptions.SecurityException> {

  @Override
  public Response toResponse(com.blackdog.framework.exceptions.SecurityException exception) {

    exception.printStackTrace();
    
    /*ErrorMessage dto = new ErrorMessage();
    dto.setStatus(Status.UNAUTHORIZED.name());
    dto.setStatusCode(Status.UNAUTHORIZED.getStatusCode());
    
    dto.setMessage("Falha de Segurança: " + exception.getMessage());
    
    dto.getValidations().add(new Validation(exception.getCode(), null, exception.getMessage()));

    dto.setTransactionId(exception.getId());
    dto.setApplicationId(exception.getApplicationId());
    dto.setApplicationName(exception.getApplicationName());
    
    dto.setType(exception.getType());*/
    
    //return Response.status(Status.UNAUTHORIZED).entity(dto).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8")).build(); 
    return buildResponse(Status.UNAUTHORIZED, exception.getErrorMessage());
    
  }

}
	