package com.blackdog.framework.exceptions.handler;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.blackdog.framework.core.exceptions.ErrorMessage;
import com.blackdog.framework.core.exceptions.Validation;
import com.blackdog.framework.exceptions.SystemException;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
@Provider
public class SystemExceptionHandler extends AbstractExceptionHandler implements ExceptionMapper<SystemException> {

  @Override
  public Response toResponse(SystemException exception) {
    
    exception.printStackTrace();
    
    /*ErrorMessage dto = new ErrorMessage();
    dto.setStatus(Status.INTERNAL_SERVER_ERROR.name());
    dto.setStatusCode(Status.INTERNAL_SERVER_ERROR.getStatusCode());
    
    dto.setMessage("Falha de Sistema: " + exception.getMessage());
    
    dto.getValidations().add(new Validation(exception.getCode(), null, exception.getMessage()));
    
    dto.setTransactionId(exception.getId());
    dto.setApplicationId(exception.getApplicationId());
    dto.setApplicationName(exception.getApplicationName());
    
    dto.setType(exception.getType());*/
        
    //return Response.status(Status.INTERNAL_SERVER_ERROR).entity(dto).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8")).build();
    return buildResponse(Status.INTERNAL_SERVER_ERROR, exception.getErrorMessage());
  }

}
  