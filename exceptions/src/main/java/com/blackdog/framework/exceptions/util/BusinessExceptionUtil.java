package com.blackdog.framework.exceptions.util;

import java.util.ArrayList;
import java.util.List;

import com.blackdog.framework.core.exceptions.Validation;
import com.blackdog.framework.exceptions.BusinessException;
import com.blackdog.framework.exceptions.SecurityException;
import com.blackdog.framework.core.model.ErrorMessageConstant;
import com.blackdog.framework.utils.string.StringUtils;

/**
 * Utilitario para lançamento de Excessões
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com)
 *
 */
public class BusinessExceptionUtil {
  
  private List<Validation> validations;
  
  public BusinessExceptionUtil() {
    super();
    this.validations = new ArrayList<Validation>();
  }
  
  /*
   * Business Utils
   */
  
  public void addError(ErrorMessageConstant errorMessage, Object... args) {
    this.validations.add(new Validation(errorMessage.getCode(), null, StringUtils.messageFormat(errorMessage.getMessage(), args)));
  }
  
  public void addError(String code, String message, Object... args) {
    this.validations.add(new Validation(code, null, StringUtils.messageFormat(message, args)));
  }
  
  public void throwBusinessException() throws BusinessException {
    if(hasError()) {
      throw new BusinessException(validations);
    }
  }
  
  
  public static void throwBusinessException(String code, String message, Object... args) throws BusinessException {
    throw new BusinessException(new Validation(code, null, StringUtils.messageFormat(message, args)));
  }
  
  public static void throwBusinessException(ErrorMessageConstant errorMessage, Object... args) throws BusinessException {
    throw new BusinessException(new Validation(errorMessage.getCode(), null, StringUtils.messageFormat(errorMessage.getMessage(), args)));
  }
  
  public static void throwBusinessException(Throwable e,ErrorMessageConstant errorMessage, Object... args) throws BusinessException {
    throw new BusinessException(e, new Validation(errorMessage.getCode(), null, StringUtils.messageFormat(errorMessage.getMessage(), args)));
  }
  
  
  /*
   * Secutity Utils
   */
  
  public static void throwSecurityException(ErrorMessageConstant errorMessage, Object... args) throws SecurityException {
    throw new SecurityException(errorMessage, args);
  }
  
  public static void throwSecurityException(Throwable e,ErrorMessageConstant errorMessage, Object... args) throws SecurityException {
    throw new SecurityException(e, errorMessage, args);
  }
  
  
  /*
   * SystemException Utils
   */
  
  public static void throwSystemException(ErrorMessageConstant errorMessage, Object... args) throws SecurityException {
    throw new SecurityException(errorMessage, args);
  }
  
  public static void throwSystemException(Throwable e,ErrorMessageConstant errorMessage, Object... args) throws SecurityException {
    throw new SecurityException(e, errorMessage, args);
  }
  
  public Boolean hasError() {
    return !this.validations.isEmpty();
  }

}
