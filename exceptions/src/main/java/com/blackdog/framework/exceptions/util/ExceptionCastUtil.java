package com.blackdog.framework.exceptions.util;

import java.util.ArrayList;
import java.util.List;

import com.blackdog.framework.core.exceptions.Validation;

public class ExceptionCastUtil {

  public static List<Validation> cast(String error) {
    return cast(new String[] { cleanNaturalMessage(error) });
  }

  public static List<Validation> cast(String[] erros) {

    List<Validation> validations = new ArrayList<>();

    if (erros != null && erros.length > 0) {

      for (String error : erros) {

        if (error != null && !error.trim().equals("")) {
          validations.add(new Validation(cleanNaturalMessage(error)));
        }

      }

    }

    return validations;
  }

  public static List<Validation> cast(List<String> erros) {

    List<Validation> validations = new ArrayList<>();

    if (erros != null && erros.size() > 0) {

      for (String error : erros) {

        if (error != null && !error.trim().equals("")) {
          validations.add(new Validation(cleanNaturalMessage(error)));
        }

      }

    }

    return validations;
  }

  private static String cleanNaturalMessage(String s) {

    s = s.trim();

    return s;
  }
    
  
  public static String getExceptionThrowClass(Throwable e) {
    
    List<String> list = new ArrayList<>();
    for (StackTraceElement ste : e.getStackTrace())  {
      if(ste.getClassName().startsWith("br.gov.pa")
         && !ste.getClassName().contains("$$")) { //TODO Isso está ruimmmm demaisssss
        list.add(ste.getClassName()+"::"+ste.getLineNumber());
      }
    }
    return list.toString();
  }
}
