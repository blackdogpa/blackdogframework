package com.blackdog.framework.exceptions;

import static org.junit.Assert.*;

import org.junit.Test;

import com.blackdog.framework.exceptions.BusinessException;

public class BusinessExceptionTest {

  @Test
  public void testBusinessException() throws BusinessException {
    throw new BusinessException();
  }

  @Test
  public void testBusinessExceptionString() {
    
    try {
      throw new BusinessException("Exceção!!!!");
    } catch (BusinessException e) {
      assertEquals("Exceção!!!!", e.getMessage());
    }
  }
  
  @Test
  public void testBusinessExceptionStringStringArray() {
    try {
      throw new BusinessException("Exceção {0}", "Customizada");
    } catch (BusinessException e) {
      assertEquals("Exceção Customizada", e.getMessage());
    }
  }

  @Test
  public void testBusinessExceptionThrowable() {
    try {
      throw new BusinessException(new Exception("OK"));
    } catch (BusinessException e) {
      assertEquals("OK", e.getMessage());
    }
  }

  @Test
  public void testBusinessExceptionStringThrowable() {
    try {
      throw new BusinessException(new Exception("EXX"), "Exceção {0}", "Customizada");
    } catch (BusinessException e) {
      assertEquals("Exceção Customizada", e.getMessage());
    }
  }

  @Test
  public void testBusinessExceptionListOfValidation() {
    fail("Not yet implemented");
  }

  @Test
  public void testBusinessExceptionStringListOfValidation() {
    fail("Not yet implemented");
  }

  @Test
  public void testBusinessExceptionListOfValidationThrowable() {
    fail("Not yet implemented");
  }

  @Test
  public void testBusinessExceptionStringListOfValidationThrowable() {
    fail("Not yet implemented");
  }

  @Test
  public void testGetValidations() {
    fail("Not yet implemented");
  }

  @Test
  public void testSetValidations() {
    fail("Not yet implemented");
  }

  @Test
  public void testGetId() {
    fail("Not yet implemented");
  }

  @Test
  public void testSetId() {
    fail("Not yet implemented");
  }

  @Test
  public void testToString() {
    fail("Not yet implemented");
  }

}
