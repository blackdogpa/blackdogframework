package com.blackdog.framework.utils.image;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import net.coobird.thumbnailator.Thumbnails;

public class ImageScaler {
  
  
  public static byte[] scale(File source, int width, int height, Float quality) throws IOException {
    
    return scale(source, width, height, quality, null);
    
    /*if(quality != null && quality > 0) {
      ByteArrayOutputStream os = new ByteArrayOutputStream();
      Thumbnails.of(source)
                .size(width, height)
                .outputQuality(quality)
                .toOutputStream(os);
      return os.toByteArray();
    } else {
      
      ByteArrayOutputStream os = new ByteArrayOutputStream();
      Thumbnails.of(source)
                .size(width, height)
                .toOutputStream(os);
      return os.toByteArray();
      
    }
*/
    
    /*
    Thumbnails.of(source)
              .size(width, height)
              .outputQuality(0.5)
              .toFile(target);
    
    Thumbnails.of(source)
              .scale(0.25)
              .toFile(target);
     */
    
    
    
    /*ByteArrayOutputStream os = new ByteArrayOutputStream();
    Thumbnails.of(source)
              .scale(0.1).scalingMode(ScalingMode.PROGRESSIVE_BILINEAR)
              .outputQuality(quality)
              .toOutputStream(os);*/
    
    
    
  }
  
  public static byte[] scale(File source, int width, int height, Float quality, String outputFormat) throws IOException {
    
    if(outputFormat == null || outputFormat.equals("")) {
      outputFormat = "jpg";
    }
    
    if(quality != null && quality > 0) {

      ByteArrayOutputStream os = new ByteArrayOutputStream();
      Thumbnails.of(source)
                .size(width, height)
                .outputQuality(quality).outputFormat(outputFormat)
                .toOutputStream(os);
      return os.toByteArray();
      
    } else {

      ByteArrayOutputStream os = new ByteArrayOutputStream();
      Thumbnails.of(source)
                .size(width, height)
                .outputFormat(outputFormat)
                .toOutputStream(os);
      return os.toByteArray();
      
    }
  }
  
  
}
