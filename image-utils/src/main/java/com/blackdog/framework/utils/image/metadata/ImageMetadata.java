package com.blackdog.framework.utils.image.metadata;

import java.io.File;
import java.io.IOException;

import org.apache.commons.imaging.ImageInfo;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata.ImageMetadataItem;
import org.apache.commons.io.FileUtils;

public class ImageMetadata {

  private ImageInfo imageInfo;
  
  private ImageMetadata() {
    super();
    // TODO Auto-generated constructor stub
  }

  
  public static ImageMetadata getMetaData(byte[] imageBytes, boolean debug) throws Exception {
    return extractrMetadata(imageBytes, debug);
  }
  
  public static ImageMetadata getMetaData(String path, boolean debug) throws Exception {
    return getMetaData(new File(path), debug);
  }
  
  public static ImageMetadata getMetaData(File file, boolean debug) throws Exception {

    byte[] imageBytes = FileUtils.readFileToByteArray(file);
    
    return extractrMetadata(imageBytes, debug);
  }

  private static ImageMetadata extractrMetadata(byte[] imageBytes, boolean debug) throws Exception {

    //byte[] imageBytes = FileUtils.readFileToByteArray(imageFile);

    ImageInfo imageInfo = null ;
    try {
      imageInfo = Imaging.getImageInfo(imageBytes);
    } catch (ImageReadException e) {
      throw new Exception(e.getMessage());
    }
    
    
    ImageMetadata imageMetadata = new ImageMetadata();
    imageMetadata.setImageInfo(imageInfo);
    

    if(debug) {
      System.out.println("IMAGE INFO DEBUG");
    
  
      //System.out.println("Image Path: ________________" + imageFile.getPath());
      System.out.println("Image Height:_______________" + imageInfo.getHeight());
      System.out.println("Image Width:________________" + imageInfo.getWidth());
      System.out.println("Image BitsPerPixel:_________" + imageInfo.getBitsPerPixel());
      System.out.println("Image FormatName:___________" + imageInfo.getFormatName());
  
      System.out.println("Image PhysicalHeightDpi:____" + imageInfo.getPhysicalHeightDpi());
      System.out.println("Image PhysicalHeightInch:___" + imageInfo.getPhysicalHeightInch());
  
      System.out.println("Image PhysicalWidthDpi:_____" + imageInfo.getPhysicalWidthDpi());
      System.out.println("Image PhysicalWidthInch:____" + imageInfo.getPhysicalWidthInch());
  
      
      System.out.println("Image ColorType:____________" + imageInfo.getColorType());
  
      System.out.println("Image CompressionAlgorithm:_" + imageInfo.getCompressionAlgorithm());
      System.out.println("Image Format:_______________" + imageInfo.getFormat());
      System.out.println("Image isProgressive:________" + imageInfo.isProgressive());
      
  
      //System.out.println("IMAGE FORMAT DEBUG");
      /*org.apache.commons.imaging.common.ImageMetadata meta = Imaging.getMetadata(imageBytes);
      if(meta != null) {
        for(ImageMetadataItem me : meta.getItems()) {
          System.out.println(me.toString());
        }
      }*/
      
      //System.out.println(imageInfo);
      
    }
    

    return imageMetadata;

  }

  public ImageInfo getImageInfo() {
    return imageInfo;
  }

  public void setImageInfo(ImageInfo imageInfo) {
    this.imageInfo = imageInfo;
  }
  
  

}


