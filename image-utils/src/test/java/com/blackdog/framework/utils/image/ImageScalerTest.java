package com.blackdog.framework.utils.image;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

public class ImageScalerTest {

  @Test
  public void testScale() throws Exception {
    
    
    //File source = loadImage("/images/jobs-1380X920.png");
    //File source = loadImage("/images/jobs-1399X2131.jpg");
    //File source = loadImage("/images/example01.jpg");
    File source = loadImage("/images/jobs_fundo_branco.jpg");
    File target = new File("/home/thiago/Downloads/TMP/jobs_fundo_branco-90X120-100p.png");
    
    //byte[] resizedData = new ImageScaler().scale(source, 120, 90, 0.3f);
    byte[] resizedData = new ImageScaler().scale(source, 90, 120, null);
    
    FileUtils.writeByteArrayToFile(target, resizedData);
    
  }
  
  protected File loadImage(String path) throws Exception {
    
    URL url = this.getClass().getResource(path);
    java.nio.file.Path resPath = java.nio.file.Paths.get(url.toURI());
    byte[] data = FileUtils.readFileToByteArray(new File(url.toURI()));
    
    File file = File.createTempFile("Images", "tmp");
    FileUtils.writeByteArrayToFile(file, data);
    
    return file;
  }
  
  protected String loadImageAsBase64(String path) throws Exception {
    
    URL url = this.getClass().getResource(path);
    java.nio.file.Path resPath = java.nio.file.Paths.get(url.toURI());
    byte[] data = FileUtils.readFileToByteArray(new File(url.toURI()));
    
    String base64 = org.apache.commons.codec.binary.Base64.encodeBase64String(data);
    System.out.println(base64.length());
    return base64;
  }

}
