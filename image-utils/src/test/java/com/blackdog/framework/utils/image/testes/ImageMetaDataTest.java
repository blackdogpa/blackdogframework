package com.blackdog.framework.utils.image.testes;

import com.google.common.base.Preconditions;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.jpeg.JpegParser;
//import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.*;

public class ImageMetaDataTest {

  private static final Logger log = LoggerFactory.getLogger(ImageMetaDataTest.class);
  private static final File fileName = new File("/home/thiago/Workspaces/BlackDog-Framework/BlackDog-Framework/image-utils/src/test/resources/images/jobs-1399X2131.jpg");
  private Tika tika;

  @Before
  public void setUp() throws IOException {
      tika = new Tika();
      log.info("Opening '{}'", fileName);
  }

  @Test
  public void testImageMetadataCameraModel() throws IOException, SAXException, TikaException {
      final Metadata metadata = new Metadata();
      final ContentHandler handler = new DefaultHandler();
      final Parser parser = new JpegParser();
      final ParseContext context = new ParseContext();
      final String mimeType;
      try (InputStream stream = Preconditions.checkNotNull(IOUtils.toBufferedInputStream(FileUtils.openInputStream(fileName)),
              "Cannot open file '%s'", fileName)) {
          mimeType = tika.detect(stream);
          metadata.set(Metadata.CONTENT_TYPE, mimeType);
      }
      try (InputStream stream = Preconditions.checkNotNull(IOUtils.toBufferedInputStream(FileUtils.openInputStream(fileName)),
              "Cannot open file '%s'", fileName)) {
          parser.parse(stream, handler, metadata, context);
      }
      log.info("Model: {}", metadata.get("Model"));
      for (final String name : metadata.names()) {
          log.info("{} = {}", name, metadata.get(name));
          System.out.println(name  + " " + metadata.get(name));
      }
      
      
      System.out.println(metadata.get("Model"));
      //Assert.assertThat("The EXIF Model should be filled",
      //        metadata.get("Model"), Matchers.not(Matchers.isEmptyOrNullString()));
  }

}
