package com.blackdog.framework.utils.image.testes;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;
import org.junit.Test;

import net.coobird.thumbnailator.Thumbnails;

public class Java2DTest {

  @Test
  public void testScale1100() throws Exception {

    BufferedImage masterImage = ImageIO.read(new File("/home/thiago/Workspaces/Prodepa/Crypto/Criptor-1.0-master/codigo_fonte/java/src/test/resources/avatars/barackobama-1100K.jpg"));
    
    BufferedImage res = getScaledInstance(masterImage, 120, 100, null, true);
    
    File outputfile = new File("/home/thiago/Downloads/TMP/TEST/image.jpg");
    ImageIO.write(res, "jpg", outputfile);
    
  }
  
  @Test
  public void testScale101() throws Exception {

    BufferedImage masterImage = ImageIO.read(new File("/home/thiago/Workspaces/Prodepa/Crypto/Criptor-1.0-master/codigo_fonte/java/src/test/resources/avatars/jobs_fundo_branco.jpg"));
    
    BufferedImage res = getScaledInstance(masterImage, 120, 100, null, true);

    File outputfile = new File("/home/thiago/Downloads/TMP/TEST/image.jpg");
    ImageIO.write(res, "jpg", outputfile);

  }

  public BufferedImage getScaledInstance(BufferedImage img, int targetWidth, int targetHeight, Object hint, boolean higherQuality) {
    int type = (img.getTransparency() == Transparency.OPAQUE) ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
    BufferedImage ret = (BufferedImage) img;
    int w, h;
    if (higherQuality) {
      // Use multi-step technique: start with original size, then
      // scale down in multiple passes with drawImage()
      // until the target size is reached
      w = img.getWidth();
      h = img.getHeight();
    } else {
      // Use one-step technique: scale directly from original
      // size to target size with a single drawImage() call
      w = targetWidth;
      h = targetHeight;
    }

    do {
      if (higherQuality && w > targetWidth) {
        w /= 2;
        if (w < targetWidth) {
          w = targetWidth;
        }
      }

      if (higherQuality && h > targetHeight) {
        h /= 2;
        if (h < targetHeight) {
          h = targetHeight;
        }
      }

      BufferedImage tmp = new BufferedImage(w, h, type);
      Graphics2D g2 = tmp.createGraphics();
      //g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, hint);
      
      g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
      g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      
      g2.drawImage(ret, 0, 0, w, h, null);
      g2.dispose();

      ret = tmp;
    } while (w != targetWidth || h != targetHeight);

    return ret;
  }

}
