package com.blackdog.framework.utils.image.testes;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;
import org.junit.Test;

import net.coobird.thumbnailator.Thumbnails;

public class ThumbnailsTest {

  
  @Test
  public void testScale() throws Exception {
    
    Thumbnails.of("/home/thiago/Workspaces/Prodepa/Crypto/Criptor-1.0-master/codigo_fonte/java/src/test/resources/avatars/barackobama-1100K.jpg")
    .size(120, 90)
    .toFile("/home/thiago/Downloads/TMP/resize.jpg");
   
    
    
    Thumbnails.of("/home/thiago/Workspaces/Prodepa/Crypto/Criptor-1.0-master/codigo_fonte/java/src/test/resources/avatars/barackobama-1100K.jpg")
    .size(120, 90)
    .outputFormat("JPG")
    .outputQuality(0.5)
    .toFile("/home/thiago/Downloads/TMP/resize2.jpg");
  }
  
}
