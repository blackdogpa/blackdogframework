package com.blackdog.framework.utils.image.testes;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;

import org.xml.sax.SAXException;
import org.junit.Test;

public class TikaLibTest {

  
  @Test
  public void testScale() throws Exception {
    
    
    //Assume that boy.jpg is in your current directory
    File file = new File("/home/thiago/Workspaces/BlackDog-Framework/BlackDog-Framework/image-utils/src/test/resources/images/jobs-1399X2131.jpg");

    //Parser method parameters
    Parser parser = new AutoDetectParser();
    BodyContentHandler handler = new BodyContentHandler();
    Metadata metadata = new Metadata();
    FileInputStream inputstream = new FileInputStream(file);
    ParseContext context = new ParseContext();
    
    parser.parse(inputstream, handler, metadata, context);
    System.out.println(handler.toString());

    //getting the list of all meta data elements 
    String[] metadataNames = metadata.names();

    
    
    System.out.println("File Size       " + metadata.get("File Size"));
    System.out.println("Image Width     " + metadata.get("Image Width"));
    System.out.println("Image Height    " + metadata.get("Image Height"));
    System.out.println("Content-Type    " + metadata.get("Content-Type"));
    System.out.println("X Resolution    " + metadata.get("X Resolution"));
    System.out.println("Y Resolution    " + metadata.get("Y Resolution"));
    System.out.println("Data Precision  " + metadata.get("Data Precision"));
    
    System.out.println("Resolution Units   " + metadata.get("Resolution Units"));
    System.out.println("File Modified Date " + metadata.get("File Modified Date"));
    System.out.println("Compression Type   " + metadata.get("Compression Type"));
   
    System.out.println("\n>>>>>>>>>>>>>\n");
    
    for(String name : metadataNames) {            
      System.out.println(name + ":");
      System.out.println("  " + metadata.get(name));
    }
   
    
    
    
  }
  
}
