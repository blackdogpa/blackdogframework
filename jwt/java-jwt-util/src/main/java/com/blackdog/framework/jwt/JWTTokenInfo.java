package com.blackdog.framework.jwt;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.impl.PublicClaims;

public class JWTTokenInfo {

  private final Map<String, Object> payloadClaims;
  private Map<String, Object> headerClaims;

  JWTTokenInfo() {
      this.payloadClaims = new HashMap<>();
      this.headerClaims = new HashMap<>();
  }

  /**
   * Add specific Claims to set as the Header.
   *
   * @param headerClaims the values to use as Claims in the token's Header.
   * @return this same JWTTokenInfo instance.
   */
  public JWTTokenInfo withHeader(Map<String, Object> headerClaims) {
      this.headerClaims = new HashMap<>(headerClaims);
      return this;
  }

  /**
   * Add a specific Issuer ("iss") claim.
   *
   * @param issuer the Issuer value.
   * @return this same JWTTokenInfo instance.
   */
  public JWTTokenInfo withIssuer(String issuer) {
      addClaim(PublicClaims.ISSUER, issuer);
      return this;
  }

  /**
   * Add a specific Subject ("sub") claim.
   *
   * @param subject the Subject value.
   * @return this same JWTTokenInfo instance.
   */
  public JWTTokenInfo withSubject(String subject) {
      addClaim(PublicClaims.SUBJECT, subject);
      return this;
  }

  /**
   * Add a specific Audience ("aud") claim.
   *
   * @param audience the Audience value.
   * @return this same JWTTokenInfo instance.
   */
  public JWTTokenInfo withAudience(String... audience) {
      addClaim(PublicClaims.AUDIENCE, audience);
      return this;
  }

  /**
   * Add a specific Expires At ("exp") claim.
   *
   * @param expiresAt the Expires At value.
   * @return this same JWTTokenInfo instance.
   */
  public JWTTokenInfo withExpiresAt(Date expiresAt) {
      addClaim(PublicClaims.EXPIRES_AT, expiresAt);
      return this;
  }

  /**
   * Add a specific Not Before ("nbf") claim.
   *
   * @param notBefore the Not Before value.
   * @return this same JWTTokenInfo instance.
   */
  public JWTTokenInfo withNotBefore(Date notBefore) {
      addClaim(PublicClaims.NOT_BEFORE, notBefore);
      return this;
  }

  /**
   * Add a specific Issued At ("iat") claim.
   *
   * @param issuedAt the Issued At value.
   * @return this same JWTTokenInfo instance.
   */
  public JWTTokenInfo withIssuedAt(Date issuedAt) {
      addClaim(PublicClaims.ISSUED_AT, issuedAt);
      return this;
  }

  /**
   * Add a specific JWT Id ("jti") claim.
   *
   * @param jwtId the Token Id value.
   * @return this same JWTTokenInfo instance.
   */
  public JWTTokenInfo withJWTId(String jwtId) {
      addClaim(PublicClaims.JWT_ID, jwtId);
      return this;
  }

  /**
   * Add a custom Claim value.
   *
   * @param name  the Claim's name.
   * @param value the Claim's value.
   * @return this same JWTTokenInfo instance.
   * @throws IllegalArgumentException if the name is null.
   */
  public JWTTokenInfo withClaim(String name, Boolean value) throws IllegalArgumentException {
      assertNonNull(name);
      addClaim(name, value);
      return this;
  }

  /**
   * Add a custom Claim value.
   *
   * @param name  the Claim's name.
   * @param value the Claim's value.
   * @return this same JWTTokenInfo instance.
   * @throws IllegalArgumentException if the name is null.
   */
  public JWTTokenInfo withClaim(String name, Integer value) throws IllegalArgumentException {
      assertNonNull(name);
      addClaim(name, value);
      return this;
  }

  /**
   * Add a custom Claim value.
   *
   * @param name  the Claim's name.
   * @param value the Claim's value.
   * @return this same JWTTokenInfo instance.
   * @throws IllegalArgumentException if the name is null.
   */
  public JWTTokenInfo withClaim(String name, Double value) throws IllegalArgumentException {
      assertNonNull(name);
      addClaim(name, value);
      return this;
  }

  /**
   * Add a custom Claim value.
   *
   * @param name  the Claim's name.
   * @param value the Claim's value.
   * @return this same JWTTokenInfo instance.
   * @throws IllegalArgumentException if the name is null.
   */
  public JWTTokenInfo withClaim(String name, String value) throws IllegalArgumentException {
      assertNonNull(name);
      addClaim(name, value);
      return this;
  }

  /**
   * Add a custom Claim value.
   *
   * @param name  the Claim's name.
   * @param value the Claim's value.
   * @return this same JWTTokenInfo instance.
   * @throws IllegalArgumentException if the name is null.
   */
  public JWTTokenInfo withClaim(String name, Date value) throws IllegalArgumentException {
      assertNonNull(name);
      addClaim(name, value);
      return this;
  }

  /**
   * Add a custom Array Claim with the given items.
   *
   * @param name  the Claim's name.
   * @param items the Claim's value.
   * @return this same JWTTokenInfo instance.
   * @throws IllegalArgumentException if the name is null.
   */
  public JWTTokenInfo withArrayClaim(String name, String[] items) throws IllegalArgumentException {
      assertNonNull(name);
      addClaim(name, items);
      return this;
  }

  /**
   * Add a custom Array Claim with the given items.
   *
   * @param name  the Claim's name.
   * @param items the Claim's value.
   * @return this same JWTTokenInfo instance.
   * @throws IllegalArgumentException if the name is null.
   */
  public JWTTokenInfo withArrayClaim(String name, Integer[] items) throws IllegalArgumentException {
      assertNonNull(name);
      addClaim(name, items);
      return this;
  }

  /**
   * Creates a new JWT and signs is with the given algorithm
   *
   * @param algorithm used to sign the JWT
   * @return a new JWT token
   * @throws IllegalArgumentException if the provided algorithm is null.
   * @throws JWTCreationException     if the claims could not be converted to a valid JSON or there was a problem with the signing key.
   */
 /* public String sign(Algorithm algorithm) throws IllegalArgumentException, JWTCreationException {
      if (algorithm == null) {
          throw new IllegalArgumentException("The Algorithm cannot be null.");
      }
      headerClaims.put(PublicClaims.ALGORITHM, algorithm.getName());
      return new JWTCreator(algorithm, headerClaims, payloadClaims).sign();
  }*/

  private void assertNonNull(String name) {
      if (name == null) {
          throw new IllegalArgumentException("The Custom Claim's name can't be null.");
      }
  }

  private void addClaim(String name, Object value) {
      if (value == null) {
          payloadClaims.remove(name);
          return;
      }
      payloadClaims.put(name, value);
  }
}
