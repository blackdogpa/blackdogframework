package com.blackdog.framework.jwt;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.impl.ClaimsHolder;
import com.auth0.jwt.interfaces.Claim;
import com.blackdog.framework.exceptions.SecurityException;

public class JWTUtil {

  public String createAndSignTokenHS256() {
    try {
      
      
      String token = JWT.create()
                        .withIssuer("auth0")
                        .sign(Algorithm.HMAC256("secret"));
      
      System.out.println(token);
      
      
      Map<String, Object> headerClaims = new HashMap<>();
      headerClaims.put("T1", "TESTE Header");
      
      String token2 = JWT.create()
              .withIssuer("auth0")
              .withClaim("IsValid", true)
              .withClaim("quando", new Date())
              .withExpiresAt(new Date())
              .withHeader(headerClaims )
              .withIssuedAt(new Date())
              .withJWTId("MEUID")
              .withNotBefore(new Date())
              .withSubject("MEU SUBJECT")
              .sign(Algorithm.HMAC256("secret"));
      
      System.out.println(token2);
      
      
      JWT jwt = JWT.decode(token2);
      
      System.out.println(jwt.getIssuer());
      System.out.println(jwt.getClaims());
      System.out.println(jwt.getExpiresAt());
      System.out.println(jwt.getKeyId());
      System.out.println(jwt.getSubject());
      System.out.println(jwt.getSignature());
      System.out.println(jwt.getHeaderClaim("T1").toString());
      
      
    } catch (JWTCreationException e) {
      e.printStackTrace();
      throw new SecurityException(e, "Falha na criação do Token: {0}", e.getMessage());
    } catch (Exception e) {
      e.printStackTrace();
      throw new SecurityException(e, "Falha na criação do Token: {0}", e.getMessage());
    }
    
    
    return "";
  }
  
  
  public String createAndSignTokenRS256() {
    
    return "";
  }
  
  public void verifyToken() {

  }
  
  /**
   * Decodificar o Token 
   * 
   * TODO  Acho que esse resultado deve ser encapsulado
   * 
   * @param token
   * @return
   */
  public JWT decodeToken(String token) {

    try {
      JWT jwt = JWT.decode(token);

      System.out.println(jwt.getAlgorithm());
      System.out.println(jwt.getId());
      System.out.println(jwt.getIssuer());
      System.out.println(jwt.getKeyId());
      System.out.println(jwt.getSignature());
      System.out.println(jwt.getSubject());
      System.out.println(jwt);
      
      return jwt;

    } catch (JWTDecodeException e) {
      throw new SecurityException(e, "Token Inválido: {0}", e.getMessage());
    }
    
    
  }
  
  
}
