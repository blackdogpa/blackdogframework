package com.blackdog.framework.jwt;

import org.junit.Test;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;

import static org.junit.Assert.fail;

import java.io.UnsupportedEncodingException;
import java.security.PrivateKey;
import java.security.interfaces.RSAKey;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JavaJWTTest {

  @Test
  public void decodeTest() {

    String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXUyJ9.eyJpc3MiOiJhdXRoMCJ9.AbIJTDMFc7yUa5MhvcP03nJPyCPzZtQcGEp-zWfOkEE";
    try {
      JWT jwt = JWT.decode(token);

      System.out.println(jwt.getAlgorithm());
      System.out.println(jwt.getId());
      System.out.println(jwt.getIssuer());
      System.out.println(jwt.getKeyId());
      System.out.println(jwt.getSignature());
      System.out.println(jwt.getSubject());
      System.out.println(jwt);

    } catch (JWTDecodeException exception) {
      // Invalid token
      exception.printStackTrace();
    }
  }

  @Test
  public void createAndSignTokenTestHS256() throws IllegalArgumentException, UnsupportedEncodingException {

    try {

      String token = JWT.create()
                        .withIssuer("auth0")
                        .sign(Algorithm.HMAC256("secret"));
      
      System.out.println(token);
      
      
      Map<String, Object> headerClaims = new HashMap<>();
      headerClaims.put("T1", "TESTE Header");
      
      String token2 = JWT.create()
              .withIssuer("auth0")
              .withClaim("IsValid", true)
              .withClaim("quando", new Date())
              .withExpiresAt(new Date())
              .withHeader(headerClaims )
              .withIssuedAt(new Date())
              .withJWTId("MEUID")
              .withNotBefore(new Date())
              .withSubject("MEU SUBJECT")
              .sign(Algorithm.HMAC256("secret"));
      
      System.out.println(token2);
      
      
      JWT jwt = JWT.decode(token2);
      
      System.out.println(jwt.getIssuer());
      System.out.println(jwt.getClaims());
      System.out.println(jwt.getExpiresAt());
      System.out.println(jwt.getKeyId());
      System.out.println(jwt.getSubject());
      System.out.println(jwt.getSignature());
      System.out.println(jwt.getHeaderClaim("T1").toString());
      
      
      
      
      JWTVerifier verifier = JWT.require(Algorithm.HMAC256("secret"))
              .withIssuer("auth02")
              .build(); //Reusable verifier instance
      
      JWT jwtVer = (JWT) verifier.verify(token);
      
      System.out.println(jwtVer.getIssuer());
      
    } catch (JWTCreationException exception) {
      // Invalid Signing configuration / Couldn't convert Claims.
      exception.printStackTrace();
    }

  }
  
  @Test
  public void verifyTokenHS256Test() throws IllegalArgumentException, UnsupportedEncodingException {
    String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXUyJ9.eyJpc3MiOiJhdXRoMCJ9.AbIJTDMFc7yUa5MhvcP03nJPyCPzZtQcGEp-zWfOkEE";
    try {
        JWTVerifier verifier = JWT.require(Algorithm.HMAC256("secret"))
            .withIssuer("auth0")
            .build(); //Reusable verifier instance
        JWT jwt = (JWT) verifier.verify(token);
    } catch (JWTVerificationException exception){
        //Invalid signature/claims
      exception.printStackTrace();
    }
  }
  
  
  
  
  public void createRSA() {
    //RsaJsonWebKey chave = RsaJwkGenerator.generateJwk(2048);
    
    
    RSAKey key = null ;
       
    Algorithm.RSA256(key);
    
  }
  
  @Test
  public void createAndSignTokenTestRS256() throws IllegalArgumentException, UnsupportedEncodingException {
    
    /*PrivateKey key = null;//Get the key instance
            try {
                String token = JWT.create()
                    .withIssuer("auth0")
                    .sign(Algorithm.RSA256(key));
            } catch (JWTCreationException exception){
                //Invalid Signing configuration / Couldn't convert Claims.
            }*/
    
  }

}
