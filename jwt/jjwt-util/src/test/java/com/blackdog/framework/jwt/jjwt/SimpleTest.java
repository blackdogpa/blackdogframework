package com.blackdog.framework.jwt.jjwt;

import static org.junit.Assert.fail;

import java.security.Key;

import org.junit.Test;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.CompressionCodecs;
import io.jsonwebtoken.IncorrectClaimException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MissingClaimException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;

public class SimpleTest {

  @Test
  public void testCreatSignToken() {
  
    Key key = MacProvider.generateKey();

    //Sem Compressao
    String compactJws = Jwts.builder()
      .setSubject("Joe")
      .signWith(SignatureAlgorithm.HS512, key)
      .compact();
    
    System.out.println("NO COMPRESS: " + compactJws);
    
    try {
      Jws<Claims> claims = Jwts.parser()
          .requireSubject("Joe")
          //.require("hasMotorcycle", true)
          .setSigningKey(key)
          .parseClaimsJws(compactJws);
      
      System.out.println("SUBJECT: " + claims.getBody().getSubject());
    } catch (MissingClaimException e) {
      e.printStackTrace();
    } catch (IncorrectClaimException e) {
      e.printStackTrace();
    }
    
    
    System.out.println();
    
    //Com Compressao
    compactJws =  Jwts.builder()
            .setSubject("Joe")
            .compressWith(CompressionCodecs.DEFLATE)
            .signWith(SignatureAlgorithm.HS512, key)
            .compact();
    
    System.out.println("   COMPRESS: " + compactJws); 
    
    
    
    try {
      Jws<Claims> claims = Jwts.parser()
          .requireSubject("Joe")
          //.require("hasMotorcycle", true)
          .setSigningKey(key)
          .parseClaimsJws(compactJws);
      
      System.out.println("SUBJECT: " + claims.getBody().getSubject());
    } catch (MissingClaimException e) {
      e.printStackTrace();
    } catch (IncorrectClaimException e) {
      e.printStackTrace();
    }
    
  }

  @Test
  public void testCreateAndSignTokenRS256() {
    fail("Not yet implemented");
  }

  @Test
  public void testVerifyToken() {
    fail("Not yet implemented");
  }

}
