package com.blackdog.framework.mail;

public class MailEvent {

  private String to; //recipient address
  private String toPerson;
  private String subject;
  private String message;
  
  public String getTo() {
    return to;
  }
  public void setTo(String to) {
    this.to = to;
  }
  public String getMessage() {
    return message;
  }
  public void setMessage(String message) {
    this.message = message;
  }
  public String getSubject() {
    return subject;
  }
  public void setSubject(String subject) {
    this.subject = subject;
  }
  public String getToPerson() {
    return toPerson;
  }
  public void setToPerson(String toPerson) {
    this.toPerson = toPerson;
  }
  
  
}
