package com.blackdog.framework.mail;

import javax.ejb.Asynchronous;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;

import com.blackdog.framework.mail.exception.MailException;

@Singleton
public class MailService {

  //@Inject 
  //private Instance<SessionConfig> sessionConfigInstance;
  
  //@Inject
  //private ApplicationConfig appConfig; 
  
  
  @Asynchronous
  @Lock(LockType.READ)
  public void sendMail(@Observes(during = TransactionPhase.AFTER_SUCCESS) MailEvent event) throws MailException {
      SessionConfig config = getSessionConfig();
      
      if(config != null) {
        
        MailUtils mail = new MailUtils(config);
        
        //mail.sendSimpleMail(event.getTo(), event.getToPerson(), event.getSubject(), UUID.randomUUID().toString() + ": Teste de Email " + event.getMessage() );
        
        mail.sendHtmlMail(event.getTo(), event.getToPerson(), event.getSubject(), event.getMessage() );
      }
  }
  
  public SessionConfig getSessionConfig() {
    /*try {
      String strConfig = appConfig.get(PropertiesUtils.MAIL_CONFIG);
      
      SessionConfig config = null;
      if(strConfig != null) {
        config  = JacksonJsonUtils.fromJson(strConfig, SessionConfig.class);
      } else {
        return null;
      }
      
      if(config == null) {
        return null;
      } else {
        return config;
      }
    } catch (Exception e) {
      return null;
    }*/
    
    SessionConfig config = new SessionConfig();
    /*config.setSmtpHost("mail.prodepa.pa.gov.br");
    config.setSmtpPort("25");
    config.setSmtpAuth("true");
    config.setSmtpSocketFactory("javax.net.ssl.SSLSocketFactory");
    config.setSmtpSocketFactoryPort("25");
    config.setSendName("Mail Service");
    config.setSendUser("thiago.soares@prodepa.pa.gov.br");
    config.setSendPasswd("6556");*/
    
    
    config.setSmtpHost("mx.sistemas.pa.gov.br");
    config.setSmtpPort("587");
    config.setSmtpAuth("true");
    config.setSmtpSocketFactory("javax.net.ssl.SSLSocketFactory");
    config.setSmtpSocketFactoryPort("587");
    config.setSendName("Mail Service");
    config.setSendUser("sispatweb@sistemas.pa.gov.br");
    config.setSendPasswd("gn577i9o");
    return config;
  }
  
}
