package com.blackdog.framework.mail;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import com.blackdog.framework.mail.exception.MailException;

public class MailUtils {
  
  private SessionConfig mailConfig;
  
  public MailUtils(SessionConfig config) {
    super();
    this.mailConfig = config;
  }
  
  private Session getSession() throws MailException {
    
    if(mailConfig != null) {

      Properties props = new Properties();
      props.put("mail.smtp.host", mailConfig.getSmtpHost());
      props.put("mail.smtp.auth", mailConfig.getSmtpAuth());
      props.put("mail.smtp.port", mailConfig.getSmtpPort());
      //props.put("mail.smtp.socketFactory.port", mailConfig.getSmtpSocketFactoryPort());
      //props.put("mail.smtp.socketFactory.class", mailConfig.getSmtpSocketFactory());
      
      //props.put("mail.smtp.starttls.enable", false);
      //props.put("mail.smtp.port", 25);
      
      props.put("mail.smtp.starttls.enable","true");
      props.put("mail.smtp.ssl.trust", mailConfig.getSmtpHost());

      
      props.put("mail.transport.protocol", "smtp");
      props.put("mail.smtp.starttls.enable", "true");
      props.put("mail.smtp.starttls.enable", "true");
      
      props.put("mail.smtp.socketFactory.fallback", "true"); // Should be true
      
      
      //Session session = Session.getDefaultInstance(props, null);
      /*Session session = Session.getDefaultInstance(props,
              new javax.mail.Authenticator() {
        protected PasswordAuthentication getPasswordAuthentication() {
          return new PasswordAuthentication(mailConfig.getSendUser(),mailConfig.getSendPasswd());
        }
      });*/
      
      Session session = Session.getInstance(props,
              new javax.mail.Authenticator() {
        protected PasswordAuthentication getPasswordAuthentication() {
          return new PasswordAuthentication(mailConfig.getSendUser(),mailConfig.getSendPasswd());
          //return new PasswordAuthentication("sispatweb@sistemas.pa.gov.br", "gn577i9o");
        }
      });
      
      session.setDebug(mailConfig.getDebug());
      
      return session;
      
    } else {
      throw new MailException("Mail configuration not defined");
    }
    
  }

  public void sendMailAsync() {
    
  }
  
  public void sendSimpleMail(String toAddress, String toPerson, String subject, String textBody) throws MailException {

    Session session = getSession();

    try {
      Message msg = new MimeMessage(session);
      msg.setFrom(new InternetAddress(mailConfig.getSendUser(), mailConfig.getSendName()));
      msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress, toPerson));
      
      msg.setSubject(subject);
      
      msg.setText(textBody);
      
      Transport.send(msg);
      
      Transport transport = session.getTransport();
      transport.connect();
      transport.sendMessage(msg, InternetAddress.parse(toAddress));
      //transport.send(msg, InternetAddress.parse("myemail@gmail.com"));//(message);
    } catch (AddressException e) {
      // ...
      e.printStackTrace();
      throw new MailException(e, "Mail Fail");
    } catch (MessagingException e) {
      // ...
      e.printStackTrace();
      throw new MailException(e, "Mail Fail");
    } catch (UnsupportedEncodingException e) {
      // ...
      e.printStackTrace();
      throw new MailException(e, "Mail Fail");
    }
  }
  
  
  public void sendHtmlMail(String toAddress, String toPerson, String subject, String htmlBody) throws MailException {

    Session session = getSession();

    try {
      Message msg = new MimeMessage(session);
      msg.setFrom(new InternetAddress(mailConfig.getSendUser(), mailConfig.getSendName()));
      msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress, toPerson));
      
      msg.setSubject(subject);
      
      //msg.setText("");
      
      Multipart mp = new MimeMultipart();
      
      MimeBodyPart htmlPart = new MimeBodyPart();
      htmlPart.setContent(htmlBody, "text/html");
      mp.addBodyPart(htmlPart);
      
      msg.setContent(mp);
      
      Transport.send(msg);
    } catch (AddressException e) {
      // ...
      e.printStackTrace();
      throw new MailException(e, "Mail Fail");
    } catch (MessagingException e) {
      // ...
      e.printStackTrace();
      throw new MailException(e, "Mail Fail");
    } catch (UnsupportedEncodingException e) {
      // ...
      e.printStackTrace();
      throw new MailException(e, "Mail Fail");
    }
  }
  
  
  public void sendMultipartMail(String toAddress, String toPerson, String subject, String htmlBody, String attachmentName, String attachmentPath, String attachmentMime) throws IOException, MailException {
    
    Session session = getSession();

    try {
      Message msg = new MimeMessage(session);
      msg.setFrom(new InternetAddress(mailConfig.getSendUser(), mailConfig.getSendName()));
      msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress, toPerson));
      msg.setSubject(subject);
      //msg.setText(msgBody);

      
      
      Multipart mp = new MimeMultipart();

      MimeBodyPart htmlPart = new MimeBodyPart();
      htmlPart.setContent(htmlBody, "text/html");
      mp.addBodyPart(htmlPart);

      if(attachmentPath != null) {
        
        byte[] attachmentData = loadFile(attachmentPath);
        MimeBodyPart attachment2 = new MimeBodyPart();
        DataSource dataSrc = new ByteArrayDataSource(attachmentData, attachmentMime);
        attachment2.setDataHandler(new DataHandler(dataSrc));
        attachment2.setFileName(attachmentName);
        mp.addBodyPart(attachment2);
      
      }
      
      msg.setContent(mp);

      Transport.send(msg);

    } catch (AddressException e) {
      // ...
      e.printStackTrace();
      throw new MailException(e, "Mail Fail");
    } catch (MessagingException e) {
      // ...
      e.printStackTrace();
      throw new MailException(e, "Mail Fail");
    } catch (UnsupportedEncodingException e) {
      // ...
      e.printStackTrace();
      throw new MailException(e, "Mail Fail");
    }
  }
  
  private byte[] loadFile(String sourcePath) throws IOException {
    InputStream inputStream = null;
    try {
      inputStream = new FileInputStream(sourcePath);
      return readFully(inputStream);
    } finally {
      if (inputStream != null) {
        inputStream.close();
      }
    }
  }

  private byte[] readFully(InputStream stream) throws IOException {
    byte[] buffer = new byte[8192];
    ByteArrayOutputStream baos = new ByteArrayOutputStream();

    int bytesRead;
    while ((bytesRead = stream.read(buffer)) != -1) {
      baos.write(buffer, 0, bytesRead);
    }
    return baos.toByteArray();
  }
  
}
