package com.blackdog.framework.mail;

public class SessionConfig {

  private String smtpHost;//", "smtp.gmail.com");
  private String smtpSocketFactoryPort;//", "465");
  private String smtpSocketFactory;//.class", "javax.net.ssl.SSLSocketFactory");
  private String smtpAuth;//", "true");
  private String smtpPort;//", "465");
  
  private String sendName;
  private String sendUser; //"thiago.soares.jr@gmail.com"
  private String sendPasswd; //"337626337"
  
  private Boolean debug = false;
  
  public SessionConfig() {
    super();
  }

  public String getSmtpHost() {
    return smtpHost;
  }

  public void setSmtpHost(String smtpHost) {
    this.smtpHost = smtpHost;
  }

  public String getSmtpSocketFactoryPort() {
    return smtpSocketFactoryPort;
  }

  public void setSmtpSocketFactoryPort(String smtpSocketFactoryPort) {
    this.smtpSocketFactoryPort = smtpSocketFactoryPort;
  }

  public String getSmtpSocketFactory() {
    return smtpSocketFactory;
  }

  public void setSmtpSocketFactory(String smtpSocketFactory) {
    this.smtpSocketFactory = smtpSocketFactory;
  }

  public String getSmtpAuth() {
    return smtpAuth;
  }

  public void setSmtpAuth(String smtpAuth) {
    this.smtpAuth = smtpAuth;
  }

  public String getSmtpPort() {
    return smtpPort;
  }

  public void setSmtpPort(String smtpPort) {
    this.smtpPort = smtpPort;
  }

  public String getSendUser() {
    return sendUser;
  }

  public void setSendUser(String sendUser) {
    this.sendUser = sendUser;
  }

  public String getSendPasswd() {
    return sendPasswd;
  }

  public void setSendPasswd(String sendPasswd) {
    this.sendPasswd = sendPasswd;
  }

  public String getSendName() {
    return sendName;
  }

  public void setSendName(String sendName) {
    this.sendName = sendName;
  }

  public Boolean getDebug() {
    return debug;
  }

  public void setDebug(Boolean debug) {
    this.debug = debug;
  }
  
  
}
