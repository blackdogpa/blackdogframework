package com.blackdog.framework.mail.exception;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.WebFault;

import com.blackdog.framework.core.constants.BlackDogConstants;
import com.blackdog.framework.core.exceptions.BlackExceptions;
import com.blackdog.framework.core.exceptions.ErrorMessage;
import com.blackdog.framework.core.exceptions.Validation;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
@WebFault(name="MailExceptionFault", targetNamespace= BlackDogConstants.EXCEPTION_NAMESPACE)
public class MailException extends Exception implements BlackExceptions {

  private static final long serialVersionUID = -5022967813902834759L;

  private String id;
  private String code;
  
  private List<Validation> validations = new ArrayList<Validation>();
  
  private String transactionId;
  private String applicationId;
  private String applicationName;
  
  public MailException() {
    super();
  }

  public MailException(String message, Object... args) {
    super(messageFormat(message, args));
    this.validations = new ArrayList<Validation>();
    this.validations.add(new Validation(messageFormat(message, args)));
  }
  
  public MailException(Throwable cause) {
    super(cause);
    this.validations = new ArrayList<Validation>();
    this.validations.add(new Validation(cause.getMessage()));
  }
  
  public MailException(Throwable cause, Validation validation) {
    super(cause);
    this.validations = new ArrayList<Validation>();
    this.validations.add(validation);
  }
  
  public MailException(Validation validation) {
    super();
    this.validations = new ArrayList<Validation>();
    this.validations.add(validation);
  }

  public MailException(Throwable cause, String message, Object... args) {
    super(messageFormat(message, args), cause);
    this.validations = new ArrayList<Validation>();
    this.validations.add(new Validation(messageFormat(message, args)));
  }
  
  
  public MailException(List<Validation> validations) {
    super();
    this.validations = validations;
  }
  
  public MailException(List<Validation> validations, String message,  Object... args) {
    super(messageFormat(message, args));
    this.validations = validations;
  }
  
  public MailException(List<Validation> validations, Throwable cause) {
    super(cause);
    this.validations = validations;
  }
  
  public MailException(List<Validation> validations, Throwable cause, String message, Object... args) {
    super(messageFormat(message, args), cause);
    this.validations = validations;
  }

  public List<Validation> getValidations() {
    return validations;
  }

  public void setValidations(List<Validation> validations) {
    this.validations = validations;
  }
  
  @Override
  public ErrorMessage getErrorMessage() {
    
    ErrorMessage dto = new ErrorMessage();
    dto.setStatus("Internal Server Error");
    dto.setStatusCode(500);
    
    dto.setMessage("Falha de Sistema: " + this.getMessage());
    
    dto.getValidations().add(new Validation(this.getCode(), null, this.getMessage()));
    
    dto.setTransactionId(this.getId());
    dto.setApplicationId(this.getApplicationId());
    dto.setApplicationName(this.getApplicationName());
    
    dto.setType(this.getType());
    
    return dto;
  }
  
  @Override
  public String getType() {
    return MAIL;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    
    this.id = id;
  }
  
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
  
  public String getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(String applicationId) {
    this.applicationId = applicationId;
  }

  public String getApplicationName() {
    return applicationName;
  }

  public void setApplicationName(String applicationName) {
    this.applicationName = applicationName;
  }

  
  public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }
  
  @Override
  public String getMessage() {
    
    if(super.getMessage() == null || super.getMessage().equals("")) {
      return null;
    }
    
    StringBuilder builder = new StringBuilder();

    if(applicationId != null && applicationName != null) {
      
      builder.append("[");
      builder.append(applicationId);
      builder.append(".");
      builder.append(applicationName);
      builder.append("]");
      
      builder.append("::");
    }
    if (id != null) {
      //builder.append(id);
      //builder.append(" : ");
      builder.append(super.getMessage());
    } else {
      builder.append(super.getMessage());
    }
    return builder.toString();
  }
  
  @Override
  public String toString() {
    return this.getClass().getName() + "["
            + "id="+getId()
            + "applicationId"+getApplicationId()
            + "applicationName="+getApplicationName()
            //+ "validations=" + validations 
            + "#\n#"
            + ", Class=" + getClass() 
            + ", Cause=" + getCause() 
            + ", Message=" + getMessage() 
            + ", LocalizedMessage=" + getLocalizedMessage()
            + "]";
  }
  
  public static String messageFormat(String pattern, Object[] arguments) {
    if(pattern == null || pattern.equals("")) {
        return "";
    }
    return MessageFormat.format(pattern, arguments);
  }
  
  
}
