package com.blackdog.framework.mail;

import static org.junit.Assert.*;

import org.junit.Test;

//[START simple_includes]
import java.io.IOException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
//[END simple_includes]

//[START multipart_includes]
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import javax.activation.DataHandler;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

public class GmailUtilsTest {

  @Test
  public void testSendMail() {
    

    
    
  }

  @Test
  public void testSendMailAsync() {
    fail("Not yet implemented");
  }
  
  
  
  @Test
  public void test01() {
    
    //String type = "multipart";
    String type = "simple";
    
    if (type != null && type.equals("multipart")) {
      sendMultipartMail();
    } else {
      sendSimpleMail();
    }
    
  }
  
  String fromAddress = "tfs.capanema@gmail.com";
  String fromPerson = "Admin Sender";
  
  String toAddress = "thiago.soares.jr@gmail.com";
  String toPerson = "Person To";
  
  String subject = "Mail Teste";
  
  private void sendSimpleMail() {
    // [START simple_example]
    Properties props = new Properties();
    props.put("mail.smtp.host", "smtp.gmail.com");
    props.put("mail.smtp.socketFactory.port", "465");
    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.port", "465");
    
    //Session session = Session.getDefaultInstance(props, null);
    Session session = Session.getDefaultInstance(props,
            new javax.mail.Authenticator() {
              protected PasswordAuthentication getPasswordAuthentication() {
                //return new PasswordAuthentication("tfs.capanema@gmail.com","337626337");
                return new PasswordAuthentication("thiago.soares.jr@gmail.com","337626337");
              }
            });

    try {
      Message msg = new MimeMessage(session);
      msg.setFrom(new InternetAddress(fromAddress, fromPerson));
      msg.addRecipient(Message.RecipientType.TO, new InternetAddress(fromAddress, toPerson));
      msg.setSubject(subject);
      msg.setText("Teste de email");
      
      
      Transport.send(msg);
      
      
    } catch (AddressException e) {
      // ...
      e.printStackTrace();
    } catch (MessagingException e) {
      // ...
      e.printStackTrace();
    } catch (UnsupportedEncodingException e) {
      // ...
      e.printStackTrace();
    }
    // [END simple_example]
    System.out.println("Done");
  }

  private void sendMultipartMail() {
    Properties props = new Properties();
    Session session = Session.getDefaultInstance(props, null);

    String msgBody = "...";

    try {
      Message msg = new MimeMessage(session);
      msg.setFrom(new InternetAddress("admin@example.com", "Example.com Admin"));
      msg.addRecipient(Message.RecipientType.TO,
                       new InternetAddress("user@example.com", "Mr. User"));
      msg.setSubject("Your Example.com account has been activated");
      msg.setText(msgBody);

      // [START multipart_example]
      String htmlBody = "";          // ...
      byte[] attachmentData = null;  // ...
      Multipart mp = new MimeMultipart();

      MimeBodyPart htmlPart = new MimeBodyPart();
      htmlPart.setContent(htmlBody, "text/html");
      mp.addBodyPart(htmlPart);

      MimeBodyPart attachment = new MimeBodyPart();
      InputStream attachmentDataStream = new ByteArrayInputStream(attachmentData);
      attachment.setFileName("manual.pdf");
      attachment.setContent(attachmentDataStream, "application/pdf");
      mp.addBodyPart(attachment);

      msg.setContent(mp);
      // [END multipart_example]

      Transport.send(msg);

    } catch (AddressException e) {
      // ...
    } catch (MessagingException e) {
      // ...
    } catch (UnsupportedEncodingException e) {
      // ...
    }
  }

}
