package com.blackdog.framework.mail;

import static org.junit.Assert.*;

import org.junit.Test;

import com.blackdog.framework.mail.exception.MailException;

//[START simple_includes]
import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;
import java.util.UUID;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
//[END simple_includes]

//[START multipart_includes]
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

public class MailUtilsTest {
  
  String fromAddress = "tfs.capanema@gmail.com";
  String fromAddressExp = "thiago.soares@prodepa.pa.gov.br";
  String fromPerson = "Admin Sender";
  
  String toAddress = "thiago.soares.jr@gmail.com";
  String toPerson = "Person To";
  
  String subject = "Mail Teste";
  
  
  @Test
  public void testSendSimpleMail() throws MailException {
    
    SessionConfig config = new SessionConfig();
    config.setSmtpHost("mail.prodepa.pa.gov.br");
    config.setSmtpSocketFactory("javax.net.ssl.SSLSocketFactory");
    config.setSmtpSocketFactoryPort("25");
    config.setSmtpAuth("true");
    config.setSmtpPort("25");
    config.setSendName("CDAWS Mail");
    config.setSendUser("thiago.soares@prodepa.pa.gov.br");
    config.setSendPasswd("6556");
    
    new MailUtils(config).sendSimpleMail(fromAddressExp, toPerson, subject, "Teste de Email " + UUID.randomUUID().toString());
    
  }
  
  @Test
  public void test01() throws IOException {
    
    //String type = "simple";
    //String type = "multipart";
    String type = "html";
    
    if (type != null && type.equals("multipart")) {
      sendMultipartMail();
    } else if (type != null && type.equals("html")) {
      sendSimpleMailHtml();
    } else {
      sendSimpleMail();
    }
    
  }
  
  private Session getSession(String type) {
    
    if(type == "gmail") {
      Properties props = new Properties();
      props.put("mail.smtp.host", "smtp.gmail.com");
      props.put("mail.smtp.socketFactory.port", "465");
      props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
      props.put("mail.smtp.auth", "true");
      props.put("mail.smtp.port", "465");
      
      //Session session = Session.getDefaultInstance(props, null);
      Session session = Session.getDefaultInstance(props,
              new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                  //return new PasswordAuthentication("tfs.capanema@gmail.com","337626337");
                  return new PasswordAuthentication("thiago.soares.jr@gmail.com","337626337");
                }
              });
      
      return session;
    } else if(type == "expresso") {
      
      Properties props = new Properties();
      props.put("mail.smtp.host", "mail.prodepa.pa.gov.br");
      props.put("mail.smtp.socketFactory.port", "25");
      props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
      props.put("mail.smtp.auth", "true");
      props.put("mail.smtp.port", "25");
      
      //Session session = Session.getDefaultInstance(props, null);
      Session session = Session.getDefaultInstance(props,
              new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                  //return new PasswordAuthentication("tfs.capanema@gmail.com","337626337");
                  return new PasswordAuthentication("thiago.soares","6556");
                }
              });
      return session;
    } else {
      System.out.println("OPS...........");
      return null;
    }
    
  }
  
  private void sendSimpleMail() {
    // [START simple_example]
    /*Properties props = new Properties();
    props.put("mail.smtp.host", "smtp.gmail.com");
    props.put("mail.smtp.socketFactory.port", "465");
    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.port", "465");
    
    //Session session = Session.getDefaultInstance(props, null);
    Session session = Session.getDefaultInstance(props,
            new javax.mail.Authenticator() {
              protected PasswordAuthentication getPasswordAuthentication() {
                //return new PasswordAuthentication("tfs.capanema@gmail.com","337626337");
                return new PasswordAuthentication("thiago.soares.jr@gmail.com","337626337");
              }
            });*/
    
    //Session session = getSession("expresso");
    Session session = getSession("gmail");

    try {
      Message msg = new MimeMessage(session);
      //msg.setFrom(new InternetAddress(fromAddressExp, fromPerson));
      //msg.addRecipient(Message.RecipientType.TO, new InternetAddress(fromAddressExp, toPerson));
      msg.setFrom(new InternetAddress(fromAddress, fromPerson));
      msg.addRecipient(Message.RecipientType.TO, new InternetAddress(fromAddressExp, toPerson));
      msg.setSubject(subject);
      msg.setText("Teste de email");
      
      
      Transport.send(msg);
      
      
    } catch (AddressException e) {
      // ...
      e.printStackTrace();
    } catch (MessagingException e) {
      // ...
      e.printStackTrace();
    } catch (UnsupportedEncodingException e) {
      // ...
      e.printStackTrace();
    }
    // [END simple_example]
    System.out.println("Done");
  }
  
  private void sendSimpleMailHtml() {
    // [START simple_example]
    /*Properties props = new Properties();
    props.put("mail.smtp.host", "smtp.gmail.com");
    props.put("mail.smtp.socketFactory.port", "465");
    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.port", "465");
    
    //Session session = Session.getDefaultInstance(props, null);
    Session session = Session.getDefaultInstance(props,
            new javax.mail.Authenticator() {
              protected PasswordAuthentication getPasswordAuthentication() {
                //return new PasswordAuthentication("tfs.capanema@gmail.com","337626337");
                return new PasswordAuthentication("thiago.soares.jr@gmail.com","337626337");
              }
            });*/

    Session session = getSession("expresso");
    try {
      Message msg = new MimeMessage(session);
      msg.setFrom(new InternetAddress(fromAddressExp, fromPerson));
      msg.addRecipient(Message.RecipientType.TO, new InternetAddress(fromAddressExp, toPerson));
      msg.setSubject(subject);
      msg.setText("Teste de email");
      
      
      //String htmlBody = "<html> <head> <title>Example document</title> </head><body> <div> OK HTML Bydy Gmeil to exp </div>  </body> </html>"; // ..
      
      
      String htmlBody = load("html_templates/mail_tpl.html");
      
      Multipart mp = new MimeMultipart();
      
      MimeBodyPart htmlPart = new MimeBodyPart();
      htmlPart.setContent(htmlBody, "text/html");
      mp.addBodyPart(htmlPart);
      
      msg.setContent(mp);
      
      Transport.send(msg);
      
      
    } catch (AddressException e) {
      // ...
      e.printStackTrace();
    } catch (MessagingException e) {
      // ...
      e.printStackTrace();
    } catch (UnsupportedEncodingException e) {
      // ...
      e.printStackTrace();
    }
    // [END simple_example]
    System.out.println("Done");
  }
  
  public String load(String path) {
    
    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource(path).getFile());
    
    StringBuffer result= new StringBuffer();
    try (Scanner scanner = new Scanner(file)) {

      while (scanner.hasNextLine()) {
        String line = scanner.nextLine();
        result.append(line).append("\n");
      }

      scanner.close();

    } catch (IOException e) {
      e.printStackTrace();
    }
    
    

    return result.toString();    
  }

  private void sendMultipartMail() throws IOException {
    /*Properties props = new Properties();
    props.put("mail.smtp.host", "smtp.gmail.com");
    props.put("mail.smtp.socketFactory.port", "465");
    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.port", "465");
    
    Session session = Session.getDefaultInstance(props,
            new javax.mail.Authenticator() {
              protected PasswordAuthentication getPasswordAuthentication() {
                //return new PasswordAuthentication("tfs.capanema@gmail.com","337626337");
                return new PasswordAuthentication("thiago.soares.jr@gmail.com","337626337");
              }
            });*/
    
    Session session = getSession("expresso");

    String msgBody = "teste de email ...";

    try {
      Message msg = new MimeMessage(session);
      msg.setFrom(new InternetAddress(fromAddressExp, fromPerson));
      msg.addRecipient(Message.RecipientType.TO,
                       new InternetAddress(fromAddressExp, toPerson));
      msg.setSubject(subject);
      msg.setText(msgBody);

      // [START multipart_example]
      String htmlBody = "<html> <head> <title>Example document</title> </head>"+
                        "<body> <div> OK HTML  </div>  </body> </html>"; // ..
      
      byte[] attachmentData = loadFile("/home/thiago/Downloads/documento20160425111290.pdf");  // ...
      Multipart mp = new MimeMultipart();

      MimeBodyPart htmlPart = new MimeBodyPart();
      htmlPart.setContent(htmlBody, "text/html");
      mp.addBodyPart(htmlPart);

      //MimeBodyPart attachment = new MimeBodyPart();
      //InputStream attachmentDataStream = new ByteArrayInputStream(attachmentData);
      //attachment.setFileName("manual.pdf");
      //attachment.setDisposition(MimeBodyPart.ATTACHMENT);
      //attachment.setContent(attachmentDataStream, "application/pdf");
      //mp.addBodyPart(attachment);

      
      MimeBodyPart attachment2 = new MimeBodyPart();
      DataSource dataSrc = new ByteArrayDataSource(attachmentData, "application/pdf");
      attachment2.setDataHandler(new DataHandler(dataSrc));
      attachment2.setFileName("myPdfDocument.pdf");
      
      mp.addBodyPart(attachment2);
      
      msg.setContent(mp);
      // [END multipart_example]

      Transport.send(msg);

    } catch (AddressException e) {
      // ...
      e.printStackTrace();
    } catch (MessagingException e) {
      // ...
      e.printStackTrace();
    } catch (UnsupportedEncodingException e) {
      // ...
      e.printStackTrace();
    }
  }
  
  public static byte[] loadFile(String sourcePath) throws IOException {
    InputStream inputStream = null;
    try {
      inputStream = new FileInputStream(sourcePath);
      return readFully(inputStream);
    } finally {
      if (inputStream != null) {
        inputStream.close();
      }
    }
  }

  public static byte[] readFully(InputStream stream) throws IOException {
    byte[] buffer = new byte[8192];
    ByteArrayOutputStream baos = new ByteArrayOutputStream();

    int bytesRead;
    while ((bytesRead = stream.read(buffer)) != -1) {
      baos.write(buffer, 0, bytesRead);
    }
    return baos.toByteArray();
  }

}
