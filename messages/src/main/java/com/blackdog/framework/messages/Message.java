package com.blackdog.framework.messages;

import java.io.Serializable;

public class Message implements Serializable {
  
  private static final long serialVersionUID = 6712408750849365339L;
  
  private String severity;
  private String code;
  private String field;
  private String message;
  
  public Message() {
    super();
  }


  public Message(String severity, String message) {
    super();
    this.severity = severity;
    this.message = message;
  }


  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getField() {
    return field;
  }

  public void setField(String field) {
    this.field = field;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getSeverity() {
    return severity;
  }

  public void setSeverity(String severity) {
    this.severity = severity;
  }


  @Override
  public String toString() {
    return "Message [severity=" + severity + ", code=" + code + ", field=" + field + ", message=" + message + "]";
  }

}
