package com.blackdog.framework.messages;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;

@RequestScoped
public class MessageContext implements Serializable {

  private static final long serialVersionUID = -815751613517039934L;
  
  private List<Message> messages = new ArrayList<Message>();

  public MessageContext() {
    super();
    this.messages = new ArrayList<>();
  }

  /*public MessageContext(List<Message> messages) {
    super();
    this.messages = messages;
  }*/

  public List<Message> getMessages() {
    return messages;
  }

  public void setMessages(List<Message> messages) {
    this.messages = messages;
  }
  
  public Boolean hasMessages() {
    return !messages.isEmpty();
  }
  
  public void addInfo(String msg) {
    this.messages.add(new Message("INFO", msg));
  }
          

  @Override
  public String toString() {
    return "MessageContext [messages=" + messages + "]";
  }
  
}
