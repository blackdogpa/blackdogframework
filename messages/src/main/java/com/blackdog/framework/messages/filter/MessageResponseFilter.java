package com.blackdog.framework.messages.filter;

import java.io.IOException;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

import com.blackdog.framework.messages.Message;
import com.blackdog.framework.messages.MessageContext;

@Provider
public class MessageResponseFilter implements ContainerResponseFilter {

  
  @Inject 
  private MessageContext messages;
  
  
  @Override
  public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {

    if(messages.hasMessages()) {
      
      for(Message m : messages.getMessages()) {
        
        if(responseContext.getHeaders().containsKey("X-simasApp-alert")) {
          
          responseContext.getHeaders().get("X-simasApp-alert").add(m.getMessage());
          
        } else {
          responseContext.getHeaders().add("X-simasApp-alert", m.getMessage());
        }
        
      }
      
    }

  }
}
