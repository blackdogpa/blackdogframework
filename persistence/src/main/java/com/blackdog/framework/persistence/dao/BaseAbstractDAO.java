package com.blackdog.framework.persistence.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import com.blackdog.framework.configurations.ApplicationConfig;
import com.blackdog.framework.configurations.dao.ConfigurationDAO;
import com.blackdog.framework.exceptions.BusinessException;
import com.blackdog.framework.persistence.pagination.Pageable;
import com.blackdog.framework.persistence.pagination.Sort;
import com.blackdog.framework.utils.date.DateUtil;

public abstract class BaseAbstractDAO {

  //@Inject
  //private EntityManager em;
  
  @Inject
  private ApplicationConfig config;
  
  /*public BaseAbstractDAO() {
    super();
  }*/

  /*public BaseAbstractDAO(EntityManager em, ApplicationConfig config) {
    super();
    this.em = em;
    this.config = config;
  }*/

  protected void aplicarOrder(StringBuffer query, Pageable pagination, Boolean isCount) {

    if(!isCount) {
      if(pagination != null && pagination.getSort() != null &&  !pagination.getSort().isEmpty()) {
        
        query.append("ORDER BY ");
  
        for(Sort sort :pagination.getSort()) {
          query.append(sort.getProperty() + " "+ sort.getDirection() + ", ");
        }
  
        query.delete(query.length()-2, query.length());
        
      } else {
        //TODO Deve pegar o campo da classe que ordena por default. 
        //query.append("ORDER BY l.numeroRemessa");
      }
    }
    
  }
  
  protected void aplicaParametros(Query query, Map<String, Object> parametros, Pageable pagination, Boolean isCount) throws BusinessException {
    
    aplicaParametros(query, parametros);
    
    if(!isCount) {
      Integer pageSize = pagination.getPageSize();
      
      if(pageSize == null || pageSize < 1) {
        pageSize  = config.getDefaultPageSize().intValue();
      } else if(pageSize > config.getMaxPageSize()) {
        pageSize = config.getMaxPageSize().intValue();
      }
      
      if(pagination.getPageSize() != null) {
        query.setMaxResults(pagination.getPageSize());
      }
      
      if(pagination.getOffset() != null) {
        query.setFirstResult(pagination.getOffset());
      }
    }
    
  }

  protected void aplicaParametros(Query query, Map<String, Object> parametros) {
    for (Entry<String, Object> parametro : parametros.entrySet()) {
      
      if(parametro.getValue() != null) {
        
        if(parametro.getValue() instanceof TemporalParameter) {
          TemporalParameter tparam = (TemporalParameter) parametro.getValue();
          query.setParameter(parametro.getKey(), tparam.getDate(), tparam.getTemporalType());
        } else {
          query.setParameter(parametro.getKey(), parametro.getValue());
        }
        
      } else {
        query.setParameter(parametro.getKey(), parametro.getValue());
      }
    }
  }

  
  protected List<Integer> normalizaColecaoDeNumerosInteiros(List<? extends Number> numeros) {
    
    List<Integer> inteiros = new ArrayList<Integer>();
    for (Number numero : numeros) {
      if (numero != null) {
        inteiros.add(numero.intValue());
      } else {
        inteiros.add(null);
      }
    }
    return inteiros;
  }


  /*public class TemporalParameter {
  
    private Date date;
    private TemporalType temporalType;
    
    public TemporalParameter(Date date, TemporalType temporalType) {
      super();
      this.date = date;
      this.temporalType = temporalType;
    }
    
    public Date getDate() {
      if(this.date != null) {
        if(this.temporalType.equals(TemporalType.DATE)) {
          return DateUtil.truncateToDate(date);  
        } else {
          return date;
        }
      } else {
        return null;
      }
    }
    
    public void setDate(Date date) {
      this.date = date;
    }
    
    public TemporalType getTemporalType() {
      return temporalType;
    }
    
    public void setTemporalType(TemporalType temporalType) {
      this.temporalType = temporalType;
    }
  }*/
}