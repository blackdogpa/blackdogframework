package com.blackdog.framework.persistence.dao;

import java.util.Date;

import javax.persistence.TemporalType;

import com.blackdog.framework.utils.date.DateUtil;

public class TemporalParameter {
  
  private Date date;
  private TemporalType temporalType;
  
  public TemporalParameter(Date date, TemporalType temporalType) {
    super();
    this.date = date;
    this.temporalType = temporalType;
  }
  
  public Date getDate() {
    if(this.date != null) {
      if(this.temporalType.equals(TemporalType.DATE)) {
        return DateUtil.truncateToDate(date);  
      } else {
        return date;
      }
    } else {
      return null;
    }
  }
  
  public void setDate(Date date) {
    this.date = date;
  }
  
  public TemporalType getTemporalType() {
    return temporalType;
  }
  
  public void setTemporalType(TemporalType temporalType) {
    this.temporalType = temporalType;
  }
}
