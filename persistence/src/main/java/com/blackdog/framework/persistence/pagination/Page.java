package com.blackdog.framework.persistence.pagination;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public interface Page<T> {
  /**
   * Returns the number of total pages.
   * 
   * @return the number of total pages
   */
  int getTotalPages();

  /**
   * Returns the total amount of elements.
   * 
   * @return the total amount of elements
   */
  long getTotalOfElements();

  /**
   * Returns a new {@link Page} with the content of the current one mapped by the given {@link Converter}.
   * 
   * @param converter must not be {@literal null}.
   * @return a new {@link Page} with the content of the current one mapped by the given {@link Converter}.
   * @since 1.10
   */
  //<S> Page<S> map(Converter<? super T, ? extends S> converter);
  
  /**
   * Returns the number of the current {@link Slice}. Is always non-negative.
   * 
   * @return the number of the current {@link Slice}.
   */
  int getNumber();

  /**
   * Returns the size of the {@link Slice}.
   * 
   * @return the size of the {@link Slice}.
   */
  int getSize();

  /**
   * Returns the number of elements currently on this {@link Slice}.
   * 
   * @return the number of elements currently on this {@link Slice}.
   */
  int getNumberOfElements();

  /**
   * Returns the page content as {@link List}.
   * 
   * @return
   */
  List<T> getContent();

  /**
   * Returns whether the {@link Slice} has content at all.
   * 
   * @return
   */
  boolean hasContent();

  /**
   * Returns the sorting parameters for the {@link Slice}.
   * 
   * @return
   */
  List<Sort> getSort();

  /**
   * Returns whether the current {@link Slice} is the first one.
   * 
   * @return
   */
  boolean isFirst();

  /**
   * Returns whether the current {@link Slice} is the last one.
   * 
   * @return
   */
  boolean isLast();

  /**
   * Returns if there is a next {@link Slice}.
   * 
   * @return if there is a next {@link Slice}.
   */
  boolean hasNext();

  /**
   * Returns if there is a previous {@link Slice}.
   * 
   * @return if there is a previous {@link Slice}.
   */
  boolean hasPrevious();

  /**
   * Returns the {@link Pageable} to request the next {@link Slice}. Can be {@literal null} in case the current
   * {@link Slice} is already the last one. Clients should check {@link #hasNext()} before calling this method to make
   * sure they receive a non-{@literal null} value.
   * 
   * @return
   */
  Pageable nextPageable();

  /**
   * Returns the {@link Pageable} to request the previous {@link Slice}. Can be {@literal null} in case the current
   * {@link Slice} is already the first one. Clients should check {@link #hasPrevious()} before calling this method make
   * sure receive a non-{@literal null} value.
   * 
   * @return
   */
  Pageable previousPageable();

  /**
   * Returns a new {@link Slice} with the content of the current one mapped by the given {@link Converter}.
   * 
   * @param converter must not be {@literal null}.
   * @return a new {@link Slice} with the content of the current one mapped by the given {@link Converter}.
   * @since 1.10
   */
  //<S> Slice<S> map(Converter<? super T, ? extends S> converter);
  
  
  
  public String getNextUrl();
  public void setNextUrl(String nextUrl);

  public String getPrevUrl();
  public void setPrevUrl(String prevUrl);

  public String getLastUrl();
  public void setLastUrl(String lastUrl);

  public String getFirstUrl();
  public void setFirstUrl(String firstUrl);
  
  
  //TODO GAMBI
  /**
   * TODO Isso é um gambi sim. Mas não consegui fazer de outro jeito com Reflection.
   * 
   * @return
   */
  public Class<T> getClazz();
  
  /**
   * TODO Isso é um gambi sim. Mas não consegui fazer de outro jeito com Reflection.
   * 
   * @param clazz
   */
  public void setClazz(Class<T> clazz);
  
  
  public String getAliasList();
  
  public void setAliasList(String aliasList);
  
}
