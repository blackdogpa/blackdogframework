package com.blackdog.framework.persistence.pagination;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.thoughtworks.xstream.annotations.XStreamInclude;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@JsonPropertyOrder({"pageNumber","pageSize", "totalOfElements", "totalOfPages", "isFirst", "isLast", "hasNext", "hasPrevious", "hasContent", "contentCount", "sort",
                    "nextPageUrl", "previusPageUrl", "lastPageUrl", "firstPageUrl",
                    "currentPage", "nextPage", "previousPage", "result"})
//public class PageImpl<T> extends Page2<T> {
public class PageImpl<T> implements Page<T> {

  private Class<T> clazz; //TODO GAMBI. Não consegui usar o generics
  
  private final Long total;
  private final List<T> content = new ArrayList<T>();
  private final Pageable pageable;
  
  
  private String nextUrl;
  private String prevUrl;
  private String lastUrl;
  private String firstUrl;

  @JsonIgnore
  @XStreamOmitField
  private String aliasList;
  
  
  /**
   * Creates a new {@link PageImpl} with the given content. This will result in the created {@link Page} being identical
   * to the entire {@link List}.
   * 
   * @param content must not be {@literal null}.
   */
  public PageImpl(List<T> content) {
    this(content, null, null == content ? 0L : content.size());
  }
  
  
  /**
   * Constructor of {@code PageImpl}.
   * 
   * @param content
   * @param pageable
   */
  public PageImpl(List<T> content, Pageable pageable) {
    this(content, pageable, null);
  }
  
  /**
   * Constructor of {@code PageImpl}.
   * 
   * @param content the content of this page, must not be {@literal null}.
   * @param pageable the paging information, can be {@literal null}.
   * @param total the total amount of items available. The total might be adapted considering the length of the content
   *          given, if it is going to be the content of the last page. This is in place to mitigate inconsistencies
   */
  public PageImpl(List<T> content, Pageable pageable, Long total) {
    super();
    
    if(pageable == null) {
      this.pageable = new PageableQueryParam(0, 10);
    } else {
      this.pageable = pageable;
    }
    if(content != null) {
      this.content.clear();
      this.content.addAll(content);
    } else {
      content = new ArrayList<>();
    }
    
    if(total == null) {
      total = 0L;
    }
    
    this.total = !content.isEmpty() && pageable != null && pageable.getOffset() + pageable.getPageSize() > total
            ? pageable.getOffset() + content.size() : total;
  }

  

  /*
   * (non-Javadoc)
   * @see org.springframework.data.domain.Page#getTotalPages()
   */
  @Override
  @JsonProperty("totalOfPages")
  public int getTotalPages() {
    return getSize() == 0 ? 1 : (int) Math.ceil((double) total / (double) getSize());
  }

  /*
   * (non-Javadoc)
   * @see org.springframework.data.domain.Page#getTotalElements()
   */
  @Override
  @JsonProperty("totalOfElements")
  public long getTotalOfElements() {
    return total;
  }
  
  

  /* 
   * (non-Javadoc)
   * @see org.springframework.data.domain.Slice#hasNext()
   */
  @Override
  @JsonProperty("hasNext")
  public boolean hasNext() {
    return getNumber() + 1 < getTotalPages();
  }

  /* 
   * (non-Javadoc)
   * @see org.springframework.data.domain.Slice#isLast()
   */
  @Override
  @JsonProperty("isLast")
  public boolean isLast() {
    return !hasNext();
  }
  
  /* 
   * (non-Javadoc)
   * @see org.springframework.data.domain.Slice#nextPageable()
   */
  @JsonProperty("nextPage")
  public Pageable nextPageable() {
    return hasNext() ? pageable.next() : null;
  }
  
  /*
   * (non-Javadoc)
   * @see org.springframework.data.domain.Slice#hasPrevious()
   */
  @JsonProperty("hasPrevious")
  public boolean hasPrevious() {
    return getNumber() > 0;
  }
  
  
  /*
   * (non-Javadoc)
   * @see org.springframework.data.domain.Slice#isFirst()
   */
  @JsonProperty("isFirst")
  public boolean isFirst() {
    return !hasPrevious();
  }
  
  
  
  
  

  /* 
   * (non-Javadoc)
   * @see org.springframework.data.domain.Slice#transform(org.springframework.core.convert.converter.Converter)
   */
 /* @Override
  @JsonProperty("isFirst")
  public <S> Page<S> map(Converter<? super T, ? extends S> converter) {
    return new PageImpl<S>(getConvertedContent(converter), pageable, total);
  }*/

 

  

  
  
  @JsonProperty("pageNumber")
  public int getNumber() {
    return pageable == null ? 0 : pageable.getPageNumber();
  }

  /*
   * (non-Javadoc)
   * @see org.springframework.data.domain.Slice#getSize()
   */
  @JsonProperty("pageSize")
  public int getSize() {
    return pageable == null ? 0 : pageable.getPageSize();
  }

  /*
   * (non-Javadoc)
   * @see org.springframework.data.domain.Slice#getNumberOfElements()
   */
  @JsonProperty("contentCount")
  public int getNumberOfElements() {
    return content.size();
  }

 

  


  
  
  /*
   * (non-Javadoc)
   * @see org.springframework.data.domain.Slice#previousPageable()
   */
  @JsonProperty("previousPage")
  public Pageable previousPageable() {

    if (hasPrevious()) {
      return pageable.previousOrFirst();
    }

    return null;
  }

  /*
   * (non-Javadoc)
   * @see org.springframework.data.domain.Slice#hasContent()
   */
  @JsonProperty("hasContent")
  public boolean hasContent() {
    return !content.isEmpty();
  }

  /*
   * (non-Javadoc)
   * @see org.springframework.data.domain.Slice#getContent()
   */
  public List<T> getContent() {
    return Collections.unmodifiableList(content);
  }

  /*
   * (non-Javadoc)
   * @see org.springframework.data.domain.Slice#getSort()
   */
  @JsonProperty("sort")
  public List<Sort> getSort() {
    return pageable == null ? null : pageable.getSort();
  }
  
  
  
  @JsonProperty("nextPageUrl")
  public String getNextUrl() {
    return nextUrl;
  }
  public void setNextUrl(String nextUrl) {
    this.nextUrl = nextUrl;
  }

  @JsonProperty("previusPageUrl")
  public String getPrevUrl() {
    return prevUrl;
  }
  public void setPrevUrl(String prevUrl) {
    this.prevUrl = prevUrl;
  }

  @JsonProperty("lastPageUrl")
  public String getLastUrl() {
    return lastUrl;
  }
  public void setLastUrl(String lastUrl) {
    this.lastUrl = lastUrl;
  }

  @JsonProperty("firstPageUrl")
  public String getFirstUrl() {
    return firstUrl;
  }
  public void setFirstUrl(String firstUrl) {
    this.firstUrl = firstUrl;
  }
  
  public String getAliasList() {
    return aliasList;
  }


  public void setAliasList(String aliasList) {
    this.aliasList = aliasList;
  }


  /*
   * (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {

    int result = 17;

    result += 31 * (int) (total ^ total >>> 32);
    result += 31 * super.hashCode();

    return result;
  }
  
  /*
   * (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {

    if (this == obj) {
      return true;
    }

    if (!(obj instanceof PageImpl<?>)) {
      return false;
    }

    PageImpl<?> that = (PageImpl<?>) obj;

    return this.total == that.total && super.equals(obj);
  }
  
  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {

    String contentType = "UNKNOWN";
    List<T> content = getContent();

    if (content.size() > 0) {
      contentType = content.get(0).getClass().getName();
    }

    return String.format("Page %s of %d containing %s instances", getNumber() + 1, getTotalPages(), contentType);
  }


  public Class<T> getClazz() {
    return clazz;
  }


  public void setClazz(Class<T> clazz) {
    this.clazz = clazz;
  }
  

}
