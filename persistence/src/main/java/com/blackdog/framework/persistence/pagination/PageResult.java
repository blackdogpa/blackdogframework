package com.blackdog.framework.persistence.pagination;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PageResult<T> {

  @JsonProperty("rowCount")
  private final Long total;

  @JsonProperty("result")
  private final List<T> content = new ArrayList<T>();

  @JsonProperty("pageable")
  private final Pageable pageable;
  
  
  @JsonProperty("next")
  private String next;
  
  @JsonProperty("prev")
  private String prev;
  
  @JsonProperty("last")
  private String last;
  
  @JsonProperty("first")
  private String first;

  public PageResult(List<T> content, Long total, Pageable pageable) {
    super();
    this.total = total;
    this.pageable = pageable;
  }

  public Long getTotal() {
    return total;
  }

  public List<T> getContent() {
    return content;
  }

  public Pageable getPageable() {
    return pageable;
  }

  public String getNext() {
    return next;
  }

  public void setNext(String next) {
    this.next = next;
  }

  public String getPrev() {
    return prev;
  }

  public void setPrev(String prev) {
    this.prev = prev;
  }

  public String getLast() {
    return last;
  }

  public void setLast(String last) {
    this.last = last;
  }

  public String getFirst() {
    return first;
  }

  public void setFirst(String first) {
    this.first = first;
  }

  @Override
  public String toString() {
    return "PageResult [total=" + total + ", content=" + content + ", pageable=" + pageable + ", next=" + next + ", prev=" + prev + ", last=" + last
            + ", first=" + first + "]";
  }

  
}
