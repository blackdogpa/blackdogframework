package com.blackdog.framework.persistence.pagination;

import java.io.Serializable;
import java.util.List;

import javax.ws.rs.QueryParam;

/**
 * @author thiago.soares (thiago.soares.jr@gmail.com)
 *
 */
//@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(name = "Pageable", namespace = BlackDogConstants.DTO_NAMESPACE/*TODO , propOrder = { "numero" }*/)
public class PageableQueryParam implements Pageable, Serializable  {

  private static final long serialVersionUID = -8118967106109505828L;
  
  @QueryParam("page")
  private final Integer page;

  @QueryParam("size")
  private final Integer size;

  @QueryParam("sort")
  private final List<Sort> sort;
  
  public PageableQueryParam() {
    super();
    this.page = 0;
    this.size = 10;
    this.sort = null;
  }
  
  /**
   * Creates a new {@link AbstractPageableImpl}. Pages are zero indexed, thus providing 0 for {@code page} will return
   * the first page.
   * 
   * @param page must not be less than zero.
   * @param size must not be less than one.
   */
  public PageableQueryParam(int page, int size) {
    this(page, size, null);
  }
  
  //public PageableImpl(int page, int size, Sort sort) {
  public PageableQueryParam(Integer page, Integer size, List<Sort> sort) {
    super();
    
    if (page == null || page < 0) {
      throw new IllegalArgumentException("Page index must not be less than zero!");
    }

    if (size == null || size < 1) {
      throw new IllegalArgumentException("Page size must not be less than one!");
    }

    this.page = page;
    this.size = size;
    
    this.sort = sort; 
  }

  public Integer getPageSize() {
    return size == null || size < 1 ? 10 : size;
  }
  
  public Integer getPageNumber() {
    return page == null || page < 0 ? 0 : page;
  }

  public Integer getOffset() {
    return getPageNumber() * getPageSize();
  }

  public boolean hasPrevious() {
    return getPageNumber() > 0;
  }

  public Pageable previousOrFirst() {
    return hasPrevious() ? previous() : first();
  }
  
  public List<Sort> getSort() {
    return sort;
  }

  public Pageable next() {
    return new PageableQueryParam(getPageNumber() + 1, getPageSize(), getSort());
  }

  public PageableQueryParam previous() {
    return getPageNumber() == 0 ? this : new PageableQueryParam(getPageNumber() - 1, getPageSize(), getSort());
  }

  public Pageable first() {
    return new PageableQueryParam(0, getPageSize(), getSort());
  }

  @Override
  public boolean equals(final Object obj) {

    if (this == obj) {
      return true;
    }

    if (!(obj instanceof PageableQueryParam)) {
      return false;
    }

    PageableQueryParam that = (PageableQueryParam) obj;

    boolean sortEqual = this.sort == null ? that.sort == null : this.sort.equals(that.sort);

    return super.equals(that) && sortEqual;
  }

  @Override
  public int hashCode() {
    return 31 * super.hashCode() + (null == sort ? 0 : sort.hashCode());
  }

  @Override
  public String toString() {
    return String.format("Page request [number: %d, size %d, sort: %s]", getPageNumber(), getPageSize(),  sort == null ? null : sort.toString());
  }
}
