package com.blackdog.framework.persistence.pagination;

public class Sort {

  public static final String ASC = "asc"; 
  public static final String DESC = "desc";
  
  private String property;
  private String direction;
  
  public Sort(String property, String direction) {
    super();
    this.property = property;
    
    if(direction == null) {
      this.direction = ASC;
    } else {
      this.direction = direction;
    }
  }
  
  public Sort(String sentence) {
    super();

    String[] split = sentence.split(",");
    
    if(split.length == 1) {
      this.property = split[0];
      this.direction = Sort.ASC;
      
    } else if(split.length == 2) {
      this.property = split[0];
      this.direction = split[1];
      
    } else {
      //?????
    }
  }

  public String getProperty() {
    return property;
  }

  public void setProperty(String property) {
    this.property = property;
  }

  public String getDirection() {
    return direction;
  }

  public void setDirection(String direction) {
    this.direction = direction;
  }

  @Override
  public String toString() {
    //return "Sort [property=" + property + ", direction=" + direction + "]";
    return property + "," + direction;
  }
  
}
