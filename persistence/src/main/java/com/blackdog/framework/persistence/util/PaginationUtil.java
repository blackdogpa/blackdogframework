package com.blackdog.framework.persistence.util;

/*
 * import org.springframework.data.domain.Page; import
 * org.springframework.http.HttpHeaders; import
 * org.springframework.web.util.UriComponentsBuilder;
 */

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import com.blackdog.framework.persistence.pagination.Page;
import com.blackdog.framework.utils.rest.HttpHeaders;
import com.blackdog.framework.utils.serialization.xml.xstream.SerializationException;
import com.blackdog.framework.utils.serialization.xml.xstream.XStreamXmlUtils;
import com.thoughtworks.xstream.XStream;

/**
 * Utility class for handling pagination.
 *
 * <p>
 * Pagination uses the same principles as the
 * <a href="https://developer.github.com/v3/#pagination">Github API</a>, and
 * follow <a href="http://tools.ietf.org/html/rfc5988">RFC 5988 (Link
 * header)</a>.
 */
public class PaginationUtil {
                         
  
  
  
  
  /* ***********
   *  Herader Bassed Pagination
   * ***********
   */
  
  public static Response buildResultPaginationHeaders(Page<?> page, String baseUrl) throws SerializationException {
    return buildResultPaginationHeaders(Status.OK, page, baseUrl);
  }

  
  public static Response buildResultPaginationHeaders(Page<?> page, UriInfo uriInfo) throws SerializationException {
    return buildResultPaginationHeaders(Status.OK, page, uriInfo);
  }
  
  public static Response buildResultPaginationHeaders(Status status, Page<?> page, UriInfo uriInfo) throws SerializationException {
   
    String baseUri = uriInfo.getBaseUri().toString();
    if(baseUri.endsWith("/")) {
      baseUri = baseUri.substring(0, baseUri.length()-1);
    }
    
    String path = uriInfo.getPath();
    if(!path.startsWith("/")) {
      path = "/" + path;
    }
    
    String baseUrl = baseUri  + path;
    
    return buildResultPaginationHeaders(status, page, baseUrl);
  }
  
  public static Response buildResultPaginationHeaders(Status status, Page<?> page, String baseUrl) throws SerializationException {
    return buildResultPaginationHeaders(status, page, baseUrl, MediaType.APPLICATION_JSON);
  }
  
  public static Response buildResultPaginationHeaders(Status status, Page<?> page, String baseUrl, String mediaType) throws SerializationException {

    try {

      ResponseBuilder builder = Response.status(status);
      String link = "";

      if (page == null) {

        builder.header("X-Total-Count", "" + 0);
        
        builder.header("X-Current-Page", "" + 0);
        builder.header("X-Page-Size", "" + 10);

        link = "<" + generateUri(baseUrl, 1, 10) + ">; rel=\"next\",";
        link += "<" + generateUri(baseUrl, 1, 10) + ">; rel=\"prev\",";

        link += "<" + generateUri(baseUrl, 1, 10) + ">; rel=\"last\",";
        link += "<" + generateUri(baseUrl, 0, 10) + ">; rel=\"first\"";

      } else {

        builder.header("X-Total-Count", "" + page.getTotalOfElements());
        builder.header("X-Current-Page", "" + page.getNumber());
        builder.header("X-Page-Size", "" + page.getSize());

        if ((page.getNumber() + 1) < page.getTotalPages()) {
          link = "<" + generateUri(baseUrl, page.getNumber() + 1, page.getSize()) + ">; rel=\"next\",";
        }
        // prev link
        if ((page.getNumber()) > 0) {
          link += "<" + generateUri(baseUrl, page.getNumber() - 1, page.getSize()) + ">; rel=\"prev\",";
        }
        // last and first link
        int lastPage = 0;
        if (page.getTotalPages() > 0) {
          lastPage = page.getTotalPages() - 1;
        }
        link += "<" + generateUri(baseUrl, lastPage, page.getSize()) + ">; rel=\"last\",";
        link += "<" + generateUri(baseUrl, 0, page.getSize()) + ">; rel=\"first\"";

      }

      builder.header(HttpHeaders.LINK, link);

      
      if(mediaType != null) {

        if(mediaType.equals(MediaType.APPLICATION_XML)) {
          
          //return builder.type(mediaType).entity(XmlUtils.toXml(page)).build();
          return builder.type(mediaType).entity(toXml(page)).build();
          
          /*FAIL
          List<Object> list = new ArrayList<>(); 
          for(Object m : page.getContent()) {
            list.add(m);
          }
          return builder.entity(XmlUtils.toXml(page.getClazz(), list)).build();*/
        } else {
          return builder.type(mediaType).entity(page.getContent()).build();
        }
        
      } else {
        return builder.type(mediaType).entity(page.getContent()).build();
      }
      
    } catch (URISyntaxException e) {
      return Response.status(Status.BAD_REQUEST).build();
    }
  }

  public static Response buildResultPaginationHeaders(Status status, Page<?> page, List<?> result, String baseUrl) throws URISyntaxException {

    ResponseBuilder builder = Response.status(status);

    builder.header("X-Total-Count", "" + page.getTotalOfElements());
    builder.header("X-Current-Page", "" + page.getNumber());
    builder.header("X-Page-Size", "" + page.getSize());

    String link = "";
    if ((page.getNumber() + 1) < page.getTotalPages()) {
      link = "<" + generateUri(baseUrl, page.getNumber() + 1, page.getSize()) + ">; rel=\"next\",";
    }
    // prev link
    if ((page.getNumber()) > 0) {
      link += "<" + generateUri(baseUrl, page.getNumber() - 1, page.getSize()) + ">; rel=\"prev\",";
    }
    // last and first link
    int lastPage = 0;
    if (page.getTotalPages() > 0) {
      lastPage = page.getTotalPages() - 1;
    }
    link += "<" + generateUri(baseUrl, lastPage, page.getSize()) + ">; rel=\"last\",";
    link += "<" + generateUri(baseUrl, 0, page.getSize()) + ">; rel=\"first\"";

    builder.header(HttpHeaders.LINK, link);
    return builder.entity(result).build();
  }

  /*public static Response generateSearchPaginationHttpHeaders(Status status, String query, Page<?> page, String baseUrl) throws URISyntaxException {

    ResponseBuilder builder = Response.status(status);

    builder.header("X-Total-Count", "" + page.getTotalElements());
    builder.header("X-Current-Page", "" + page.getNumber());
    builder.header("X-Page-Size", "" + );

    String link = "";
    if ((page.getNumber() + 1) < page.getTotalPages()) {
      link = "<" + generateUri(baseUrl, page.getNumber() + 1, page.getSize()) + "&query=" + query + ">; rel=\"next\",";
    }
    // prev link
    if ((page.getNumber()) > 0) {
      link += "<" + generateUri(baseUrl, page.getNumber() - 1, page.getSize()) + "&query=" + query + ">; rel=\"prev\",";
    }
    // last and first link
    int lastPage = 0;
    if (page.getTotalPages() > 0) {
      lastPage = page.getTotalPages() - 1;
    }
    link += "<" + generateUri(baseUrl, lastPage, page.getSize()) + "&query=" + query + ">; rel=\"last\",";
    link += "<" + generateUri(baseUrl, 0, page.getSize()) + "&query=" + query + ">; rel=\"first\"";
    builder.header(HttpHeaders.LINK, link);
    return builder.entity(page.getContent()).build();
  }*/

  private static String generateUri(String baseUrl, int page, int size) throws URISyntaxException {
    // return UriComponentsBuilder.fromUriString(baseUrl).queryParam("page",
    // page).queryParam("size", size).toUriString();

    return UriBuilder.fromUri(baseUrl).queryParam("page", page).queryParam("size", size).build().toString();
  }
  
  
  
  /* ***********
   *  Object Bassed Pagination
   * ***********
   */
  
  
  public static Response buildResultPaginationObject(Page<?> page, String baseUrl) {
    return buildResultPaginationObject(Status.OK, page, baseUrl);
  }

  public static <T> Response buildResultPaginationObject(Status status, Page<T> page, String baseUrl) {

    try {

      ResponseBuilder builder = Response.status(status);
      
      //PageResult<T> pageResult = new PageResult<>(page.getContent(), page.getTotalElements(), page.getpageable);
      
      if (page == null) {

       /* page.setNextUrl(generateUri(baseUrl, 1, 10));
        page.setPrevUrl(generateUri(baseUrl, 1, 10));

        page.setLastUrl(generateUri(baseUrl, 1, 10));
        page.setFirstUrl(generateUri(baseUrl, 0, 10));
       */

      } else {

        //builder.header("X-Total-Count", "" + page.getTotalElements());
        builder.header("X-Current-Page", "" + page.getNumber());
        builder.header("X-Page-Size", "" + page.getSize());

        if ((page.getNumber() + 1) < page.getTotalPages()) {
          page.setNextUrl(generateUri(baseUrl, page.getNumber() + 1, page.getSize()));
        }
        // prev link
        if ((page.getNumber()) > 0) {
          page.setPrevUrl(generateUri(baseUrl, page.getNumber() - 1, page.getSize()));
        }
        // last and first link
        int lastPage = 0;
        if (page.getTotalPages() > 0) {
          lastPage = page.getTotalPages() - 1;
        }
        page.setLastUrl(generateUri(baseUrl, lastPage, page.getSize()));
        page.setFirstUrl(generateUri(baseUrl, 0, page.getSize()));

      }

      return builder.entity(page).build();
    } catch (Exception e) {
      return Response.status(Status.BAD_REQUEST).build();
    }
  }
  
  
  /**
   * TODO Isso estava em com.blackdog.framework.utils.xml.XmlUtils mas precisou vir para essa classe por causa das dependencias entre Persistence-Util_persistence
   * @param model
   * @return
   * @throws SerializationException
   */
  private static <T> String toXml(Page<T> model) throws SerializationException {
    
    try {
      
      XStream xstream = new XStream();
      xstream.processAnnotations(model.getClazz());
      
      xstream.ignoreUnknownElements();
      
      xstream.addDefaultImplementation(java.sql.Date.class, Date.class);
      xstream.addDefaultImplementation(java.sql.Timestamp.class, Date.class);
      
      xstream.alias(model.getAliasList(), List.class);
      
      List<T> list = new ArrayList<>(); 
              
      for(T m : model.getContent()) {
        list.add(m);
      }
      
      String responseObject = xstream.toXML(list);
      
      return responseObject;
      
    } catch(Exception e) {
      throw new SerializationException(e, "Não foi possivel serializar o objeto para XML.");
    }
    
  }

  
}
