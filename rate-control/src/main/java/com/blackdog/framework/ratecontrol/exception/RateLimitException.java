package com.blackdog.framework.ratecontrol.exception;

import javax.xml.ws.WebFault;

import org.apache.commons.lang3.StringUtils;

import com.blackdog.framework.core.exceptions.BlackExceptions;
import com.blackdog.framework.core.exceptions.ErrorMessage;
import com.blackdog.framework.ratecontrol.model.UserRateConfig;


@WebFault(name="RateLimitException", targetNamespace= "Constants.BASE_WS_PRODEPA_NAMESPACE" + "/Exceptions")
public class RateLimitException extends Exception implements BlackExceptions {

  private static final long serialVersionUID = 6408753078777190175L;
  
  private String id;
  private String code;
  
  private UserRateConfig rateConfig;
  
  private String transactionId;
  private String applicationId;
  private String applicationName;
  

  public RateLimitException(UserRateConfig rateConfig) {
    super();
    this.rateConfig = rateConfig;
  }

  public RateLimitException(String message) {
    super(message);
  }
  
  public RateLimitException(String message, UserRateConfig rateConfig) {
    super(message);
    this.rateConfig = rateConfig;
  }
  
  public RateLimitException(String message, UserRateConfig rateConfig, Throwable cause) {
    super(message, cause);
    this.rateConfig = rateConfig;
  }
  
  
  
  @Override
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public UserRateConfig getRateConfig() {
    return rateConfig;
  }

  public void setRateConfig(UserRateConfig rateConfig) {
    this.rateConfig = rateConfig;
  }

  @Override
  public ErrorMessage getErrorMessage() {
    
    //Lancar o 429 Too MAny Resquests
    
    // Enviar os xheaders
    //X-Rate-Limit-Limit, the maximum number of requests allowed with a time period
    //X-Rate-Limit-Remaining, the number of remaining requests in the current time period
    //X-Rate-Limit-Reset, t
    //Ou mais ou menos isso
    
    ErrorMessage dto = new ErrorMessage();
    dto.setStatus("429-Too Many Requests");
    dto.setStatusCode(429);
    
    if(StringUtils.isNotBlank(this.getMessage())) {
      dto.setMessage(this.getMessage());
    }
    
    
    //exception.getRateConfig().get
    Integer limiteExcedido  = 100; //TODO  Pegar esse info da Exception 
    
    /*if(exception.getValidations() != null && !exception.getValidations().isEmpty()) {
      dto.setValidations(exception.getValidations());
    }*/
    
    dto.setTransactionId(this.getId());
    dto.setApplicationId(this.getApplicationId());
    dto.setApplicationName(this.getApplicationName());
    
    return dto;
  }
  
  @Override
  public String getType() {
    return RATE_LIMIT;
  }

  @Override
  public String getApplicationId() {
    return this.applicationId;
  }

  @Override
  public void setApplicationId(String applicationId) {
    this.applicationId = applicationId;
  }

  @Override
  public String getApplicationName() {
    return this.applicationName;
  }

  @Override
  public void setApplicationName(String applicationName) {
    this.applicationName = applicationName;
  }

  public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }
  
  @Override
  public String getMessage() {
    
    if(super.getMessage() == null || super.getMessage().equals("")) {
      return null;
    }
    
    StringBuilder builder = new StringBuilder();

    if(applicationId != null && applicationName != null) {
      
      builder.append("[");
      builder.append(applicationId);
      builder.append(".");
      builder.append(applicationName);
      builder.append("]");
      
      builder.append("-");
    }
    if (id != null) {
      //builder.append(id);
      //builder.append(" : ");
      builder.append(super.getMessage());
    } else {
      builder.append(super.getMessage());
    }
    return builder.toString();
  }

}
