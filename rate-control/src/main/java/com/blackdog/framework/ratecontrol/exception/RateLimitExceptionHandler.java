package com.blackdog.framework.ratecontrol.exception;

import javax.inject.Inject;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;

import com.blackdog.framework.core.exceptions.ErrorMessage;

@Provider
public class RateLimitExceptionHandler implements ExceptionMapper<RateLimitException> {

  @Override
  public Response toResponse(RateLimitException exception) {

    exception.printStackTrace();
    
    
    /*//Lancar o 429 Too MAny Resquests
    
    // Enviar os xheaders
    //X-Rate-Limit-Limit, the maximum number of requests allowed with a time period
    //X-Rate-Limit-Remaining, the number of remaining requests in the current time period
    //X-Rate-Limit-Reset, t
    //Ou mais ou menos isso
    
    ErrorMessage dto = new ErrorMessage();
    dto.setStatus("429-Too Many Requests");
    dto.setStatusCode(429);
    
    if(StringUtils.isNotBlank(exception.getMessage())) {
      dto.setMessage(exception.getMessage());
    }
    
    
    //exception.getRateConfig().get
    Integer limiteExcedido  = 100; //TODO  Pegar esse info da Exception 
    
    if(exception.getValidations() != null && !exception.getValidations().isEmpty()) {
      dto.setValidations(exception.getValidations());
    }
    
    dto.setTransactionId(exception.getId());
    dto.setApplicationId(exception.getApplicationId());
    dto.setApplicationName(exception.getApplicationName());*/
    
    //return Response.status(Status.BAD_REQUEST).entity(dto).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8")).build();
    
    Integer limiteExcedido  = 100; //TODO  Pegar esse info da Exception
       
    if(exception.getRateConfig() != null) {
      return Response.status(429)
                     .header("X-Rate-Limit-Limit", limiteExcedido) 
                     .type(MediaType.APPLICATION_JSON)
                     //.header("X-Rate-Limit-Limit", exception.getRateConfig().getMaxRequestCount())
                     //.header("X-Rate-Limit-Reset-Time", exception.getRateConfig().getProhibitTimePeriod())
                     //.header("X-Rate-Limit-Remaining", "")
                     .entity(exception.getErrorMessage()).build();
    } else {
      return Response.status(429).type(MediaType.APPLICATION_JSON).entity(exception.getErrorMessage()).build();
    }
    
  }

}
