package com.blackdog.framework.ratecontrol.interceptors;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.blackdog.framework.core.security.CAOperation;
import com.blackdog.framework.core.security.LoggedUser;
import com.blackdog.framework.ratecontrol.UserRateLimit;
import com.blackdog.framework.ratecontrol.singleton.RateLimiterSingleton;
import com.blackdog.framework.security.sso.Identity;


@UserRateLimit
@Interceptor
public class RateLimitInterceptor {

  private static final Logger LOGGER = LoggerFactory.getLogger(RateLimitInterceptor.class);
  
  @Inject
  private RateLimiterSingleton rate;
    
  @Inject
  protected Identity contextRequestInfo;
  
  @AroundInvoke
  public Object interceptar(InvocationContext invocationContext) throws Throwable {
    long start = System.currentTimeMillis();
    
    Object result = null;
    try {
      
      //TODO Pode haver uma funcionalidade Publica. Nesse caso, é necessários aplicar limites padroes paseados em IP, por exemplo. 
      
      //Identificação do usuário
      if(contextRequestInfo != null && contextRequestInfo.isLoggedIn()) {
        System.out.println(contextRequestInfo.getAuthenticatedUser());
      } else {
        throw new SecurityException("Usuário não identificado.");
      }

      //Operacao
      CAOperation operationAnnot = invocationContext.getMethod().getAnnotation(CAOperation.class);
      if(operationAnnot == null || operationAnnot.value() == null) {
        throw new SecurityException("Operação não identificadas para o rateLimit.");
      }
      
      rate.acquire(contextRequestInfo.getAuthenticatedUser().getId(), operationAnnot.value());
      
      result = invocationContext.proceed();
    
    } catch (Throwable e) {
      //
      e.printStackTrace();
      throw e;
    } finally {
      long end = System.currentTimeMillis();
    }
    return result;
  }
  
}

