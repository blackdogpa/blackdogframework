package com.blackdog.framework.ratecontrol.model;

public class CountItem {

  private String id;
  private Long count;

  public CountItem(String id, Long count) {
    super();
    this.id = id;
    this.count = count;
  }

  public Long getCount() {
    return count;
  }

  public void setCount(Long count) {
    this.count = count;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return "CountItem [id=" + id + ", count=" + count + "]";
  }

}
