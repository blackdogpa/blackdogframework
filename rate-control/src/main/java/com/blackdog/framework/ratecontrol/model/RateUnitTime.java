package com.blackdog.framework.ratecontrol.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

@JsonFormat(shape = JsonFormat.Shape.STRING)
public enum RateUnitTime {
  MONTH, DAY, HOUR, MINUTE, SECOND;
}
