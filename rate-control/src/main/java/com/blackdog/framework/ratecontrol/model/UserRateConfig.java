package com.blackdog.framework.ratecontrol.model;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;


public class UserRateConfig {

  private Long id;

  private RateUnitTime timeUnit;
  
  private Integer timeAmount;
  
  private Map<String, Integer> limits;  //Operacao / maxRequestCount
  
  @JsonIgnore(true)
  private Boolean used; 
  
  //TODO Pode ser usado para estabelescer outro limite de proibicao mais tarde, não o bloquieo por 429 na funcionalidade
  //private Integer prohibitTimePeriod;
  
  public UserRateConfig() {
    super();
    this.timeUnit = RateUnitTime.MINUTE;
    this.timeAmount = 1;
    this.limits = new HashMap<>();
  }
  
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public RateUnitTime getTimeUnit() {
    return timeUnit;
  }

  public void setTimeUnit(RateUnitTime timeUnit) {
    this.timeUnit = timeUnit;
  }

  public Map<String, Integer> getLimits() {
    return limits;
  }

  public void setLimits(Map<String, Integer> limits) {
    this.limits = limits;
  }

  public Integer getTimeAmount() {
    return timeAmount;
  }

  public void setTimeAmount(Integer timeAmount) {
    this.timeAmount = timeAmount;
  }

  public Boolean getUsed() {
    return used;
  }

  public void setUsed(Boolean used) {
    this.used = used;
  }

  

}
