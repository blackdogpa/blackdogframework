package com.blackdog.framework.ratecontrol.singleton;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.transaction.SystemException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.blackdog.framework.configurations.ApplicationConfig;
import com.blackdog.framework.configurations.dao.ConfigurationDAO;
import com.blackdog.framework.exceptions.BusinessException;
import com.blackdog.framework.ratecontrol.exception.RateLimitException;
import com.blackdog.framework.ratecontrol.model.RateUnitTime;
import com.blackdog.framework.ratecontrol.model.UserRateConfig;
import com.blackdog.framework.utils.date.DateUtil;
import com.blackdog.framework.utils.serialization.jackson.JacksonJsonUtils;


@Startup
@Singleton
@Lock(LockType.READ)//TODO Testar esta anotação
@ApplicationScoped
public class RateLimiterSingleton implements Serializable {

  private static final long serialVersionUID = -214320450332094867L;
  
  private static final Integer DEFAULT_MAX_REQUEST_COUNT = 100;

  private static final Logger LOGGER = LoggerFactory.getLogger(RateLimiterSingleton.class);
  
  @Resource
  private TimerService timerService;
  
  @Inject 
  private ApplicationConfig applicationConfig;
  
  private Map<Long, UserRateConfig> userRateConfigs; //Cache de configurações de usuários.
  private Map<Long, List<String>> operations; //Na hora do reset, preciso saber quais operacoes estao em cache por usuário (para ão varrer todas)
  private Map<String, Long> counts; //Lista com a contagem de requisições por ciclo.
  private Map<String, Date> waitList;  //Lista para usuário bloqueados por excesso de uso.
  
  /**
   * Create the Service and wait until it is started.<br/>
   * Will log a message if the service will not start in 10sec. 
   */
  @PostConstruct
  protected void startup() {
    LOGGER.info("RateLimiterSingleton will be initialized!");
        
    this.userRateConfigs = new HashMap<>();
    this.counts = new HashMap<>();
    this.operations = new HashMap<>();
    this.waitList = new HashMap<>();

    /*try {
      this.configurationDao = configurationDaoSource.get();
    } catch (Exception e) {
      e.printStackTrace();
    }*/
    
    try {
      LOGGER.info("RateLimiterSingleton has started the Service");
    } catch (IllegalStateException e) {
      LOGGER.warn("Singleton Service {} not started, are you sure to start in a cluster (HA) environment?", "EnvironmentService.SINGLETON_SERVICE_NAME");
    }
  }

  /**
   * Remove the service during undeploy or shutdown
   */
  @PreDestroy
  protected void destroy() {
    LOGGER.info("RateLimiterSingleton will be removed!");
    try {
      LOGGER.info("RateLimiterSingleton has destroyed. ");
    } catch (IllegalStateException e) {
      LOGGER.warn("Singleton Service {} has not be stopped correctly!", "EnvironmentService.SINGLETON_SERVICE_NAME");
    }
  }
  
  @Timeout
  public void reset(Timer timer) {
      
      Long userId = (Long) timer.getInfo();
      
      UserRateConfig config = userRateConfigs.get(userId);
      
      if(config != null && config.getUsed()) {
        LOGGER.info("RateLimiterSingleton: Reset=" + timer.getInfo());
        //Reset para os counts
        for(String wsOp :operations.get(userId)) {
          counts.put(getCounterID(userId, wsOp), 0L);
          waitList.remove(getCounterID(userId, wsOp));
        }
        config.setUsed(false);
      } else {
        LOGGER.info("RateLimiterSingleton: Exit Cache=" + timer.getInfo());
        //No reset, Se XXXX, limpar caches do usuários (TODAS)
        userRateConfigs.remove(userId);
        for(String wsOp :operations.get(userId)) {
          counts.remove(getCounterID(userId, wsOp));
          waitList.remove(getCounterID(userId, wsOp));
        }
        operations.remove(userId);
        
        //Cancelar o Agendamento
        timer.cancel();
      }
  }
  
  
  public void acquire(Long idUser, String operation) throws RateLimitException, BusinessException {
    
    //LOGGER.info(">>>>>>>>>>>>>> ACQUIRE " + operation + " for " + idUser);
    
    String counterID = getCounterID(idUser, operation);
    
    UserRateConfig userRateConfig = userRateConfigs.get(idUser);
    if(userRateConfig == null) {
      userRateConfig = loadUserRateConfig(idUser); 
      
      if(userRateConfig == null) {
        throw new RateLimitException("Os limites desse usuário não foram definidos");
      }
    }
    
    Date waitRemaining = waitList.get(counterID);
    if(waitRemaining != null) {
      throw new RateLimitException("Limite de Requisições atingido. Aguarde até " + DateUtil.completo(waitRemaining), userRateConfig); 
    }
    
    if(!operations.containsKey(idUser)) {
      operations.put(idUser, new ArrayList<String>());
    } 
    
    if(!operations.get(idUser).contains(operation)) {
      operations.get(idUser).add(operation);
    }
    
    if(!counts.containsKey(counterID)) {
      //Uniciar limites //TODO Essa operação não deveria estar aki 
      counts.put(counterID, 0L);
    }
    
    Long count = counts.get(counterID);
    
    Integer maxRequestCount = userRateConfig.getLimits().get(operation);
    
    if(maxRequestCount == null) {
      maxRequestCount = DEFAULT_MAX_REQUEST_COUNT;
    }
    
    if(count >= maxRequestCount) {
      Date remaining = new Date();

      Collection<Timer> timers = timerService.getTimers();
      for(Timer t : timers) {
        if(t.getInfo().equals(idUser)) {
          remaining = t.getNextTimeout();
        }
      }
      
      waitList.put(counterID, remaining);
      throw new RateLimitException("Limite de Requisições atingido. Aguarde até " + DateUtil.completo(remaining), userRateConfig);
    }
    
    counts.put(counterID, ++count);
    
    //KeepAlive para a configuracao
    userRateConfig.setUsed(true);

    //System.out.println("[OPERATIONS ] " + operations.get(idUser));
    //System.out.println("[COUNTS] " + counts);
    
  }

  
  private String getCounterID(Long idUser, String operation) {
    return idUser + ":"+operation;
  }

  private UserRateConfig loadUserRateConfig(Long idUser) throws BusinessException {
    
    /*try {
      this.configurationDao = configurationDaoSource.get();
    } catch (Exception e) {
      e.printStackTrace();
      throw new com.blackdog.framework.exceptions.SystemException("Não foi possível consultar a base de configurações. Considere criar um @Producer para o \"ConfigurationDAO\". ");
    }*/
    
    //Carregar Configuracao de Rate do Usuário 
    String json = applicationConfig.getString("RATE_LIMIT_USER_"+ idUser);
    
    if(json == null) {
      return null;
    }
    
    UserRateConfig rateConfig = JacksonJsonUtils.fromJson(json, UserRateConfig.class);
    
    if(rateConfig == null) {
      return null;
    }
    
    rateConfig.setUsed(true);
    
    userRateConfigs.put(idUser, rateConfig);
    
    //Agengar reset 
    //TODO O tempo do ciclo pode ser um só para os usuário, desde que cada funcionalidade tenha seu limite. 
    ScheduleExpression sexpr = new ScheduleExpression();
    if(rateConfig.getTimeUnit().equals(RateUnitTime.MONTH)) {
      
      sexpr.dayOfMonth(1).hour("0").minute("0").second("0");
      
    } else if(rateConfig.getTimeUnit().equals(RateUnitTime.DAY)) {
      
      sexpr.dayOfMonth("0/"+rateConfig.getTimeAmount()).hour("0").minute("0").second("0");
      
    } else if(rateConfig.getTimeUnit().equals(RateUnitTime.HOUR)) {
      
      sexpr.dayOfMonth("*").hour("0/"+rateConfig.getTimeAmount()).minute("0").second("0");
      
    } else if(rateConfig.getTimeUnit().equals(RateUnitTime.MINUTE)) {
      
      sexpr.dayOfMonth("*").hour("*").minute("0/"+rateConfig.getTimeAmount()).second("0");
      
    } else if(rateConfig.getTimeUnit().equals(RateUnitTime.SECOND)) {
      
      sexpr.dayOfMonth("*").hour("*").minute("*").second("0/"+rateConfig.getTimeAmount());
      
    }
    timerService.createCalendarTimer(sexpr, new TimerConfig(idUser, false));
    
    return rateConfig;
  }

}
