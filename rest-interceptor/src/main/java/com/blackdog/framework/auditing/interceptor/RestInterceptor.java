package com.blackdog.framework.auditing.interceptor;

import java.util.Arrays;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.jws.WebService;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;

import com.blackdog.framework.auditing.Audit;
import com.blackdog.framework.auditing.RestComponent;
import com.blackdog.framework.auditing.dto.AuditingLogWSDto;
import com.blackdog.framework.auditing.service.AuditingService;
import com.blackdog.framework.configurations.ApplicationConfig;
import com.blackdog.framework.core.constants.CoreErrorsConstants;
import com.blackdog.framework.core.dto.RequestInfo;
import com.blackdog.framework.core.exceptions.BlackExceptions;
import com.blackdog.framework.core.exceptions.ErrorMessage;
import com.blackdog.framework.core.exceptions.Validation;
import com.blackdog.framework.core.model.EndpointType;
import com.blackdog.framework.core.security.CAOperation;
import com.blackdog.framework.core.security.Secure;
import com.blackdog.framework.exceptions.BusinessException;
import com.blackdog.framework.exceptions.NotAuthenticatedException;
import com.blackdog.framework.exceptions.NotAuthorizedException;
import com.blackdog.framework.exceptions.SecurityException;
import com.blackdog.framework.exceptions.SystemException;
import com.blackdog.framework.exceptions.util.BusinessExceptionUtil;
import com.blackdog.framework.exceptions.util.ExceptionCastUtil;
import com.blackdog.framework.security.constants.SecurityConstants.SecurityErrors;
import com.blackdog.framework.security.service.Claims;
import com.blackdog.framework.security.service.JWTServiceJose;
import com.blackdog.framework.security.sso.Identity;
import com.blackdog.framework.utils.serialization.jackson.JacksonJsonUtils;

@RestComponent
@Interceptor
public class RestInterceptor {

  @Inject
  private Identity identity;
  
  @Inject
  private JWTServiceJose jwtService;
  
  @Inject
  private AuditingService logService;
  
  @Inject
  private RequestInfo requestInfo; 
  
  @Inject
  private ApplicationConfig applicationConfig;
  
  
  private Boolean hasAuditingAnnot = false;
  private Boolean hasSecureAnnot = false;
  private AuditingLogWSDto log;
  
  @AroundInvoke
  public Object interceptar(InvocationContext invocationContext) throws Throwable {
    long start = System.currentTimeMillis();
    
    log = new AuditingLogWSDto(); 
    
    Object result = null;
    
    Throwable ex = null;
    
    try {
      
      if(invocationContext.getMethod().isAnnotationPresent(Secure.class)) {
        hasSecureAnnot = true;
      }
      
      if(invocationContext.getMethod().isAnnotationPresent(Audit.class)) {
        hasAuditingAnnot = true;
        Audit auditAnnot = invocationContext.getMethod().getAnnotation(Audit.class);
        if(auditAnnot != null) {
          log.setLimitReturnLog(auditAnnot.limitReturnLog());
        }
      }
      
      //Check Secure Annotation
      auditingPrepare(invocationContext);
      
      //Check Security
      securityVerification(invocationContext);
      
      result = invocationContext.proceed();
      
      log.setReturnObject(result);
      
    } catch (Throwable e) {
      
      if(hasAuditingAnnot) {
        log.setSuccess(false);
        log.setReturnObject(e);
        
        log.setErrorComponent(ExceptionCastUtil.getExceptionThrowClass(e));
      }
      
      
      if(e instanceof BlackExceptions) {
        
        BlackExceptions be = (BlackExceptions) e;
        be.setId(log.getLogId());
        be.setApplicationId(applicationConfig.getCaApplicationID().toString());
        be.setApplicationName(applicationConfig.getApplicationShortName() + " / " + applicationConfig.getEnvironmentId() );
        log.setExceptionType(be.getType());
        log.setExceptionCode(be.getCode());
        
      } else if(e instanceof javax.ejb.EJBException) {
        
        if(e.getCause() instanceof javax.persistence.PersistenceException) {
          //throw new SystemException(log.getLogId(), CoreErrorsConstants.PersistenceErrors.PERSISTENCE_001);
          ex = new SystemException(log.getLogId(), CoreErrorsConstants.PersistenceErrors.PERSISTENCE_001);
        }
        
      } else {
        log.setExceptionType("SYSTEM");
        //Se não for qualquer dos tipos da aplicação, relancar dendro de uma system 
        
        if(e.getCause() instanceof SystemException) {
          
          BlackExceptions be = (BlackExceptions) e.getCause();
          be.setId(log.getLogId());
          be.setApplicationId(applicationConfig.getCaApplicationID().toString());
          be.setApplicationName(applicationConfig.getApplicationShortName() + " / " + applicationConfig.getEnvironmentId() );
          log.setExceptionType(be.getType());
          log.setExceptionCode(be.getCode());
          
          //throw new SystemException(log.getLogId(), e, be.getMessage());
          ex = new SystemException(log.getLogId(), e, be.getMessage());
        } else 
        if(e.getCause() instanceof SecurityException) {
          
          BlackExceptions be = (BlackExceptions) e.getCause();
          be.setId(log.getLogId());
          be.setApplicationId(applicationConfig.getCaApplicationID().toString());
          be.setApplicationName(applicationConfig.getApplicationShortName() + " / " + applicationConfig.getEnvironmentId() );
          log.setExceptionType(be.getType());
          log.setExceptionCode(be.getCode());
          
          //throw new SecurityException(log.getLogId(), e, be.getMessage());
          ex = new SecurityException(log.getLogId(), e, be.getMessage());
        } else
        if(e.getCause() instanceof javax.ws.rs.NotFoundException) {
          //throw e.getCause();  
          ex = e.getCause();
        }
        
        //throw new SystemException(log.getLogId(), e, "Desculpe, tivemos problemas com a solicitação.");
        ex = new SystemException(log.getLogId(), e, "Desculpe, tivemos problemas com a solicitação.");
      }
      
      if(ex == null) {
        ex = e;
      }
      
      log.setReturnObject(ex);
      
      throw ex;
    } finally {
      
      if(ex != null) {
        System.out.println(ex.getMessage());
        
        if(ex instanceof BlackExceptions) {
          log.setErrorReturn(JacksonJsonUtils.toJson(((BlackExceptions)ex).getErrorMessage()));
        } else {
          
          ErrorMessage dto = new ErrorMessage();
          
          dto.setStatus(Status.INTERNAL_SERVER_ERROR.name());
          dto.setStatusCode(Status.INTERNAL_SERVER_ERROR.getStatusCode());
          
          dto.setMessage("Falha de Sistema: " + ex.getMessage());
          
          //dto.getValidations().add(new Validation(this.getCode(), null, this.getMessage()));
          
          //dto.setTransactionId(applicationConfig.getCaApplicationID().toString());
          dto.setApplicationId(applicationConfig.getCaApplicationID().toString());
          dto.setApplicationName(applicationConfig.getApplicationShortName() + " / " + applicationConfig.getEnvironmentId());
          
          dto.setType("SYSTEM");
          
          log.setErrorReturn(JacksonJsonUtils.toJson(dto));
        }
        
      }
      
      if(hasAuditingAnnot) {
        
        //Caso o Id do usuario não tenha sido preenchido, pagar do token
        if(log.getUserId() == null) {
          try {
            Claims tkClaims = jwtService.verifyToken(requestInfo.getToken());
            log.setUserId(tkClaims.getUserId());
            requestInfo.setUserId(tkClaims.getUserId());
          } catch (Exception e2) {
            // TODO: handle exception
          }
        }
        
        long end = System.currentTimeMillis();
        log.setOperationTime(end - start);
        logService.registerAsyn(log);
      }
    }
    
    return result;
  }
  
  private void securityVerification(InvocationContext ic) throws BusinessException {
    
    /*if(hasSecureAnnot) {

      CAOperation caOperation = ic.getMethod().getAnnotation(CAOperation.class);
      
      if (    ( caOperation.value() == null || caOperation.value().equals("") )
              && ( caOperation.hasRole() == null    || caOperation.hasRole().length == 0    || (caOperation.hasRole().length == 1 && caOperation.hasRole()[0].equals("")) )   
              && ( caOperation.hasAnyRole() == null || caOperation.hasAnyRole().length == 0 || (caOperation.hasAnyRole().length == 1 && caOperation.hasAnyRole()[0].equals("")))
            ) {
        BusinessExceptionUtil.throwSecurityException(SecurityErrors.SECURITY_001);
      }
      
      if(applicationConfig.isCacheAuthorizarion()) {
        
        //Se a cache será utilizada. Deve-se ler as credenciais agora.
        identity.login(caOperation != null);
      
      } else {
      
        //Se não será utilizada cache, as credenciais não serão lidas no início, mas o backend fará a validação. 
        identity.login();
        
      }
      
      if(!identity.isLoggedIn()) {
        throw new NotAuthenticatedException(SecurityErrors.SECURITY_002);
      }
      
      log.setUserId(identity.getAuthenticatedUser().getId());
      log.setUser(identity.getAuthenticatedUser().getLogin());
      
      requestInfo.setUserId(identity.getAuthenticatedUser().getId());
      
      Boolean checkAutorization = caOperation != null;
      
      if(checkAutorization) {
        
        //Realiza a Autorizacao da ACAO
        if(applicationConfig.isCacheAuthorizarion()) {
          
          //TODO Valida a autorizacao com a cache do Identity
          
          if(caOperation != null) {
            identity.hasRoleInCache(caOperation.value());
          }
          
        } else {
          //identity.validarAutorizacaoNatural(ntOperation.value());
          
          if(!identity.hasRole(caOperation.value())) {
            throw new NotAuthorizedException(SecurityErrors.SECURITY_003);
          }
          
        }
      }
    }*/
  }
  
  private void auditingPrepare(InvocationContext ic) {
    
    //Check Audit Annotation
    if(hasAuditingAnnot) {
      EndpointType endpointType = null;
      
      //EndpontType
      if(ic.getMethod().isAnnotationPresent(Path.class) || ic.getMethod().getDeclaringClass().isAnnotationPresent(Path.class)) {
        endpointType = EndpointType.REST;
      } else 
      if(ic.getMethod().isAnnotationPresent(WebService.class)) {
        endpointType = EndpointType.SOAP;
      } else {
        throw new SecurityException("Tipo de Endpoint não identificado.");
      }
      
      log.setEndpointType(endpointType);
      log.setFacadeClasse(ic.getMethod().getDeclaringClass().getName());    
      log.setFacadeMetodo(ic.getMethod().getName());
      
      log.setRequestIp(requestInfo.getIp());
      log.setRequestUri(requestInfo.getRequestUri());
      log.setLogId(requestInfo.getTransactionID());
      
      //Tipo de operacao @NaturalOperation
      /*NaturalOperation operationAnnot = invocationContext.getMethod().getAnnotation(NaturalOperation.class);
      if(operationAnnot != null) {
        log.setOperation(operationAnnot.value());
      }*/
      
      //TODO Tambem tem a @CAOperation
      CAOperation caOperationAnnot = ic.getMethod().getAnnotation(CAOperation.class);
      if(caOperationAnnot != null) {
       
        //log.setOperation(caOperationAnnot.value());
        
        if ( caOperationAnnot.value() != null && !caOperationAnnot.value().equals("") ) {
          log.setOperation(caOperationAnnot.value());
        } else 
        if ( caOperationAnnot.hasRole() != null && !caOperationAnnot.hasRole()[0].equals("") ) {
          
          
          log.setOperation(StringUtils.join(caOperationAnnot.hasRole(), ","));
          
        } else
        if ( caOperationAnnot.hasAnyRole() != null && !caOperationAnnot.hasAnyRole()[0].equals("") ) {
          
          log.setOperation(StringUtils.join(caOperationAnnot.hasAnyRole(), ","));
          
        }
        
        
        
      }
      
      if(StringUtils.isBlank(log.getOperation())) {
        throw new SecurityException("Operação não definida.");
      }
      
      //Parametros da operacao
      Object[] params = ic.getParameters();
      log.setParameters(Arrays.asList(params));
      
    }
    
  }
 
  
  
}

