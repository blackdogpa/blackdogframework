package com.blackdog.framework.utils.compress.gzip;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Base64OutputStream;
import org.apache.commons.compress.utils.IOUtils;

public class GZipUtils {

  
  public static String ungzip(String data) throws Exception {
    return ungzip(Base64.decodeBase64(data));
    //return ungzip(data.getBytes(Charset.defaultCharset()));
  }
  
  public static String gzipToString(String s) throws Exception {
    return Base64.encodeBase64String(gzip(s));
  }
  
  /*public static String ungzip(byte[] bytes) throws Exception {
    InputStreamReader isr = new InputStreamReader(new GZIPInputStream(new ByteArrayInputStream(bytes)), StandardCharsets.UTF_8);
    StringWriter sw = new StringWriter();
    char[] chars = new char[1024];
    for (int len; (len = isr.read(chars)) > 0;) {
      sw.write(chars, 0, len);
    }
    return sw.toString();
  }

  public static byte[] gzip(String s) throws Exception {
    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    GZIPOutputStream gzip = new GZIPOutputStream(bos);
    OutputStreamWriter osw = new OutputStreamWriter(gzip, StandardCharsets.UTF_8);
    osw.write(s);
    osw.close();
    return bos.toByteArray();
  }*/
  
  
  
  //Implementacao Hitoshi
  public static byte[] gzip(String s) throws Exception {

    ByteArrayOutputStream os = new ByteArrayOutputStream(s.length());
    GZIPOutputStream gos = new GZIPOutputStream(os);
    gos.write(s.getBytes());
    gos.close();
    byte[] compressed = os.toByteArray();
    os.close();
    return compressed;
  }
  
  public static byte[] gzip(byte[] s) throws Exception {

    ByteArrayOutputStream os = new ByteArrayOutputStream(s.length);
    GZIPOutputStream gos = new GZIPOutputStream(os);
    gos.write(s);
    gos.close();
    byte[] compressed = os.toByteArray();
    os.close();
    return compressed;
  }

  public static String ungzip(byte[] compressed) throws Exception {

    final int BUFFER_SIZE = 32;
    ByteArrayInputStream is = new ByteArrayInputStream(compressed);
    GZIPInputStream gis = new GZIPInputStream(is, BUFFER_SIZE);
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    byte[] data = new byte[BUFFER_SIZE];
    int bytesRead;
    while ((bytesRead = gis.read(data)) != -1) {
        baos.write(data, 0, bytesRead);
    }
    gis.close();
    return baos.toString("UTF-8");
  }

  

  /*public static void compressGZIP(File input, File output) throws IOException {
    try (GZIPOutputStream out = new GZIPOutputStream(new FileOutputStream(output))) {
      try (FileInputStream in = new FileInputStream(input)) {
        byte[] buffer = new byte[1024];
        int len;
        while ((len = in.read(buffer)) != -1) {
          out.write(buffer, 0, len);
        }
      }
    }
  }

  public static void decompressGzip(File input, File output) throws IOException {
    try (GZIPInputStream in = new GZIPInputStream(new FileInputStream(input))) {
      try (FileOutputStream out = new FileOutputStream(output)) {
        byte[] buffer = new byte[1024];
        int len;
        while ((len = in.read(buffer)) != -1) {
          out.write(buffer, 0, len);
        }
      }
    }
  }*/
  
  
  /**
   * Compress the files and encode as Base64
   * 
   * @param originalFile
   * @param outFile
   * @throws IOException
   */
  public static void zipAndEncodeFiles(File originalFile, File outFile) throws IOException {
    FileInputStream fis = null;
    GZIPOutputStream zos = null;
    try {
      fis = new FileInputStream(originalFile);
      FileOutputStream fos = new FileOutputStream(outFile);
      Base64OutputStream base64Out = new Base64OutputStream(fos);
      zos = new GZIPOutputStream(base64Out);
      IOUtils.copy(fis, zos);
    } finally {
      IOUtils.closeQuietly(fis);
      IOUtils.closeQuietly(zos);
    }
  }
	
	
}
