package com.blackdog.framework.utils.data;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

public class AdabasDataUtil {
  
  
  
  public static BigDecimal toBigDecimal(Long value) throws EntireXDataException {
    if(value == null) {
      return null; 
    }
    return toBigDecimal(value.toString());
  }
  
  public static BigDecimal toBigDecimal(Integer value) throws EntireXDataException {
    if(value == null) {
      return null; 
    }
    return toBigDecimal(value.toString());
  }
  
  public static BigDecimal toBigDecimal(String value) throws EntireXDataException {
    
    if(value == null || value.trim().equals("")) {
      return null;
    }
    
    if(StringUtils.isNumeric(value)) {
      return new BigDecimal(value);
    }
    
    //TODO Deveria lancar excessão. Mas a EX deve ser expe 
    throw new EntireXDataException(value + " não é um número válido. ");
  }

  public static BigDecimal toBigDecimalDate(String dateString) throws EntireXDataException {
    
    if(dateString == null || dateString.trim().equals("")) {
      return null;
    }
    
    if(dateString.length() == 10) {
      String[] split = dateString.split("/");
      
      return new BigDecimal(split[2]+split[1]+split[0]);
    } else 
    
    //2016-01-01T03:00:00.000Z
    if(dateString.length() == 24) {
      return new BigDecimal(dateString.substring(0, 4)+ "" +dateString.substring(5, 7) +""+dateString.substring(8, 10));
    } else {
      
      throw new EntireXDataException("A data não é válida. ");
      
    }
    
    /*try {
      Date dt = new SimpleDateFormat("dd/MM/yyyy").parse(dateString);
    } catch (ParseException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }*/
    
    //String[] split = dateString.split("/");
    
    //return new BigDecimal(split[2]+split[1]+split[0]);
  }

  public static BigDecimal toBigDecimalDate(Date date) throws EntireXDataException  {
    
    try {
      if(date != null) {
        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        return new BigDecimal(formatter.format(date));
      }
    } catch (Exception e) {
      throw new EntireXDataException("A data informada é inválida. ");
    }
    return null;
    
  }
  
}
