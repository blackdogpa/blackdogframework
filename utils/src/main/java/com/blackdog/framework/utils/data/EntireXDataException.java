package com.blackdog.framework.utils.data;

import javax.xml.ws.WebFault;

import com.blackdog.framework.utils.string.StringUtils;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
@WebFault(name="BusinessExceptionFault", targetNamespace= "http://prodepa.pa.gov.br/apis/simas/Exceptions")
public class EntireXDataException extends Exception {

  private static final long serialVersionUID = -5022967813902834759L;

  private String id;
  
  private String applicationId;
  private String applicationName;
  
  public EntireXDataException() {
    super();
  }

  public EntireXDataException(String message, Object... args) {
    super(StringUtils.messageFormat(message, args));
  }
  
  public EntireXDataException(Throwable cause) {
    super(cause);
  }

  public EntireXDataException(Throwable cause, String message, Object... args) {
    super(StringUtils.messageFormat(message, args), cause);
  }
  
  public String getId() {
    return id;
  }

  public void setId(String id) {
    
    this.id = id;
  }
  
  public String getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(String applicationId) {
    this.applicationId = applicationId;
  }

  public String getApplicationName() {
    return applicationName;
  }

  public void setApplicationName(String applicationName) {
    this.applicationName = applicationName;
  }

  @Override
  public String getMessage() {
    StringBuilder builder = new StringBuilder();

    if(applicationId != null && applicationName != null) {
      
      builder.append("[");
      builder.append(applicationId);
      builder.append(".");
      builder.append(applicationName);
      builder.append("]");
      
      builder.append("-");
    }
    if (id != null) {
      builder.append(id);
      builder.append(" : ");
      builder.append(super.getMessage());
    } else {
      builder.append(super.getMessage());
    }
    return builder.toString();
  }
  
  
  
  
}
