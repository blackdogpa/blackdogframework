package com.blackdog.framework.utils.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Pattern;

import org.apache.commons.lang3.time.DateUtils;


public class DateUtil {

  
  public static Pattern datePTPattern = Pattern.compile("^(((0[1-9]|[12][0-9]|30)[-/]?(0[13-9]|1[012])|31[-/]?(0[13578]|1[02])|(0[1-9]|1[0-9]|2[0-8])[-/]?02)[-/]?[0-9]{4}|29[-/]?02[-/]?([0-9]{2}(([2468][048]|[02468][48])|[13579][26])|([13579][26]|[02468][048]|0[0-9]|1[0-6])00))$");
   
  public static Pattern dateENPattern = Pattern.compile("^([0-9]{4}[-/]?((0[13-9]|1[012])[-/]?(0[1-9]|[12][0-9]|30)|(0[13578]|1[02])[-/]?31|02[-/]?(0[1-9]|1[0-9]|2[0-8]))|([0-9]{2}(([2468][048]|[02468][48])|[13579][26])|([13579][26]|[02468][048]|0[0-9]|1[0-6])00)[-/]?02[-/]?29)$");
  
  public static Pattern dateENPattern2 = Pattern.compile("^(?=\\d)(?:(?!(?:1582(?:\\.|-|\\/)10(?:\\.|-|\\/)(?:0?[5-9]|1[0-4]))|(?:1752(?:\\.|-|\\/)0?9(?:\\.|-|\\/)(?:0?[3-9]|1[0-3])))(?=(?:(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:\\d\\d)(?:[02468][048]|[13579][26]))\\D0?2\\D29)|(?:\\d{4}\\D(?!(?:0?[2469]|11)\\D31)(?!0?2(?:\\.|-|\\/)(?:29|30))))(\\d{4})([-\\/.])(0?\\d|1[012])\\2((?!00)[012]?\\d|3[01])(?:$|(?=\\x20\\d)\\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\\d){0,2}(?:\\x20[aApP][mM]))|(?:[01]\\d|2[0-3])(?::[0-5]\\d){1,2})?$");
  
  public static Pattern dateENPattern3 = Pattern.compile("^((\\d\\d\\d\\d)(-|/)(\\d\\d)(-|/)(\\d\\d) (\\d\\d):(\\d\\d):(\\d\\d).(\\d\\d\\d))$");
  
  //99999999:999999
  public static Pattern specialDatePattern01 = Pattern.compile("^([0-9]{8}):([0-9]{6})$");
  
  public static Pattern specialDatePattern02 = Pattern.compile("^([0-9][-/][0-9][-/][0-9])$");
  
  public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
  public static SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyyMMddhhMMss");
  public static SimpleDateFormat dateFormat5 = new SimpleDateFormat("yyyyMMddhhMMssSSS");
  
  public static SimpleDateFormat dateFormat3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
  public static SimpleDateFormat dateFormat4 = new SimpleDateFormat("yyyy-MM-dd");
  
  public static Date parseDate(String strDate) throws Exception {
    
    if(strDate == null || strDate.isEmpty()) {
      return null;
    }
    
    strDate = strDate.trim();
    
    Date date = null;
    if(Pattern.matches("[0-9]*", strDate)) {
      
      if(dateENPattern.matcher(strDate).matches()) {
        date  = parseDateEN(strDate);
      } else 
      if(dateENPattern2.matcher(strDate).matches()) {
          date  = parseDateEN(strDate);
      } else
      if(datePTPattern.matcher(strDate).matches()) {
        date  = parseDatePT(strDate);
      } else if(strDate.length() == 14) {
        
        try {
          //date = dateFormat2.parse(strDate); //TODO Questao do TimeZone
          date = DateUtils.parseDate(strDate, "yyyyMMddHHmmss");
          //date = DateUtils.parseDate(strDate, "yyyyMMddhhMMss");
        } catch (Exception e) { e.printStackTrace(); }
        
      } else {
        try {
          date = new Date(Long.parseLong(strDate));
        } catch (Exception e) { e.printStackTrace(); }
      }
    } else {
      
      if(dateENPattern.matcher(strDate).matches()) {
        date  = parseDateEN(strDate);
      } else 
      if(dateENPattern2.matcher(strDate).matches()) {
        date  = parseDateEN(strDate);
      } else
      if(dateENPattern3.matcher(strDate).matches()) {
        date  = parseDateEN(strDate);
      } else  
      if(datePTPattern.matcher(strDate).matches()) {
        date  = parseDatePT(strDate);
      } else 
      if(specialDatePattern01.matcher(strDate).matches()) {
        date  = parseDateEN(strDate);
        //TODO Não consegui fazer a expressao para definir se a data está em PT ou EN aqui. Ver depois
        /*if(date == null) {
          date  = parseDatePT(strDate);  
        }*/
      } else {
        try {
          date = parseDateEN(strDate);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }

      if(date == null) {
        throw new Exception("A data " +strDate + " é inválida.");
      }
    }
    
    if(date == null) {
      throw new Exception("A data "+ strDate + " é inválida.");
    }
    
    return date;
  }
  
  
  public static Date parseDatePT(String strDate) throws  Exception {
    final String[] possibleFormats = {
            "ddMMyyyy",
            "EEE, dd MMM yyyy HH:mm:ss z", // RFC_822
            "EEE, dd MMM yyyy HH:mm zzzz",
            "dd-MM-yyyy'T'HH:mm:ssZ",
            "dd-MM-yyyy'T'HH:mm:ss.SSSzzzz", // Blogger Atom feed has millisecs also
            "dd-MM-yyyy'T'HH:mm:sszzzz",
            "dd-MM-yyyy'T'HH:mm:ss z",
            "dd-MM-yyyy'T'HH:mm:ssz", // ISO_8601
            "dd-MM-yyyy'T'HH:mm:ss",
            "dd-MM-yyyy'T'HHmmss.SSSz",
            "dd-MM-yyyy HH:mm:ss",
            "dd-MM-yyyy",
            "dd/MM/yyyy'T'HH:mm:ssZ",
            "dd/MM/yyyy'T'HH:mm:ss.SSSzzzz", // Blogger Atom feed has millisecs also
            "dd/MM/yyyy'T'HH:mm:sszzzz",
            "dd/MM/yyyy'T'HH:mm:ss z",
            "dd/MM/yyyy'T'HH:mm:ssz", // ISO_8601
            "dd/MM/yyyy'T'HH:mm:ss",
            "dd/MM/yyyy'T'HHmmss.SSSz",
            "dd/MM/yyyy HH:mm:ss",
            "dd/MM/yyyy",
            "ddMMyyyy:HHmmss",
            
        };
    
    try {
      return DateUtils.parseDate(strDate, possibleFormats);
    } catch (ParseException e) {
      throw new Exception(e.getMessage(), e);
    }
  }
  
  public static Date parseDateEN(String strDate) throws  Exception    {
    final String[] possibleFormats = {
            "yyyyMMdd",
            "EEE, dd MMM yyyy HH:mm:ss z", // RFC_822
            "EEE, dd MMM yyyy HH:mm zzzz",
            "yyyy-MM-dd'T'HH:mm:ssZ",
            "yyyy-MM-dd'T'HH:mm:ss.SSSzzzz", // Blogger Atom feed has millisecs also
            "yyyy-MM-dd'T'HH:mm:sszzzz",
            "yyyy-MM-dd'T'HH:mm:ss z",
            "yyyy-MM-dd'T'HH:mm:ssz", // ISO_8601
            "yyyy-MM-dd'T'HH:mm:ss",
            "yyyy-MM-dd'T'HHmmss.SSSz",
            "yyyy-MM-dd HH:mm:ss.SSS",
            "yyyy-MM-dd HH:mm:ss",
            "yyyy-MM-dd",
            "yyyy/MM/dd'T'HH:mm:ssZ",
            "yyyy/MM/dd'T'HH:mm:ss.SSSzzzz", // Blogger Atom feed has millisecs also
            "yyyy/MM/dd'T'HH:mm:sszzzz",
            "yyyy/MM/dd'T'HH:mm:ss z",
            "yyyy/MM/dd'T'HH:mm:ssz", // ISO_8601
            "yyyy/MM/dd'T'HH:mm:ss",
            "yyyy/MM/dd'T'HHmmss.SSSz",
            "yyyy/MM/dd HH:mm:ss",
            "yyyy/MM/dd",
            "yyyyMMdd:HHmmss",
        };
    try {
      return DateUtils.parseDate(strDate, possibleFormats);
    } catch (ParseException e) {
      throw new Exception(e.getMessage(), e);
    }
  }
  
  public static String parse(Date dt, String pattern) {
    SimpleDateFormat format = new SimpleDateFormat(pattern);
    if(dt != null) {
      return format.format(dt);
    } else {
      return "";
    }
  }
  
  public static String simples(Date dt) {
    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    if(dt != null) {
      return format.format(dt);
    } else {
      return "";
    }
  }
  
  public static String completo(Date dt) {
    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    if(dt != null) {
      return format.format(dt);
    } else {
      return "";
    }
  }
  
  
	public static String extenso(Date dt) {

	  if(dt == null) {
	    return "";
	  }
		String s = "";
		// retorna os valores ano, mês e dia da variável "dt"
		int d = dt.getDate();
		int m = dt.getMonth() + 1;
		int a = dt.getYear() + 1900;
		// retorna o dia da semana: 1=domingo, 2=segunda-feira, ..., 7=sábado
		Calendar data = new GregorianCalendar(a, m - 1, d);
		int ds = data.get(Calendar.DAY_OF_WEEK);
		s = d + " de " + NomeDoMes(m, 0) + " de " + a;
		return s.toUpperCase();

	}

	// Retorna o nome do mês.
	// Parâmetros: "i" = índice para o vetor "mes"
	// "tipo" = 0 para retornar o nome completo e
	// 1 para o nome abreviado do mês.
	public static String NomeDoMes(int i, int tipo) {
		String mes[] = { "janeiro", "fevereiro", "março", "abril", "maio",
				"junho", "julho", "agosto", "setembro", "outubro", "novembro",
				"dezembro" };
		if (tipo == 0)
			return (mes[i - 1]); // extenso
		else
			return (mes[i - 1].substring(0, 3)); // abreviado
	}

	public static String DiaDaSemana(int i, int tipo) {
		String diasem[] = { "domingo", "segunda-feira", "terça-feira",
				"quarta-feira", "quinta-feira", "sexta-feira", "sábado" };
		if (tipo == 0)
			return (diasem[i - 1]); // extenso
		else
			return (diasem[i - 1].substring(0, 3));
	}
	
	public static Date truncateToDate(Date date) {
	  return DateUtils.truncate(date, Calendar.DATE);
	}
	
	
	public static String now1() {
	  return dateFormat.format(new Date());
	}
	
	
}
