package com.blackdog.framework.utils.date;

import java.math.BigDecimal;

public class SimasDateUtil {

  
  
  
  public static String dateFormat(BigDecimal decimalDate) { 
  
    String strDecimal = decimalDate.toString();
    
    String dia = null;
    String mes = null;
    String ano = null;
    
    if(strDecimal.length() == 8) {
      
      dia = strDecimal.substring(0, 2);
      mes = strDecimal.substring(2, 4);
      ano = strDecimal.substring(4, 8);
      
    } else if(strDecimal.length() == 7) {
      
      dia = strDecimal.substring(0, 1);
      mes = strDecimal.substring(1, 3);
      ano = strDecimal.substring(3, 7);
      
    } else {
      //TODO Não conhcido
    }
    
    if(dia.length() == 1) {
     dia = "0"+dia;
    }
    
    return dia + "/" + mes + "/" + ano;
  }
  
}
