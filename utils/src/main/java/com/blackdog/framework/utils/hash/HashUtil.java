package com.blackdog.framework.utils.hash;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashUtil {

  public static String generateHashSHA256(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
    MessageDigest md = null;
    byte[] hash = null;
    md = MessageDigest.getInstance("SHA-256");
    hash = md.digest(text.getBytes("UTF-8"));
    return convertToHex(hash);
  }
  
  public static String generateHashSHA512(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
    MessageDigest md = null;
    byte[] hash = null;
    md = MessageDigest.getInstance("SHA-512");
    hash = md.digest(text.getBytes("UTF-8"));
    return convertToHex(hash);
  }
  
  /**
  * Converts the given byte[] to a hex string.
  * @param raw the byte[] to convert
  * @return the string the given byte[] represents
  */
  private static String convertToHex(byte[] raw) {
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < raw.length; i++) {
          sb.append(Integer.toString((raw[i] & 0xff) + 0x100, 16).substring(1));
      }
      return sb.toString();
  }
  
  
}
