package com.blackdog.framework.utils.rest;

import java.util.List;

import javax.ws.rs.core.Response.Status;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
//@javax.xml.bind.annotation.XmlRootElement
//@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2017-02-02T18:36:11.447Z")
public class ApiResponseMessage {

  /*public static final int OK = 200;
  //public static final int INFO = 202; //TODO 202 Accepted - Não ficou bom 
  
  public static final int CREATED = 201; //TODO 
  public static final int WARNING = 203; //Non-Authoritative Information
  
  public static final int ERROR = 500;

  //public static final int WARNING = 2;
	public static final int TOO_BUSY = 429; //429 Too Many Requests
*/
	private int statusCode;
	private String type;
	private String message;
	private List<String> infos;
		
	private String errorClass;
	private List<String> errors;
	
	public ApiResponseMessage(){}
	
	/**
	 * Contrutor Sucesso
	 * 
	 * @param code
	 * @param message
	 */
	public ApiResponseMessage(Status status, String message) {

	  this.statusCode = status.getStatusCode();
		this.message = message;
		
		
		
		switch(this.statusCode){
		case 200:
		  setType("ok");
		  break;
		/*case INFO:
		  setType("info");
		  break;*/
		case 201:
		  setType("created");
      break;
		case 500:
			setType("error");
			break;
		case 203:
			setType("warning");
			break;
		case 429:
			setType("too busy");
			break;
		default:
			//setType(status.name());
		  setType(status.getReasonPhrase());
			break;
		}
	}
	
	public ApiResponseMessage(Status status, String message, List<String> infos) {
    this(status, message);

    this.infos = infos;
  }

  /**
	 * Construtor de Error
	 * @param code
	 * @param message
	 * @param errorClass
	 * @param errors
	 */
	public ApiResponseMessage(Status status, String message, String errorClass, List<String> errors) {
    this(status, message);
    
    this.errorClass = errorClass;
    this.errors = errors;
  }

	@JsonProperty(value = "errors")
  public List<String> getErrors() {
    return errors;
  }

  public void setErrors(List<String> errors) {
    this.errors = errors;
  }

  @JsonProperty(value = "errorClass")
  public String getErrorClass() {
    return errorClass;
  }

  public void setErrorClass(String errorClass) {
    this.errorClass = errorClass;
  }

  @JsonProperty(value = "statusCode")
  public int getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(int statusCode) {
    this.statusCode = statusCode;
  }
  
	@JsonProperty(value = "type")
	public String getType() {
		return type;
	}

  public void setType(String type) {
		this.type = type;
	}

	
	@JsonProperty(value = "message")
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@JsonProperty(value = "infos")
  public List<String> getInfos() {
    return infos;
  }

  public void setInfos(List<String> infos) {
    this.infos = infos;
  }

	
	
	
	
}
