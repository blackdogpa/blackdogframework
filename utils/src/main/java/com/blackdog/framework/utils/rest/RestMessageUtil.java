package com.blackdog.framework.utils.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.blackdog.framework.core.model.Model;


/**
 * Utility class for HTTP headers creation.
 * 
 * TODO Fazer Merge entre  RestMessageUtils e RestMessageUtil
 */
public class RestMessageUtil {

  private static final Logger log = LoggerFactory.getLogger(RestMessageUtil.class);


  /*
   *  Create 
   */
  
  public static Response createEntityCreationAlert(String uri) {
    return createEntityCreationAlert(uri, null);
  }
  
  public static Response createEntityCreationAlert(String uri, com.blackdog.framework.core.model.Model model) {
    try {
      if (model == null) {
        return createAlert(Response.created(new URI(uri)), "O registro foi criado. ", "").build();
      } else {
        com.blackdog.framework.core.model.annotation.Model annot = getModelAnnotation(model);
        if (annot != null) {
          // TODO Tratar o Masculino/Feminino
          return createAlert(Response.created(new URI(uri)), "O " + annot.name() + " com o identificador " + model.getId()
                  + " foi criado. ", model.getId().toString()).entity(model).build();
        } else {
          return createAlert(Response.created(new URI(uri)), "O registro com o identificador " + model.getId()
                  + " foi criado. ", model.getId().toString()).entity(model).build();
        }
      }
    } catch (URISyntaxException e) {
      return Response.serverError().build();
    }
    
  }

  /*
   * Update 
   */
  
  public static Response createEntityUpdateAlert() {
    return createEntityUpdateAlert(null);
  }
    
  public static Response createEntityUpdateAlert(com.blackdog.framework.core.model.Model model) {

    if (model == null) {
      return createAlert(Response.ok(), "O registro foi atualizado. ", "").build();
    } else {
      com.blackdog.framework.core.model.annotation.Model annot = getModelAnnotation(model);
      if (annot != null) {
        // TODO Tratar o Masculino/Feminino
        return createAlert(Response.ok(), "O " + annot.name() + " com o identificador " + model.getId()
                + " foi atualizado. ", model.getId().toString()).entity(model).build();
      } else {
        return createAlert(Response.ok(), "O registro com o identificador " + model.getId()
                + " foi atualizado. ", model.getId().toString()).entity(model).build();
      }
    }
  }

  /*
   * Delete
   */
  
  public static Response createEntityDeletionAlert() {
    return createEntityDeletionAlert(null);
  }
  
  public static Response createEntityDeletionAlert(com.blackdog.framework.core.model.Model model) {

    if (model == null) {
      return createAlert(Response.ok(), "O registro foi removido. ", "").build();
    } else {
      com.blackdog.framework.core.model.annotation.Model annot = getModelAnnotation(model);
      if (annot != null) {
        // TODO Tratar o Masculino/Feminino
        return createAlert(Response.ok(), "O " + annot.name() + " com o identificador " + model.getId()
                + " foi removido. ", model.getId().toString()).entity(model).build();
      } else {
        return createAlert(Response.ok(), "O registro com o identificador " + model.getId()
                + " foi removido. ", model.getId().toString()).entity(model).build();
      }
    }

  }

  /*public static Response createEntityDeletionAlert(String entityName, Long id) {
    return createAlert(Response.ok(), "A " + entityName + " is deleted with identifier " + id, id.toString()).build();
  }*/
  
  
  
  /*
   * Search
   */
  public static Response createEntitySearchAlert(List<?> content) {
    return createEntitySearchAlert(Status.OK, content);
  }
    
  public static Response createEntitySearchAlert(Status status, List<?> content) {

    ResponseBuilder builder = Response.status(status);
    
    if(content == null || content.isEmpty() ) {

      builder.header("X-blackDog-alert", "Não foram localizados resultados");
      builder.header("X-blackDog-params", "0");

      
    } else {
      
      if(content.size() == 1) {

        builder.header("X-blackDog-alert", "Um resultado foi localizado");
        builder.header("X-blackDog-params", "1");

      } else {

        builder.header("X-blackDog-alert", "Foram localizados "+ content.size() +" registros");
        builder.header("X-blackDog-params", content.size());

      }
      
    }

    return builder.entity(content).build();
  }
  
  
  /*
   * ***** SUCCESS
   */
  public static Response success(String message){
    //RestMessageUtils msg = new RestMessageUtils(200, message);
    return success(message, null);
  }
  
  public static Response success(String message, List<String> infos){
    //RestMessageUtils msg = new RestMessageUtils(200, message);
    return Response.ok(new ApiResponseMessage(Status.OK, message, infos)).build();
  }
  
  public static Response success(Model entity){
    //RestMessageUtils msg = new RestMessageUtils(200, message);
    return Response.ok(entity).build();
  }
  
  
  /*
   * WARN SUCCESS
   */
  public static Response warn(String message){
    //RestMessageUtils msg = new RestMessageUtils(200, message);
    return success(message, null);
  }
  
  public static Response warn(String message, List<String> infos){
    //RestMessageUtils msg = new RestMessageUtils(200, message);
    return Response.ok(new ApiResponseMessage(Status.OK, message, infos)).build();
  }
  
  
  /*
   * ERROR SUCCESS
   */
  public static Response error(String message){
    return error(message, null, null);
  }
  
  public static Response error(List<String> errors){
    return error(null, null, errors);
  }
  
  public static Response error(String message, String errorClass, List<String> errors){
    return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new ApiResponseMessage(Status.INTERNAL_SERVER_ERROR, message, errorClass, errors)).build();
  }
  
  /*public static Response error(List<Error> errors){
    return Response.status(ApiResponseMessage.ERROR).entity(new ApiResponseMessage(ApiResponseMessage.ERROR, null, errors)).build();
  }*/

  public static Response custom(Status status, String message, String errorClass, List<String> errors){
    return Response.status(status).entity(new ApiResponseMessage(status, message, errorClass, errors)).build();
  }
  

  /*
   * Custom Message
   */
  
  public static Response createSuccessAlert(String message) {
    return createCustomAlert(Status.OK, message);
  }
  
  public static Response createSuccessAlertData(Object obj) {
    return Response.ok().entity(obj).build();
    //return createCustomAlert(Status.OK, message);
  }
  
  public static Response createCustomAlert(Status status, String message) {
    return createAlert(Response.ok(), message, "").build();
  }
  
  
  public static Response createCustomAlert(String message, com.blackdog.framework.core.model.Model model) {
    return createCustomAlert(Status.OK, message, model);
  }
  
  public static Response createCustomAlert(Status status, String message, com.blackdog.framework.core.model.Model model) {
    return createAlert(Response.ok(), message, "").entity(model).build();
  }

  
  public static ResponseBuilder createAlert(ResponseBuilder builder, String message, String param) {

    builder.header("X-blackDog-alert", message);
    builder.header("X-blackDog-params", param);
    //builder.header("X-jHipsterProdepaApp-params", param);

    return builder;
  }
  
  
  public static Response createFailureAlert(Status status, String entityName, String errorKey, String defaultMessage) {
    log.error("Entity creation failed, {}", defaultMessage);

    ResponseBuilder builder = Response.status(status);

    builder.header("X-jHipsterProdepaApp-error", defaultMessage);
    builder.header("X-jHipsterProdepaApp-params", entityName);
    return builder.build();
  }

  
  
  

  private static com.blackdog.framework.core.model.annotation.Model getModelAnnotation(com.blackdog.framework.core.model.Model model) {

    com.blackdog.framework.core.model.annotation.Model annot = model.getClass().getAnnotation(com.blackdog.framework.core.model.annotation.Model.class);

    return annot;
  }

}
