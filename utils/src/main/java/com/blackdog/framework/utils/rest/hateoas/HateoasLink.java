package com.blackdog.framework.utils.rest.hateoas;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * "links": 
 *    [ 
 *      {
 *        "rel": "self",
 *        "href": "http://localhost:8080/product/1",
 *        "type" : "GET"
 *      },
 *      {
          "rel": "product.search",
          "href": "http://localhost:8080/product/search",
          "type" : "GET"
        }
 *    ]
 *       
 *
 *
 * @author thiago
 */
public class HateoasLink {

  private String rel;
  private String href;
  private String type = "GET";

  public HateoasLink() {
    super();
    // TODO Auto-generated constructor stub
  }

  public HateoasLink(String rel, String href) {
    super();
    this.rel = rel;
    this.href = href;
  }

  public HateoasLink(String rel, String href, String type) {
    super();
    this.rel = rel;
    this.href = href;
    this.type = type;
  }

  /**
   **/
  @JsonProperty("rel")
  public String getRel() {
    return rel;
  }

  public void setRel(String rel) {
    this.rel = rel;
  }

  /**
   **/
  @JsonProperty("href")
  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  /**
   **/
  @JsonProperty("type")
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return "HateoasLink [rel=" + rel + ", href=" + href + ", type=" + type + "]";
  }
   

}
