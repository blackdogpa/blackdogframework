package com.blackdog.framework.utils.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.container.ContainerRequestContext;


public class GDSSOUtils {

	public static final boolean DEBUG = false; //TODO Isso deve ser determionado pelo ambiente, de alguma forma

	//GD configurations
	public static final String FACADE_REMOTE = "jnp://controleacesso-srv:1099/controleacesso/ControleAcessoSSOFacadeBean/remote";

	public static final String CHECK_XHTML = "/check.xhtml";
	public static final String CHECK_SEAM = "/check.seam";
	public static final String CHECK_JSF = "/check.jsf";

	// Client
	public static final String CLIENT_ERROR_XHTML = "/error.html";

	public static final String CLIENT_REDIRECT_PARAMETER = "redirect";
	public static final String CLIENT_APPLICATION_ID_PARAMETER = "app.sso.application_id";
	public static final String CLIENT_MAIN_PAGE_VALUE          = "app.main.page";

	// Server

	public static final String SERVER_CONTEXT = "/governodigital";
	public static final String SERVER_CHECK_SEAM = SERVER_CONTEXT + CHECK_SEAM;
	public static final Long SERVER_APPLICATION_ID = 7L;

	// Cookie

	public static final String COOKIE_PATH = "/";
	public static final String SESSION_COOKIE = "KSESSIONID";

	// ///////////
	// Cookies //
	// ///////////

	/**
	 * Retorna o Cookie de Sessao.
	 * 
	 * @param request
	 * @return
	 */
	public static String getSessionCookie(HttpServletRequest request) {
		print("[SSOConfig] [getSessionCookie]");
		return HttpUtil.getCookie(request, SESSION_COOKIE);
	}
	
	
	public static String getSessionCookie(ContainerRequestContext requestContext) {
    print("[SSOConfig] [getSessionCookie]");
    return HttpUtil.getCookie(requestContext, SESSION_COOKIE);
  }

	/**
	 * Grava o Cookie de Sessao.
	 * 
	 * @param value
	 * @param request
	 * @param response
	 */
	public static void setSessionCookie(String value, HttpServletRequest request, HttpServletResponse response) {
		print("[SSOConfig] [setSessionCookie]");
		print("[serverName][" + request.getServerName() + "]", 1);
		HttpUtil.setCookie(response, SESSION_COOKIE, value, request.getServerName(), COOKIE_PATH, -1);
	}

	/**
	 * Apaga Cookie de Sessao.
	 * 
	 * @param request
	 * @param response
	 */
	public static void deleteSessionCookie(HttpServletRequest request, HttpServletResponse response) {
		print("[SSOConfig] [deleteSessionCookie]");
		print("[serverName][" + request.getServerName() + "]", 1);
		String serverName = request.getServerName();
		if (serverName.startsWith("dev.")) {
			serverName = serverName.substring(4);
			print("[serverName][changed][" + serverName + "]", 1);
		}
		HttpUtil.setCookie(response, SESSION_COOKIE, "", serverName, COOKIE_PATH, 0);
	}

	/**
	 * Apaga Cookie de Sessao.
	 * 
	 * @param request
	 * @param response
	 */
	public static void deleteJSESSIONID(HttpServletRequest request, HttpServletResponse response) {
		print("[SSOConfig] [deleteJSESSIONID]");
		print("[serverName][" + request.getServerName() + "]", 1);
		HttpUtil.setCookie(response, "JSESSIONID", "", request.getServerName(), COOKIE_PATH, 0);
	}

	// /////////////
	// Depuracao //
	// /////////////

	/**
	 * 
	 * @param text
	 */
	public static void print(String text) {
		if (GDSSOUtils.DEBUG) {
			print(text, 0);
		}
	}

	/**
	 * 
	 * @param text
	 * @param ident
	 */
	public static void print(String text, int ident) {
		if (GDSSOUtils.DEBUG) {
			StringBuffer space = new StringBuffer("");
			while (ident > 0) {
				space.append("    ");
				ident--;
			}
		}
	}
}