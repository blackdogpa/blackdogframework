package com.blackdog.framework.utils.security;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

//import javax.faces.application.FacesMessage;
//import javax.faces.application.FacesMessage.Severity;
//import javax.faces.context.ExternalContext;
//import javax.faces.context.FacesContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.container.ContainerRequestContext;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com)
 *
 */
public abstract class HttpUtil {

	/**
	 * Recupera o valor de um cookie.
	 * 
	 * @param request
	 *            Requisicao HTTP.
	 * @param cookieName
	 *            Nome do cookie.
	 * @return Valor do cookie.
	 */
	public static String getCookie(HttpServletRequest request, String cookieName) {
		Cookie cookies[] = request.getCookies();
		if (cookies != null && cookies.length > 0) {
			for (int i = 0; i < cookies.length; i++) {
				if (cookies[i].getName().equals(cookieName)) {
					return cookies[i].getValue();
				}
			}
		}
		return null;
	}
	
	public static String getCookie(ContainerRequestContext requestContext, String cookieName) {
    Map<String, javax.ws.rs.core.Cookie> cookies = requestContext.getCookies();
    
    if (cookies != null && cookies.keySet().size() > 0) {
      for(String key : cookies.keySet()) {
        if (cookies.get(key).getName().equals(cookieName)) {
          return cookies.get(key).getValue();
        }
      }
    }
    return null;
  }

	/**
	 * Cria um cookie.
	 * 
	 * @param response
	 *            Contexto externo.
	 * @param name
	 *            Nome do cookie.
	 * @param value
	 *            Valor do cookie.
	 * @param domain
	 *            Dominio de visibilidade do cookie.
	 * @param path
	 *            Caminho de visibilidade do cookie.
	 * @param maxAge
	 *            Tempo de validade do cookie em segundos.
	 *            Utilizar -1 em cookie de sessao, que so expira ao fechar o navegador.
	 *            Utilizar 0 para definir o cookie como expirado.
	 */
	public static void setCookie(HttpServletResponse response, String name,
			String value, String domain, String path, int maxAge) {
		Cookie cookie = new Cookie(name, value);
		cookie.setDomain(domain);
		cookie.setPath(path);
		cookie.setMaxAge(maxAge);
		response.addCookie(cookie);
	}

	/**
	 * Cria um cookie como expirado.
	 * 
	 * @param response
	 *            Contexto externo.
	 * @param name
	 *            Nome do cookie.
	 * @param domain
	 *            Dominio de visibilidade do cookie.
	 * @param path
	 *            Caminho de visibilidade do cookie.
	 */
	public static void deleteCookie(HttpServletResponse response, String name,
			String domain, String path) {
		setCookie(response, name, "", domain, path, 0);
	}

	/**
	 * 
	 * @param response
	 * @param view
	 * @throws IOException
	 */
	public static void redirect(HttpServletResponse response, String view) throws IOException {
		// String urlWithSessionID = response.encodeRedirectURL(view);
		response.sendRedirect(view);
	}

	public static void forward(HttpServletRequest request,
			HttpServletResponse response, String view) throws ServletException,
			IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher(view);
		dispatcher.forward(request, response);
	}

}