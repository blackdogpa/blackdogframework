package com.blackdog.framework.utils.serialization.hex;

import org.apache.commons.codec.binary.Hex;

public class HexUtils {

	public static String encodeHexString(byte[] data) {
		return Hex.encodeHexString(data);
	}
}
