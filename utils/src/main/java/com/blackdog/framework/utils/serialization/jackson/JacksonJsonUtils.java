package com.blackdog.framework.utils.serialization.jackson;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;


/** 
 * @author thiago.soares
 */
public class JacksonJsonUtils {

  private static ObjectMapper mapper;
  
  private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
  
  private JacksonJsonUtils() {
    super();
  }

  private static void initializeMapper() {
    if (mapper == null) {
      
      mapper = new ObjectMapper();
      
      //mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
      
      mapper.configure(SerializationFeature.WRITE_ENUMS_USING_INDEX, true);
      mapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
      
      //mapper.configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false);
      mapper.setDateFormat(format);
      
      mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

      mapper.configure(com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true );
      
      mapper.setSerializationInclusion(Include.NON_NULL);
      mapper.setSerializationInclusion(Include.NON_EMPTY);
      
      
      //mapper.registerModule(new JodaModule());
      //mapper.registerModule(new Hibernate3Module());
      
      //mapper.registerModule(new DateModule());
      
    }
  }
  
  public static ObjectMapper getObjectMapper() {
    initializeMapper();
    return mapper;
  }
  
  public static <T> T fromJson(String json, Class<T> clazz) {
    try {
      
      initializeMapper();
      
      return mapper.readValue(json, clazz);
    
    } catch (JsonParseException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return null;
    } catch (JsonMappingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return null;
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return null;
    }
  }
  
  
  public static <T> T fromJson(String json, TypeReference<T> clazz) {
    try {
      
      if(json == null || json.equals("")) {
        return null;
      }
      
      initializeMapper();
      
      return mapper.readValue(json, clazz);
    
    } catch (JsonParseException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return null;
    } catch (JsonMappingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return null;
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return null;
    }
  }
  
  /*public static <T> List<T> fromJsonList(String json, Class<T> clazz) {
    try {
      
      initializeMapper();
      
      return mapper.readValue(json, new TypeReference<List<T>>(){});
    
    } catch (JsonParseException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return null;
    } catch (JsonMappingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return null;
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return null;
    }
  }*/
  
  
  public static String toJson(Object obj) {
    return toJson(obj, -1);
  }
  
  public static String toJson(Object obj, int limit) {
    
    try {

      initializeMapper();
      
      if(limit == -1) {
        
        return mapper.writeValueAsString(obj);
        
      } else {
        
        if(limit == 0) {
          return null;
        } else {
          
          String ret =  mapper.writeValueAsString(obj);
          
          if(ret.length() > limit) {
            ret = ret.substring(0, limit) + "..........";
          }
          
          return ret;
        }
        
      }
        
      
    
    } catch (JsonProcessingException e) {
      e.printStackTrace();
      return null;
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
    
  }
  

}
