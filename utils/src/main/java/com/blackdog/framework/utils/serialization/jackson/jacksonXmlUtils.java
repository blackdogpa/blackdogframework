package com.blackdog.framework.utils.serialization.jackson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.blackdog.framework.core.model.Model;
//import com.blackdog.framework.utils.pagination.Page;
//import com.blackdog.framework.utils.pagination.PageImpl;
import com.blackdog.framework.utils.reflection.Reflections;
import com.blackdog.framework.utils.serialization.xml.xstream.SerializationException;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.thoughtworks.xstream.XStream;

public class jacksonXmlUtils {
  
  private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
  
  private static XmlMapper xmlMapper;
  
  private static XmlMapper getXmlMapper() {
    
    if(xmlMapper == null) {

      xmlMapper = new XmlMapper();
      
      xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      
      xmlMapper.configure(SerializationFeature.WRITE_ENUMS_USING_INDEX, true);
      xmlMapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
      
      //mapper.configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false);
      xmlMapper.setDateFormat(format);
      
      xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      
      xmlMapper.configure(com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true );
      
      xmlMapper.setSerializationInclusion(Include.NON_NULL);
      xmlMapper.setSerializationInclusion(Include.NON_EMPTY);
      
    }
    
    return xmlMapper;
  }
  
  public static <T> T fromXml(Class<T> clazz, String xml) throws SerializationException {
    
    try {
      
      T responseObject = getXmlMapper().readValue(xml, clazz);
      
      /*XStream xstream = new XStream();
      xstream.processAnnotations(clazz);
      
      xstream.ignoreUnknownElements();
      
      T responseObject = (T) xstream.fromXML(xml);*/
      
      return responseObject;
      
    } catch(Exception e) {
      e.printStackTrace();
      throw new SerializationException(e, "Não foi possivel ler o arquivo XML.");
    } catch(Throwable e) {
      e.printStackTrace();
      throw new SerializationException(e, "Não foi possivel ler o arquivo XML.");
    }
  }
  
  
  public static String toXml(Model model) throws SerializationException {
    
    try {
      
      /*XStream xstream = new XStream();
      xstream.processAnnotations(model.getClass());
      
      xstream.ignoreUnknownElements();
      
      xstream.addDefaultImplementation(java.sql.Date.class, Date.class);
      xstream.addDefaultImplementation(java.sql.Timestamp.class, Date.class);

      
      String responseObject = xstream.toXML(model);*/
      
      String responseObject = getXmlMapper().writeValueAsString(model);
      
      return responseObject;
      
    } catch(Exception e) {
      throw new SerializationException(e, "Não foi possivel serializar o objeto para XML.");
    }
    
  }
  
  public static String toXml(Class<?> clazz, List<?> listModel) throws SerializationException {
    
    try {
      
      /*XStream xstream = new XStream();
      xstream.processAnnotations(clazz);
      
      xstream.ignoreUnknownElements();
      
      xstream.addDefaultImplementation(java.sql.Date.class, Date.class);
      xstream.addDefaultImplementation(java.sql.Timestamp.class, Date.class);
      
      String responseObject = xstream.toXML(listModel);*/
      
      String responseObject = getXmlMapper().writeValueAsString(listModel);
      
      return responseObject;
      
    } catch(Exception e) {
      throw new SerializationException(e, "Não foi possivel serializar o objeto para XML.");
    }
  }
  

}
