package com.blackdog.framework.utils.serialization.xml.xstream;

import javax.ws.rs.core.Response.Status;
import javax.xml.ws.WebFault;

import com.blackdog.framework.core.constants.BlackDogConstants;
import com.blackdog.framework.core.exceptions.BlackExceptions;
import com.blackdog.framework.core.exceptions.ErrorMessage;
import com.blackdog.framework.utils.string.StringUtils;

/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
@WebFault(name="SerializationExceptionFault", targetNamespace= BlackDogConstants.EXCEPTION_NAMESPACE)
public class SerializationException extends Exception implements BlackExceptions {

  private static final long serialVersionUID = -5022967813902834759L;

  private String id;
  private String code;
  
  private String applicationId;
  private String applicationName;
  
  public SerializationException() {
    super();
  }

  public SerializationException(String message, Object... args) {
    super(StringUtils.messageFormat(message, args));
  }
  
  public SerializationException(Throwable cause) {
    super(cause);
  }

  public SerializationException(Throwable cause, String message, Object... args) {
    super(StringUtils.messageFormat(message, args), cause);
  }
  
  @Override
  public ErrorMessage getErrorMessage() {
    
    ErrorMessage dto = new ErrorMessage();
    dto.setStatus(Status.BAD_REQUEST.name());
    dto.setStatusCode(Status.BAD_REQUEST.getStatusCode());
    
    if(org.apache.commons.lang3.StringUtils.isNotBlank(this.getMessage())) {
      dto.setMessage(this.getMessage());
    }
    
    dto.setApplicationId(this.getApplicationId());
    dto.setApplicationName(this.getApplicationName());
    return dto;
  }
  
  @Override
  public String getType() {
    return SERIALIZATION;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
  
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(String applicationId) {
    this.applicationId = applicationId;
  }

  public String getApplicationName() {
    return applicationName;
  }

  public void setApplicationName(String applicationName) {
    this.applicationName = applicationName;
  }

  @Override
  public String getMessage() {
    
    if(super.getMessage() == null || super.getMessage().equals("")) {
      return null;
    }
    
    StringBuilder builder = new StringBuilder();

    if(applicationId != null && applicationName != null) {
      
      builder.append("[");
      builder.append(applicationId);
      builder.append(".");
      builder.append(applicationName);
      builder.append("]");
      
      builder.append("-");
    }
    if (id != null) {
      //builder.append(id);
      //builder.append(" : ");
      builder.append(super.getMessage());
    } else {
      builder.append(super.getMessage());
    }
    return builder.toString();
  }
  
  @Override
  public String toString() {
    return this.getClass().getName() + "["
            + "id="+getId()
            + "applicationId"+getApplicationId()
            + "applicationName="+getApplicationName()
            + "#\n#"
            + ", Class=" + getClass() 
            + ", Cause=" + getCause() 
            + ", Message=" + getMessage() 
            + ", LocalizedMessage=" + getLocalizedMessage()
            + "]";
  }
  
  
  
  
}
