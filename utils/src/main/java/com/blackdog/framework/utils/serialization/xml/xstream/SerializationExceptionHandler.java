package com.blackdog.framework.utils.serialization.xml.xstream;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;

import com.blackdog.framework.core.exceptions.ErrorMessage;


/**
 * 
 * @author thiago.soares (thiago.soares.jr@gmail.com) 
 *
 */
@Provider
public class SerializationExceptionHandler implements ExceptionMapper<SerializationException> {

  @Override
  public Response toResponse(SerializationException exception) {
    
    exception.printStackTrace();
    
    /*ErrorMessage dto = new ErrorMessage();
    dto.setStatus(Status.BAD_REQUEST.name());
    dto.setStatusCode(Status.BAD_REQUEST.getStatusCode());
    
    if(StringUtils.isNotBlank(exception.getMessage())) {
      dto.setMessage(exception.getMessage());
    }
    
    dto.setApplicationId(exception.getApplicationId());
    dto.setApplicationName(exception.getApplicationName());*/
    
    return Response.status(Status.BAD_REQUEST).entity(exception.getErrorMessage()).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8")).build(); 
  }

}
