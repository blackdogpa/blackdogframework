package com.blackdog.framework.utils.serialization.xml.xstream;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.blackdog.framework.core.model.Model;
//import com.blackdog.framework.utils.pagination.Page;
//import com.blackdog.framework.utils.pagination.PageImpl;
import com.blackdog.framework.utils.reflection.Reflections;
import com.thoughtworks.xstream.XStream;

public class XStreamXmlUtils {
  
  public static <T> T loadClassFromXml(Class<T> clazz, String xml) throws SerializationException {
    
    try {
      
      XStream xstream = new XStream();
      xstream.processAnnotations(clazz);
      
      xstream.ignoreUnknownElements();
      
      T responseObject = (T) xstream.fromXML(xml);
      
      return responseObject;
      
    } catch(Exception e) {
      throw new SerializationException(e, "Não foi possivel ler o arquivo XML.");
    }
  }
  
  
  public static String toXml(Model model) throws SerializationException {
    
    try {
      
      XStream xstream = new XStream();
      xstream.processAnnotations(model.getClass());
      
      xstream.ignoreUnknownElements();
      
      xstream.addDefaultImplementation(java.sql.Date.class, Date.class);
      xstream.addDefaultImplementation(java.sql.Timestamp.class, Date.class);

      
      String responseObject = xstream.toXML(model);
      
      return responseObject;
      
    } catch(Exception e) {
      throw new SerializationException(e, "Não foi possivel serializar o objeto para XML.");
    }
    
  }
  
  public static String toXml(Class<?> clazz, List<?> listModel) throws SerializationException {
    
    try {
      
      XStream xstream = new XStream();
      xstream.processAnnotations(clazz);
      
      xstream.ignoreUnknownElements();
      
      xstream.addDefaultImplementation(java.sql.Date.class, Date.class);
      xstream.addDefaultImplementation(java.sql.Timestamp.class, Date.class);
      
      String responseObject = xstream.toXML(listModel);
      
      return responseObject;
      
    } catch(Exception e) {
      throw new SerializationException(e, "Não foi possivel serializar o objeto para XML.");
    }
  }
  
  /* O util não pode depender de Persistence por causa de referencia ciclica. 
  public static <T> String toXml(Page<T> model) throws SerializationException {
    
    try {
      
      XStream xstream = new XStream();
      xstream.processAnnotations(model.getClazz());
      
      xstream.ignoreUnknownElements();
      
      xstream.addDefaultImplementation(java.sql.Date.class, Date.class);
      xstream.addDefaultImplementation(java.sql.Timestamp.class, Date.class);
      
      xstream.alias(model.getAliasList(), List.class);
      
      List<T> list = new ArrayList<>(); 
              
      for(T m : model.getContent()) {
        list.add(m);
      }
      
      String responseObject = xstream.toXML(list);
      
      return responseObject;
      
    } catch(Exception e) {
      throw new SerializationException(e, "Não foi possivel serializar o objeto para XML.");
    }
    
  }*/
  


}
