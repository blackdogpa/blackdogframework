package com.blackdog.framework.utils.string;

import java.text.MessageFormat;

public class StringUtils {

  public static String messageFormat(String pattern, Object[] arguments) {
    if(pattern == null || pattern.equals("")) {
        return "";
    }
    return MessageFormat.format(pattern, arguments);
  }
  
  public static boolean hasLength(String str) {
    return hasLength((CharSequence) str);
  }
  
  public static boolean hasLength(CharSequence str) {
    return (str != null && str.length() > 0);
  }
  
  public static boolean hasText(String str) {
    return hasText((CharSequence) str);
  }
  
  public static boolean hasText(CharSequence str) {
    if (!hasLength(str)) {
      return false;
    }
    int strLen = str.length();
    for (int i = 0; i < strLen; i++) {
      if (!Character.isWhitespace(str.charAt(i))) {
        return true;
      }
    }
    return false;
  }
  
  
  /**
   * Check if the given array contains the given value (with case-insensitive comparison).
   *
   * @param array The array
   * @param value The value to search
   * @return true if the array contains the value
   */
  public static boolean containsIgnoreCase(String[] array, String value) {
    for (String str : array) {
      if (value == null && str == null) return true;
      if (value != null && value.equalsIgnoreCase(str)) return true;
    }
    return false;
  }

  /**
   * Join an array of strings with the given separator.
   * <p>
   * Note: This might be replaced by utility method from commons-lang or guava someday
   * if one of those libraries is added as dependency.
   * </p>
   *
   * @param array     The array of strings
   * @param separator The separator
   * @return the resulting string
   */
  public static String join(String[] array, String separator) {
    int len = array.length;
    if (len == 0) return "";

    StringBuilder out = new StringBuilder();
    out.append(array[0]);
    for (int i = 1; i < len; i++) {
      out.append(separator).append(array[i]);
    }
    return out.toString();
  }

}
