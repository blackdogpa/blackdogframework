package com.blackdog.framework.utils.uuid;

import java.util.UUID;

public class UUIDUtils {

  
  public static String generateId() {
    
    return UUID.randomUUID().toString();

  }
  
}
