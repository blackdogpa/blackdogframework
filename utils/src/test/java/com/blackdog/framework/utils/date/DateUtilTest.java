package com.blackdog.framework.utils.date;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import org.junit.Test;

import com.blackdog.framework.utils.date.DateUtil;

public class DateUtilTest {

  @Test
  public void testParseDate() throws Exception {
    System.out.println(DateUtil.parseDateEN("2016-01-01T03:00:00.000Z"));
  }

  @Test
  public void testParseDatePT() throws ParseException {
    
    //Pattern specialDatePattern01 = Pattern.compile("^([0-9]{8}):([0-9]{6})$");
    
    //Pattern specialDatePattern02 = Pattern.compile("^([0-9]{4}[-/][0-9]{2}[-/][0-9]{2}[-/][0-9]{2}):([0-9]{2}:([0-9]{3}'Z')$");
    Pattern specialDatePattern03 = Pattern.compile("^([0-9]{4}[-/][0-9]{2}[-/][0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}Z)$");
    
    
    assertTrue(specialDatePattern03.matcher("2016-01-01T03:00:00.000Z").matches());
    //assertTrue(specialDatePattern02.matcher("2016-01-01T03:00:00.000Z").matches());
    
    
    Date dt1 = new Date("2016-01-01T03:00:00.000Z");
    
    Date dt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").parse("2016-01-01T03:00:00.000Z");
    
    System.out.println(dt);
    
  }

  /*@Test
  public void testParseDateEN() {
    // Parser
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS Z");

    // Parse date/time with time zone
    // OffsetDateTime odtWithTime = OffsetDateTime.parse("2011/02/14 00:00:00.000 -0800", formatter);
    OffsetDateTime odtWithTime = OffsetDateTime.parse("2011-02-14 09:30:00.999 -0800", formatter);

    // odtWithTime: 2011-02-14T09:30:00.999-08:00

    // Remove time from odtWithTime
    LocalDateTime ldtWithoutTime = odtWithTime.toLocalDate().atStartOfDay();
    OffsetDateTime odtWihtoutTime = OffsetDateTime.of(ldtWithoutTime, odtWithTime.getOffset());

    // odtWihtoutTime: 2011-02-14T00:00-08:00
    // All time information are reset to Zero
  }*/

  
  @Test
  public void testAllJuntos() throws Exception {
    //System.out.println(DateUtil.parseDate("20160101000000"));
    
    //System.out.println(DateUtil.parseDateEN("2016-01-01T00:00:00"));
    
    System.out.println(DateUtil.dateFormat3.parse("2017-04-05 00:00:00.000"));
  }
  
  @Test
  public void testSimples() {
    fail("Not yet implemented");
  }

  @Test
  public void testCompleto() {
    fail("Not yet implemented");
  }

  @Test
  public void testExtenso() {
    fail("Not yet implemented");
  }

  @Test
  public void testNomeDoMes() {
    fail("Not yet implemented");
  }

  @Test
  public void testDiaDaSemana() {
    fail("Not yet implemented");
  }

  @Test
  public void testTruncateToDate() {
    fail("Not yet implemented");
  }

  @Test
  public void testNow1() {
    fail("Not yet implemented");
  }

}
