package com.blackdog.framework.utils.date;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.text.MessageFormat;

import org.junit.Test;

import com.blackdog.framework.utils.date.SimasDateUtil;

public class SimasDateUtilTest {

  @Test
  public void testDateFormat() {
    
    
    assertEquals("08/06/2016", SimasDateUtil.dateFormat(new BigDecimal("08062016")));
    assertEquals("31/06/2016", SimasDateUtil.dateFormat(new BigDecimal("31062016")));
    
    assertEquals("08/06/2016", SimasDateUtil.dateFormat(new BigDecimal("8062016")));
    
    
  }
  

}
