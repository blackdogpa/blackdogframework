package com.blackdog.framework.utils.reflection;

public class AbsFoo<T> {

  public AbsFoo() {
    super();
    
    Class<T> implClass = (Class<T>)Clazz.typeForGenericSuperclass(getClass(), 0);
    System.out.println(implClass);
    
    Class<T> implClass2 =Reflections.getGenericTypeArgument(getClass(), 0);
    System.out.println(implClass2);
  }

  
}
