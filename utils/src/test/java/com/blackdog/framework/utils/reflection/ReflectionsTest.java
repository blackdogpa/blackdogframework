package com.blackdog.framework.utils.reflection;

import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.net.StandardSocketOptions;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.reflect.TypeUtils;
import org.junit.Test;


public class ReflectionsTest {

  @Test
  public void testReflections() {
    fail("Not yet implemented");
  }

  @Test
  public void testGetGenericTypeArgumentClassOfQInt() throws NoSuchFieldException, SecurityException {
    
    List<String> content = new ArrayList<>();
    //PageImpl<String> page = new PageImpl<String>(content);
    
    
    /*System.out.println(page.getTypeParameterClass());
    
    
    for(TypeVariable<?> tp: page.getClass().getTypeParameters()) {
      System.out.println(tp.getClass());
      System.out.println(tp.getGenericDeclaration());
      System.out.println(tp.getBounds()[0]);
    }
    
    
    System.out.println("#####");
    
    Type type = page.getClass().getGenericInterfaces()[0];
    
    if (type instanceof ParameterizedType) {
      Type actualType = ((ParameterizedType) type).getActualTypeArguments()[0];
      System.out.println(actualType);
      System.out.println(actualType.getClass());
    }
    */
    
    //System.out.println(findSuperClassParameterType(page, 0));
    
    //System.out.println(findSubClassParameterType(page, String.class, 0));
    
    
    /*Field stringListField = page.getClass().getDeclaredField("content");
    ParameterizedType stringListType = (ParameterizedType) stringListField.getGenericType();
    Class<?> stringListClass = (Class<?>) stringListType.getActualTypeArguments()[0];
    System.out.println(stringListClass); // class java.lang.String.
*/
    /*Field integerListField = Test.class.getDeclaredField("integerList");
    ParameterizedType integerListType = (ParameterizedType) integerListField.getGenericType();
    Class<?> integerListClass = (Class<?>) integerListType.getActualTypeArguments()[0];
    System.out.println(integerListClass); // class java.lang.Integer.
*/    
  }
  
  
  
  /*public static Class<?> findSuperClassParameterType(Object instance, int parameterIndex) {
    Class<?> subClass = instance.getClass();
    while (subClass != subClass.getSuperclass()) {
      // instance.getClass() is no subclass of classOfInterest or instance is a direct instance of classOfInterest
      subClass = subClass.getSuperclass();
      if (subClass == null) throw new IllegalArgumentException();
    }
    ParameterizedType parameterizedType = (ParameterizedType) subClass.getGenericSuperclass();
    return (Class<?>) parameterizedType.getActualTypeArguments()[parameterIndex];
  }
  
  public static Class<?> findSubClassParameterType(Object instance, Class<?> classOfInterest, int parameterIndex) {
    Map<Type, Type> typeMap = new HashMap<Type, Type>();
    Class<?> instanceClass = instance.getClass();
    while (classOfInterest != instanceClass.getSuperclass()) {
      extractTypeArguments(typeMap, instanceClass);
      instanceClass = instanceClass.getSuperclass();
      if (instanceClass == null) throw new IllegalArgumentException();
    }
   
    ParameterizedType parameterizedType = (ParameterizedType) instanceClass.getGenericSuperclass();
    Type actualType = parameterizedType.getActualTypeArguments()[parameterIndex];
    if (typeMap.containsKey(actualType)) {
      actualType = typeMap.get(actualType);
    }
    if (actualType instanceof Class) {
      return (Class<?>) actualType;
    } else {
      throw new IllegalArgumentException();
    }
  }
   
  private static void extractTypeArguments(Map<Type, Type> typeMap, Class<?> clazz) {
    Type genericSuperclass = clazz.getGenericSuperclass();
    if (!(genericSuperclass instanceof ParameterizedType)) {
      return;
    }
   
    ParameterizedType parameterizedType = (ParameterizedType) genericSuperclass;
    Type[] typeParameter = ((Class<?>) parameterizedType.getRawType()).getTypeParameters();
    Type[] actualTypeArgument = parameterizedType.getActualTypeArguments();
    for (int i = 0; i < typeParameter.length; i++) {
      if(typeMap.containsKey(actualTypeArgument[i])) {
        actualTypeArgument[i] = typeMap.get(actualTypeArgument[i]);
      }
      typeMap.put(typeParameter[i], actualTypeArgument[i]);
    }
  }*/
  

  @Test
  public void testGetGenericTypeArgumentFieldInt() {
    fail("Not yet implemented");
  }

  @Test
  public void testGetGenericTypeArgumentMemberInt() {
    fail("Not yet implemented");
  }

  @Test
  public void testGetGenericTypeArgumentMethodInt() {
    fail("Not yet implemented");
  }

  @Test
  public void testGetFieldValue() {
    fail("Not yet implemented");
  }

  @Test
  public void testSetFieldValue() {
    fail("Not yet implemented");
  }

  @Test
  public void testGetNonStaticDeclaredFields() {
    fail("Not yet implemented");
  }

  @Test
  public void testGetNonStaticFields() {
    fail("Not yet implemented");
  }

  @Test
  public void testInstantiate() {
    fail("Not yet implemented");
  }

  @Test
  public void testIsOfType() {
    fail("Not yet implemented");
  }

  @Test
  public void testGetClassLoaderForClass() {
    fail("Not yet implemented");
  }

  @Test
  public void testGetClassLoaderForResource() {
    fail("Not yet implemented");
  }

  @Test
  public void testGetResourceAsURL() {
    fail("Not yet implemented");
  }

  @Test
  public void testGetResourceAsStream() {
    fail("Not yet implemented");
  }

  @Test
  public void testForName() {
    fail("Not yet implemented");
  }

}
