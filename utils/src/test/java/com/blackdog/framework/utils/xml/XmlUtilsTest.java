package com.blackdog.framework.utils.xml;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

public class XmlUtilsTest {

  @Test
  public void testLoadClassFromXml() {
    
    List<Foo> lista = new ArrayList<>();
    lista.add(new Foo("1"));
    lista.add(new Foo("2"));
    lista.add(new Foo("3"));
    lista.add(new Foo("4"));
    
    

    XStream xstream = new XStream();
    xstream.processAnnotations(Foo.class);
    
    xstream.ignoreUnknownElements();
    
    xstream.addDefaultImplementation(java.sql.Date.class, Date.class);
    xstream.addDefaultImplementation(java.sql.Timestamp.class, Date.class);

    xstream.alias("Employees", List.class);
    
    String responseObject = xstream.toXML(lista);
    
    System.out.println(responseObject);
    
  }

  @Test
  public void testToXmlModel() {
    fail("Not yet implemented");
  }

  @Test
  public void testToXmlClassOfQListOfQ() {
    fail("Not yet implemented");
  }

  @Test
  public void testToXmlPageOfT() {
    fail("Not yet implemented");
  }
  
  
  @XStreamAlias("Foot")
  class Foo {
    
    String teste;

    public Foo(String teste) {
      super();
      this.teste = teste;
    }
    
          
  }

}
