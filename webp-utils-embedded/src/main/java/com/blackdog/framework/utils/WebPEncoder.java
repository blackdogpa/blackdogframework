package com.blackdog.framework.utils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.scijava.nativelib.NativeLibraryUtil;

import net.sprd.image.webp.WebPRegister;
import net.sprd.image.webp.WebPWriteParam;

public class WebPEncoder {

  
  
  public String convertToWebPBase64(String base64Image, float quality) throws IOException {
    
    File sourceFile = null;
    File webpDestFile = null;
    try {

      byte[] sourceImage = Base64.decodeBase64(base64Image);
      
      sourceFile = File.createTempFile("cryptor", "origin.temp");
      FileUtils.writeByteArrayToFile(sourceFile, sourceImage);
      
      webpDestFile = convertToWebPBase64(sourceFile, quality);
      
      String webPBase64 = Base64.encodeBase64String(FileUtils.readFileToByteArray(webpDestFile));
      
      return webPBase64;
      
    } finally {
      
      if(sourceFile != null) {
        sourceFile.delete();
      }
      
      if(webpDestFile != null) {
        webpDestFile.delete();
      }
      
    }
    
  }
  
  public File convertToWebPBase64(File sourceFile, float quality) throws IOException {

    
    //Load WebP Lib
    //NativeLibraryUtil.loadNativeLibrary(WebPEncoder.class, "webp_jni");
    
    //final int version = libwebp.WebPGetDecoderVersion();
    //System.out.println("libwebp version: " + Integer.toHexString(version));

    /*System.out.println("libwebp methods:");
    final Method[] libwebpMethods = libwebp.class.getDeclaredMethods();
    for (int i = 0; i < libwebpMethods.length; i++) {
      System.out.println(libwebpMethods[i]);
    }*/

    /*String osName = System.getProperty("os.name");
    String osArch = System.getProperty("os.arch");
    String osVersion = System.getProperty("os.version");

    System.out.println("OS-NAME :" + osName);
    System.out.println("OS-ARCH :" + osArch);
    System.out.println("OS-VERS :" + osVersion);*/

    //String distro = fromStream(Runtime.getRuntime().exec("lsb_release -i -s").getInputStream());
    //System.out.println("OS-DISTRO :" + distro);

    //String origFileName = "src/test/resources/test.png";
    
    
    
    
    if(sourceFile == null) {
      //BusinessExceptionUtil.throwBusinessException(QRCodeErrorMessages.FILE_NOT_FOUND);
    }
    
    BufferedImage img = ImageIO.read(sourceFile);

    WebPRegister.registerImageTypes();

    //File f1 = new File(origFileName);
    //System.out.println("size of orig image file :" + f1.length());

    //System.out.println("lossy webp size (0.5):" + toWebp("src/test/resources/lossy1.webp", img, 0.5f).length());
    //System.out.println("lossy webp size (0.8):" + toWebp("src/test/resources/lossy2.webp", img, 0.8f).length());
    //System.out.println("lossy webp size (1.0):" + toWebp("src/test/resources/lossy3.webp", img, 1f).length());
    //System.out.println("lossless webp size :" + toWebp("src/test/resources/lossless.webp", img, -1f).length());
    
    File webpDestFile = File.createTempFile("cryptor", "web.temp");;
    
    toWebp(img, webpDestFile, quality);
    
    //String webPBase64 = Base64.encodeBase64String(FileUtils.readFileToByteArray(webpDestFile));
    
    return webpDestFile;
  }

  private static void toWebp(BufferedImage img, File webpDestFile, float quality) throws IOException {
    
    //File f = new File(fileName);
    try {
      webpDestFile.delete();
    } catch (Exception e) {
    }
    
    Iterator<ImageWriter> writerList = ImageIO.getImageWritersByFormatName("webp");
    ImageWriter writer = writerList.next();
    ImageWriteParam param = writer.getDefaultWriteParam();
    WebPWriteParam writeParam = (WebPWriteParam) param;
    if (quality < 0) {
      writeParam.setCompressionType(WebPWriteParam.LOSSLESS);
    } else {
      writeParam.setCompressionQuality(quality);
    }
    ImageOutputStream ios = ImageIO.createImageOutputStream(webpDestFile);
    writer.setOutput(ios);
    IIOImage outimage = new IIOImage(img, null, null);
    writer.write(null, outimage, writeParam);
    
    ios.close();
    //return f;
  }

  /*public static String fromStream(InputStream in) throws IOException {
    try {
      BufferedReader reader = new BufferedReader(new InputStreamReader(in));
      StringBuilder out = new StringBuilder();
      String newLine = System.getProperty("line.separator");
      String line;
      while ((line = reader.readLine()) != null) {
        out.append(line);
        out.append(newLine);
      }
      return out.toString();
    } catch (Exception e) {
    } finally {
      if (in != null) {
        try {
          in.close();
        } catch (Exception e) {
        }

      }
    }
    return "";
  }*/
}
