package com.blackdog.framework.utils;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

public class WebPEncoderTest {

  @Test
  public void testConvertToWebPBase64() throws IOException, Exception {
    
    
    WebPEncoder encoder = new WebPEncoder();
    
    String b64Webp = encoder.convertToWebPBase64(loadImageAsBase64("/test.png"), 0.5f);
    System.out.println(b64Webp);
  }
  
  
  @Test
  public void testConvertToWebPBase642() throws IOException, Exception {
    
    
    String content = loadImageAsBase64("/exemplo-01.jpg");
        
    
    System.out.println(">> " +  content);
    
    WebPEncoder encoder = new WebPEncoder();
    
    String b64Webp = encoder.convertToWebPBase64(content, 0.2f);
    System.out.println(">> " +  b64Webp.length());
  }
  
  @Test
  public void testConvertToWebPSave() throws IOException, Exception {
    
    File input = loadImage("/exemplo-01.jpg");
    
    WebPEncoder encoder = new WebPEncoder();
    
    saveFile(encoder.convertToWebPBase64(input, 0.2f), "/home/thiago/Downloads/TMP/TesteWebP/out20.webp");
    saveFile(encoder.convertToWebPBase64(input, 0.3f), "/home/thiago/Downloads/TMP/TesteWebP/out30.webp");
    saveFile(encoder.convertToWebPBase64(input, 0.4f), "/home/thiago/Downloads/TMP/TesteWebP/out40.webp");
    saveFile(encoder.convertToWebPBase64(input, 0.5f), "/home/thiago/Downloads/TMP/TesteWebP/out50.webp");
    saveFile(encoder.convertToWebPBase64(input, 0.6f), "/home/thiago/Downloads/TMP/TesteWebP/out60.webp");
    saveFile(encoder.convertToWebPBase64(input, 0.7f), "/home/thiago/Downloads/TMP/TesteWebP/out70.webp");
    saveFile(encoder.convertToWebPBase64(input, 0.8f), "/home/thiago/Downloads/TMP/TesteWebP/out80.webp");
    saveFile(encoder.convertToWebPBase64(input, 0.9f), "/home/thiago/Downloads/TMP/TesteWebP/out90.webp");
    saveFile(encoder.convertToWebPBase64(input, 1.0f), "/home/thiago/Downloads/TMP/TesteWebP/out100.webp");
    
    
    
  }
  
  
  protected String loadImageAsBase64(String path) throws Exception {
    
    URL url = this.getClass().getResource(path);
    java.nio.file.Path resPath = java.nio.file.Paths.get(url.toURI());
    byte[] data = FileUtils.readFileToByteArray(new File(url.toURI()));
    
    String base64 = org.apache.commons.codec.binary.Base64.encodeBase64String(data);
    System.out.println(base64.length());
    return base64;
  }
  
  protected File loadImage(String path) throws Exception {
    URL url = this.getClass().getResource(path);
    return new File(url.toURI());
  }
  
  protected static void saveFile(File content, String destFilePath) throws Exception {
    FileOutputStream fos = new FileOutputStream(destFilePath);
    fos.write(FileUtils.readFileToByteArray(content));
    fos.close();
  }
  
  protected static void saveFile(String content, String destFilePath) {
    try {
      
      byte[] data = Base64.decodeBase64(content);
      try (OutputStream stream = new FileOutputStream(destFilePath)) {
          stream.write(data);
      }
      
      //Files.write(Paths.get(new File(destFilePath).getPath()), content.getBytes());
      
    } catch (Exception e) {
      // TODO: handle exception
    }
  }

}
