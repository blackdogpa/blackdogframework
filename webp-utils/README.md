#definicao do Modulo do JBOSS

## Definir Modulo Jboss
	module
	  blackdog
	    webp
	  	  main
	  	    module.xml
	  	  lib
	  	    linux-i686
	  	 	  libwebp_jni.so
	  	    linux-x86_64	 
	  	 	  libwebp_jni.so

## Copiar as Libs
	webp-utils-1.0-SNAPSHOT.jar: Conteudo desse projeto
	webp-5.0.0.jar: [Download](https://chromium.googlesource.com/webm/libwebp/+/master/swig/)
	libwebp_jni.so: [Download](https://storage.googleapis.com/downloads.webmproject.org/releases/webp/index.html)
	
## Conteudo do Modules.xml	  	 	
	<?xml version="1.0" encoding="UTF-8"?> 
	<module xmlns="urn:jboss:module:1.0" name="prodepa.webp"> 
	     <resources> 
	       <resource-root path="webp-utils-1.0-SNAPSHOT.jar"/> 
	       <resource-root path="webp-5.0.0.jar"/> 
	     </resources> 
	     <dependencies> 
			<module name="javax.api"/>
		 </dependencies> 
	</module>
	
## Java JNI bindings (webp-5.0.0.jar)

 Install webp-5.0.0.jar localmente para o maven

      mvn install:install-file -Dfile=libwebp.jar -DgroupId=com.google.webp -DartifactId=webp -Dversion=5.0.0 -Dpackaging=jar	
      
##jboss-deployment-structure
Adicionar uma <dependencies>
    
      <module name="blackdog.webp" services="import"/>