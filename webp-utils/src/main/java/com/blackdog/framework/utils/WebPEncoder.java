package com.blackdog.framework.utils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;

import net.sprd.image.webp.WebPRegister;
import net.sprd.image.webp.WebPWriteParam;


public class WebPEncoder {

  public String convertToWebPBase64(String base64Image, float quality) throws IOException {
    
    File sourceFile = null;
    try {

      byte[] sourceImage = Base64.decodeBase64(base64Image);
      
      sourceFile = File.createTempFile("cryptor", "origin.temp");
      FileUtils.writeByteArrayToFile(sourceFile, sourceImage);
      
      byte[] webpDestFile = convertToWebPBase64(sourceFile, quality);
      
      String webPBase64 = Base64.encodeBase64String(webpDestFile);
      
      return webPBase64;
      
    } finally {
      
      if(sourceFile != null) {
        sourceFile.delete();
      }
      
    }
    
  }
  
  public byte[] convertToWebPBase64(File sourceFile, Float quality) throws IOException {

    File webpDestFile = null;
    try {
      if(sourceFile == null) {
        //BusinessExceptionUtil.throwBusinessException(QRCodeErrorMessages.FILE_NOT_FOUND);
      }
      
      BufferedImage img = ImageIO.read(sourceFile);
  
      WebPRegister.registerImageTypes();
  
      webpDestFile = File.createTempFile("cryptor", "web.temp");
      
      toWebp(img, webpDestFile, quality);
      
      return FileUtils.readFileToByteArray(webpDestFile);
    } finally {
      
      if(webpDestFile != null) {
        webpDestFile.delete();
      }
      
    }
  }
  
  private static void toWebp(BufferedImage img, File webpDestFile, Float quality) throws IOException {
    
    try {
      webpDestFile.delete();
    } catch (Exception e) { }
    
    Iterator<ImageWriter> writerList = ImageIO.getImageWritersByFormatName("webp");
    ImageWriter writer = writerList.next();
    ImageWriteParam param = writer.getDefaultWriteParam();
    WebPWriteParam writeParam = (WebPWriteParam) param;
    
    if (quality == null || quality < 0) {
      writeParam.setCompressionType(WebPWriteParam.LOSSLESS);
    } else {
      writeParam.setCompressionQuality(quality);
    }
    
    //Não suportado
    //writeParam.setProgressiveMode(WebPWriteParam.MODE_DISABLED);
    
    //No compression type set!
    //writeParam.setCompressionType(WebPWriteParam.LOSSLESS);
    //writeParam.setCompressionMode(WebPWriteParam.MODE_EXPLICIT);
    
    ImageOutputStream ios = ImageIO.createImageOutputStream(webpDestFile);
    writer.setOutput(ios);
    IIOImage outimage = new IIOImage(img, null, null);
    writer.write(null, outimage, writeParam);
    
    ios.close();
  }
  
}
