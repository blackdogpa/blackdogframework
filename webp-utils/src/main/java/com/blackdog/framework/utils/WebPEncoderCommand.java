package com.blackdog.framework.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.concurrent.TimeUnit;


public class WebPEncoderCommand {

  public String convertToWebPBase64(String base64Image, float quality) throws IOException {
    
    /*File sourceFile = null;
    File webpDestFile = null;
    try {

      byte[] sourceImage = Base64.decodeBase64(base64Image);
      
      sourceFile = File.createTempFile("cryptor", "origin.temp");
      FileUtils.writeByteArrayToFile(sourceFile, sourceImage);
      
      webpDestFile = convertToWebPBase64(sourceFile, quality);
      
      String webPBase64 = Base64.encodeBase64String(FileUtils.readFileToByteArray(webpDestFile));
      
      return webPBase64;
      
    } finally {
      
      if(sourceFile != null) {
        sourceFile.delete();
      }
      
      if(webpDestFile != null) {
        webpDestFile.delete();
      }
      
    }*/
    return "";
  }
  
  public byte[] convertToWebPBase64(File sourceFile, float quality) throws IOException {

    
    if(sourceFile == null) {
      //BusinessExceptionUtil.throwBusinessException(QRCodeErrorMessages.FILE_NOT_FOUND);
    }
    
    //BufferedImage img = ImageIO.read(sourceFile);

    //WebPRegister.registerImageTypes();

    File webpDestFile = File.createTempFile("cryptor", "web.temp");
    //File webpDestFile = new File("/home/thiago/Downloads/TMP/TesteWebP/TMP-Cryptor.webp");
    
    toWebp(sourceFile, webpDestFile, quality);
    
    byte[] bt = Files.readAllBytes(webpDestFile.toPath());
    
    System.out.println(bt.length);
    
    return bt; 
  }

  private static void toWebp(File sourceFile, File webpDestFile, float quality) throws IOException {
    
    //File f = new File(fileName);
    try {
      webpDestFile.delete();
    } catch (Exception e) { }
    
    /*Iterator<ImageWriter> writerList = ImageIO.getImageWritersByFormatName("webp");
    ImageWriter writer = writerList.next();
    ImageWriteParam param = writer.getDefaultWriteParam();
    WebPWriteParam writeParam = (WebPWriteParam) param;
    if (quality < 0) {
      writeParam.setCompressionType(WebPWriteParam.LOSSLESS);
    } else {
      writeParam.setCompressionQuality(quality);
    }
    
    
    ImageOutputStream ios = ImageIO.createImageOutputStream(webpDestFile);
    writer.setOutput(ios);
    IIOImage outimage = new IIOImage(img, null, null);
    writer.write(null, outimage, writeParam);
    
    ios.close();*/
    
    StringBuffer cmd = new StringBuffer("cwebp ");
    
    if (quality < 0) {
      //writeParam.setCompressionType(WebPWriteParam.LOSSLESS);
    } else {
      cmd.append("-q "+quality * 10);
    }
    
    cmd.append(" " + sourceFile.getAbsolutePath());
    cmd.append(" -o " + webpDestFile.getAbsolutePath());
    
    System.out.println(cmd.toString());
    
    //String cmds = "cwebp -q 50"
    //        + " /home/thiago/Workspaces/Prodepa/Crypto/Criptor-1.0-master/codigo_fonte/java/src/test/resources/avatars/exemplo-01.jpg"
    //        + " -o /home/thiago/Downloads/TMP/teste.webp";
    
    Runtime run  = Runtime.getRuntime();
    Process proc = run.exec(cmd.toString());
    
    
    try {
      //proc.waitFor(5, TimeUnit.SECONDS);
      proc.waitFor();
    } catch (InterruptedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    /*try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }*/
    
    
  }
}
