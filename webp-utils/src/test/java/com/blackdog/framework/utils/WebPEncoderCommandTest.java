package com.blackdog.framework.utils;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.nio.file.Files;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

public class WebPEncoderCommandTest {

  @Test
  public void testConvertToWebPBase64StringFloat() {
    fail("Not yet implemented");
  }

  @Test
  public void testConvertToWebPBase64FileFloat() throws Exception {
    
    File input = loadImage("/exemplo-01.jpg");
    
    WebPEncoderCommand encoder = new WebPEncoderCommand();
    
    saveFile(encoder.convertToWebPBase64(input, 0.2f), "/home/thiago/Downloads/TMP/TesteWebP/out20.webp");
    /*saveFile(encoder.convertToWebPBase64(input, 0.3f), "/home/thiago/Downloads/TMP/TesteWebP/out30.webp");
    saveFile(encoder.convertToWebPBase64(input, 0.4f), "/home/thiago/Downloads/TMP/TesteWebP/out40.webp");
    saveFile(encoder.convertToWebPBase64(input, 0.5f), "/home/thiago/Downloads/TMP/TesteWebP/out50.webp");
    saveFile(encoder.convertToWebPBase64(input, 0.6f), "/home/thiago/Downloads/TMP/TesteWebP/out60.webp");
    saveFile(encoder.convertToWebPBase64(input, 0.7f), "/home/thiago/Downloads/TMP/TesteWebP/out70.webp");
    saveFile(encoder.convertToWebPBase64(input, 0.8f), "/home/thiago/Downloads/TMP/TesteWebP/out80.webp");
    saveFile(encoder.convertToWebPBase64(input, 0.9f), "/home/thiago/Downloads/TMP/TesteWebP/out90.webp");
    saveFile(encoder.convertToWebPBase64(input, 1.0f), "/home/thiago/Downloads/TMP/TesteWebP/out100.webp");*/
    
    
  }

  protected File loadImage(String path) throws Exception {
    URL url = this.getClass().getResource(path);
    return new File(url.toURI());
  }
  protected static void saveFile(byte[] bs, String destFilePath) throws Exception {
    
    System.out.println( "TESTE" +  bs.length );
    
   // FileOutputStream out = FileUtils.openOutputStream(bs.getAbsoluteFile());
    
    //FileUtils.copyFile(content, new File(destFilePath));
    
    FileOutputStream fos = new FileOutputStream(destFilePath);
    fos.write( bs );
    fos.close();
  }
  
}
